# qml

qml is a perl script that generates [quakeML](https://quake.ethz.ch/quakeml) from AQMS database. 

## Dependencies

*  [Qtime](http://www.ncedc.org/ftp/pub/programs/Qtime/)  
*  mtmanip ???
*  Perl module, perl-XML-Writer

See manpage for information on how to run qml.  
  

# post processing utilities  
  
**These utilities are TEMPLATES which users are encouraged to modify as needed.**  

These ulitlies are used in post processing tasks at RSNs. Most of the utilities use *jiggle.jar*. Each utility has it's own directory and has a directory structure shown below.  
  
For e.g.  
  
```  
perl_utils  
    ampgen   
        bin - executable scripts  
        cfg - configuration files  
    adhoc  
        bin  
        cfg  
    ...    
docsrc
    perl_utils
        ampgen.md  
        adhoc.md  
        ...
```

| Name | Description |
------ | ------------
| adhoc | create, distribute or get channel information in Adhoc format |
| alarm | send or cancel alarms for an event | 
| ampgen | calculate amps and write to database | 
| swarmon | a PCS client program for monitor for swarms of earthquakes in configured geographic regions |


  
# perl_utils  

## Command line utilities  
  
**All command line utilities checked into the repo should be considered as TEMPLATES. Users should modify to suit their purposes.**  
  
AQMS has a set of command-line tools (mostly Perl) that perform a number of important functions. These are used by PCS tasks, the Duty Review Page, etc. These are present under *perl_utils/bin*. For ease of use, symbolic links are created with the name of the script but without the .pl or .sh extension.

These utilities use a set of perl modules (.pm), and they are present under the *perl_utils/bin/perlmodules* directory.  

Files with network specific data read by the bin scripts go under *perl_utils/bin/cfg*.  
Scripts to create/remove symbolic links for bin commands without filename suffix of .sh or .pl go under *perl_utils/bin/setup*.  


| **Command** | **Arguments** | **Function** |
--------- | --------- | ---------
| **Event Info**| | |
| cattail | -[ht] [-d dbase] \<hours-back\> (default = 24) | dump events since \<hours-back\> |
| cattailDatumGetDepth | [-h] [-c] [-d dbase] [-a or [ [-D] [-B0\|1]]] [-iI] <br> [-f file] [-e types] [-m mag -M mag] <br> [-s esrc -S osrc ] [-rtw] [-u d\|w\|m ] | 
| trigtail | -[ht] [-d dbase] \<hours-back\> (default = 24) | dump triggers since \<hours-back\> | 
| eventhist | [-d dbase] \<evid\> | show the event's history | 
| catone | [-d dbase] \<evid\> | output summary line for one event | 
| eventAge | [-d dbase] \<evid\> | output event age in seconds |   
| eventMag | [-d dbase] \<evid\> | output event mag |
| eventVersion | [-d dbase]  \<evid\> | print the db version number of evid |
| getEtype | -[h] [-d dbase] \<evid\> | print etype column value of the EVENT table row for evid | 
| setEtype | [-d dbasename]  \<evid\> \<etype\> | set the etype of an event in the dbase | 
| getGtype | -[h] [-d dbase] \<id\> | print gtype column value of the ORIGIN table row for prefor of evid | 
| setGtype | -[h] [-d dbasename] \<evid\> \<gtype\> | set the gtype for the origin | 
| setMag | [-d dbase] \<evid\> \<mag-value\> \<mag-type\> [auth] [subsource] [algorithm] [nsta] [nobs] [quality] [rflag] | creates a new event preferred magnitude in the db | 
| getMagPriority |  -[h] [-d dbase] \<id\> | prints the magnitude priority | 
| getRflag | -[h] [-d dbase] \<evid\> | prints the rflag column value of the ORIGIN table row for evid | 
| prefmags | [-q] [-e] [-d dbase] \<evid\> | show info about the preferred magnitudes | 
| dumpDbRows4Evid | [-d dbase ] [-u user/passwd ] [-p out-dir] \<evid\> | print the database table data associated with evid |
| isInsideNetwork | [-h] [-d dbase] [-n name] \<evid\> | print 1 if event is located inside event auth network region | 
| showalarms | [-d dbase] \<evid\> | show alarms sent for event |
| **Event Parameter Info** | | 
| hypophases | -[h] [-d dbase] \<evid\> | dump event's phases in Hypoinverse format |
| ampdump | -[ht] [-d dbase] \<event-id\> | dump amps contributing to mag (AssocAmM) |
| ampdumpo | -[ht] [-d dbase] \<event-id\> | dump amps associated with origin (AssocAmO) |
| ampdumpsm | [-h] [-a type] [-c cnt] [-d dbase] [-i,-r, or -s subsrc] [-q] \<evid\> | dump strong motion amp data, or total amp count, associated with event (AssocEvAmpset) |
| checkwavefiles | [-d dbase] [-1  or -wW wavefilehost] \<evid\> | show dir and list archived waveform files for event (requires ssh) |
| wavedump | [-h] [-c|k] [-C] [-d dbase] [-q] [-f] [-r] [-R] \<evid\> | dump list of this event's waveforms |
| allmags | [-d dbase] [-e] [-w] [-q] \<evid\> | show info about the magnitudes associated with all event origins |
| allmecs | [-d dbase] [-w] [-q] \<evid\> | show info about the mechanisms associated with all event origins | 
| allorgs | [-h] [-q] [-e] [-w] [-d dbase] \<evid\> | show info about the origins associated with event evid |
| phaseCnt | [-h] [-d dbase] [-iozpsPS] \<evid\> | print count of phases associated with the preferred origin of evid | 
| archive_db_total | [-d dbase] \<start_date\> \<end_date\> | report of the totals of events, arrivals, and waveforms for time period |  
| **Station/Channel Info** | |
| hypostalist | [-abd:qX:x:I:i:h] [\<start-date\>] [\<end-date\>] | dump station list in Hypo200 format | 
| stainfo | -[ahqt] [-d dbase] [-f type] \<station string\> | dump summary station info ordered by sta, net, location, seedchan |
| stadist | -[h] [-d dbase] \<lat.dec\> \<lon.dec\> <br> -[h] [-d dbase] \<latdeg\> \<latmin\> \<londeg\> \<lonmin\> | dump a station list in distance order from this lat/lon |
| chanlist | -[h] [-d dbase] \<list-name\> | dump a list of channels defined by a "named channel list" | 
| depthdatum | -[h] [-d dbase] [-n avgcnt] [-v\|V] [-f id_file] \<evid\> | print station elevation datum data |  
| **Duty Review Page Support** | |
| acceptEvent | [-d dbase]  \<evid\> | mark event as reviewed | 
| acceptTrigger | [-d dbase] \<evid\> | mark trigger as reviewed |
| deleteEvent  | [-d dbase] \<evid\> | delete event or trigger |
| undeleteEvent | [-p] [-d dbase] \<evid\> | undelete event or trigger |
| alarmSend | [-d dbase] \<evid\> | send alarms |
| alarmCancel | [-d dbase] \<evid\> | cancel alarms | 
| uncancel | [-h] [-q] -[D\|F] [-d dbase] \<evid\> | updates db rows, for input evid having an action_state 'CANCELLED' to the action_state='!CANCELLED' | 
| finalizeEvent | [-d dbase] \<evid\> | finalize the event |
| cubeMessage | [-dehvCDEHLP][-d dbase] \<evid\> [\<comment\>] | return a Cubic message for this evid |
| cubeTowns | [-d dbase] | print the list of towns in the db(GazetteerPt table) in CUBE file format | 
| **Process Control System (PCS)** | | 
| puttrans | [-d dbase] [-s char] \<oldGrp\> \<oldSrc\> \<oldState\> \<oldResult\> [\<newGrp\> \<newSrc\> \<newState\> \<newRank\> [\<newResult\>]] | create transition definition |
| deltrans | [-d dbase] [-s char] \<oldGrp\> [\<oldSrc\>] [\<oldState\>] [\<oldResult\>] [\<newGrp\>] [\<newSrc\>] [\<newState\>] [\<newRank\>] [\<newResult\>] | delete a transition definition |
| gettrans | [-d dbase] [-q] [-s char] [\<group\>] [\<table\>] [\<stateold\>] | show transition definitions |
| getstates | [-d dbase] [-c\|C] [-iI] [-q] [-r] [-n] [[group] [table] [state] [rank] [result]] | show posted events |
| getStatesAllDbs | | run 'getstats' for all databases | 
| post | [-d dbase] \<id\> \<group\> \<table\> \<state\> \<rank\> [\<result\>] | post event to a state |
| postEvidSince | [option-list] \< [days] or [startDate endDate]> <state> [rank] [source] [group] <br>options: [-h] [-d dbase] [-e types] [-m mag -M mag] [-s esrc -S osrc ] [-p] [-r] | post all non-bogus events within specified date range | 
| postFromList | [-d dbase] \<id-file\> \<group\> \<table\> \<state\> \<rank\> [result] | post events listed in a file to a state |
| autoposter | [-d dbase] [-q] [-s char] [\<instancename\>] | print the row column data found in AUTOPOSTER database table |
| unpost | [-d dbase] \<evid\> [ evid2 evid3 ...] | remove an event from all states |
| recycle | [-d dbase] [-s char] \<oldGroup\> \<oldSource\> \<oldState\> \<oldResult\> [\<newGroup\> \<newSource\> \<newState\> \<newRank\> [\<newResult\>]] | change an existing state posting description to a new state posting description in database PCS_STATE table | 
| delstates | [-d dbase] [-i id] \| \<group\> [\<table\> \<state\> \<rank\> \<result\>] | remove an event from all states |
| transition | [-d dbase] [[\<group\> \<table\>] [state] [evid]] | transitions all non-zero result posts for input parameters | 
| next | [-d dbase] [-s secs] \<group\> \<table\> \<state\> | Get the next event in this state |
| result | [-d dbase] [-r rank] \<id\> \<group\> \<table\> \<state\> \<result\> | set event's result value |
| resultAllIds | [-h] [-d dbase] \<group\> \<table\> \<state\> \<result\> | sets the RESULT value of all PCS_STATE table rows matching the input state description | 
| deleteWavefiles | [-d dbase] | remove the event directory found under the wavefile root path |
| enabledForPCS | [-d dbase] \<appName\> \<pcsState\> | prints true if PCS application mapped to input 'appName' | 
| **Post Processing Tasks** | |
| ampgenpp | -[h] [-p property-file] \<evid\> | create amps and write to dbase | 
| ampgenPPcont | [-d -c -l -v] [prop-file] | continuously running script to create amps and write to dbase |
| ampgen2shake | -[h] [-p propfile] [-s] \<evid\> | create amps and if successful, execute shakemap alarm scripts |  
| rcgone | -[h] [-d dbase] [-q -o -r -w -s] \<evid\> | generate request cards (archive waveform requests) for evid |
| newtrig | -[options] [ \<lat\> \<lon\> [z] [magValue] [bogusFlag] [selectFlag] [etype] [\#locPh] [\#magSta] [locRMS] [magRMS] ] | create a new subnet trigger, or with option -i use an existing evid | 
| makeChannelRequest | [-h] [-d dbase] [-r] \<evid\> \<n.s.c.l\> \<ondate\> \<duration\> | Create a request_card table row for evid and net.sta.seedchan.location having specified window starting datetime and duration |
| requestdump | [-h] [-s names] [-d dbase] [-c] [ -aA \| [evid]] | print a summary of REQUEST_CARD table data of request type 'T' | 
| deleteRequests | [-d dbase] evid | delete request_card table rows associated with the input event evid | 
| **Miscellaneous Utilities** | | 
| masterrt | | output default master RT database alias name |
| masterdb | | output default postproc archive database alias name |
| dbinfo | [key] | print database username, password and name | 
| showlocks | [-d dbase] | show events locked by Jiggle |
| unlockevent | [-d dbase] \<evid\> | unlock an event locked by Jiggle |
| epoch | | show system time in epoch seconds |
| epochdate | [-l leap_true_epochseconds] [-n unix_nominal_epochseconds ] [-s\|-S datestring] | show : Epoch (unix, leap)=(\<unix-epochsecs\>, \<leap-epochsecs\>) (date-utc = date-local) | 
| whereIs | [-d database]  \<lat\> \<lon\> | print the locale of the input lat,lon, using database WHERES package | 
| whereIsEvent | [-d database] \<evid\> | print the locale of the input evid, using database WHERES package | 
| hypolocate | -[h][-P] [-d dbase] [-o\|O]  [-f arcfile] [-u URL] [-p port] [-x] \<evid\> | locate this event with a [SolServer](https://aqms-swg.gitlab.io/aqms-docs/man-solserver.html) |
| gazregions | [-h] [-d dbase] [-n names] [-N] [-q]  [lat lon] | list name and coordinates of regions found in gazetteer_region table | 
| magprefrules | [-h] [-d dbase] [-r name] [-t magtype] | print summary listing of magprefpriority table rules | 
| orgprefrules | [-h] [-d dbase] [-r name] [-t type] | print summary listing of orgprefpriority table rules | 
| waveroots | [-h] [-d dbase] [-t T\|C ] [-c wcopy\# ] | list the database waveroots table rows | 
| adminMail | | send a message about a runtime code/cfg change on this host to $TPP_ADMIN_MAIL addresses |
| amPrimaryDb | [-d dbase] | returns true if the connected database's role is set primary in RT_ROLE table |
| amPrimaryHost | [-d dbase] [hostname] | returns current role value from AQMS_HOST_ROLE table for hostname |
| cleanup | \<dir\> \<days\> [find command options in quotes] | remove all non-executable type files | 
| dbidrivers | | prints the available perl DBI drivers |
| showdbdrivers | | prints a the available perl DBI drivers and the DBI->data_sources(driver) | 
| getHostRole | [-h] [-a] [-A] [-qr] [-d \<dbase\>] [-n host] | list host row column values found in AQMS_HOST_ROLE table | 
| getAppHostRole | [-h] [-a] [-A] [-H] [-d \<dbase\>] [-n host] [-p appname] [-s state] [-m mode A,P, or S] | list host row column values found in APP_HOST_ROLE table | 
| setHostAppRole | [-h] [-d dbase] \<[-f fname] or [\<host\> \<appname\> \<state\> \<role_mode\> [ondate] [offdate]]\> | Insert a APP_HOST_ROLE table row | 
| setHostRole | [-h] [-d <dbase>] \<host\> \<role-code\> | changes the PRIMARY_SYSTEM role status for the named host in the in AQMS_HOST_ROLE table | 
| setHostPairRoles | [-h] [-d \<dbase\>] \<primary-host\> \<shadow-host\> | changes the PRIMARY_SYSTEM role status for a matched pair of hosts in AQMS_HOST_ROLE table | 
| deleteAppRole | [-h] [-d dbase] [-a] \<host\> [\<appname\>] [\<state\>] | delete matching APP_HOST_ROLE table rows |
| getHostsForAppState | [-d dbase] \<pcsAppName\> \<pcsState\> [role] | return hosts enabled in the db for named application and state | 
| localHostAppRole | [-d dbase] \<appName\> \<pcsState\> | prints the role flag mapped to the localhost, application name and PCS state configuration | 
| getDbRole | [-h] [-A] [-d \<dbase\>] [-q] | list row column values found in database RT_ROLE table | 
| evids2cmd | [-f filename] \<command\> [list of evid arguments ...] | executes command specified for one or more evid | 
| help4tpp | | invokes the '-h' option for all scripts under bin |
| hexify | -[h] \<string\> | hexify an ASCII string | 
| psgrep | [-h] \<string\> | Print a list of those process that contain the input string |  
| showPPcont | | lists process table info for configured job-descriptions | 
| killPPcont | | kills configured aqms (postproc) processes that normally run as crontab jobs | 
| killPPgrep | \<grep-string\> | kill one or more aqms pids returned by 'pgrep -f grep-string' |
| killPPjob | \<pid\> | kill process whose pid matched first input arg pid |  
| lsInstalledPm | [grep-string (e.g. a module name)] | list perl modules found in the @INC paths | 

  
### Installation  
  
**Installation**  
  
Steps in installing the utility scripts:  
  
*  Choose a target directory, e.g. /app/aqms/tpp/bin. 
*  Download the distribution from gitlab. 
*  Untar the distribution into the target directory
*  Add required Environmental Variables (see below) 
  
  
**Requirements**  
  
*  Perl 5, DBI package
*  Oracle client and valid tnsnames.ora file
*  Define environmental variables:
    *  TPP_HOME - the directory above where the scripts are located
    *  TPP_BIN_HOME - where the scripts are located 

  
**Config files**  
  
Configuration files are typically installed in a directory paralle to the *bin* directory, e.g. /app/aqms/tpp/cfg.  
  
The config files containing database connection information the most used. They are present under the *dbinfo/cfg* folder. ??? Add link to documentation for dbinfo???








