# mkv0 {#man-mkv0}  
  
Generate and publish COSMOS V0 waveforms files.  
  
## PAGE LAST UPDATED ON  
  
2021-09-27  
  
## NAME  
  
mkv0.pl  
  
## VERSION and STATUS  
  
version: v0.0.8  
status:  ACTIVE  
  
## PURPOSE  
  
Generate and publish COSMOS V0 waveforms files.  
  
## HOW TO RUN  
  
```
mkv0 version 0.0.8
        mkv0 - generate and publish COSMOS V0 waveforms files
Syntax:
        mkv0 -c config [-d DB] [-FMSX] eventID
        mkv0 -h
where:
        -c config   - specify the configuration file (required)
        -d DB       - specify masterDB instead of that returned by
        $TPP_BIN_HOME/masterdb.
        -f file     - Add stations listed in <file> to event list
        -F          - Copy V0 files to configured FTP directory
        -M          - Send email about new V0 files in FTP directory
        -S          - Copy new V0 files to configured sendfile directory
        -X          - don't replace existing V0 SCNL files (saves time)
        -h          - print usage syntax and exit.
        -v          - verbose mode: tell what's happening
```  
  
## CONFIGURATION FILE  
  
Configuration file for mkv0.pl is *mkv0.conf* and is described below.  
  
Format is  
``` 
<parameter name> <data type> <default value>
<description>
``` 
  
**WorkDir** *string*   
Path where we create event directories for work.  
  

**ProgName** *string* MK-V0  
Program name to look up eligible SNCLs in Applictions/AppChannels tables.  
  

**MinACC** *float* 0.1  
Acceleration threshold (percent g) for making files for station's SNCLs.  
  

**vmodelFile** *string*  /some/path/allcal_model.d
Velocity model file for travel-time calculations.  
  

**PreEventTime** *number*  
Seconds before estimated P arrival to start waveform.  
  

**SecsPerMag** *number*  
Waveform duration should be this many seconds per magnitude.  
  

**minComplete** *float*  
The minimum level of waveform completeness (0 to 1) required to create V0 files.  
  

**DataSource** *string*  
DataSource: source of miniSEED data  
Basic syntax is "DataSource <type> <options>  
where type can be either "DB" or "WVC".  
The DB DataSource gets event waveforms from the database using SQL queries to the Wave stored procedure package, similar to how jiggle gets waveforms.  By default this will use the same database connection as used for parametric info; in that case no options are given after "DB".  
Alternatively, a different database connection can be used. To specify an alternate database connection, use the following:   

Database DB dbname dbuser dbpass  
where dbname, dbuser and dbpass are replaced with appropriate values.  
  
The WVC DataSource uses the external program "wvc" to get waveforms from AQMS-style wave servers such as pws, rws or dws. The syntax is   
  
DataSource WVC <startday> <endday> <wvc path and server-option>  
where *\<startday\>* and *\<endday\>* specify the time period for which this WVC command will be used.  

*\<startday\>* is the number of days before present before which the waveforms are to be requested; use "0" for current waveforms.  
    
  *\<endday\>* is the number of days before present after which the waves are requested. Use "-1" for requests going back to the "beginning of time".  
    
  *\<wvc path and server-option\>* is used to specify the full path to the wvc command as well as the server option (-S hostname:port) to use.  
  
For example, to get waveforms no more than 4 days old from  mono.geo.berkeley.edu, you would use this command:  

DataSource WVC 0 4 /home/ncss/run/bin/wvc -S mono.geo.berkeley.edu:9321  
  
You can specify as many DataSource commands as you need. They will be tried in the order given (provided the time limits are met for WVC datasources). Once 100% of the requested waveform is obtained for a given SCNL, no more requests will be made for that SCNL.  
  
For example :  
  
DataSource  DB  
DataSource  WVC 0 4 /app/aqms/utils/wvc/bin/wvc -S lhotse.gps.caltech.edu:9101  
  
  
**domain** *string*  
1st char of originator's net code. For e.g. C for the CI network code.    
  

**procNet** *string*  
Processing network. The IRIS/FDSN network code of agency processing or producing COSMOS V0 files; see COSMOS Table 4 for "IRIS Code" values.  
  

**scpTarget** *string*  
scpTarget is [user@]host to which files are copied by scp. This is the hostname of your FTP server.  
  

**scpDir** *string*  
scpDir is directory on scpTarget where files are to be put so that they are accessible to your FTP server.  
  

**ftpServer** *string*  
ftpServer is name of FTP server as accessed by ftp, not by scp.  
  

**ftpBase** *string*  
ftpBase is base directory in which new event directory will be found by ftp (not by scp).  
  

**notifyList** *string*  
List of email addresses to notify of new event data, separated by comma. All addresses must be given on a single notifyList line.  
  

**destinationDirs** *string*  
Directories to which files will be placed for sendfile/getfile transport. These directories must already exist. All directories must be given on one destinationDirs line.  
  

**tempSubDir** *string*  
Subdirectory within destinationDirs for writing temporary files before moving to destinationDirs.
  

A sample configuration is shown below :   
  
```
# Configuration file for mkv0
# Lines starting with '#' or empty lines are ignored

# workDir: where we create event directories for work
WorkDir    /app/aqms/tpp/exportwf/work

# Program name to look up eligible SNCLs in Applictions/AppChannels tables
ProgName   MK-V0

# minAcc: acc. threshold (percent g) for making files for station's SNCLs
MinACC    0.1

# Velocity model file for travel-time calculations
vmodelFile   /app/aqms/tpp/exportwf/cfg/allcal_model.d

# PreEventTime: seconds before estimated P arrival to start waveform
PreEventTime   30

# SecsPermag: waveform duration should be this many seconds per magnitude
SecsPerMag     30

# minComplete: the minimum level of waveform completeness (0 to 1) required
# to create V0 files. 
minComplete    0.15

# DataSource: source of miniSEED data
# Basic syntax is "DataSource <type> <options>
# where type can be either "DB" or "WVC".
# The DB DataSource gets event waveforms from the database using SQL queries
#    to the Wave stored procedure package, similar to how jiggle gets
#    waveforms.  By default this will use the same database connection as used
#    for parametric info; in that case no options are given after "DB".
#    Alternatively, a different database connection can be used.
#    To specify an alternate database connection, use the following:
#    Database DB dbname dbuser dbpass
#    where dbname, dbuser and dbpass are replaced with appropriate values.
#
# The WVC DataSource uses the external program "wvc" to get waveforms
#    from AQMS-style wave servers such as pws, rws or dws.
#    The syntax is 
#        "DataSource WVC <startday> <endday> <wvc path and server-option>
#    <startday> and <endday> specify the time period for which this WVC
#    command will be used. <startday> is the number of days before present
#    before which the waveforms are to be requested; use "0" for current 
#    waveforms.
#    <endday> is the number of days before present after which the waves are
#    requested. Use "-1" for requests going back to the "beginning of time".
#    <wvc path and server-option> is used to specify the full path to
#    the wvc command as well as the server option (-S hostname:port) to use.
#
# For example, to get waveforms no more than 4 days old from
# mono.geo.berkeley.edu, you would use this command:
#  DataSource WVC 0 4 /home/ncss/run/bin/wvc -S mono.geo.berkeley.edu:9321
#
# You can specify as many DataSource commands as you need. They
# will be tried in the order given (provided the time limits are met
# for WVC datasources). Once 100% of the requested waveform is
# obtained for a given SCNL, no more requests will be made for that SCNL.
#DataSource  DB
#DataSource  WVC 0 4 /app/aqms/utils/wvc/bin/wvc -S lhotse.gps.caltech.edu:9101
DataSource  WVC 0 -1 /app/aqms/utils/wvc/bin/wvc -S <server name>:<port number>

# data originator - 1st char of originator's net code
domain       C

# Processing network: IRIS/FDSN network code of agency processing
# or producing COSMOS V0 files; see COSMOS Table 4 for "IRIS Code" values.
procNet      CI

# scpTarget is [user@]host to which files are copied by scp
# This is the hostname of your FTP server.
scpTarget    username@ftp-servername.example.com
# scpDir is directory on scpTarget where files are to be put so that they
# are accessible to your FTP server.
scpDir       /app/ftp/pub/V0DataFiles2

# ftpServer is name of FTP server as accessed by ftp, not by scp.
ftpServer    ftp-servername.example.com
# ftpBase is base directory in which new event directory will be found by ftp
# (not by scp).
ftpBase      /ftp/V0DataFiles2

# List of email addresses to notify of new event data, separated by comma.
# All addresses must be given on a single notifyList line.
notifyList some-emailid@example.com

# Directories to which files will be placed for sendfile/getfile transport
# These directories must already exist. All directories must be given
# on one destinationDirs line.
destinationDirs /app/aqms/tpp/exportwf/destination/send_v0/cgs

# subdirectory within destinationDirs for writing temporary files before 
# moving to destinationDirs.
tempSubDir temp.dir
#
```
  

## ENVIRONMENT  
  
  
## DEPENDENCIES  
  
  *  wvc
  *  qmerge
  *  scp
  *  mailx  
  

## MAINTENANCE  
  

## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues  
  

## MORE INFORMATION  
  
The documentation for the COSMOS V0 format can be found at https://www.strongmotioncenter.org/vdc/cosmos_format_1_20.pdf  
  





























