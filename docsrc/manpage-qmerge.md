# qmerge {#man-qmerge}  
  
*qmerge* is a program to convert, reformat, and manipulate SEED files.   
  
## PAGE LAST UPDATED ON  
  
2021-11-15  
  
## NAME  
  
qmerge  
  
## VERSION and STATUS  
  
version 1.4.57 (2014.329)
status : ACTIVE  
  
## PURPOSE  
  
The program qmerge program performs file merging and data extraction operations on Quanterra data files and MiniSEED files. Qmerge can merge one or more Quanterra single channel input streams containing overlapping or adjacent time intervals into a single output stream, eliminating overlapping or redundant data. The single output data stream consists of MiniSEED or plain SEED data records wieh data blocks that span the continuous time interval of all of the intput data streams. In addition, it can extract or output data for a specified time window.  
  
The input data blocksize can be any valid MiniSEED blocksize from 512-byte to 4K-bytes, and the input file format can be MiniSEED, SEED data records, Steim binary, or Adebahr DRM file formats. The data encoding format can be either STEIM1, STEIM2, INT_16, INT_24, or INT_32 compression format as described in the FDSN SEED manual. The output data format is the same as the input data format. Input files with different data formats should not be merged.  
  
Qmerge will operate only on files that contain data from a single station, channel, and network. If files contain data from multiple stations, channels, or networks, the myst first be de-multiplexed into separate station-channel-network files with the sdrsplit program.  
  
Qmerge can be used for many different purposes. It may be used to create a single continuous data stream from multipe input files that may contain data from overlapping time intervals. It may be used to extract data from 1 or more data file that contain data from consecutive time intervals. Qmerge can be used to reblock the data into larger or smaller blocks. The default output blocksize is 4K bytes, irregardless of the input blocksize. You may explicitly specify the output blocksize to any valid blocksize (a power of 2) from a minimum of 512 byte to a maximum of 4K bytes.  
  
Qmerge will automatically provide appropriate time tolerance values for deciding when blocks can safely be merged together without loss of significant time information, or the user may specify explicit time tolerance values.   
  

## HOW TO RUN

```
./qmerge -h
qmerge version 1.4.57 (2014.329)
qmerge  [-o output_file | -n] [-a] [-m] [-T] [-f date] [-t date | -s interval] [-e]
    [-u] [-P duration] [-pn | -pd]
    [-g tol] [-G tol] [-V version] [-x blockette_list|all]
    [-S station] [-C channel] [-N network] [-L location]
    [-D j|m] [-v] [-b blksize] [-i time | d0 | no_d0 | rate] [-c] [-H] [-d n]
    [-F file_list_file] [-w dn | hn] [-I input_data_fmt] [-O output_data_fmt]
    [-r] [-R record_type] [-h] [-l c] [input_file_1 ... input_file_n]
    where:
	-o outfile  Name of output file (or template filename with -P option).
	-a	    Append output to file instead of overwriting file.
	-n	    No data output.  Equivalent to -o /dev/null.
	-m	    Multichannel mode.  You can specify only 1 input file,
		    but it can contain multiple channels of data.
	-T	    Exact trim -- trim data to closest specified time.
		    Default is to trim to nearest inclusive block boundary.
	-f date	    From date - ignore data before this date.
	-t date	    To date - ignore data from this date on.
		    Date can be in formats:
			yyyy/mm/dd/hh:mm:ss.ffff
			yyyy/mm/dd.hh:mm:ss.ffff
			yyyy/mm/dd,hh:mm:ss.ffff
			yyyy.ddd,hh:mm:ss.ffff
			yyyy,ddd,hh:mm:ss.ffff
		    You may leave off any trailing 0 components of the time.
	-s interval span interval.  Alternate way of specifying end time.
		    Interval can be an integer followed immediately by
		    S, M, H, or d for seconds, minutes, hours, or days.
	-e	    Output only data blocks that have event flags turned on.
	-u	    Unique data flag.  Assume ALL input data is unique,
		    and suppress the removal of apparent duplicate data caused
		    by apparent data overlaps between data records.
	-P duration Partition output into distinct files of specified duration.
		    Duration may be 'NU' where N = decimal number, and
		    U is one of the following characters: y = year,
		    m = months, d = days, H = hours, M = minutes, S = seconds.
	-pn | -pd   Create name for partitioned output file based on nominal
		    partition time or on first data time.
		    Default is based on nominal parition time.
	-b blksize  Specify output blksize in bytes (or 0).  Valid blksize
		    is between 256 and 8192 inclusive, and blksize=2^N.
		    0 means use the blksize of the first used input record.
	-g tol	    Explicit tolerance for merging blocks in ticks (1/10 msec).
		    Blocks will be merged if the time diff is < tol.
		    Default is .1 of the data sample interval.
	-G TOL	    Explicit tolerance for time gaps in ticks (1/10 msec).
		    Default is .4 of data sample interval.
	-V version  Specify SEED version number for data header and blockettes.
		    Default version is 2.2
	-x list|all Strip the specified blockettes or all blockettes.
		    List can be comma-delimted and include a span, such as:
		    200-500,1001 or the string 'all' for all blockettes.
	-S station  Set the 5 char station name to the specified station name.
	-C channel  Set the 3 char channel name to the specified channel name.
	-N network  Set the 2 char network code (eg BK).
	-L location Set the 2 char location code (normally blank).
	-D j | m    Display dates in julian or month and day format.
	-i time	    Ignore time span info in header.
	-i rate	    Ignore changes in sample rate.
	-i d0	    Ignore (silently fix) d0 discrepencies between blocks.
	-i no_d0    Ignore and do NOT fix d0 discrepencies between blocks
		    (unless data is reblocked).
	-i dq	    Ignore MiniSEED Data Quality type (Q,D,R,M) when
		    selecting best input stream
	-v	    Verify internal consistency of all data frames and blocks.
	-c	    Write a content index line of station, channel, network,
		    location, and blksize of the first file to stdout.
		    Blank network and location code will be displayed as '--'.
		    Do not process files.
	-H	    Write content index info and file header timespan info
		    to stdout for each file.  Timespan will be displayed only.
		    for SEED telemetry files.  Do not process files.
	-d n	    Debug output (OR of any of the following values:)
			1 = time slews
			2 = time info
			4 = block info
			8 = block headers
			16 = flags
			32 = input stream
			64 = output stream
			128 = filelimits
			1024 = output ascii
			2048 = output ASCII - 1 hdr per contiguous data segment
	-F file_list_file
		    File containing a list of data files, one file per line.
		    If this option is used, no input_file names are read from
		    the command line.
	-w output_wordorder
		    Specified the word order for the header and/or data
		    portions of the output MiniSEED records.
		    Option may be used more than once.  Valid word orders are:
		    0 = little endian (Intel/VAX) header and data.
		    1 = bit endian (SPARC/Motorola) header and data.
		    h0 = little endian header.	h1 = big endian header.
		    d0 = little endian data.	d1 = big endian data.
		    This option may be used multiple times to specify different
		    header and data word orders.
		    The default word order is big endian header and data.
	-I input_data_format
		    Specifies the data format of input pre-MiniSEED SEED Data
		    Records (SDR) without a blockette 1000 that defines
		    the data format within the record.
		    Valid formats are STEIM1, STEIM2, INT_16, INT_32, INT_24,
			IEEE_FP_SP, IEEE_FP_DP.
		    Default data format for SEED Data Records is STEIM1.
	-O output_data_format
		    Specifies the output data format for the MiniSEED records.
		    Valid formats are STEIM1, STEIM2, INT_16, INT_32, INT_24,
			IEEE_FP_SP, IEEE_FP_DP.
		    The default is the data format of the input data.
		    This option MUST be specified if there are multiple
		    input data formats.
	-r	    Repack the data into new records.
	-R record_type
		    Specify the SEED record_type field used for data quality.
			D = unknown quality (backwards compatible)
			R = Raw data (not QC-ed)
			M = Merged data
			Q = QC-ed (Quality controlled)
		    Default is the same as input records.
	-l c	    Specify character for optional delimiter for offset and len
		    in input filename.
	-h	    Help - prints syntax message.
	input_file(s)
		    Input file(s) containing quanterra data.
		    If no input file is specified, input is read from stdin.
```
  
### Examples  
  
```
1.  Extract data from 1 or more input files.

qmerge -f 93.154,23:00 -s 2H -o BKS.LHZ.event \

	/data/archive/bbdata/1993/93.15[45]/BKS.BK.BHZ.D*



2.  Convert DRM files to SEED data records and merge dialup data.

qmerge -f 93.155,00:00 -s 1d -o BKS.BK.BHZ.D.93.155 \

  /data/aq01/data/BKS/BHZ.D/* /data/aq01/dialup.stream/BKS.BHZ.D.93.155*
```


## CONFIGURATION FILE  
  
NA  

## ENVIRONMENT  
  
NA  

## MAINTENANCE  
  
NA  
  
## BUG REPORTING
  
The only types of SEED data records that are currently recognized are Steim-1 and Steim-2 compressed data. Only data in big-endian (eg SPARC or Motorola word order) is recognized.   
  
## MORE INFORMATION  
  
Author : Doug Neuhauser, UC Berkeley Seismological Laboratory.