# qml {#man-qml}

Perl script to generate quakeML for AQMS database.

## PAGE LAST UPDATED ON

2021-02-11

## NAME

qml

## VERSION and STATUS

version: qml 8678 2021-02-09 10:05:54Z stephane
status: ACTIVE

## PURPOSE

Generate [quakeML](https://quake.ethz.ch/quakeml) for AQMS database. 

## HOW TO RUN

```
./qml qml.conf

./qml --help

Usage: qml [-OPTIONS [-MORE_OPTIONS]] [--] [PROGRAM_ARG1 ...]

The following single-character options are accepted:
	With arguments: -a -c -C -d -f -i -m -o -O -p
	Boolean (without arguments): -A -D -F -h -M -S -T -w

Options may be merged together.  -- stops processing of options.
Space is not required between options and their arguments.
  [Now continuing due to backward compatibility and excessive paranoia.
   See ``perldoc Getopt::Std'' about $Getopt::Std::STANDARD_HELP_VERSION.]
No event IDs found
    qml version $Id: qml 8665 2020-09-28 10:36:36Z aparnab $
	qml - generate quakeml XML for one or more events
    Syntax:
	qml [-c config] [-d debug] [-o out-file] [-C createTime] 
    [-A|[[-F|-f n][-M|-m type-list][-T]]] [-S] [-p pdl_eventID]  [-O o-type] 
    [-w] event-id...

	qml [-c config] [-d debug] [-o out-file] [-C createTime] 
    [-A|[[-F|-f n][-M|-m type-list][-T]]] [-S] [-O o-type] [-w] 
     -i eventlist_file

	qml -D [-c config] [-d debug] [-o out-file] [-T|-f n [-a orid]] 
    [-p pdl_eventID] event-id

	qml -h
    where:
	-c config	- specify an alternate configuration file.
		The default config file is /app/aqms/utils/qml/cfg/qml.conf.
	-C time		- no longer used; for backward compatibility only
	-d level	- specify the debug level.
	-D		- deletion QuakeML for a single event-id.
	-i file		- read event IDs from <file> instead of from
		commandline. To read from STDIN, use "-i -".
	-p eventID	- specify PDL eventID if different from local event ID.
	-h		- prints this help message.
	-o out_file	- writer QuakeML to out_file; default is to stdout.

      Element selection criteria:
      Default is to include only the preferred origin and magnitude,
      without any of their supporting picks, codas, etc.
	-a orid		- select first motion mecs from alternate orid instead
		of event preferred origin. Used only for deletion QuakeML of
		first motion mecs.
	-A		- include all focal mecs, preferred mags,
		moment tensor, and their supporting origins, but without any
		PDL product markings.
	-F		- include all focal mechansisms, without PDL product
		marks.
	-f n		- only the nth (starting at 1) first-motion mechanism
		from all mec's of the preferred origin, ordered by their
		mecids.
		Mark dataID as a PDL focal mechanism product.
	-M		- include each of the preferred magnitude types.
		Default is to include only the event preferred magnitude.
	-m type-list	- Include magnitudes only from type-list, a comma
		separated list of magnitude types without the leading 'M'.
		Example: -m d,l will include magnitudes of types Md and Ml.
		Magnitude types are those within the database, not as mapped
		to NEIC conventions.
	-O o_type	- Limit included origins to those of type o_type;
		include the preferred o_type origin if it would not otherwise
		be included. Requires the EventPrefOr database table.
	-T		- Include the preferred moment tensor and its
		supporting origins. Mark dataID as a PDL moment tensor product.
	-S		- Include supporting picks, codas, WA amps for 
		contained origins, magnitudes.
	-w		- adds an event name description text element.
		The event name string is its distance and bearing from
		the closest town found in the database gazetteerpt table.

NOTES:
   qml does not do event selection. It tries to find and report whatever
   event parameters you have selected for the event IDs given to it.
```
  

## CONFIGURATION FILE
  
*qml.conf* is described below.  
  
Format is  
``` 
<parameter name> <data type> <default value>
<description>
```
  
**Database Connection Parameters**  

**DB_USER**  *string*  
The name of the database user account.  
  
**DB_PASSWORD**  *string*  
The password for DB_USER.  
  
**DB_NAME**  *string*  
The name of the database being used.  
  
**AUTH_ID**  *string*  
Authority ID of the RSN producing the quakeml.  

**DATA_SOURCE**  *string*  
Network code of data source in upper case.  
  
**EVENT_SOURCE**  *string*  
Network code of event ID; leave blank if same as DATA_SOURCE  

**DOI**  *string*  
Digital Object Identifier for the event; optional; experimental  
  
**MTMANIP**  *string*  
Path to mtmanip program; only needed for moment tensor products.  
  
**FMMECHTYPE**  *string*  
First-motion (FM) mechanism type used in DB mec table.  
  
**Configurable templates for QuakeML resouce identifiers**  

See QuakeML documentation:  
https://quake.ethz.ch/quakeml/docs/REC?action=AttachFile&do=view&target=QuakeML-BED-20130214a.pdf  
For most users, the default values should be adequate.  

| ELEMENT IDENTIFIERS | DATABASE RESOURCE | DEFAULT VALUE & DESCRIPTION |
| ------ | ------ | ------ |   
| IDeventParameters | Event | IDeventParameters = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID#QML_TIME |
| IDevent | Event | IDevent = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID  |
| IDorigin | Origin | IDorigin = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID  |
| IDarrival | AssocArO | IDarrival = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID  |
| IDmagnitude | Netmag | IDmagnitude = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID  |
| IDmomentTensor | Mec | IDmomentTensor = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID |
| IDpick | Arrival | IDpick = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID |
| IDcoda | Coda | IDcoda = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID |
| IDamp | Amp | IDamp = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID |
| IDstationMagnitude_Coda | Coda | IDstationMagnitude_Coda = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID |
| IDstationMagnitude_Amp | Amp | IDstationMagnitude_Amp = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID |
| IDfocalMechanism | Mec | IDfocalMechanism = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID |
| IDgreensFunction | tdmt | IDgreensFunction = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID. For the greensFunction identifier, QML_ID is the name of the Green's Function velocity model being used. |

Template parameters available for substitution within qml:  
*  **QML_AUTHID** = AUTH_ID read from config file  
*  **QML_RESOURCE**  
*  **QML_DATASOURCE** = DATA_SOURCE read from config file  
*  **QML_ID**  database table ID number, except for IDgreensFunction where it is  
         the name of the Green's Function velocity model being used.           
*  **QML_TIME** = high-res time stamp, epoch time * 100 (without leapseconds)  
         QML_TIME should be part of IDeventParameters to ensure that a           
         unique string is generated for each QuakeML file.  
  
  
  

A sample qml.conf is shown below:  
  

```
# database connection parameters:
DB_USER = dbuser
DB_PASSWORD = dbpassword
DB_NAME = dbname

# Authority ID (lower case), tylically net-code.anss.org
AUTH_ID = ci.anss.org
# Network code of data source, (upper case)
DATA_SOURCE = CI
# Network code of event ID; leave blank if same as DATA_SOURCE

EVENT_SOURCE = CI

# Digital Object Identifier for the event; optional; experimental
DOI = quakeml:doi.org/10.7909/C3WD3xH1
# path to mtmanip program; only needed for moment tensor products
MTMANIP = /app/aqms/tmts2/bin/mtmanip
# first-motion (FM) mechanism type used in DB mec table
FMMECHTYPE = FA
#
# Configurable templates for QuakeML resouce identifiers
# See QuakeML documentation:
# https://quake.ethz.ch/quakeml/docs/REC?action=AttachFile&do=view&target=QuakeML-BED-20130214a.pdf
# For most users, the default values should be adequate.
#
# ELEMENT IDENTIFIERS           RESOURCE
# IDeventParameters             Event
#    default:  
# IDeventParameters = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID#QML_TIME
#
# IDevent                       Event
#    default:  
# IDevent = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID
#
# IDorigin                      Origin
#    default:  
# IDorigin = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID
#
# IDarrival                     AssocArO
#    default:  
# IDarrival = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID
# 
# IDmagnitude                   Netmag
#    default:  
# IDmagnitude = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID
#
# IDmomentTensor = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID
#
# IDpick                        Arrival
#    default: 
# IDpick = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID
#
# IDcoda                        Coda
#    default:  
# IDcoda = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID
#
# IDamp                         Amp
#    default:  
# IDamp = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID
#
# IDstationMagnitude_Coda       Coda
#    default:  
# IDstationMagnitude_Coda = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID
#
# IDstationMagnitude_Amp        Amp
#    default:  
# IDstationMagnitude_Amp = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID
#
# IDfocalMechanism              Mec
#    default:  
# IDfocalMechanism = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID
# 
# IDgreensFunction              tdmt
#    default:  
# IDgreensFunction = quakeml:QML_AUTHID/QML_RESOURCE/QML_DATASOURCE/QML_ID
#   For the greensFunction identifier, QML_ID is the name of the Green's
#   Function velocity model being used.
# 
# Template parameters available for substitution within qml:
# QML_AUTHID = AUTH_ID read from config file
# QML_RESOURCE
# QML_DATASOURCE = DATA_SOURCE read from config file
# QML_ID  database table ID number, except for IDgreensFunction where it is 
#         the name of the Green's Function velocity model being used.
# QML_TIME = high-res time stamp, epoch time * 100 (without leapseconds)
#         QML_TIME should be part of IDeventParameters to ensure that a
#         unique string is generated for each QuakeML file.
IDeventParameters = quakeml:service.scedc.caltech.edu/fdsnws/event/1/query?eventid=QML_ID
IDevent = quakeml:service.scedc.caltech.edu/fdsnws/event/1/query?eventid=QML_ID
IDorigin = quakeml:service.scedc.caltech.edu/fdsnws/event/1/query?originid=QML_ID
IDarrival = quakeml:service.scedc.caltech.edu/fdsnws/event/1/query?assocaro=QML_ID
IDmagnitude = quakeml:service.scedc.caltech.edu/fdsnws/event/1/query?magnitudeid=QML_ID
IDmomentTensor = quakeml:service.scedc.caltech.edu/fdsnws/event/1/query?mecid=QML_ID
IDpick = quakeml:service.scedc.caltech.edu/fdsnws/event/1/query?arid=QML_ID
IDcoda = quakeml:service.scedc.caltech.edu/fdsnws/event/1/query?codaid=QML_ID
IDamp = quakeml:service.scedc.caltech.edu/fdsnws/event/1/query?ampid=QML_ID
IDstationMagnitude_Coda = quakeml:service.scedc.caltech.edu/fdsnws/event/1/query?assoccooid=QML_ID
IDstationMagnitude_Amp = quakeml:service.scedc.caltech.edu/fdsnws/event/1/query?assocammid=QML_ID
IDfocalMechanism = quakeml:service.scedc.caltech.edu/fdsnws/event/1/query?mecid=QML_ID
IDgreensFunction = quakeml:service.scedc.caltech.edu/fdsnws/event/1/query?tdmtid=QML_ID
```


## ENVIRONMENT

None

## DEPENDENCIES

qml depends on  
*  Qtime  
*  mtmanip  
*  Perl module, perl-XML-Writer

## MAINTENANCE

None

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities/-/issues

## MORE INFORMATION

Author : Pete Lombard
