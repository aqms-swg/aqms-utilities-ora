# masterrt {#masterrt}

Prints the default real time database alias name (e.g. rtdb) used by scripts.      

## PAGE LAST UPDATED ON

2021-06-09  

## NAME

masterrt.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Prints the default rtem db alias name (e.g. rtdb) used by scripts.  

## HOW TO RUN

```
usage: ./masterrt

Prints the default rtem db alias name (e.g. rtdb) used by scripts.
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


