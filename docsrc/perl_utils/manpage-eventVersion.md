# eventVersion {#eventVersion}  
  
   
Print the db version number of event matching input evid, a numeric value.    

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

eventVersion.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Print the db version number of event matching input evid, a numeric value.  
  
Prints "-1" if no event matching the input evid is found in the db.  

## HOW TO RUN  
  
```
usage: ./eventVersion [-d dbase]  <evid>
     
Print the db version number of event matching input evid, a numeric value.

Prints "-1" if no event matching the input evid is found in the db.

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-c        : print CUBIC version number, which may be an alpha char
            other than a number.

example: ./eventVersion -d k2db 8735346
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues