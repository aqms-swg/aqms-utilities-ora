# eventAge {#eventAge}  
  
Output event age in seconds.      
  
  
## PAGE LAST UPDATED ON

2021-05-28  
  

## NAME  

eventAge.pl   


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE  

Prints total integer seconds elapsed since event origin time, or "0" if the input event evid does not match an event row in db.   
  
  
## HOW TO RUN   
  
```
usage: ./eventAge [-d dbase] <evid>
```

 
Prints total integer seconds elapsed since event origin time, or "0" if the input event evid does not match an event row in db. 
  
    -h        : print usage info  
    -d dbase  : alias to use for db connection (defaults to "archdbe")  
  
example: ./eventAge -d k2db 8735346  
  
  

## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues