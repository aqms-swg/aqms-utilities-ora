# finalizeEvent {#finalizeEvent}

Finalize the event.  

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

finalizeEvent.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Finalize an event.  
This typically means set the rflag = "F" and post to a PCS state to handle updated alarm notifications, etc.

## HOW TO RUN

```
usage: ./finalizeEvent [-d dbasename]  <evid>
     
-h        : print usage info
-d dbase  : use this particular dbase (defaults to "archdbe")

example: ./finalizeEvent -d k2db 8735346
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


