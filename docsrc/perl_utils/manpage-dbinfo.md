# dbinfo {#dbinfo}  
  
Print database username, password and name   
  

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

dbinfo.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Without key argument prints all key-value pairs found in file:  
    /app/aqms/tpp/dbinfo/cfg/masterinfo.cfg
  

## HOW TO RUN  
  
```
usage: ./dbinfo [key]

Without key argument prints all key-value pairs found in file:
/app/aqms/tpp/dbinfo/cfg/masterinfo.cfg

Skips over empty lines or lines beginning with '#';
otherwise lines in this file should be key-value pairs
that describe db and account connection data used 
by scripts.

When a key input argument is provided, it prints the value 
found in the file for the line beginning with that keyword.
e.g. keywords:

masterrtuser
masterrtpwd
masterrt

masterdbuser    
masterdbpwd
masterdb

  example: ./dbinfo masterdb
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues