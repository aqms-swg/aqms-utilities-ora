# setGtype {#setGtype}  
  
Set the gtype for the origin whose orid is the prefor of the event matching the input evid argument.  

## PAGE LAST UPDATED ON

2021-06-14  
  

## NAME  

setGtype.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Set the gtype for the origin whose orid is the prefor of the event matching the input evid argument.  
  

## HOW TO RUN  
  
```
Set the gtype for the origin whose orid is the prefor of the event matching the input evid argument.

Possible gtype values are 'l', 'r', 't', or use '-' or '' for null.
    
usage: ./setGtype [-d dbasename] <evid> <gtype>
    
    -h        : print usage info
    -d dbase  : use this particular dbase (defaults to "archdbe")

example: ./setGtype -d k2db 8735346 l
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues