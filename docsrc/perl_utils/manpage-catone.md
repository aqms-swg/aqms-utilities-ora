# catone {#catone}  
  
Output summary line for one event   
  
  
## PAGE LAST UPDATED ON

2021-05-28  
  

## NAME  

catone.pl   


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE  

Write a summary line for one event.  
  
  
## HOW TO RUN   
  
```
usage: ./catone -[ht] [-d dbase] <event-id>
```

Write a summary line for one event.  
  
    -h        : print usage info  
    -t        : write column header  
    -d dbase  : alias to use for db connection (defaults to "archdbe")  
  
example: ./catone -d k2db -t 7395710  
  
  

## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues