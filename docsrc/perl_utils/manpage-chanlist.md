# chanlist {#chanlist}  
  
Dump a list of channels defined by a "named channel list".  
  

## PAGE LAST UPDATED ON

2021-06-09  
  

## NAME  

chanlist.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Dump a list of channels defined by a "named channel list".  
(Used 'Applications' and 'AppChannels' tables)    
  

## HOW TO RUN  
  
```
usage: ./chanlist -[h] [-d dbase] <list-name> 
     
Dump a list of channels defined by a "named channel list".
(Used 'Applications' and 'AppChannels' tables)

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-l        : dump list names with a count of channels in each list.
-a        : show Active stations only

example: ./chanlist -d k2db  RCG-V0
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues