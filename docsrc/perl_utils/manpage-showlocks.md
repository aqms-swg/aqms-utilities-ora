# showlocks {#showlocks}

Show events locked by Jiggle   

## PAGE LAST UPDATED ON

2021-06-09  

## NAME

showlocks.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Show current event locks in the JASIEVENTLOCK table.

## HOW TO RUN

```
usage: ./showlocks [-d dbase] 
     
Show current event locks in the JASIEVENTLOCK table.

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

example: ./showlocks -d mydb
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


