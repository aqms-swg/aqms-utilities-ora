# cubeTowns {#cubeTowns}  
  
Print the list of towns in the db(GazetteerPt table) in CUBE file format.     
  

## PAGE LAST UPDATED ON

2021-06-09  
  

## NAME  

cubeTowns.sh     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Print the list of towns in the db(GazetteerPt table) in CUBE file format.  
  

## HOW TO RUN  
  
```
usage: ./cubeTowns [-d dbase]
     
Print the list of towns in the db(GazetteerPt table) in CUBE file format.

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

example: ./cubeTowns -d k2db 
-118.943 35.005 1,,,,, Wheeler Ridge, CA
...

```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues