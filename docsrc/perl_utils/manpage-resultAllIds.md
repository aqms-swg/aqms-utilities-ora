# resultAllIds {#resultAllIds}

Sets the RESULT value of all PCS_STATE table rows matching the input state description.      

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

resultAllIds.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Sets the RESULT value of all PCS_STATE table rows matching the input state description. Can be used to force bulk transitions if the input result > 0.  

## HOW TO RUN

```
usage: ./resultAllIds [-h] [-d dbase] <group> <table> <state> <result>

Sets the RESULT value of all PCS_STATE table rows matching the input state
description. Can be used to force bulk transitions if the input result > 0. 

-h        : print this usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

example: ./resultAllIds -d k2db 992875 TPP TPP INTERIM 1
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


