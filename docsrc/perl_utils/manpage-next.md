# next {#next}

Get the next event in this state.      

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

next.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Print the next event evid to be processed for input state description.  
  
The evid is parsed from the oldest row in the PCS_STATE table that matches the input description. No evid is returned if the same evid has an active post (result=0) for another state description at higher rank than the input state description (i.e. it is rank blocked).  

## HOW TO RUN

```
usage: ./next [-d dbase] [-s secs] <group> <table> <state> 
     
Print the next event evid to be processed for input state description.

The evid is parsed from the oldest row in the PCS_STATE table that matches
the input description. No evid is returned if the same evid has an active
post (result=0) for another state description at higher rank than the input
state description (i.e. it is rank blocked).

-h        : print this usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

-s secs   : print an evid only if its posting's age is >= secs

example: ./next -d k2db -s 30 EventStream archdb MakeGif 
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


