# amPrimaryDb {#amPrimaryDb}

Returns true if the connected database's role is set primary in RT_ROLE table.  

## PAGE LAST UPDATED ON

2021-06-09  

## NAME

amPrimaryDb.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Returns true if the connected database's role is set primary in RT_ROLE table.  
  
NOTE: Alarm action scripts are executed when a database is primary, otherwise, the action is overruled.   


## HOW TO RUN

```
usage: ./amPrimaryDb [-d dbase] 

Returns true if the connected database's role is set primary in RT_ROLE table.

NOTE: Alarm action scripts are executed when a database is primary,
        otherwise, the action is overruled. 
    
    -h        : print usage info
    -d dbase  : alias to use for db connection (defaults to "archdbe")

example: ./amPrimaryDb -d mydb
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


