# alarmCancel {#alarmCancel}

Cancel alarms for an event   

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

alarmCancel.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Cancel alarms for an event.  

## HOW TO RUN

```
usage: ./alarmCancel [-d dbasename]  <evid>
     
-h        : print usage info
-d dbase  : use this particular dbase (defaults to "archdbe")

example: ./alarmCancel -d k2db 8735346
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


