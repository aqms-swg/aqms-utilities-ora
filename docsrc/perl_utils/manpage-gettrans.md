# gettrans {#gettrans}

Show transition definitions.    

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

gettrans.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Print the PCS transition row data found in PCS_TRANSITION database table.  

## HOW TO RUN

```
usage: ./gettrans [-d dbase] [-q] [-s char] [<group>] [<table>] [<stateold>]
     
Print the PCS transition row data found in PCS_TRANSITION database table.

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-q        : do not print title header
-s char   : character separating for old and new state descriptions
            defaults to '=', e.g instead use ':', ',', '+'

example: ./gettrans -d mydb EventStream
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


