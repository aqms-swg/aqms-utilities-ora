# epoch {#epoch}

Show system time in epoch seconds 

## PAGE LAST UPDATED ON

2021-06-09  

## NAME

epoch.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Show system time in epoch seconds 

## HOW TO RUN

```
usage: ./epoch

Prints: UNIX Epoch = <now_secs> (date-utc) (date-local)
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


