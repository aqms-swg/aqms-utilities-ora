# adminMail {#adminMail}

Send a message about a runtime code/cfg change on this host to $TPP_ADMIN_MAIL addresses.  

## PAGE LAST UPDATED ON

2021-06-09  

## NAME

adminMail.sh  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Send a message about a runtime code/cfg change on this host to $TPP_ADMIN_MAIL addresses.  
Prompts for message input, CTRL-D ends message; if $TPP_ADMIN_MAIL is undefined, prompts for list of email recipients.  
  

## HOW TO RUN

```
usage: ./adminMail

Send a message about a runtime code/cfg change on this host to $TPP_ADMIN_MAIL addresses.
Prompts for message input, CTRL-D ends message; if $TPP_ADMIN_MAIL is undefined,
prompts for list of email recipients.
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


