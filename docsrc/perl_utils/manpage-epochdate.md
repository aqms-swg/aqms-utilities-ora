# epochdate {#epochdate}

Prints: Epoch (unix, leap)=(\<unix-epochsecs\>, \<leap-epochsecs\>) (date-utc = date-local)
ø
## PAGE LAST UPDATED ON

2021-06-09  

## NAME

epochdate.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Prints: Epoch (unix, leap)=(\<unix-epochsecs\>, \<leap-epochsecs\>) (date-utc = date-local).  
  
Time defaults to "now" epoch unix seconds (non-leap), if no input option times are specified.  

## HOW TO RUN

```
usage: ./epochdate [-l leap_true_epochseconds] [-n unix_nominal_epochseconds ] [-s|-S datestring]

Prints: Epoch (unix, leap)=(<unix-epochsecs>, <leap-epochsecs>) (date-utc = date-local)

Time defaults to "now" epoch unix seconds (non-leap), if no input option times are specified.

-l time : time in leap seconds (TRUE) time.
-n time : time in nominal seconds epoch (UNIX) time 

NOTE need Perl 5.12 or higher for string epoch before '1970-01-01'
-s time : time as string yyyy-mm-dd,HH:MM:SS (UTC)
-S time : time as string yyyy-mm-dd,HH:MM:SS (local time zone)

example:  epochdate -s 2014-08-07,23:14:00
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


