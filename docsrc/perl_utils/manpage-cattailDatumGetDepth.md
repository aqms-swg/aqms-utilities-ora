# cattailDatumGetDepth {#cattailDatumGetDepth}  
  
Dump events since \<hours-back\>
  

## PAGE LAST UPDATED ON

2021-06-09  
  

## NAME  

cattailDatumGetDepth.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Writes a summary list for  events in the past 'N' hours (default = 24), or specified date range.    
  

## HOW TO RUN  
  
```
usage: ./cattailDatumGetDepth.pl [option-list] [[#interval] or [startDate [endDate]]]
    options: [-h] [-c] [-d dbase] [-a or [ [-D] [-B0|1]]] [-iI]
             [-f file] [-e types] [-m mag -M mag]
             [-s esrc -S osrc ] [-rtw] [-u d|w|m ]

    Writes a summary list for  events in the past 'N' hours (default = 24), or specified date range
    Column values are from the db tables Event e, Origin o, Netmag n outer joined on prefor and prefmag: 
    e.EVID,n.MAGNITUDE,n.MAGTYPE,o.DATETIME,o.LAT,o.LON,o.DEPTH,o.WRMS,o.GAP,e.ETYPE,o.RFLAG,o.SUBSOURCE 
    followed by counts of of waveforms, strong-motion amps, and non-zero weight magnitude amps or codas

    if event.selectflag=0,a "<-sel=0" label is appended to end of row (deleted or shadow system event).
    If origin.bogusflag=1 a "<-bogus" label is appended to end of event row.

    Time interval units defaults to hours unless -u option is used for days, weeks, or months units. 

    Defaults to last 24 hrs if no input arguments or options. Input arguments must be hours back,
    or a starting date, and optionally, an ending date, if no ending date is specified it defaults
    to the current GMT time.

    Date are of the form yyyy-mm-dd ; you can use either '-' or '/' as the year, month and day separator.

    Date arguments can optionally include a time of the form: HH:MM or HH:MM:SS
    Quote string if using a white space separator between them: '2013-12-01 13:00',
    otherwise, use character set [,-_] to separate a date from its optional time field:
    2013-03-13,13:00 2013-03-13-13:00 or 2013-03-13:13:00 or  2013/03/13_13:00:00 
     
     -h        : print this usage info

     -d dbase  : alias to use for db connection (defaults to "archdbe")

     -c        : print only the total count of events.

     -t        : print a column header title at beginning of list
     -T        : include a legend for the title column acronyms

     -r        : reverse time order sort (most recent at top)

     -w        : add info on nearest town and its distance from event

     -2        : print 2-decimal magnitude and origin time seconds values

     Query list filter options:

     -f file   : file with one or more lines whose 1st column is an event evid.
     -F file   : file with one or more lines whose 1st column is an event evid.
                 but ignores all other filter criteria including time bounds
                 (e.g. cattail -F idFile.txt)

     -i evid   : event specified by id if its data matches filter criteria
     -I evid   : event specified by id ignoring all other filter criteria
                 including time bounds (e.g. cattail -I 12345)

     -a        : all events (includes deleted and shadow system duplicates)
                 note: not compatible together with -D and/or -B option

     -D        : only deleted or shadow system duplicate events

     -B flag   : =1 show bogus origins only, =0 show non-bogus origins

     -e types  : only events matching specified types, where types
                 is a concatenation of Event.etype codes delimited by
                 ',' or ':', for example: 'eq:ex:qb' or 'eq,ex,qb'

     -L algo   : only events whose magnitude algorithmn matches tag
                 e.g. CISNml2 or trimag_slow

     -m mag    : only events whose magnitude >= mag
     -M mag    : only events whose magnitude <= mag

     -s subsrc : only events with like matching event subsource
                 string can contain db sql wildcard characters '%' or '_'

     -S subsrc : only events with like matching origin subsource
                 string can contain db sqlwildcard characters '%' or '_'

     -o        : prints column numbers below title
     -u d|w|m  : time interval units is days, weeks, or months
     -x        : for debugging only,  prints database sql query  string

    example: ./cattailDatumGetDepth.pl -wti 15253617 -d mydb  3
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues