# hypophases {#hypophases}  
  
Dump event phases in Hypoinverse format   
  

## PAGE LAST UPDATED ON

2021-05-28  
  

## NAME  

hypophases.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Prints a hypoinverse ARC listing of the phase arrival data associated with the prefor of the event, unless using -o option, in which case the  input id is origin orid.    
  

## HOW TO RUN  
  
```
usage: ./hypophases [-h] [-d dbase] [-o] <id>
```

Prints a hypoinverse ARC listing of the phase arrival data associated with the prefor of the event, unless using -o option, in which case the  input id is origin orid.  
  

    -h        : print this usage info  
    -d dbase  : alias to use for db connection (defaults to archdbe)  
    -o        : input id argument is origin orid, not event evid.  
  
example: ./hypophases -d k2db 9277430  
  
  

## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues