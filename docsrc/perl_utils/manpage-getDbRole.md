# getDbRole {#getDbRole}  
  
List row column values found in database RT_ROLE table. Default is most current role (modication time) for archdbe.      

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

getDbRole.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
List row column values found in database RT_ROLE table. Default is most current role (modication time) for archdbe.   

## HOW TO RUN  
  
```
usage: ./getDbRole  [-h] [-A] [-d <dbase>] [-q]

List row column values found in database RT_ROLE table.
Default is most current role (modication time) for archdbe. 

-h       : this usage
-d dbase : alias to use for db connection (default to archdbe)
-A       : list entire history sorted in mod time order
-r       : reverse sort, modification_time order descending
-q       : do not print table column header title 

example: ./getDbRole -d mydb -A
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues