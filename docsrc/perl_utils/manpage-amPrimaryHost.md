# amPrimaryHost {#amPrimaryHost}

Returns current role value from AQMS_HOST_ROLE table for hostname.  

## PAGE LAST UPDATED ON

2021-06-09  

## NAME

amPrimaryHost.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Returns current role value from AQMS_HOST_ROLE table for hostname.  
The hostname default is the localhost machine name (i.e. no domain)  
  
PCS state driven applications use this role value to determine how they should process an event when the application has a configuration described in a row in the APP_HOST_ROLE table.  


## HOW TO RUN

```
usage: ./amPrimaryHost [-d dbase] [hostname]

Returns current role value from AQMS_HOST_ROLE table for hostname.
The hostname default is the localhost machine name (i.e. no domain)

PCS state driven applications use this role value to determine how
they should process an event when the application has a configuration
described in a row in the APP_HOST_ROLE table.

Returns "true" or "false" when table row value exists, else "unknown". 

    -h        : print usage info
    -d dbase  : alias to use for db connection (defaults to "archdbe")

example: ./amPrimaryHost -d mydb amosite
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


