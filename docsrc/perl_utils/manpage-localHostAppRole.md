# localHostAppRole {#localHostAppRole}  
  
Prints the role flag mapped to the localhost and the input application name tag and PCS state configuration.   

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

localHostAppRole.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Prints the role flag mapped to the localhost and the input application name tag and PCS state configuration.   
  
Queries the database APP_HOST_ROLE table to lookup the localhost's mapping to a configured application name tag and state.  

## HOW TO RUN  
  
```
usage: ./localHostAppRole [-d dbase] <appName> <pcsState>
     
Prints the role flag mapped to the localhost and the input 
application name tag and PCS state configuration. 

Queries the database APP_HOST_ROLE table to lookup the localhost's
mapping to a configured application name tag and state.

Mode flag values are:
P,A,S or U (i.e. primary any, shadow, or unknown role).

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

example: ./localHostAppRole -d mydb rcg_cont rcg_rt
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues