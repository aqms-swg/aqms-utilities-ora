# eventMag {#eventMag}  
  
Output event mag.   
  

## PAGE LAST UPDATED ON

2021-05-28  
  

## NAME  

eventMag.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Print the magnitude value of an event. Prints "   " if no magnitude, or the input evid is not found in database.    
  

## HOW TO RUN  
  
```
usage: ./eventMag [-d dbase] [-i] [-t] <evid>  
```
  
Print the magnitude value of an event.
Prints "   " if no magnitude, or the input evid is not found in database.

Use command switch options to print magnitude value formatted along with
the event evid, origin time, auth, and/or version values from the database.
    
    -h        : print this usage info
    -d dbase  : alias to use for db connection (defaults to "archdbe")

    -a        : append the event auth after evid for the event info string  
    -v        : append the event auth and its version after evid for the event info string  
    -i        : prepend an event info string before the magnitude value   
    -I        : append an event info string after the magnitude value   
    -l        : format origin as local time and date  
    -L        : format origin as date and local time  
    -o        : origin date time appended, default is UTC with date first  
    -O        : origin date time prepended, default is UTC with date first  
    -p        : magnitude in parentheses  
    -t        : prepend the magnitude type before the magnitude value  
    -T        : append the magnitude type after the magnitude value  
    -u        : format origin time as UTC time and date  
    -U        : format origin time as UTC date and time  

example: ./eventMag -d mydb  826653  
2.2  
example: ./eventMag -d mydb -t 826653  
Ml 2.2  
example: ./eventMag -d mydb -T 826653  
2.2 Ml  
example: ./eventMag -d mydb -tI 826653  
Ml 2.2 826653  
example: ./eventMag -d mydb -TIa 826653  
2.2 Ml 826653 CI  
example: ./eventMag -d mydb -tIv 826653  
Ml 2.2 826653 CI v1  

  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues