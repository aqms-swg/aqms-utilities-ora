# makeChannelRequest {#makeChannelRequest}  
  
Create a request_card table row for the input event id and net.sta.seedchan.location having specified window starting datetime and duration.  

## PAGE LAST UPDATED ON

2021-06-23  
  

## NAME  

makeChannelRequest.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Create a request_card table row for the input event id and net.sta.seedchan.location having specified window starting datetime and duration. Times specified must be integer seconds. Ondate format is \<yyyy-mm-dd,HH:MM:SS\> or \<yyyy/mm/dd,HH:MM:SS\>. A comma or a colon can separate the date and time string components. Location code defaults to '  ' when not specified, else use '--' string to specify blank code.   
  

## HOW TO RUN  
  
```
usage: ./makeChannelRequest.pl [-h] [-d dbase] [-r] <evid> <n.s.c.l> <ondate> <duration>

Create a request_card table row for the input event id and net.sta.seedchan.location having
specified window starting datetime and duration. Times specified must be integer seconds.
Ondate format is <yyyy-mm-dd,HH:MM:SS> or <yyyy/mm/dd,HH:MM:SS>. A comma or a colon
can separate the date and time string components. Location code defaults to '  ' when not
specified, else use '--' string to specify blank code.


-h        : print usage info

-d dbase  : alias to use for db connection (defaults to "archdbe")

-r        : delete any pre-existing request_card table row matching input evid and n.s.c.l
            before doing insert, otherwise if row exists, ignore input timespan and row
            reset retry count to 0.

-s src    : request_card subsource, default = makeCR 

example:
./makeChannelRequest.pl -r 123456 CI.RVR.HHZ.-- 2014-08-07,23:15:15 120
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues