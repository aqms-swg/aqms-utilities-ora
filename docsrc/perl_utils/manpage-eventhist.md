# eventhist {#eventhist}  
  
Show the event's history 
  
  
## PAGE LAST UPDATED ON

2021-05-28  
  

## NAME  

eventhist.pl   


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE  

Show origin,magnitude change history info for an event.  
  
  
## HOW TO RUN   
  
```
usage: ./eventhist [-q] [-d dbase]  <evid>
```
     
Show origin,magnitude change history info for an event. List is sorted in magnitude lddate descending order. (The most recent update first, oldest last).  
  
Event prefmag is flagged by "<+++" after its lddate, otherwise if its the preferred of a magtype its flagged by  "+" after.
  
When the orid is event prefor and the magid is the event prefmag, the both the waveform count and event version numbers are printed.  
  
NOTE: database does not contain sufficient data to completely recover the evolution of an event, like event version number.  
  
    -h        : print usage info  
    -d dbase  : alias to use for db connection (defaults to "archdbe")  
    -q        : do not print title header  
  
example: ./eventhist -d mydb 8735346  

  

## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues