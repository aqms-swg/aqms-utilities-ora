# cubeMessage {#cubeMessage}

Return a Cubic message for this evid.    

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

cubeMessage.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Return the CUBE pager format message for an event.  
If "version" is not specified Event.Version will be used. CUBIC format only supports versions 0 through 61. The event must exist in the dbase specified.  

## HOW TO RUN

```
 usage: ./cubeMessage [-dehvCDEHLP][-d dbase] <evid> [<comment>]
     
    Return the CUBE pager format message for an event.
    If "version" is not specified Event.Version will be used.
    CUBIC format only supports versions 0 through 61.
    The event must exist in the dbase specified.
    
    -d dbase  : alias to use for db connection (defaults to "archdbe")
    -e        : dump an explanation of the CUBE "E" format
    -h        : print usage info
    -v #      : set version number (defaults to Event.Version)

    Format options:
    -C        : "Long" email/web format CANCEL
    -D        : CUBIC Delete format (as sent via QDDS)
    -E        : CUBIC Event format (as sent via QDDS) [DEFAULT]
    -H        : Probability report as HTML 
    -L        : "Long" email/web format
    -P        : Probability report as text

   example: ./cubeMessage -D -d k2db 8735346 "Deleted after human review"
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


