# newtrig {#newtrig}  
  
The newtrig.pl perl script is a wrapper around the newtrig class in the jiggle.jar bundle. It takes two arguments, -s for start time and -l for length of trigger in seconds. It will create an event in the database and build request cards for waveforms to be archived. The trigger in Jiggle will show up as a NewTrig source. 


## PAGE LAST UPDATED ON

2021-06-23  
  

## NAME  

newtrig.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Create a new subnet trigger, or with option -i use an existing evid, and use the properties file configured channel time window model to create a set of new channel waveform requests to be written to db.  
  
  
## HOW TO RUN  
  
To run the wrapper on the unix command line requires escaping the start time. See an example below:   
  

```
newtrig.pl -s "\" 2010-11-16 10:16:00.00 "\" -l 360
```
    
**USAGE**  
  

```
Create a new subnet trigger, or with option -i use an existing evid, and use
  the properties file configured channel time window model to create a set of 
  new channel waveform requests to be written to db.

  Default properties file: ../cfg/newtrig.props.
  Use the 'model' property to declare which channel time window model to use:
  model=org.trinet.jasi.PowerLawTimeWindowModel
  model=org.trinet.jasi.NewTriggerChannelTimeWindowModel

  Runs a java application: org.trinet.apps.NewTrig, found in jiggle.jar.

openjdk version "1.8.0_275"
OpenJDK Runtime Environment (build 1.8.0_275-b01)
OpenJDK 64-Bit Server VM (build 25.275-b01, mixed mode)


Usage:  -[options] [ <lat> <lon> [z] [magValue] [bogusFlag] [selectFlag] [etype] [#locPh] [#magSta] [locRMS] [magRMS] ]

options:
 -h              this help info
 -v              verbose output
 -P <props-file> file with the waveform request generator properties; default: newtrig.props
 -i <evid>       associate requests with an existing event.evid (does not create new event)
 -X              do not write new event or its requests to dbase (test mode)
 --- Optional db connection values (override property file values) ---
 -U <URL>        override URL of dbase host property
 -d <name>       override dbaseName property
 -u <username>   override dbaseUser property
 -p <password>   override dbasePasswd property
 --- Options for specifying the timespan of request cards ---
 -s <start-time> a quoted "date time" string of format: "yyyy-MM-dd HH:mm:ss.SSS"
                 or to avoid quotes, put a ',' between the date and time fields
 -e <end-time>   date-time string with same format as the start-time
 -l <seconds>    length of waveform request window from start-time

  Using the NewTriggerChannelTimeWindowModel, you specify either the starting and ending times
  or the starting time and a length seconds, if you enter all three, the length is ignored.
  When no -e or -l option, is specified, the duration defaults to the model's 'maxWindowSize'.
  Using the -i option without the -s start-time option sets the model's window start to the
  datetime of the event's prefor minus the model's preEventSize seconds.

  Alternatively, you can use the PowerLawTimeWindowModel with the -i <id> option, or by instead
  specifying the -s start-time option followed by an argument list (not options) consisting of 
  the new event lat and lon, and optionally its depth, magnitude, bogusFlag, selectFlag, and etype,
  the respective defaults are: 0. deg, 0. deg, 0. km, 2.0 Mh, 1, 1 and 'st'.

  When using the -i switch option, the existing db values are used instead of trailing arguments.

  For either model, the input properties file should specify the request generator's properties;
  properties 'auth' and 'subsource' specify the new event's auth and subsource values, respectively.
```  
    
## DEPENDENCIES  
  
jiggle.jar    
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues
  

