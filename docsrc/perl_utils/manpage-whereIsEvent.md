# whereIsEvent {#whereIsEvent}  
  
Print the locale of the event. Uses database WHERES package.  

## PAGE LAST UPDATED ON

2021-06-15  
  

## NAME  

whereIsEvent.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Print the locale of the event. Uses database WHERES package.    

## HOW TO RUN  
  
```
usage: ./whereIsEvent [-d database]  <evid> 

Print the locale of the event. Uses database WHERES package.

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-t        : report closest town name
-l        : report closest town and the distance and azimuth
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues