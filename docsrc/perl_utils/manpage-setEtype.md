# setEtype {#setEtype}  
  
Set the etype of an event in the dbase.  

## PAGE LAST UPDATED ON

2021-06-14  
  

## NAME  

setEtype.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Set the etype of an event in the dbase.    
  

## HOW TO RUN  
  
```
Set the etype of an event in the dbase
        
usage: ./setEtype [-d dbasename]  <evid> <etype>
    
    -h        : print usage info
    -d dbase  : use this particular dbase (defaults to "archdbe")

example: ./setEtype -d k2db 8735346 qb
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues