# acceptEvent {#acceptEvent}  
  
Mark event as reviewed.  
  

###### PAGE LAST UPDATED ON

2021-05-28  
  

#### NAME  

acceptEvent.pl     


#### VERSION and STATUS

status: ACTIVE  
  

#### PURPOSE
  
Accept an event in the dbase (mark as human reviewed).    
  

#### HOW TO RUN  
  
```        
usage: ./acceptEvent [-d dbasename]  <evid>
```
  
Accept an event in the dbase (mark as human reviewed)   
     
     -h        : print usage info
     -d dbase  : use this particular dbase (defaults to "archdbe")
  

example: ./acceptEvent -d k2db 8735346
  


#### BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues