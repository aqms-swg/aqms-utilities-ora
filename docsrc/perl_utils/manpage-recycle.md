# recycle {#recycle}  
  
Change an existing state posting description to a new state posting description in database PCS_STATE table.    

## PAGE LAST UPDATED ON

2021-06-14  
  

## NAME  

recycle.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Change an existing state posting description to a new state posting description in database PCS_STATE table. 
  

## HOW TO RUN  
  
```
usage: ./recycle [-d dbase] [-s char] <oldGroup> <oldSource> <oldState> <oldResult> [<newGroup> <newSource> <newState> <newRank> [<newResult>]]
     
Change an existing state posting description to a new state posting description in 
database PCS_STATE table.

Requires at least 4 command line arguments. When the additional 8 or 9 arguments are absent
the rows matching the old state description will be deleted from the PCS_STATE table.
If a "newResult" is not specified it will default to null (i.e. 0).

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

-i id     : match input id (evid), single row
-s char   : optional character delimiter between the old and new state description groups
            if not specified, default is to allow '=', ':' or ' '

example: ./recycle -d mydb2 TPP TPP INTERIM 1  EventStream mydb2 MakeGif 95
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues