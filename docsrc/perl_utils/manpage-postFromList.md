# postFromList {#postFromList}

Post events listed in a file to a state.   

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

postFromList.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Posts every event evid found in the input file to the input state description.  

## HOW TO RUN

```
usage: ./postFromList [-d dbase] <id-file> <group> <table> <state> <rank> [result]
     
Posts every event evid found in the input file to the input state description.

The evid must be the first white-space delimited token of each line in the
input file. Lines not begining with a number > 0 will be skipped.

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

-v        : verbose - print a line for each event posted
-s secs   : post each id at interval secs (sleep for specifed seconds)

example: ./postFromList -d mydb eids.txt EventStream archdb MakeGif 100
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


