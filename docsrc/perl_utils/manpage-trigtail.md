# trigtail {#trigtail}  
  
Dump triggers since \<hours-back\>  
  
  
## PAGE LAST UPDATED ON

2021-05-28  
  

## NAME  

trigtail.pl  


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE  

Print a listing summarizing the subnet trigger events (etype=st) that occurred in the past '#hrs-back' hours.  
  
  
## HOW TO RUN   
  
```
usage: ./trigtail [-d dbase] [-h] [-aqr] <#hrs-back> (default = 24)
```
     
Print a listing summarizing the subnet trigger events (etype=st) that occurred in the past '#hrs-back' hours.

    -h        : print this usage info
    -d dbase  : alias to use for db connection (defaults to archdbe)

    -a        : include all events including deleted and duplicates
    -q        : do not print column header title
    -r        : reverse sort order (newest at top)
  
  
example: ./trigtail -d mydb 24

  

## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues