# archive_db_total {#archive_db_total}

Report of the totals of events, arrivals, and waveforms for time period.

## PAGE LAST UPDATED ON

2021-06-09  

## NAME

archive_db_total.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Write a report of the totals of events, arrivals, and waveforms for time period inclusive specified.  
The \<start_date\> and \<end_date\> are in the format  YYYY/MM/DD or "YYYY/MM/DD hh:mm:ss".  


## HOW TO RUN

```
usage: ./archive_db_total  [-d dbase] <start_date> <end_date>

Write a report of the totals of events, arrivals, and waveforms for time period inclusive specified.
The <start_date> and <end_date> are in the format  YYYY/MM/DD or "YYYY/MM/DD hh:mm:ss".

-d dbase  : alias to use for db connection (defaults to "archdbe")

example: ./archive_db_total -d mydb 2013/10/1 "2013/10/31 12:34:00"
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


