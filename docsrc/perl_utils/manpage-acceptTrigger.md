# acceptTrigger {#acceptTrigger}  
  
Accept a trigger in the dbase.    
  

## PAGE LAST UPDATED ON

2021-05-28  
  

## NAME  

acceptTrigger.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Accept a Trigger in the dbase (mark as human reviewed).      
  

## HOW TO RUN  
  
``` 
usage: ./acceptTrigger [-d dbasename]  <evid>

   
Accept a Trigger in the dbase (mark as human reviewed) 
     
     -h        : print usage info
     -d dbase  : use this particular dbase (defaults to "$masterdb")
  

example: $0 -d k2db 8735346
```  

## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues