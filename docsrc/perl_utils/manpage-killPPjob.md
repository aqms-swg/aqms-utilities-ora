# killPPjob {#killPPjob}  
  
Kill process whose pid matched first input arg pid. Sends email to $TPP_ADMIN_MAIL list if defined else prompts user for email address list to be notified kill action

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

killPPjob.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Kill process whose pid matched first input arg pid. Sends email to $TPP_ADMIN_MAIL list if defined else prompts user for email address list to be notified kill action    

## HOW TO RUN  
  
```
usage: ./killPPjob <pid>

Kill process whose pid matched first input arg pid.
Sends email to $TPP_ADMIN_MAIL list if defined else prompts
user for email address list to be notified kill action
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues