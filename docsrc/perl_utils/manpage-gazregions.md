# gazregions {#gazregions}  
  
   
List name and coordinates of regions found in gazetteer_region table sorted in name order.   

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

gazregions.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
List name and coordinates of regions found in gazetteer_region table sorted in name order.  
  
If optional input argument lat,lon coordinates are specified the listed region(s) must contain them.  

## HOW TO RUN  
  
```
usage: ./gazregions [-h] [-d dbase] [-n names] [-N] [-q]  [lat lon]
     
List name and coordinates of regions found in gazetteer_region table
sorted in name order.

If optional input argument lat,lon coordinates are specified the listed
region(s) must contain them.

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-N        : list names but not coordinates
-n names  : list name and coordinates matching list of region names 
            delimited by [,: ]
-q        : do not print a column header title

example: ./gazregions -d mydb
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues