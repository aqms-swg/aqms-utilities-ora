# ampdumpo {#ampdumpo}  
  
Dump amps associated with origin (AssocAmO).    
  

#### PAGE LAST UPDATED ON

2021-05-28  
  

#### NAME  

ampdumpo.pl     


#### VERSION and STATUS

status: ACTIVE  
  

#### PURPOSE
  
Print amp data associated with the preferred origin of event in (AssocAmO). Or input origin id with -o option.  
  

#### HOW TO RUN  
  
```
usage: ./ampdumpo [-h] [-d dbase] [-a] [-n cnt] [-q] <id>
```
  
Print amp data associated with the preferred origin of event in (AssocAmO). Or input origin id with -o option.  
  
Amps are ordered by distance,net,sta,location,seedchan,amptype. All associated amps are listed unless optional command switches are used to restrict list by amptype and/or subsource.  
  

    -h         : print usage info  
    -d dbase   : alias to use for db connection (defaults to "archdbe")  
    -a amptype : amptype to match ( default is all types)  
    -c         : print total count only, no data rows  
    -n cnt     : max rows to print  
    -o         : input arg is orid not evid  
    -s subsrc  : assocamo subsource  
    -q         : do not print column header title  
      

example: ./ampdumpo -d mydb 7395710    


#### BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues