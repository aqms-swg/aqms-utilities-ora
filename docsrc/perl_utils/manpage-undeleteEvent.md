# undeleteEvent {#undeleteEvent}

Undelete event or trigger   

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

undeleteEvent.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Undelete (virtually) an event in the database. By default script does NOT post evid for PCS processing; option -p can be used if undeleting a shadow system event and you also want autoposter type state transitions in the connnected db, assuming they are configured for the post signatures described below for option -p.  

## HOW TO RUN

```
usage: ./undeleteEvent [-d dbasename] [-p]  <evid>

By default script does NOT post evid for PCS processing; option -p can be
used if undeleting a shadow system event and you also want autoposter type
state transitions in the connnected db, assuming they are configured for
the post signatures described below for option -p.

    -h        : print usage info
    -d dbase  : use this particular db alias (defaults to "archdbe")

    -p        : force autoposter transitions
                if event etype is not 'st' :
                post <evid> EventStream <dbase> NewEvent 100 1
                otherwise, if event etype is 'st' :
                post <evid> EventStream <dbase> NewTrigger 100 1

example: ./undeleteEvent -d mydb 8735346
```

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


