# postEvidSince {#postEvidSince}

Post all non-bogus events within specified date range.  

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

postEvidSince.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Posts all selected non-bogus events with origins in the past 'N' days or within a specified date range to a specified state description.  
  
Required input arguments are days back, or a starting and ending date string pair of format yyyy/mm/dd or yyyy-mm-dd, followed by a required 'state' and optionally a rank (DEFAULT=100), source (DEFAULT=archdbe), and/or group (DEFAULT='EventStream') value.

## HOW TO RUN

```
usage: ./postEvidSince [option-list] < [days] or [startDate endDate]> <state> [rank] [source] [group] 
    options: [-h] [-d dbase] [-e types] [-m mag -M mag] [-s esrc -S osrc ] [-p] [-r]

Posts all selected non-bogus events with origins in the past 'N' days or
within a specified date range to a specified state description. 

Required input arguments are days back, or a starting and ending date string pair of format
yyyy/mm/dd or yyyy-mm-dd, followed by a required 'state' and optionally a rank (DEFAULT=100),
source (DEFAULT=archdbe), and/or group (DEFAULT='EventStream') value.
    
    -h        : print this usage info

    -d dbase  : alias to use for db connection (defaults to "archdbe")

    -r        : reverse time order sort (most recent at top)

    Query list filter options:

    -e types  : only events matching specified types, where types
                is a concatenation of Event.etype codes delimited by
                ',' or ':', for example: 'eq:qb:st' or 'eq,qb,st'

    -gG types : only events matching specified gtypes, where types
                is a concatenation of Origin.gtype codes delimited by
                ',' or ':', for example: 'l:r:t' or 'l,r,t'.
                Use -G to include null values.

    -l        : use event.lddate instead of origin datetime for start and end time range.

    -m mag    : only events whose magnitude >= mag
    -M mag    : only events whose magnitude <= mag

    -s subsrc : only events with like matching event subsource
                string can contain db sql wildcard characters '%' or '_'

    -S subsrc : only events with like matching origin subsource
                string can contain db sqlwildcard characters '%' or '_'

    -p flags  : only events whose review flag matches specified flags, 
                a concatenation of Origin.rflag codes delimited by
                ',' or ':', for example: 'I:F' or 'A,H'

    -x        : for debugging only,  prints database sql query  string

example: ./postEvidSince  -d mydb 3 MakeGif

```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


