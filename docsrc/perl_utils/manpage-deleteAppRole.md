# deleteAppRole {#deleteAppRole}  
  
Delete matching APP_HOST_ROLE table rows   
  

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

deleteAppRole.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Delete matching APP_HOST_ROLE table rows. Must have at least 1 input argument, the host name string.  

## HOW TO RUN  
  
```
usage: ./deleteAppRole [-h] [-d dbase] [-a] <host> [<appname>] [<state>]

Delete matching APP_HOST_ROLE table rows. Must have at least 1
input argument, the host name string.  The input string arguments
can contain db wildcards ( Oracle: % or _ ).

  -h        : print this usage info
  -d dbase  : alias to use for db connection (defaults to "archdbe")
  -a        : delete a matching row only if it is currently active,
              i.e. offdate>sysdate; otherwise, by default, dates are
              ignored.

example:   ./deleteAppRole -d archdb2 mypp2 rcg_cont rtg_rt 

          For specified host and appname remove all matching state rows:
            ./deleteAppRole -d archdb2 mypp2 rcg_cont

          For specified host, remove all rows:
            ./deleteAppRole -d archdb2 mypp2
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues