# ampgenpp {#man-ampgenpp}  
  
Ampgen in post processing.  
  
## PAGE LAST UPDATED ON  
  
2022-02-01       
  
## NAME  
  
ampgenpp  
  
## VERSION and STATUS
  
version: NA  
statis: ACTIVE  
  
## PURPOSE  
  
*ampgenpp* essentially does the same task as the real-time code AmpGen. It will :  

*  Select the set of channels that should be scanned for peak amplitudes  
*  Retrieve waveforms for each for these channels from the Waveservers  
*  Filter the waveforms to PGD, PGV, PGA, SP30, SP01, and SP03  
*  Scan each waveform for the peak in the window  
*  Write the peak for each channel to the database  


The resulting ground motions are used primarily by ShakeMap.  
  
**NOTE: an event must have a good location for this program to create reasonable time windows to scan.**    
  
##### Differences from the RT version  
  
Unlike the RT program *[AmpGen](https://aqms-swg.gitlab.io/aqms-docs/man-ampgen.html)*,  *ampgenpp* does not use the 5-second peak amps stored in the ADA (%Amplitude Data Area) by [RAD](https://aqms-swg.gitlab.io/aqms-docs/man-rad2.html). Rather it calculates peak ground motion parameters from scratch by scanning the timeseries. The results of ampgenpp have been demonstrated to be the same as those produced by the RT version of AmpGen. WARNING: Changing the runtime properties from the default values may produce results different from the RT version.

Waveforms are retrieved from the waveservers that are defined in the properties file.  
  

#### Channel Time Window Selection  
  
The channel list scanned for peak amps is defined using the “channelGroupName” property. The “name” must match an entry in the Program table that points to a list in the Config_Channel table. These lists are specified using Chanloader. The default list name used is \<AmpGen\>.  
  
Time windows are specified using the EnergyWindowModel class; a TimeSpan defining a window where you are likely to find seismic energy.  
  
The window is defined by:  
   
*  start = preP seconds before the P-wave arrival time as calculated by the TravelTime model.  
*  end = P-wave arrival time + S-P time + coda duration time.  
  
The window will never be less than the minWindow size (default = 10sec) or greater then minWindow (default = 120sec). This method uses the standard TraveTime model if none is set using setTravelTimeModel(). If setCodaModel() is not called this method uses a CodeModelMD model with the factors (0.87, 2.1, 0.0).  
  
#### The Technique  
  
ampgenpp applies a recursive integration filter to generate all ground motion types. It uses the same algorithm as the RT system code AmpGen. This was developed by Hiroo Kanamori and is described in “Continuous Monitoring of Ground-Motion Parameters”, BSSA, V89, pp. 311-316, Feb. 1999. In the real-time AmpGen program it is implemented using Hiroo Kanimori's FORTRAN subroutine recres6(). It only works for 20, 80, 100 and 200 sps data.  
  
In the *ampgenp*p implementation the displacement record is Butterworth filtered to remove long period signal. This is necessary to get the same result as we see from AmpGen.  
  
The ground motions created are WAS, PGD, PGV, PGA, SP03, SP10 & SP30.  


#### Implementation notes  
  
NEEDS TO BE UPDATED  
Tested on a 2.26Ghz Pentium. The CPU is about 20% busy. Most of the elapse time is in data retrieval. When reading timeseries from the database it takes about 2.8 sec per channel of which 2.6 sec is time to retreive timeseries from the database. This does not include the time to write results to the dbase.  
  
When getting timeseries from the disk Waveserver it runs at a rate of about 5-10 channels per second. I'm not sure why this varies but may be Waveserve load because the same event can show this variance when rerun. The Rapid wave pool should be faster but the single test I did indicates its about the same speed.  
  
A full event takes about 4 minutes to compute.  
  

## HOW TO RUN  
  
```
Usage: ampgenpp.pl [-OPTIONS [-MORE_OPTIONS]] [--] [PROGRAM_ARG1 ...]

The following single-character options are accepted:
	With arguments: -p
	Boolean (without arguments): -h

Options may be merged together.  -- stops processing of options.
Space is not required between options and their arguments.
  [Now continuing due to backward compatibility and excessive paranoia.
   See 'perldoc Getopt::Std' about $Getopt::Std::STANDARD_HELP_VERSION.]

Calculate ground motions for one event.

Runs the AmpGenPp application found in jiggle jar to create
a database ampset having amptypes: PGD, PGV, PGA, and SP.

Print output is redirected to file:
  /app/aqms/tpp/ampgen/logs/ampgenpp_<evid>.log

usage: ./ampgenpp.pl -[h] [-p property-file] <event-id>

  -h          : print this usage info
  -p filename : property file (default is /app/aqms/tpp/ampgen/cfg/ampgenpp.props)

  example: ./ampgenpp -p ../cfg/ampgenpp.props 7395710
```  
  
## CONFIGURATION FILE (aka Property file)  

The program requires a properties file(named *ampgen-generic.props*) that specifies details of the database, waveservers, and behavior. Database and waveserver arguments are identical to those found in other programs that use JASI database views (e.g. Jiggle, SnapGif). Arguments specific to ampgenpp described below. Format is:          
    
```
     <parameter name> <data type> <default value>
               <description>
```  
  
**waveformReadMode** *integer*  
Defines where waveforms come from : 0 = read from DataBase, 1 = read from Waveserver.   
  
**localNetCode** *string*  
The local two-character FDSN network code. This is written to the “auth” field of the resulting Amplitude rows.   
  
**sourceName** *string*  
The string to be written to the subsource column of the Amplitude row. It cannont exceed 8 characters. Default is “AmpGenPp”.   
  
**channelGroupName** *string*  
Defines the channels that will be scanned for strong motions. This is case sensitive. The list is specified via a Chanloader channel group name (as found in Program.Name attribute). Default is “AmpGen” which mean use the same channel set as the RT version.  
  
**channelCacheFilename** *string*    
An alternative to *channelGroupName*. Set the name of the file containing the list of channels that will be scanned for strong motions. This is useful when database connectivity is down or too slow.  

**waveServerConnectTimeout** *integer*  
Sets time out, in seconds, to connect to defined waveservers.  

**waveServerGroupList** *string*  
Defines waveserver group(s) for waveform retrieval. Also, defines the waveserver:port information. See sample configuration file for details.  

**currentWaveServerGroup** *string*  
The waveserver group to use.  

**minTimeWindow** *float*  
Minimum length in seconds to scan for peaks.  
  
**maxTimeWindow** *float*  
Maximum length in seconds to scan for peaks.  
  
**preEventTime** *float*  
Seconds before P-wave to include in the scanned window.  
  
**maxDistance** *float*  
The maximum distance in km out to which include stations. This may be needed to restrict the channel set to a size that will complete in a reasonable amount of time while still getting near-field values.  
  
**writeToDB** *boolean*  
True or false - write resulting Amplitudes to the database. Set “false” for testing. Default = “true”  
  
**minAcceleration** *float*  
Accelerations below this value (cm/sec^2) are marked “bn” (Below Noise).  
  
**maxVelocity** *float*  
Velocities above this value (cm/sec) are marked “os” (Off Scale).  
  
**writeToDB** *boolean*  
Set true to write amps to dbase (set false for testing and to check results).  
  
**verbose** *boolean*  
Set true for verbose log output.  
  
**debug** *boolean*  
Set true to produce debug output, false otherwise.  

Additionally, JASI or Jiggle properties can also be set here as needed. See sample configuration files for details.  
  
**Note:** Database properties are not declared in this file. Instead, they are in an auxillary database specific file. This aux file, typically named ampgenpp-dbname.props includes the above config file. See a sample of ampgenpp-dbname.props file below.   
   

*A sample ampgen-generic.props*  
  
```
#
# NOTE no db props declared here this is an aux file included by a database specific prop file
#
# --- AmpGenPP properties ---
#
debug=true
verbose=true
# verbose details about sql commit:
#debugCommit=true
#
jasiUserName=ampgen
#
#Don't use stale cache
#channelCacheFilename=channelList.cache
#Revert to group name
channelCacheFilename=
waveServerConnectTimeout=2000
# wave servers ! Use diskbased since this usually runs too late for RWPs
waveServerGroupList=/waves+1+20000+false+false+someserver1\:port1+someserver2\:port2/
currentWaveServerGroup=waves
#waveserver mode uses EnergyWindowModel
waveformReadMode=1
#
#datasource mode uses DataDourceChannelTimeModel
#waveformReadMode=0
org.trinet.jasi.DataSourceChannelTimeModel.filterWfListByChannelList=true
#
# These are the defaults - here for clarity
# NOTE!: channelGroupName is case-sensitive
channelGroupName=AmpGen
sourceName=AmpGenPp
localNetCode=CI
#
#DEFAULTS:
preEventTime = 1.0
minTimeWindow = 6.0
maxTimeWindow = 120.0
maxVelocity=1.0
#
#DEFAULT minAcceleration=1.0
# NOTE: rtem rad has:
minAcceleration=0.75

#DEFAULT maxDistance = 500.0
#NOTE: rtem ampgen2 has:
maxDistance = 800.0
#
# TOGGLE this for testing/production
writeToDB = true
```

*A sample ampgenpp-dbname.props*  

```
auxPropFileTags= dbhost dbuser ampgen
auxPropFile.dbhost= /app/aqms/tpp/jiggle/cfg/dbd-host.props 
auxPropFile.dbuser= /app/aqms/tpp/jiggle/cfg/dbd-tpp.props
auxPropFile.ampgen= /app/aqms/tpp/ampgen/cfg/ampgen-generic.props
```
  

## ENVIRONMENT

NA

## DEPENDENCIES

*  [Jiggle](https://aqms-swg.gitlab.io/jiggle-userdocs)  


## MAINTENANCE

Use up-to-date jiiggle.jar

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues

## MORE INFORMATION

[Real-time ampgen](https://aqms-swg.gitlab.io/aqms-docs/man-ampgen.html)  
  
“Continuous Monitoring of Ground-Motion Parameters”, BSSA, V89, pp. 311-316, Feb. 1999.

