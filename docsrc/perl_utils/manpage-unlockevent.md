# unlockevent {#unlockevent}

Unlock an event locked by Jiggle 

## PAGE LAST UPDATED ON

2021-06-09  

## NAME

unlockevent.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Unlock a locked event by deleting the JASIEVENTLOCK table row that matches the input evid.   

## HOW TO RUN

```
usage: ./unlockevent [-d dbase] <evid> 

Unlock a locked event by deleting the JASIEVENTLOCK table row that
matches the input evid.

-h        : print this usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

example: ./unlockevent -d mydb 8735346
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


