# enabledForPCS {#enabledForPCS}  
     
Prints true if PCS application mapped to input 'appName' running on localhost can process an event posted to the input state. 

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

enabledForPCS.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Prints true if PCS application mapped to input 'appName' running on localhost can process an event posted to the input state.  

Prints true, if no row is found (assumes processing is allowed).   

## HOW TO RUN  
  
```
Prints true if PCS application mapped to input 'appName' running on localhost
can process an event posted to the input state. Queries the AQMS_HOST_ROLE
and APP_HOST_ROLE tables to determine the host application state mapping.

Prints true, if no row is found (assumes processing is allowed).

usage: ./enabledForPCS [-d dbase] <appName> <pcsState>
    
-h        : print this usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-v        : print details of current host role and app role on host

example: ./enabledForPCS -d mydb rcg_cont rcg_rt 
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues