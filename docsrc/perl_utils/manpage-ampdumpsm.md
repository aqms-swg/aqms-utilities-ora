# ampdumpsm {#ampdumpsm}  
  
Dump strong motion amp data, or total amp count, associated with event (AssocEvAmpset)    
  

## PAGE LAST UPDATED ON

2021-05-28  
  

## NAME  

ampdumpsm.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Print strong motion amp data, or total amp count, associated with event (AssocEvAmpset).
  

## HOW TO RUN  
  
```  
usage: ./ampdumpsm [-h] [-a type] [-c cnt] [-d dbase] [-i,-r, or -s subsrc] [-q] <evid>
```
  
Print strong motion amp data, or total amp count, associated with event (AssocEvAmpset).
  
Amps are ordered by distance,net,sta,location,seedchan,amptype. All associated amps are listed unless optional command switches are used to restrict list by amptype and/or subsource.  
  
     
     -h         : print this usage info
     -d dbase   : alias to use for db connection (defaults to "archdbe")
     -a type    : amptype to match
     -c         : print total count only, no data rows
     -g         : convert PGA acceleration units to percent of g 
     -i         : imported amp subsource (e.g. gmp)
     -p         : order by amptype, amplitude, dist, name (default order is: distance, name, amptype
     -R         : order amplitude or distance value descending (default ascending)
     -r         : local amp subsource (e.g. RT ampgen, AmpGenPp)
     -s subsrc  : assocevampset subsource (ampgen, AmpGenPp, Gmp2Db)
     -n cnt     : max rows to print
     -q         : do not print column header title
  
      
example: ./ampdumpsm -c -r -a PGA -d k2db 9277430
  


## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues