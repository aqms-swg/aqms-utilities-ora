# deleteRequests {#deleteRequests}  
  
   
Delete request_card table rows associated with the input event evid  

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

deleteRequests.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Delete request_card table rows associated with the input event evid.  

## HOW TO RUN  
  
```
usage: ./deleteRequests [-d dbase] evid
     
Delete request_card table rows associated with the input event evid.

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

-n network-list  : list of net to include, delimited by ',' or ':'
-c seedchan-list : list of seedchan to include, delimited by ',' or ':'
-s station-list  : list of sta to include, delimited by ',' or ':'

example: ./deleteRequests -d mydb -c EHZ -n NC -s PKR 1234567 
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues