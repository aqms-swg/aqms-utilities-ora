# dumpDbRows4Evid {#dumpDbRows4Evid}  
  
   
Prints the database table data associated with the input evid.    

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

dumpDbRows4Evid.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Prints the database table data associated with the input evid to a filename \<evid\>_db_rows_\<timestamp\>.txt.  

## HOW TO RUN  
  
```
usage: ./dumpDbRows4Evid [-d dbase ] [-u user/passwd ] [-p out-dir] <evid>

Prints the database table data associated with the input evid to 
a filename <evid>_db_rows_<timestamp>.txt.

Runs SQLPLUS with input file /app/aqms/tpp/bin/.sql.

-h             : this help usage
-d             : alias to use for db connection (defaults to archdbe)
-u user/passwd : alternative user login to use for sqlplus connection
-p out-dir     : output directory for data file (default is working dir.)

example: ./dumpDbRows4Evid -p /tmp -d k2db 123456
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues