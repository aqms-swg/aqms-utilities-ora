# prefmags {#prefmags}  
  
Show info about the preferred magnitudes    

## PAGE LAST UPDATED ON

2021-06-14  
  

## NAME  

prefmags.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Show info about the preferred magnitudes of magtypes found in EVENTPREFMAG table. The event preferred magnitude is the one flagged with "<----" after its lddate.  
  

## HOW TO RUN  
  
```
usage: ./prefmags [-q] [-e] [-d dbase] <evid>
     
Show info about the preferred magnitudes of magtypes found in EVENTPREFMAG table.
The event preferred magnitude is the one flagged with "<----" after its lddate.

The magid,orid values are printed at the end of the output line.

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-e        : include event evid at end of output lines
-q        : do not print title header

example: ./prefmags -d mydb 8735346
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues