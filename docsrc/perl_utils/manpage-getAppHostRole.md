# getAppHostRole {#getAppHostRole}  
     
List host row column values found in APP_HOST_ROLE table.      

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

getAppHostRole.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
List host row column values found in APP_HOST_ROLE table. Default is currently active role(s) for localhost.   
  
NOTE: A host name must have a matching name row key in the AQMS_HOSTS table, if not, an database foreign key exception error occurs.    
  
## HOW TO RUN  
  
```
usage: ./getAppHostRole  [-h] [-a] [-A] [-H] [-d <dbase>] [-n host] [-p appname] [-s state] [-m mode A,P, or S]

List host row column values found in APP_HOST_ROLE table.
Default is currently active role(s) for localhost. 

NOTE: A host name must have a matching name row key in the AQMS_HOSTS table,
        if not, an database foreign key exception error occurs.

-h       : this usage
-d name  : database alias, defaults to $masterdb=archdbe)
-n host  : name of host (default is localhost = lhotse)
-a       : list most current values for all hosts,
            sorted in host, app, state, ondate order
-A       : list entire history sorted in host, app, state, ondate order
-r       : reverse sort, ondate order descending
-p       : program appname on host
-s       : posting state, or none
-q       : do not print table column header title 
-m       : host role run mode ( A= any, P = primary, S = shadow) 

example: ./getAppHostRole -d mydb -n manaslu -p rcg_cont -s rcg_rt
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues