# getHostsForAppState {#getHostsForAppState}  
  
Return hosts enabled in the db for named application and state as found by querying the db AQMS_HOST_ROLE and APP_HOST_ROLE tables.  

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

getHostsForAppState.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Return hosts enabled in the db for named application and state as found by querying the db AQMS_HOST_ROLE and APP_HOST_ROLE tables.

## HOW TO RUN  
  
```
usage: ./getHostsForAppState [-d dbase] <pcsAppName> <pcsState> [role]

Return hosts enabled in the db for named application and state as
found by querying the db AQMS_HOST_ROLE and APP_HOST_ROLE tables.

pcsAppName : appname in APP_HOST_ROLE table
pcsState   : state in APP_HOST_ROLE table
pcsRole    : match app role mode: P, S, or  A 
            
Host is enabled if app ROLE_MODE is 'A' or 'P', and the host is primary, or
ROLE_MODE is 'A' or 'S', and the host role is shadow (PRIMARY_SYSTEM=false). 

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

example: ./getHostsForAppState -d mydb rcg_cont rcg_rt
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues