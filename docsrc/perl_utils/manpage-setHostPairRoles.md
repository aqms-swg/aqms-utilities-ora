# setHostPairRoles {#setHostPairRoles}  
  
Changes the PRIMARY_SYSTEM role status for a matched pair of hosts in AQMS_HOST_ROLE table.  

## PAGE LAST UPDATED ON

2021-06-14  
  

## NAME  

setHostPairRoles.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Changes the PRIMARY_SYSTEM role status for a matched pair of hosts in AQMS_HOST_ROLE table.  
  
Inserts two new rows into AQMS_HOST_ROLE table of the connected database.  
The named host must match like named host row in the AQMS_HOSTS table.  
  
For the primary-host argument the PRIMARY_SYSTEM value is set "true" and for the shadow-host argument its value is set "false".    
  

## HOW TO RUN  
  
```
usage: ./setHostPairRoles  [-h] [-d <dbase>] <primary-host> <shadow-host>
    
Changes the PRIMARY_SYSTEM role status for a matched pair of hosts
in AQMS_HOST_ROLE table.

Inserts two new rows into AQMS_HOST_ROLE table of the connected database.
The named host must match like named host row in the AQMS_HOSTS table.

For the primary-host argument the PRIMARY_SYSTEM value is set "true"
and for the shadow-host argument its value is set "false".

-h           : this usage
-d name      : database alias, defaults to $masterdb=archdbe)

example: ./setHostPairRoles -d mydb arch1 arch2
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues