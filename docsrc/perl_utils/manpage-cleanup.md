# cleanup {#cleanup}  
  
Remove all non-executable type files.    
  

## PAGE LAST UPDATED ON

2021-06-09  
  

## NAME  

cleanup.sh     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Remove all non-executable type files below  directory \<dir\> modified more than \<days\> ago  
  

## HOW TO RUN  
  
```
usage: cleanup <dir> <days> [find command options in quotes]

    Remove all non-executable type files below  directory <dir> modified more than <days> ago
    Adding a '-print' argument to end, lists the matched files.

WARNING:
    Do not run this script in directories with source or configurations (e.g. /src /cfg)
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues