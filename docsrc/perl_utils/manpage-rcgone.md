# rcgone {#rcgone}  
  
Generate request cards (archive waveform requests) for one event.  

## PAGE LAST UPDATED ON

2021-06-23  
  

## NAME  

rcgone.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Generate request cards (archive waveform requests) for one event. The waveforms are later archived by a wavearchiver program.  
  
Does a no-op if the input event already has waveforms archived or already has requests queued. To disable no-op action for pre-existing waveforms or requests use options described below.   
  

## HOW TO RUN  
  
```
Generate request cards (archive waveform requests) for one event.
The waveforms are later archived by a wavearchiver program.

Does a no-op if the input event already has waveforms archived
or already has requests queued. To disable no-op action for pre-
existing waveforms or requests use options described below.

usage: ./rcgone.pl [-d dbase] <evid> [java props file]
    
    -h        : print usage info
    -d dbase  : use this particular db alias (defaults to "archdbe")

    -q        : wait for java job to complete and exit with its exit status
                otherwise fork java job to background and exit 

    -o        : send error and print output to standard out, default is a redirect to:
                '/app/aqms/tpp/rcg/logs/rcgone/rcgone-<evid>-<yyyymmdd-HHMM>.log'

    -r        : skip existing request cards check
    -w        : skip existing waveforms check

    -s        : skip both existing waveforms and request cards checks

when not specified as command line argument the properties file default to:
    /app/aqms/tpp/rcg/cfg/rcgone-<dbase>.props

which is this case is:
    /app/aqms/tpp/rcg/cfg/rcgone-archdbe.props
        
example: ./rcgone.pl -d mydb 8735346  cfg/request.props
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues