# setMag {#setMag}  
  
Creates a new event preferred magnitude in the db from the input arguments.    

## PAGE LAST UPDATED ON

2021-06-14  
  

## NAME  

setMag.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Creates a new event preferred magnitude in the db from the input arguments.   
  

## HOW TO RUN  
  
```
usage: ./setMag [-d dbase] <evid> <mag-value> <mag-type> [auth] [subsource] 
                              [algorithm] [nsta] [nobs] [quality] [rflag]

Creates a new event preferred magnitude in the db from the input arguments.

The event evid, and the magnitude's value and magtype (e.g. l) are required
parameters, the other parameters have default values listed below.
    
Default magnitude parameters:

    [<auth>]       = same as event row in db 
    [<subsource>]  = HAND              
    [<algorithm>]  = HAND  
    [<nsta>]       = 0   
    [<nobs>]       = 0   
    [<quality>]    = 1
    [<rflag>]      = H

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

example: ./setMag -d archb 992875 3.5 l
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues