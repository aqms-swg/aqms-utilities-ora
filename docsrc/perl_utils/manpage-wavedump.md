# wavedump {#wavedump}  
  
Dump list of this event's waveforms 
  

## PAGE LAST UPDATED ON

2021-05-28  
  

## NAME  

wavedump.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Prints summary info about waveforms and/or waveform requests associated with event.      
  

## HOW TO RUN  
  
```  
usage: ./wavedump [-h] [-c|k] [-C] [-d dbase] [-q] [-f] [-r] [-R] <evid>  
```
  
Prints summary info about waveforms and/or waveform requests associated with event.  
  
Waveform summary info:  
SNCL, ontime, start-time, end-time, duration-secs, total-bytes, sps  
  
  
Request_Card summary info:  
SNCL, start-time, end-time, duration-secs, retrys, last-retry-time  
  

    -h        : print this usage info

    -c        : print count of waveform table rows

    -f        : print the waveform copy filenames associated with input evid
                not the waveform table info.

    -k        : print count of distinct waveform SNCL 
    -C        : print count of request_card table rows

    -d dbase  : alias to use for db connection (defaults to archdbe)

    -r        : list outstanding request info after waveform list
    -R        : only list outstanding request info (no waveform info)

    -q        : do not print a column header title for info listings
  

example: ./wavedump -d mydb -r 7395710
    
  

## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues