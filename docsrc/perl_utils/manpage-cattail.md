# cattail {#cattail}  
  
Dump events since \<hours-back\>  
  

#### PAGE LAST UPDATED ON

2021-05-28  
  

#### NAME  

cattail.pl     


#### VERSION and STATUS

status: ACTIVE  
  

#### PURPOSE
  
Writes a summary list for  events in the past 'N' hours (default = 24), or specified date range.    
  

#### HOW TO RUN  
  
```
usage: ./cattail [option-list] [[#interval] or [startDate [endDate]]]  
    options: [-h] [-c] [-d dbase] [-a or [ [-D] [-B0|1]]] [-i|I]  
             [-f file] [-e types] [-g|G gtypes] [-p rflags] [-m mag -M mag]  
             [ -mt magtypes] [-ma magalgo]  
             [-mad rval -MAD rval] [-rms rval -RMS rval]  
             [-mz] [-z depth -Z depth] [ -fz]  
             [-es esrc -os osrc -ms msrc ] -[2rwL]  
             [-u d|w|m ]  
  
Writes a summary list for  events in the past 'N' hours (default = 24), or specified date range.  
Column values are from the db tables Event e, Origin o, Netmag n outer joined on prefor and prefmag:  
EVID,MAGNITUDE,MAGTYPE,MAGALGO,UNCERTAINTY,DATETIME,LAT,LON,DEPTH,WRMS,GAP,ETYPE,GTYPE,RFLAG,SUBSOURCE followed by counts of of waveforms, strong-motion amps, and non-zero weight magnitude associated amps or codas; however, if no observations are associated with event preferred magnitude a negative netmag.nobs value is shown.  
  
Time interval units defaults to hours unless -u option is used for days, weeks, or months units.   
  
Defaults to last 24 hrs if no input arguments or options. Input arguments must be hours back, or a starting date, and optionally, an ending date, if no ending date is specified it defaults to the current GMT time.  
  
Date are of the form yyyy-mm-dd ; you can use either '-' or '/' as the year, month and day separator.  
  
Date arguments can optionally include a time of the form: HH:MM or HH:MM:SS  
Quote string if using a white space separator between them: '2013-12-01 13:00', otherwise, use character set [,-_] to separate a date from its optional time field:  
2013-03-13,13:00 2013-03-13-13:00 or 2013-03-13:13:00 or  2013/03/13_13:00:00   

    -h        : print this usage info

    -d dbase  : alias to use for db connection (defaults to "archdbe")

    -c        : print only the total count of events

    -q        : do not print title or legend, only event summmary

    -L        : include a legend for the title column acronyms

    -r        : reverse time order sort (most recent at top)

    -w        : add info on nearest town and its distance from event,
                requires populated gazetteer_towns table in database

    -2        : print 2-decimal magnitude, time seconds, and depth km values

    -fz       : append '*' character after depth for fixed depth origin  
  

Query list filter options:  
  

    -mz         : depth (z) is the origin's model depth

    -f file     : file with one or more lines whose first column is an event evid
    -F file     : file with one or more lines whose first column is an event evid
                but ignores all other filter criteria including time bounds
                (e.g. cattail -F idFile.txt)
    

     -i evid     : event specified by id if its data matches filter criteria
     -I evid     : event specified by id ignoring all other filter criteria
                   including time bounds (e.g. cattail -I 12345)

     -a          : all events (includes deleted and shadow system duplicates)
                   note: not compatible together with -D and/or -B option

     -D          : only deleted or shadow system duplicate events (selectflag=0)

     -B flag     : =1 show bogus origins only, =0 show non-bogus origins

     -e types    : only events matching specified types, where types
                   is a concatenation of Event.etype codes delimited by
                   ',' or ':', for example: 'eq:ex:qb' or 'eq,ex,qb'

     -FZ         : only events with fixed depth origin, fdepth='y'

     -gG types   : only events matching specified gtypes, where types
                   is a concatenation of Origin.gtype codes delimited by
                   ',' or ':', for example: 'l:r:t' or 'l,r,t', use -G
                   option to include events with null gtype value

     -es src     : only events with like matching event subsource
                   string can contain db sql wildcard characters '%' or '_'

     -os src     : only events with like matching origin subsource
                   string can contain db sql wildcard characters '%' or '_'

     -oa algo    : only events with like matching origin algorithm, e.g. BINDER,
                   string can contain db sql wildcard characters '%' or '_'

     -rf flags   : only events whose review flag matches specified flags, 
                   a concatenation of Origin.rflag codes delimited by
                   ',' or ':', for example: 'I:F' or 'A,H'

     -ms src     : only events with like matching netmag subsource
                   string can contain db sqlwildcard characters '%' or '_'

     -ma algo    : show magnitude algorithmn column in after evid in listing,
                   for magnitudes whose algo matches string, e.g. RichterMl2,
                   string can contain db sqlwildcard characters '%' or '_',
                   an empty string includes nulls for triggers

     -mt mtypes  : only events whose magnitude type matches specified types,
                   where mtype is a concatenation of Netmag.magtype codes 
                   delimited by ',' or ':', for example: 'l:d:w' or 'l,d,w'

     -m mag      : only events whose magnitude >= mag
     -M mag      : only events whose magnitude <= mag

     -z rval     : only events whose depth >= rval
     -Z rval     : only events whose depth <= rval

     -rms rval   : only events with origin residual rms >= rval
     -RMS rval   : only events with origin residual rms <= rval

     -mad rval   : only events with netmag uncertainty (MAD) >= rval
     -MAD rval   : only events with netmag uncertainty (MAD) <= rval

     -mobs rval  : only events with netmag nobs >= rval
     -MOBS rval  : only events with netmag nobs <= rval

     -nph rval   : only events with origin ndef phases >= rval
     -NPH rval   : only events with origin ndef phases <= rval

     -wfs rval   : only events with waveform count >= rval
     -WFS rval   : only events with waveform count <= rval

     -o          : prints column numbers below title line (e.g useful for awk)

     -u d|w|m    : time interval units (days, weeks, or months) for input times

     -x          : for debugging only, prints database sql query string

    example: ./cattail -d mydb -2 -u d -e qb 7
```  
  
  
#### BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues