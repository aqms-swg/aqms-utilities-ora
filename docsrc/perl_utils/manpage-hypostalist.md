# hypostalist {#hypostalist}  
  
Dump station list from CHANNEL_DATA table in Hypoinverse format.
  

## PAGE LAST UPDATED ON

2021-05-28  
  

## NAME  

hypostalist.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Dump station list from CHANNEL_DATA table in Hypoinverse format.      
  

## HOW TO RUN  
  
```  
usage: ./hypostalist [-abd:qX:x:I:i:h] [<start-date>] [<end-date>]
```
  
Dump station list from CHANNEL_DATA table in Hypoinverse format
    
    -h           : print usage info
    -d dbase     : alias to use for db connection (defaults to "archdbe")
    -a           : show active stations only 
    -b           : print "blank" locations codes as blanks not dashes 
    -p progid... : list only those channels also mapped to specified progid(s) in the
                   APPCHANNELS table, delimit multiple progid by ',' without spaces.
    -q           : show the SQL query, don't execute it
    -X net_list  : list of net codes to exclude, e.g. "C%, ZY", default none excluded 
    -x chn_list  : list of channel codes to exclude, default, with no option excludes:
                   A%,C%,L%,M%,V%,R%,O%,U%,V%,W%
    -I net_list  : list of net codes to include, e.g. "CI,NP", default all included
    -i chn_list  : list of channel codes to include, e.g. "HH%, HL%, SH%, %Z"

(Note: complex combinations of include and exclude may produce odd results.)
                 
Defaults for optional dates are:  
\<start-date\> : 1900/01/01  
\<end-date\>   : 4000/01/01  
      
      
So, if you specify no start or end dates you get all stations that ever were. If you specify only <start-date> you get channels active from that time to now.
  

example: ./hypostalist -d k2db  -X CI,ZY,AZ  "2008/1/31" 
    
  

## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues