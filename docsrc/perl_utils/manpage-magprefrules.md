# magprefrules {#magprefrules}  
  
Print summary listing of magprefpriority table rules

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

magprefrules.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Print summary listing of magprefpriority table rules

## HOW TO RUN  
  
```
usage: ./magprefrules [-h] [-d dbase] [-r name] [-t magtype] 

Print summary listing of magprefpriority table rules.
    
-h           : print usage info
-d dbase     : alias to use for db connection (defaults to "archdbe")
-r region    : for this region name
-t type      : for this magtype
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues