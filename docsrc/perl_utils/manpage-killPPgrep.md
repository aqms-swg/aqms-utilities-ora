# killPPgrep {#killPPgrep}  
  
Kill one or more aqms pids returned by 'pgrep -f grep-string'. Sends email to $TPP_ADMIN_MAIL list if defined else prompts user for email address list to be notified of kill actions  

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

killPPgrep.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE

Kill one or more aqms pids returned by 'pgrep -f grep-string'. Sends email to $TPP_ADMIN_MAIL list if defined else prompts user for email address list to be notified of kill actions  

## HOW TO RUN  
  
```
usage: ./killPPgrep <grep-string>

Kill one or more aqms pids returned by 'pgrep -f grep-string'.
Sends email to $TPP_ADMIN_MAIL list if defined else prompts user for 
email address list to be notified of kill actions
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues