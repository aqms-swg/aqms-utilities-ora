# ampdump {#ampdump}  
  
Dump amplitudes contributing to mag (AssocAmM)    
  

## PAGE LAST UPDATED ON

2021-05-28  
  

## NAME  

ampdump.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Print amp data associated with preferred magnitude of event (AssocAmM), or with input magid if -m option is used.    
  

## HOW TO RUN  
  
```  
usage: ./ampdump [-h] [-a type] [-d dbase] [-n cnt] [-m] [-q] <id>
```
  
Print amp data associated with preferred magnitude of event (AssocAmM), or with input magid if -m option is used.  
  
Amps are ordered by distance,net,sta,location,seedchan,amptype. All associated amps are listed unless optional command switches are used to restrict list by amptype and/or subsource.
  
  
    -h         : print usage info  
    -d dbase   : alias to use for db connection (defaults to "archdbe")  
    -a type    : amptype to match, wildcards ok.  
    -c         : print total count only, no data rows  
    -m         : input id is a magid, not an evid  
    -n cnt     : max rows to print  
    -s subsrc  : assocamm subsource match, wildcards ok.  
    -q         : do not print column header title  
  

example: ./ampdump -d mydb 7395710  
  


## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues