# getstates {#getstates}

Show posted events.   

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

getstates.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

List all postings in PCS_STATE table whose state description matches input arguments. When no input arguments, shows all post descriptions.  
  
Unprocessed postings have a result =0, processed events usually have result =1, and other result codes may flag processing error depending on the application.  

## HOW TO RUN

```
usage: ./getstates [-d dbase] [-c|C] [-iI] [-q] [-r] [-n] [[group] [table] [state] [rank] [result]]
     
List all postings in PCS_STATE table whose state description matches
input arguments. When no input arguments, shows all post descriptions.

Unprocessed postings have a result =0, processed events usually have result =1,
and other result codes may flag processing error depending on the application.

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-c        : list row counts grouped by group,table,state,rank,result (without ids)
-C        : list row counts grouped by group,table,state (without ids)
-i id     : match specified posting id (event evid) 
-I        : sort output in id, group,table,state,rank,result,lddate order
            default sort is by group,table,state,rank,result,lddate,id 
-n count  : max number of rows returned from db query
-q        : do not print column name title line
-r char   : alternative character to flag unprocessed result, (e.g. '#', default=0)

example: ./getstates -d archdb TPP TPP FINALIZE
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


