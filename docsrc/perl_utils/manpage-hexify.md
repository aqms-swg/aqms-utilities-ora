# hexify {#hexify}  
  
Hexify an ASCII string.

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

hexify.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Hexify an ASCII string.  

## HOW TO RUN  
  
```
usage: ./hexify -[h] <string>

Hexify an ASCII string.
    
    -h        : print usage info
    -r        : convert from Hex to ASCII

example: ./hexify "username"
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues