# post {#post}

Post event to a state.      

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

post.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Post an event to some state in PCS.  

## HOW TO RUN

```
usage: ./post [-d dbase] <id> <group> <table> <state> <rank> [<result>]
     
Post an event to some state in PCS

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

example: ./post -d mydb  EventStream archdb  MakeGif 100
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


