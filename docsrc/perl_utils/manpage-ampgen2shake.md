# ampgen2shake {#ampgen2shake}  
  
Calculate ground motions for one event, if successful, then executes ShakeMap alarm scripts.    

## PAGE LAST UPDATED ON

2021-06-23  
  

## NAME  

ampgen2shake.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Calculate ground motions for one event, if successful, then executes ShakeMap alarm scripts.  
  
Uses the AmpGenPp application found in jiggle jar to create a new set of database amps of amptypes: PGD, PGV, PGA, and SP.   
  
## HOW TO RUN  
  
```
usage: ./ampgen2shake.pl -[h] [-p propfile] [-s] <evid>

Calculate ground motions for one event, if successful,
then executes ShakeMap alarm scripts.

Uses the AmpGenPp application found in jiggle jar to create
a new set of database amps of amptypes: PGD, PGV, PGA, and SP.

Does event data db lookup in database: archdbe

-h          : print this usage info

-p propfile : property file used by the Java AmpGenPp db amp generator
              (default file is /app/aqms/tpp/ampgen/cfg/ampgenpp.props)

-a          : generate amps only, skip running of the ShakeMap alarm scripts 
-s          : skip amp generation, only run the ShakeMap alarm scripts
-z          : no-op if count of local subsource strong motion amps in db > 0

Example:

    Generate amps and run alarm scripts only if no amps for event in db:
    ./ampgen2shake.pl -p ../cfg/ampgenpp.props -z 7395710

    Always generate new amps and run alarm scripts:
    ./ampgen2shake.pl -p ../cfg/ampgenpp.props 7395710
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues