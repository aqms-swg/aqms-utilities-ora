# getRflag {#getRflag}  
  
Prints the rflag column value of the ORIGIN table row whose orid matches EVENT prefor for evid key.  

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

getRflag.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Prints the rflag column value of the ORIGIN table row whose orid matches EVENT prefor for evid key.  
Prints an empty string when no matching row is found in the db table.  

## HOW TO RUN  
  
```
usage: ./getRflag -[h] [-d dbase] <evid>
     
Prints the rflag column value of the ORIGIN table row whose orid matches EVENT prefor for evid key.
Prints an empty string when no matching row is found in the db table.

-h        : print usage info

-d dbase  : alias to use for db connection (defaults to "archdbe")

example: ./getRflag -d k2db 9277430
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues