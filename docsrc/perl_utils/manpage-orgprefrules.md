# orgprefrules {#orgprefrules}  
  
Print summary listing of orgprefpriority table rules.  

## PAGE LAST UPDATED ON

2021-06-14  
  

## NAME  

orgprefrules.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Print summary listing of orgprefpriority table rules.  
  

## HOW TO RUN  
  
```
usage: ./orgprefrules [-h] [-d dbase] [-r name] [-t type] 

Print summary listing of orgprefpriority table rules.
    
-h           : print usage info
-d dbase     : alias to use for db connection (defaults to "archdbe")
-r region    : for this region name
-t type      : for this origin type
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues