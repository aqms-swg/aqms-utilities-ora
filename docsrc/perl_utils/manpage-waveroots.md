# waveroots {#waveroots}  
  
List the database waveroots table rows  

## PAGE LAST UPDATED ON

2021-06-15  
  

## NAME  

waveroots.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
     
List the database waveroots table rows  

## HOW TO RUN  
  
```
List the database waveroots table rows.

Syntax: ./waveroots [-h] [-d dbase] [-t T|C ] [-c wcopy# ]

Options:

  -h         : this usage
  -d dbase   : name alias for database connection (default archdbe)
  -c copy#   : wcopy number, default all numbers
  -t T|C     : wavetype, T=triggered or C=continuous, default all types

Column header title: 
 T = wavetype, triggered or continuous
 S = status  A permanent or T temporary
 F = wave_fmt number
 C = wcopy number
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues