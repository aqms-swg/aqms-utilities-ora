# killPPcont {#killPPcont}  
  
Kills configured aqms post-processing processes that normally run as crontab jobs. Sends email to $TPP_ADMIN_MAIL list if defined else prompts user for email address list to be notified kill actions.  


## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

killPPcont.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Kills configured aqms (postproc) processes that normally run as crontab jobs. Sends email to $TPP_ADMIN_MAIL list if defined else prompts user for email address list to be notified kill actions.  


## HOW TO RUN  
  
```
usage: ./killPPcont

Kills configured aqms (postproc) processes that normally run as crontab jobs.
Sends email to $TPP_ADMIN_MAIL list if defined else prompts user
for email address list to be notified kill actions.
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues