# lsInstalledPm {#lsInstalledPm}  
  
List perl modules found in the @INC paths. Optionally grep for input.  

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

lsInstalledPm.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
List perl modules found in the @INC paths. Optionally grep for input.  

## HOW TO RUN  
  
```
usage: ./lsInstalledPm [grep-string (e.g. a module name)]

List perl modules found in the @INC paths. Optionally grep for input.
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues