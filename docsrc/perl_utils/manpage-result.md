# result {#result}

Set event's result value.   

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

result.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Set the RESULT value of rows in PCS_STATE table matching the input state description. When -r option is absent, result the highest rank row that matches the input state description arguments.  

## HOW TO RUN

```
usage: ./result [-d dbase] [-r rank] <id> <group> <table> <state> <result>

Set the RESULT value of rows in PCS_STATE table matching the input state
description. When -r option is absent, result the highest rank row that
matches the input state description arguments.

-h        : print this usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

-r rank   : for a post matching the input state description 
            > 0 set result only for the row with this rank value
            = 0 set result for all rows, any rank,  matching state 
            =-1 set result only for highest ranking row matching state
                (default)

example: ./result -d k2db 992875 EventStream k2db MakeGif 1
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


