# setHostAppRole {#setHostAppRole}  
  
Insert a APP_HOST_ROLE table row, first disabling any active matching row having a different role_mode. Does a no-op if the table already has an active row matching input.   

## PAGE LAST UPDATED ON

2021-06-14  
  

## NAME  

setHostAppRole.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Insert a APP_HOST_ROLE table row, first disabling any active matching row having a different role_mode. Does a no-op if the table already has an active row matching input.   
  

## HOW TO RUN  
  
```
usage: ./setHostAppRole [-h] [-d dbase] <[-f fname] or [<host> <appname> <state> <role_mode> [ondate] [offdate]]> 

Insert a APP_HOST_ROLE table row, first disabling any active matching row 
having a different role_mode. Does a no-op if the table already has an active row
matching input.

Requires either the 4 command line arguments or, the -f option and no arguments.
Using the -f option, the input file must have lines with token values equivalent 
to the hostname, application name, pcs posting state, and role mode arguments;
the ondate and offdate parameters are optional, defaulting to current time and
3000-01-01.

The role_mode value is restricted to 'P' for primary, 'S' for shadow or 'A' for any.

-h        : print this usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-f fname  : file containing lines with the host, appname, state, and role_mode
            tokens. All blank lines and lines starting with '#' are skipped.
            Ondate and offdate parameters are optional at end of data line.
            Date format is 'yyyy-mm-dd[,hh:mi[:ss]]', i.e. time is optional
            if specified use ',' between date and time or quote string with
            blanks.

example: ./setHostAppRole -d archdb2 pp2 rcg_cont rcg_rt P
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues