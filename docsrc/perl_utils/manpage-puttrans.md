# puttrans {#puttrans}

Vreate transition definition.    

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

puttrans.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Insert a transition row rule into the PCS_TRANSITION table.  
       
Requires at least 4 command line arguments. When the additional 8 or 9 arguments are absent resulting the old state will causes that old row entry to be deleted from the PCS_STATE table. If a "newResult" is not specified it will default to null (i.e. 0).  

## HOW TO RUN

```
usage: ./puttrans [-d dbase] [-s char] <oldGrp> <oldSrc> <oldState> <oldResult> [<newGrp> <newSrc> <newState> <newRank> [<newResult>]]

Insert a transition row rule into the PCS_TRANSITION table.
    
Requires at least 4 command line arguments. When the additional 8 or 9 arguments are absent
resulting the old state will causes that old row entry to be deleted from the PCS_STATE table.
If a "newResult" is not specified it will default to null (i.e. 0).

-h        : print this usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-s char   : optional character delimiter between the old and new state description groups
            if not specified, default is to allow '=', ':' or ' '

example: ./puttrans -d k2db TPP TPP INTERIM 1 EventStream mydb2 MakeGif 95
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


