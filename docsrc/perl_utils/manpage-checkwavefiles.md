# checkwavefiles {#checkwavefiles}  
  
Show dir and list archived waveform files for event (requires ssh).     
  

## PAGE LAST UPDATED ON

2021-05-28  
  

## NAME  

checkwavefiles.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Print the waveform file path found in database for an event evid. Optionally lists path files found on a specified wavefile host using ssh.    
  

## HOW TO RUN  
  
```
usage: ./checkwavefiles [-d dbase] [-1  or -wW wavefilehost] <evid>
```
     
Print the waveform file path found in database for an event evid.  Optionally lists path files found on a specified wavefile host using ssh.  
    

     -h        : print usage info.

     -d dbase  : alias to use for db connection (defaults to "archdbe").

     -c digit  : the waveroots.wcopy value, when option is absent, defaults to 1.

     -p        : print only the filepath. Ignores -w -W options. Printed path
                 is '-none-' if undefined or no matching evid is found in dbase.
                 When option is absent, default print format is:
                 WAVEFORM path in <dbase> for <evid> is: <path>

     -w wfhost : print the verbose path, then ssh wfhost ls -dlF wavefilepath/evid
                 requires the user to have a valid 'ssh' key to the dbhost machine. 

     -W wfhost : print the verbose path info, then ssh wfhost ls -lF wavefilepath/evid 
                 requires the user to have a valid 'ssh' key to the dbhost machine. 
  

example: ./checkwavefiles -d mydb -W wfhost 2856354 
   
  

## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues