# unpost {#unpost}

Femove an event from all states.      

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

unpost.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Delete all rows associated with one or more input evids in PCS_STATE table.  

## HOW TO RUN

```
usage: ./unpost [-d dbase] <id> [ id2 id3 ...] 
     
Delete all rows associated with one or more input evids in PCS_STATE table.

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

example: ./unpost -d mydb 9377694 9377700
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


