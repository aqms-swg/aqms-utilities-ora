# requestdump {#requestdump}  
  
Print a summary of REQUEST_CARD table data of request type 'T' either associated with an input evid or all events by using -a option without argument.  

## PAGE LAST UPDATED ON

2021-06-14  
  

## NAME  

requestdump.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Print a summary of REQUEST_CARD table data of request type 'T' either associated with an input evid or all events by using -a option without argument.     
  

## HOW TO RUN  
  
```
usage: ./requestdump [-h] [-s names] [-d dbase] [-c] [ -aA | [evid]]

Print a summary of REQUEST_CARD table data of request type 'T' either associated
with an input evid or all events by using -a option without argument.

Otherwise, use -A option without id argument to list continuous type 'C' requests.

Output listing is ordered by evid,net,sta,location,seedchan.

-h           : print usage info
-d dbase     : alias to use for db connection (defaults to "archdbe")

Data filter options:
-A           : list records for request_type='C', used without input evid argument.
-a           : all event ids request_type='T', used without input evid argument 
-c           : print count of rows in request_card table matching query conditions
-s sta-list  : only those rows matching listed sta names, delimited by ',' or ':'

example: ./requestdump -s PLM:RVR 6000123
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues