# getHostRole {#getHostRole}  
  
List host row column values found in AQMS_HOST_ROLE table. Default is most current role (modication time) for localhost.  

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

getHostRole.pl     


## VERSION and STATUS

status: ACTIVE  
  
## PURPOSE
  

List host row column values found in AQMS_HOST_ROLE table. Default is most current role (modication time) for localhost. 
  
NOTE: A host name must have a matching name row key in the AQMS_HOSTS table, if not, an database foreign key exception error occurs.  

## HOW TO RUN  
  
```
usage: ./getHostRole  [-h] [-a] [-A] [-qr] [-d <dbase>] [-n host]

List host row column values found in AQMS_HOST_ROLE table.
Default is most current role (modication time) for localhost. 

NOTE: A host name must have a matching name row key in the AQMS_HOSTS table,
        if not, an database foreign key exception error occurs.

-h       : this usage
-d name  : database alias, defaults to $masterdb=archdbe)
-n host  : name of host (default is localhost = lhotse)
-a       : list most current values for all hosts,
            sorted in host name, mod time order
-A       : list entire history sorted in host name, mod time order
-q       : do not print table column header title 
-r       : reverse sort, modification_time order descending

example: ./getHostRole -d mydb -a
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues