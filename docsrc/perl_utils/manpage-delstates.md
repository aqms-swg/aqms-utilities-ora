# delstates {#delstates}

Remove an event from all states.      

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

delstates.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Delete PCS_STATE table row entries matching the input state description.  

## HOW TO RUN

```
usage: ./delstates [-d dbase] [-i id] | <group> [<table> <state> <rank> <result>]
     
Delete PCS_STATE table row entries matching the input state description.

Input elements can contain a db wildcard (i.e. Oracle %  or _ ).
Specifying -i <id> without any state elements deletes all postings 
for that id (like unpost command).

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-i id     : all rows that match input id (evid)
-a #days  : only rows whose lddate is older than specified #days.

example: ./delstates -d mydb  EventProc  Stream1  MakeReport 100
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


