# depthdatum {#depthdatum}  
  
Print station elevation datum data   
  

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

depthdatum.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Print station elevation datum data, where datum is the average of the N closest station elevations != 0 and > -120 m elev that are associated with the event origin. Default is 5 stations.    

## HOW TO RUN  
  
```
usage: ./depthdatum  -[h] [-d dbase] [-n avgcnt] [-v|V] [-f id_file] <evid> 

Print station elevation datum data, where datum is the average of the
N closest station elevations != 0 and > -120 m elev that are associated
with the event origin. Default is 5 stations.

Must provide a single evid as only argument, or else specify with -f 
an input file containing lines where the evid is theh first token
in the line (blank lines and comment lines are skipped over).

-h        : print usage info

-d dbase  : alias to use for db connection (defaults to "archdbe")

-n avgcnt : number of closest stations with elev != 0 to average
            for datum value, defaults to 5. 

-v        : verbose output: evid datum minElev maxElev minDist maxDist count
-V        : verbose output with header title

-x        : debug, prints db sql statement and exits

-z        : if -v or -V, only print line if new depth <= 0 
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues