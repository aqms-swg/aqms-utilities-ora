# deleteEvent {#deleteEvent}

Delete event or trigger   

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

deleteEvent.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Delete (virtually) an event in the dbase and post to the PCS state to handle cancellation notifications, etc.  

## HOW TO RUN

```
usage: ./deleteEvent [-d dbasename]  <evid>
     
     -h        : print usage info
     -d dbase  : use this particular dbase (defaults to "archdbe")

    example: ./deleteEvent -d k2db 8735346
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


