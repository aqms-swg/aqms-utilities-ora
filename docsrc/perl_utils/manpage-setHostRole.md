# setHostRole {#setHostRole}  
  
Changes the PRIMARY_SYSTEM role status for the named host in the in AQMS_HOST_ROLE table. The role-code argument must be either 'p' or 'P' for primary, 's' or 'S' for shadow.

## PAGE LAST UPDATED ON

2021-06-14  
  

## NAME  

setHostRole.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Changes the PRIMARY_SYSTEM role status for the named host in the in AQMS_HOST_ROLE table. he role-code argument must be either 'p' or 'P' for primary, 's' or 'S' for shadow.  

Inserts one new rows into AQMS_HOST_ROLE table of the connected database.  
The named host must match like named host row in the AQMS_HOSTS table.  
  
For the PRIMARY_SYSTEM value is set to to true.  


## HOW TO RUN  
  
```
usage: ./setHostRole  [-h] [-d <dbase>] <host> <role-code>

Changes the PRIMARY_SYSTEM role status for the named host in the 
in AQMS_HOST_ROLE table. The role-code argument must be either 'p'
or 'P' for primary, 's' or 'S' for shadow.

Inserts one new rows into AQMS_HOST_ROLE table of the connected database.
The named host must match like named host row in the AQMS_HOSTS table.

For the PRIMARY_SYSTEM value is set to to true.

-h           : this usage
-d name      : database alias, defaults to $masterdb=archdbe)

example: ./setHostRole -d archdb nickel P
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues