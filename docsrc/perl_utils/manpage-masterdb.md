# masterdb {#masterdb}

Prints the default postproc/archive database alias name (e.g. rtdb) used by scripts.      

## PAGE LAST UPDATED ON

2021-06-09  

## NAME

masterdb.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Prints the default postproc archive db alias name currently configured for this host (archdbe). This alias is used for db connections made by scripts found under /app/aqms/tpp.  


## HOW TO RUN

```
usage: ./masterdb

Prints the default postproc archive db alias name currently configured
for this host (archdbe). This alias is used for db connections made by
scripts found in /app/aqms/tpp.
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


