# transition {#transition}  
  
Transitions all non-zero result posts found in the PCS_STATE table whose state description data matches the input parameters.      

## PAGE LAST UPDATED ON

2021-06-15  
  

## NAME  

transition.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Transitions all non-zero result posts found in the PCS_STATE table whose state description data matches the input parameters.   

When invoked with no input arguments, all non-zero result posts in the PCS_STATE table are transitioned (for all states and evids).  
  
To restrict the selection of eligible rows specify the controlgroup and sourcetable arguments filters, and add a state and/or evid filter
to further narrow the selection.
  
Use this command to transition all non-zero result posts that match a transition row rule added to the PCS_TRANSITION table after these posts
were updated in the db by previous processing.    
  

## HOW TO RUN  
  
```
usage: ./transition [-d dbase] [[<group> <table>] [state] [evid]] 
     
Transitions all non-zero result posts found in the PCS_STATE table
whose state description data matches the input parameters.

When invoked with no input arguments, all non-zero result posts in 
the PCS_STATE table are transitioned (for all states and evids).

To restrict the selection of eligible rows specify the controlgroup
and sourcetable arguments filters, and add a state and/or evid filter
to further narrow the selection.

Use this command to transition all non-zero result posts that match a
transition row rule added to the PCS_TRANSITION table after these posts
were updated in the db by previous processing. 

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

example ./transition -d mydb EventStream archdb NewEvent
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues