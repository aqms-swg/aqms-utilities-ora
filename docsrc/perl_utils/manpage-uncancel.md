# uncancel {#uncancel}  
  
Updates ALARM_ACTION table rows associated with the input evid having an action_state 'CANCELLED' to the action_state='!CANCELLED'.      

## PAGE LAST UPDATED ON

2021-06-15  
  

## NAME  

uncancel.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Updates ALARM_ACTION table rows associated with the input evid having an action_state 'CANCELLED' to the action_state='!CANCELLED'.   
If the event origin's rflag is 'C' resets it to 'H'. To also queue for alarms, use option -D or -F, depending on how the PCS_TRANSITION table rows are configured for PCS alarming. Typically, -F option, transitions to extra states to remake gif files and/or focal mechanisms, whereas -D processing is configured to just re-run the alarms, e.g. PDL update.
  

## HOW TO RUN  
  
```
usage: ./uncancel [-h] [-q] -[D|F] [-d dbase] <evid>
     
Updates ALARM_ACTION table rows associated with the input evid having an action_state 'CANCELLED'
to the action_state='!CANCELLED'.
If the event origin's rflag is 'C' resets it to 'H'. To also queue for alarms, use option -D or -F,
depending on how the PCS_TRANSITION table rows are configured for PCS alarming. Typically, -F option,
transitions to extra states to remake gif files and/or focal mechanisms, whereas -D processing is 
configured to just re-run the alarms, e.g. PDL update.

-h        : print usage info
-D        : post evid for TPP TPP DISTRIBUTE
-F        : post evid for TPP TPP FINALIZE
-d dbase  : alias to use for db connection (defaults to "archdbe")
-q        : quiet mode, do not print event summary or posting state descriptions  

example: ./uncancel -d k2db 7395710
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues