# getStatesAllDbs {#getStatesAllDbs}  
  
Invokes the 'getstates' command with any passed command line arguments for every db alias found in the file: /app/aqms/tpp/dbinfo/cfg/masterdblist.cfg

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

getStatesAllDbs.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Invokes the 'getstates' command with any passed command line arguments for every db alias found in the file: /app/aqms/tpp/dbinfo/cfg/masterdblist.cfg  

## HOW TO RUN  
  
```
usage: ./getStatesAllDbs [getstates arg list]

Invokes the 'getstates' command with any passed command line arguments for every db alias found in the file: /app/aqms/tpp/dbinfo/cfg/masterdblist.cfg
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues