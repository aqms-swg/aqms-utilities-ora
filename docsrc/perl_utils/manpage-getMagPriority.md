# getMagPriority {#getMagPriority}  
  
Prints the magnitude priority of the NETMAG table row whose magid matches the prefmag the event table row mathching the input id key.  

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

getMagPriority.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Prints the magnitude priority of the NETMAG table row whose magid matches the prefmag the event table row mathching the input id key unless the -m option is used, in which case the input id is the netmag row's magid.  
  
Prints "-1" if no database id or rule matches.  

## HOW TO RUN  
  
```
usage: ./getMagPriority -[h] [-d dbase] <id>
     
Prints the magnitude priority of the NETMAG table row whose magid matches
the prefmag the event table row mathching the input id key unless the
-m option is used, in which case the input id is the netmag row's magid.

Prints "-1" if no database id or rule matches.

-h        : print usage info

-d dbase  : alias to use for db connection (defaults to "archdbe")

-m magid  : input is the netmag row's magid, not the event row evid.

example: ./getMagPriority -d k2db 9277430
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues