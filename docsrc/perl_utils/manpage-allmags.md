# allmags {#allmags}

Show info about the magnitudes associated with all event origins.   

## PAGE LAST UPDATED ON

2021-06-09  

## NAME

allmags.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Show info about the magnitudes associated with all event origins.  
The event preferred magnitude is the one flagged with "<----" after its lddate.  

## HOW TO RUN

```
usage: ./allmags [-d dbase] [-e] [-w] [-q] <evid>
     
Show info about the magnitudes associated with all event origins.
The event preferred magnitude is the one flagged with "<----" after its lddate.

The magid,orid values are printed at the end of the output line.

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-t type   : list only magnitudes of specified type, e.g. l
-e        : include event evid at end of output lines
-w        : include credit attribution (who) commited magnitude
-q        : do not print title header

example: ./allmags -d mydb -w 8735346
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


