# dbidrivers {#dbidrivers}  
  
Prints the available perl DBI drivers.   
  

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

dbidrivers.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Prints the available perl DBI drivers.      
  

## HOW TO RUN  
  
```
usage: ./dbidrivers

Prints the available perl DBI drivers.
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues