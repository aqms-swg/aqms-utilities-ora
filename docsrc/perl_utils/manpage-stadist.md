# stadist {#stadist}  
  
Dump a station list in distance order from this lat/lon.  
  

## PAGE LAST UPDATED ON

2021-05-28  
  

## NAME  

stadist.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Dump a station list in distance order from this lat/lon. Only those stations eligible for waveform archiving (rcg) are listed.  
  

## HOW TO RUN  
  
```  
usage: ./stadist  -[h] [-d dbase] <lat.dec> <lon.dec>
       ./stadist  -[h] [-d dbase] <latdeg> <latmin> <londeg> <lonmin>
```    
  

Dump a station list in distance order from this lat/lon. Only those stations eligible for waveform archiving (rcg) are listed.  
      
    -h        : print usage info
    -d dbase  : alias to use for db connection (defaults to "archdbe")
  

example: ./stadist -d k2db  33.5343 -119.343
  


## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues