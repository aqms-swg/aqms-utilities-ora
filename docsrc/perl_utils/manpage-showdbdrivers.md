# showdbdrivers {#showdbdrivers}  
  
Prints a the available perl DBI drivers and the DBI->data_sources(driver).    

## PAGE LAST UPDATED ON

2021-06-14  
  

## NAME  

showdbdrivers.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Prints a the available perl DBI drivers and the DBI->data_sources(driver).      
  

## HOW TO RUN  
  
```
usage: /app/aqms/tpp/bin/showdbdrivers

Prints a the available perl DBI drivers and the DBI->data_sources(driver).
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues