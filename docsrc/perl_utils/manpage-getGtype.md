# getGtype {#getGtype}  
  
Prints the gtype column value of the ORIGIN table row whose orid matches the prefor of the event table row mathching the input id key unless the -o option is used, in which case the input id is the origin row's orid.  
  
Prints an '-' string when no matching row is found in the db table.     

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

getGtype.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Prints the gtype column value of the ORIGIN table row whose orid matches the prefor of the event table row mathching the input id key unless the -o option is used, in which case the input id is the origin row's orid.  
  
Prints an '-' string when no matching row is found in the db table.   

## HOW TO RUN  
  
```
usage: ./getGtype -[h] [-d dbase] <id>
     
Prints the gtype column value of the ORIGIN table row whose orid matches
the prefor of the event table row mathching the input id key unless the
-o option is used, in which case the input id is the origin row's orid.

Prints an '-' string when no matching row is found in the db table.

-h        : print usage info

-d dbase  : alias to use for db connection (defaults to "archdbe")

-o orid   : input is the origin row's orid, not the event row evid.

example: ./getGtype -d k2db 9277430
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues