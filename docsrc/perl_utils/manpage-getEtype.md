# getEtype {#getEtype}  
  
   
Prints the etype column value of the EVENT table row with the input evid key.  
Prints an empty string when no matching row is found in the db table.  

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

getEtype.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Prints the etype column value of the EVENT table row with the input evid key.  
Prints an empty string when no matching row is found in the db table.   

## HOW TO RUN  
  
```
usage: ./getEtype -[h] [-d dbase] <evid>
     
Prints the etype column value of the EVENT table row with the input evid key.
Prints an empty string when no matching row is found in the db table.

-h        : print usage info

-d dbase  : alias to use for db connection (defaults to "archdbe")

example: ./getEtype -d k2db 9277430
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues