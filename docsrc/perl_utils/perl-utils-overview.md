# Perl Utilities  {#perl-utils-overview} 
  
## Stand alone perl utilities  
  
**These utilities should be considered as TEMPLATES. Users should modify to suit their purposes.**  
  
Most of the utilities use *jiggle.jar*. Each utility has it's own directory, it's documentation is available under *docsrs*.  
  
For e.g.  
  
```  
perl_utils  
    ampgen   
        bin - executable scripts  
        cfg - configuration files  
    adhoc  
        bin  
        cfg  
    ...    
docsrc
    perl_utils
        ampgen.md  
        adhoc.md  
        ...
```
  
## Command line utilities  
  
**All command line utilities should be considered as TEMPLATES. Users should modify to suit their purposes.**  
  
AQMS has a set of command-line tools (mostly Perl) that perform a number of important functions. These are used by PCS tasks, the Duty Review Page, etc. These are present under *perl_utils/bin*. For ease of use, symbolic links are created with the name of the script but without the .pl or .sh extension.

These utilities use a set of perl modules (.pm), and they are present under the *perl_utils/bin/perlmodules* directory.  

Files with network specific data read by the bin scripts go under *perl_utils/bin/cfg*.  
Scripts to create/remove symbolic links for bin commands without filename suffix of .sh or .pl go under *perl_utils/bin/setup*.  


| **Command** | **Arguments** | **Function** |
--------- | --------- | ---------
| **%Event Info**| | | 
| [cattail](manpage-cattail.md) | -[ht] [-d dbase] \<hours-back\> (default = 24) | dump events since \<hours-back\> | 
| [cattailDatumGetDepth](manpage-cattailDatumGetDepth.md) | [-h] [-c] [-d dbase] [-a or [ [-D] [-B0 or -B1]]] [-iI] [-f file] [-e types] [-m mag -M mag] [-s esrc -S osrc ] [-rtw] [-u d or w or m ] | dump events since \<hours-back\> |  
| [trigtail](manpage-trigtail.md) | -[ht] [-d dbase] \<hours-back\> (default = 24) | dump triggers since \<hours-back\> |  
| [eventhist](manpage-eventhist.md) | [-d dbase] \<evid\> | show the event's history |  
| [catone](manpage-catone.md) | [-d dbase] \<evid\> | output summary line for one event |  
| [eventAge](manpage-eventAge.md) | [-d dbase] \<evid\> | output event age in seconds |   
| [eventMag](manpage-eventMag.md) | [-d dbase] \<evid\> | output event mag | 
| [eventVersion](manpage-eventVersion.md) | [-d dbase]  \<evid\> | print the db version number of evid | 
| [getEtype](manpage-getEtype.md) | -[h] [-d dbase] \<evid\> | print etype column value of the EVENT table row for evid | 
| [setEtype](manpage-setEtype.md) | [-d dbasename]  \<evid\> \<etype\> | set the etype of an event in the dbase | 
| [getGtype](manpage-getGtype.md) | -[h] [-d dbase] \<id\> | print gtype column value of the ORIGIN table row for prefor of evid | 
| [setGtype](manpage-setGtype.md) | -[h] [-d dbasename] \<evid\> \<gtype\> | set the gtype for the origin | 
| [setMag](manpage-setMag.md) | [-d dbase] \<evid\> \<mag-value\> \<mag-type\> [auth] [subsource] [algorithm] [nsta] [nobs] [quality] [rflag] | creates a new event preferred magnitude in the db | 
| [getMagPriority](manpage-getMagPriority.md) |  -[h] [-d dbase] \<id\> | prints the magnitude priority | 
| [getRflag](manpage-getRflag.md) | -[h] [-d dbase] \<evid\> | prints the rflag column value of the ORIGIN table row for evid | 
| [prefmags](manpage-prefmags.md) | [-q] [-e] [-d dbase] \<evid\> | show info about the preferred magnitudes | 
| [dumpDbRows4Evid](manpage-dumpDbRows4Evid.md) | [-d dbase ] [-u user/passwd ] [-p out-dir] \<evid\> | print the database table data associated with evid |
| [isInsideNetwork](manpage-isInsideNetwork.md) | [-h] [-d dbase] [-n name] \<evid\> | print 1 if event is located inside event auth network region | 
| [showalarms](manpage-showalarms.md) | [-d dbase] \<evid\> | show alarms sent for event | 
| **%Event Parameter Info** | | | 
| [hypophases](manpage-hypophases.md) | -[h] [-d dbase] \<evid\> | dump event's phases in Hypoinverse format | 
| [ampdump](manpage-ampdump.md) | -[ht] [-d dbase] \<event-id\> | dump amps contributing to mag (AssocAmM) | 
| [ampdumpo](manpage-ampdumpo.md) | -[ht] [-d dbase] \<event-id\> | dump amps associated with origin (AssocAmO) | 
| [ampdumpsm](manpage-ampdumpsm.md) | [-h] [-a type] [-c cnt] [-d dbase] [-i,-r, or -s subsrc] [-q] \<evid\> | dump strong motion amp data, or total amp count, associated with event (AssocEvAmpset) | 
| [checkwavefiles](manpage-checkwavefiles.md) | [-d dbase] [-1  or -wW wavefilehost] \<evid\> | show dir and list archived waveform files for event (requires ssh) | 
| [wavedump](manpage-wavedump.md) | [-h] [-c\|k] [-C] [-d dbase] [-q] [-f] [-r] [-R] \<evid\> | dump list of this event's waveforms |  
| [allmags](manpage-allmags.md) | [-d dbase] [-e] [-w] [-q] \<evid\> | show info about the magnitudes associated with all event origins | 
| [allmecs](manpage-allmecs.md) | [-d dbase] [-w] [-q] \<evid\> | show info about the mechanisms associated with all event origins |  
| [allorgs](manpage-allorgs.md) | [-h] [-q] [-e] [-w] [-d dbase] \<evid\> | show info about the origins associated with event evid | 
| [phaseCnt](manpage-phaseCnt.md) | [-h] [-d dbase] [-iozpsPS] \<evid\> | print count of phases associated with the preferred origin of evid |  
| [archive_db_total](manpage-archive_db_total.md) | [-d dbase] \<start_date\> \<end_date\> | report of the totals of events, arrivals, and waveforms for time period |   
| **Station/Channel Info** | | | 
| [hypostalist](manpage-hypostalist.md) | [-abd:qX:x:I:i:h] [\<start-date\>] [\<end-date\>] | dump station list in Hypo200 format |  
| [stainfo](manpage-stainfo.md) | -[ahqt] [-d dbase] [-f type] \<station string\> | dump summary station info ordered by sta, net, location, seedchan | 
| [stadist](manpage-stadist.md) | -[h] [-d dbase] \<lat.dec\> \<lon.dec\> <br> -[h] [-d dbase] \<latdeg\> \<latmin\> \<londeg\> \<lonmin\> | dump a station list in distance order from this lat/lon | 
| [chanlist](manpage-chanlist.md) | -[h] [-d dbase] \<list-name\> | dump a list of channels defined by a "named channel list" |  
| [depthdatum](manpage-depthdatum.md) | -[h] [-d dbase] [-n avgcnt] [-v\|V] [-f id_file] \<evid\> | print station elevation datum data |  
| **Duty Review %Page Support** | | | 
| [acceptEvent](manpage-acceptEvent.md) | [-d dbase]  \<evid\> | mark event as reviewed | 
| [acceptTrigger](manpage-acceptTrigger.md) | [-d dbase] \<evid\> | mark trigger as reviewed | 
| [deleteEvent](manpage-deleteEvent.md)  | [-d dbase] \<evid\> | delete event or trigger | 
| [undeleteEvent](manpage-undeleteEvent.md) | [-p] [-d dbase] \<evid\> | undelete event or trigger | 
| [alarmSend](manpage-alarmSend.md) | [-d dbase] \<evid\> | send alarms | 
| [alarmCancel](manpage-alarmCancel.md) | [-d dbase] \<evid\> | cancel alarms |  
| [uncancel](manpage-uncancel.md) | [-h] [-q] -[D\|F] [-d dbase] \<evid\> | updates db rows, for input evid having an action_state 'CANCELLED' to the action_state='!CANCELLED' |  
| [finalizeEvent](manpage-finalizeEvent.md) | [-d dbase] \<evid\> | finalize the event | 
| [cubeMessage](manpage-cubeMessage.md) | [-dehvCDEHLP][-d dbase] \<evid\> [\<comment\>] | return a Cubic message for this evid | 
| [cubeTowns](manpage-cubeTowns.md) | [-d dbase] | print the list of towns in the db(GazetteerPt table) in CUBE file format | 
| **Process Control %System (PCS)** | | | 
| [puttrans](manpage-puttrans.md) | [-d dbase] [-s char] \<oldGrp\> \<oldSrc\> \<oldState\> \<oldResult\> [\<newGrp\> \<newSrc\> \<newState\> \<newRank\> [\<newResult\>]] | create transition definition | 
| [deltrans](manpage-deltrans.md) | [-d dbase] [-s char] \<oldGrp\> [\<oldSrc\>] [\<oldState\>] [\<oldResult\>] [\<newGrp\>] [\<newSrc\>] [\<newState\>] [\<newRank\>] [\<newResult\>] | delete a transition definition | 
| [gettrans](manpage-gettrans.md) | [-d dbase] [-q] [-s char] [\<group\>] [\<table\>] [\<stateold\>] | show transition definitions | 
| [getstates](manpage-getstates.md) | [-d dbase] [-c\|C] [-iI] [-q] [-r] [-n] [[group] [table] [state] [rank] [result]] | show posted events | 
| [getStatesAllDbs](manpage-getStatesAllDbs.md) | | run 'getstats' for all databases |  
| [post](manpage-post.md) | [-d dbase] \<id\> \<group\> \<table\> \<state\> \<rank\> [\<result\>] | post event to a state | 
| [postEvidSince](manpage-postEvidSince.md) | [option-list] \< [days] or [startDate endDate]> <state> [rank] [source] [group] <br>options: [-h] [-d dbase] [-e types] [-m mag -M mag] [-s esrc -S osrc ] [-p] [-r] | post all non-bogus events within specified date range |  
| [postFromList](manpage-postFromList.md) | [-d dbase] \<id-file\> \<group\> \<table\> \<state\> \<rank\> [result] | post events listed in a file to a state | 
| [autoposter](manpage-autoposter.md) | [-d dbase] [-q] [-s char] [\<instancename\>] | print the row column data found in AUTOPOSTER database table | 
| [unpost](manpage-unpost.md) | [-d dbase] \<evid\> [ evid2 evid3 ...] | remove an event from all states | 
| [recycle](manpage-recycle.md) | [-d dbase] [-s char] \<oldGroup\> \<oldSource\> \<oldState\> \<oldResult\> [\<newGroup\> \<newSource\> \<newState\> \<newRank\> [\<newResult\>]] | change an existing state posting description to a new state posting description in database PCS_STATE table | 
| [delstates](manpage-delstates.md) | [-d dbase] [-i id] \| \<group\> [\<table\> \<state\> \<rank\> \<result\>] | remove an event from all states | 
| [transition](manpage-transition.md) | [-d dbase] [[\<group\> \<table\>] [state] [evid]] | transitions all non-zero result posts for input parameters |  
| [next](manpage-next.md) | [-d dbase] [-s secs] \<group\> \<table\> \<state\> | Get the next event in this state | 
| [result](manpage-result.md) | [-d dbase] [-r rank] \<id\> \<group\> \<table\> \<state\> \<result\> | set event's result value | 
| [resultAllIds](manpage-resultAllIds.md) | [-h] [-d dbase] \<group\> \<table\> \<state\> \<result\> | sets the RESULT value of all PCS_STATE table rows matching the input state description | 
| [deleteWavefiles](manpage-deleteWavefiles.md) | [-d dbase] | remove the event directory found under the wavefile root path | 
| [enabledForPCS](manpage-enabledForPCS.md) | [-d dbase] \<appName\> \<pcsState\> | prints true if PCS application mapped to input 'appName' |  
| **Post Processing Tasks** | | | 
| [ampgenpp](manpage-ampgenpp.md) | -[h] [-p property-file] \<evid\> | create amps and write to dbase | 
| [ampgenPPcont](manpage-ampgenPPcont.md) | [-d -c -l -v] [prop-file] | continuously running script to create amps and write to dbase | 
| [ampgen2shake](manpage-ampgen2shake.md) | -[h] [-p propfile] [-s] \<evid\> | create amps and if successful, execute shakemap alarm scripts |  
| [rcgone](manpage-rcgone.md) | -[h] [-d dbase] [-q -o -r -w -s] \<evid\> | generate request cards (archive waveform requests) for evid | 
| [newtrig](manpage-newtrig.md) | -[options] [ \<lat\> \<lon\> [z] [magValue] [bogusFlag] [selectFlag] [etype] [\#locPh] [\#magSta] [locRMS] [magRMS] ] | create a new subnet trigger, or with option -i use an existing evid | 
| [makeChannelRequest](manpage-makeChannelRequest.md) | [-h] [-d dbase] [-r] \<evid\> \<n.s.c.l\> \<ondate\> \<duration\> | Create a request_card table row for evid and net.sta.seedchan.location having specified window starting datetime and duration | 
| [requestdump](manpage-requestdump.md) | [-h] [-s names] [-d dbase] [-c] [ -aA \| [evid]] | print a summary of REQUEST_CARD table data of request type 'T' |  
| [deleteRequests](manpage-deleteRequests.md) | [-d dbase] evid | delete request_card table rows associated with the input event evid |  
| **Miscellaneous Utilities** | | | 
| [masterrt](manpage-masterrt.md) | | output default master RT database alias name | 
| [masterdb](manpage-masterdb.md) | | output default postproc archive database alias name | 
| [dbinfo](manpage-dbinfo.md) | [key] | print database username, password and name | 
| [showlocks](manpage-showlocks.md) | [-d dbase] | show events locked by Jiggle | 
| [unlockevent](manpage-unlockevent.md) | [-d dbase] \<evid\> | unlock an event locked by Jiggle | 
| [epoch](manpage-epoch.md) | | show system time in epoch seconds | 
| [epochdate](manpage-epochdate.md) | [-l leap_true_epochseconds] [-n unix_nominal_epochseconds ] [-s\|-S datestring] | show : Epoch (unix, leap)=(\<unix-epochsecs\>, \<leap-epochsecs\>) (date-utc = date-local) |  
| [whereIs](manpage-whereIs.md) | [-d database]  \<lat\> \<lon\> | print the locale of the input lat,lon, using database WHERES package | 
| [whereIsEvent](manpage-whereIsEvent.md) | [-d database] \<evid\> | print the locale of the input evid, using database WHERES package | 
| [hypolocate](manpage-hypolocate.md) | -[h][-P] [-d dbase] [-o\|O]  [-f arcfile] [-u URL] [-p port] [-x] \<evid\> | locate this event with a [SolServer](https://aqms-swg.gitlab.io/aqms-docs/man-solserver.html) | 
| [gazregions](manpage-gazregions.md) | [-h] [-d dbase] [-n names] [-N] [-q]  [lat lon] | list name and coordinates of regions found in gazetteer_region table |  
| [magprefrules](manpage-magprefrules.md) | [-h] [-d dbase] [-r name] [-t magtype] | print summary listing of magprefpriority table rules | 
| [orgprefrules](manpage-orgprefrules.md) | [-h] [-d dbase] [-r name] [-t type] | print summary listing of orgprefpriority table rules | 
| [waveroots](manpage-waveroots.md) | [-h] [-d dbase] [-t T\|C ] [-c wcopy\# ] | list the database waveroots table rows | 
| [adminMail](manpage-adminMail.md) | | send a message about a runtime code/cfg change on this host to $TPP_ADMIN_MAIL addresses | 
| [amPrimaryDb](manpage-amPrimaryDb.md) | [-d dbase] | returns true if the connected database's role is set primary in RT_ROLE table | 
| [amPrimaryHost](manpage-amPrimaryHost.md) | [-d dbase] [hostname] | returns current role value from AQMS_HOST_ROLE table for hostname | 
| [cleanup](manpage-cleanup.md) | \<dir\> \<days\> [find command options in quotes] | remove all non-executable type files |  
| [dbidrivers](manpage-dbidrivers.md) | | prints the available perl DBI drivers | 
| [showdbdrivers](manpage-showdbdrivers.md) | | prints a the available perl DBI drivers and the DBI->data_sources(driver) |  
| [getHostRole](manpage-getHostRole.md) | [-h] [-a] [-A] [-qr] [-d \<dbase\>] [-n host] | list host row column values found in AQMS_HOST_ROLE table |  
| [getAppHostRole](manpage-getAppHostRole.md) | [-h] [-a] [-A] [-H] [-d \<dbase\>] [-n host] [-p appname] [-s state] [-m mode A,P, or S] | list host row column values found in APP_HOST_ROLE table | 
| [setHostAppRole](manpage-setHostAppRole.md) | [-h] [-d dbase] \<[-f fname] or [\<host\> \<appname\> \<state\> \<role_mode\> [ondate] [offdate]]\> | Insert a APP_HOST_ROLE table row | 
| [setHostRole](manpage-setHostRole.md) | [-h] [-d <dbase>] \<host\> \<role-code\> | changes the PRIMARY_SYSTEM role status for the named host in the in AQMS_HOST_ROLE table | 
| [setHostPairRoles](manpage-setHostPairRoles.md) | [-h] [-d \<dbase\>] \<primary-host\> \<shadow-host\> | changes the PRIMARY_SYSTEM role status for a matched pair of hosts in AQMS_HOST_ROLE table | 
| [deleteAppRole](manpage-deleteAppRole.md) | [-h] [-d dbase] [-a] \<host\> [\<appname\>] [\<state\>] | delete matching APP_HOST_ROLE table rows |
| [getHostsForAppState](manpage-getHostsForAppState.md) | [-d dbase] \<pcsAppName\> \<pcsState\> [role] | return hosts enabled in the db for named application and state | 
| [localHostAppRole](manpage-localHostAppRole.md) | [-d dbase] \<appName\> \<pcsState\> | prints the role flag mapped to the localhost, application name and PCS state configuration | 
| [getDbRole](manpage-getDbRole.md) | [-h] [-A] [-d \<dbase\>] [-q] | list row column values found in database RT_ROLE table | 
| [evids2cmd](manpage-evids2cmd.md) | [-f filename] \<command\> [list of evid arguments ...] | executes command specified for one or more evid | 
| [help4tpp](manpage-help4tpp.md) | | invokes the '-h' option for all scripts under bin | 
| [hexify](manpage-hexify.md) | -[h] \<string\> | hexify an ASCII string |  
| [psgrep](manpage-psgrep.md) | [-h] \<string\> | Print a list of those process that contain the input string |   
| [showPPcont](manpage-showPPcont.md) | | lists process table info for configured job-descriptions | 
| [killPPcont](manpage-killPPcont.md) | | kills configured aqms (postproc) processes that normally run as crontab jobs |  
| [killPPgrep](manpage-killPPgrep.md) | \<grep-string\> | kill one or more aqms pids returned by 'pgrep -f grep-string' | 
| [killPPjob](manpage-killPPjob.md) | \<pid\> | kill process whose pid matched first input arg pid |   
| [lsInstalledPm](manpage-lsInstalledPm.md) | [grep-string (e.g. a module name)] | list perl modules found in the @INC paths |   
  
### Installation  
  
**Installation**  
  
Steps in installing the utility scripts:  
  
*  Choose a target directory, e.g. /app/aqms/tpp/bin. 
*  Download the distribution from gitlab. 
*  Untar the distribution into the target directory
*  Add required Environmental Variables (see below) 
  
  
**Requirements**  
  
*  Perl 5, DBI package
*  Oracle client and valid tnsnames.ora file
*  Define environmental variables:
    *  TPP_HOME - the directory above where the scripts are located
    *  TPP_BIN_HOME - where the scripts are located 

  
**Config files**  
  
%Configuration files are typically installed in a directory paralle to the *bin* directory, e.g. /app/aqms/tpp/cfg.  
  
The config files containing database connection information are the most used. They are present under the *dbinfo/cfg* folder.  
  
  
*rtdblist.cfg*  
  
Contains the names of the local RT dbases. For Oracle, these names must be defined in Oracle’s tnsnames.ora file.   
  
Example  
  
```
# List of RT dbases
# Will be scanned in this order to determine which is primary.
pacificdb
atlanticdb
```  

*masterdblist.cfg* and *masterinfo.cfg*  
  
Contains dbase connection info that is used by all post-processing scripts. The username defined should have write privileges.  
  
Two dbases are defined:  
  
  *  the master RT dbase  
  *  the master post-processing dbase.   
  
The scripts generally use only the dbase names and look up other connection info using the local Oracle client’s tnsnames.ora file.   
The masterinfo file should be updated to list the current masters when there is a role change. This can be automated with a chron job.     
  
Example masterdblist.cfg  
  
```
#List archvive database aliases from TNSNAMES.ora used for PCS processing
#Used by DbConn.pm lookup function
archdbw
archdbe
archdbc1
```
    
Example masterdbinfo.cfg
  
```
# master RT system 
masterrtuser some user
masterrtpwd some password
# master Dbase system 
masterdbuser some user
masterdbpwd some password
```
