# help4tpp {#help4tpp}  
  
Invokes the '-h' option for all *.sh and *.pl scripts found in /app/aqms/tpp/bin.  

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

help4tpp.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Invokes the '-h' option for all *.sh and *.pl scripts found in /app/aqms/tpp/bin.  

## HOW TO RUN  
  
```
usage: ./help4tpp 

Invokes the '-h' option for all *.sh and *.pl scripts found in /app/aqms/tpp/bin.
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues