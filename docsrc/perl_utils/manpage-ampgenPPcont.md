# ampgenPPcont {#ampgenPPcont}  
  
Continuously running script to generate strong motion amps for events    

## PAGE LAST UPDATED ON

2021-06-21  
  

## NAME  

ampgenPPcont.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Continuously running script to generate strong motion amps for events. Acts on events posted to the configured PCS state description (e.g ampgenPP).  
  
If a prop-file is unspecified, it defaults to ../cfg/ampgenPPcont.props. This script expects *ampgenpp.pl* script to be in same bin directory.


## HOW TO RUN  
  
```
Usage $0  [-d -c -l -v] [prop-file]
   -d dbase, default for PCS when not in props
   -c cutoff maxDays when not in props
   -l alternative logfile, defaults to ampgenPPcont.log
   -v verbose output (e.g command strings and current properties)
   -h usage

DEFAULT propFile when not specified on command line is: ampgenPPcont.props
Default values below are used when NOT defined in property file:
 pcsDbase         $masterdb value or -d switch override
 pcsGroup         EventStream
 pcsTable         the pcsDbase value
 pcsState         ampgenPP 
 pcsResult        1
 pcsStallTime     0   seconds
 sleepTime        60  seconds
 archivalTime     600 seconds
 maxDaysBack      0   no cutoff age
 minMag           3.4
 noop             0   always process, =1 do not process posts, only for testing
 reconfig         0   do not reload properties after sleep between next post polls
 rotateLogsDaily  0   where =0 no, =1 yes\n";

 pcsAppName = ampgenPPcont
 # NOTE: vars values are substituted at time of execution, if pcsCommmand property is not specified defaults to:
 pcsCommand = '$ENV{TPP_HOME}/jiggle/bin/javarun_jar.sh -H -v -TampgenPP org.trinet.apps.AmpGenPp $evid $pcsAppProps'
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues