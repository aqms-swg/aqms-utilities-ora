# deltrans {#deltrans}

Delete a transition definition.   

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

deltrans.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Delete a PCS transition rows from PCS_TRANSITION table.  
Specified string elements can contain db wildcards ( Oracle: % or _ ).  

## HOW TO RUN

```
usage: ./deltrans [-d dbase] [-s char] <oldGrp> [<oldSrc>] [<oldState>] [<oldResult>] [<newGrp>] [<newSrc>] [<newState>] [<newRank>] [<newResult>]

Delete a PCS transition rows from PCS_TRANSITION table.
Specified string elements can contain db wildcards ( Oracle: % or _ ).

    -h        : print this usage info
    -d dbase  : alias to use for db connection (defaults to "archdbe")
    -s char   : optional character delimiter between the old and new state descriptions
                if not specified, default allows '=', ':' or ' '.

example: ./deltrans -d mydb2 TPP TPP INTERIM 1  EventStream mydb2 FocMecAr 100 
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


