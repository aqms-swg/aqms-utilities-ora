# deleteWavefiles {#deleteWavefiles}  
  
Remove the event directory found under the wavefile root path for all event evid posted to PCS state description: DB_CLEANUP DEFAULT TRIGDEL.   
  

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

deleteWavefiles.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Remove the event directory found under the wavefile root path for all event evid posted to PCS state description: DB_CLEANUP DEFAULT TRIGDEL.

## HOW TO RUN  
  
```
usage: ./deleteWavefiles [-d dbase] 
     
Remove the event directory found under the wavefile root path for all
event evid posted to PCS state description: DB_CLEANUP DEFAULT TRIGDEL.

-h        : print this usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

example: ./deleteWavefiles -d k2db  
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues