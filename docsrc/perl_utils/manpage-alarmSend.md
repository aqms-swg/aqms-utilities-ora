# alarmSend {#alarmSend}

Send alarms 

## PAGE LAST UPDATED ON

2021-06-07  

## NAME

alarmSend.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Send alarms for an event.   

## HOW TO RUN

```
usage: ./alarmSend [-d dbasename]  <evid>
     
-h        : print usage info

-F        : posts input evid to state "TPP TPP FINALIZE"
            default action is posting to "TPP TPP DISTRIBUTE"

-d dbase  : use this particular dbase (defaults to "archdbe")

example: ./alarmSend -d k2db 8735346
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


