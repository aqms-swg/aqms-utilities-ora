# evids2cmd {#evids2cmd}  
  
Executes the command specified as the first input argument for one or more event evid.   

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

evids2cmd.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Executes the command specified as the first input argument for one or more event evid.   
  
The command argument MUST be surrounded by single-quotes and must have a variable named $evid in the appropriate place in the command string.  
  
The command is run in a loop for all evid values that either follow the command argument, or are instead parsed as the first token of lines read from a file specified with the -f option. The $evid placeholder in the command string is replaced with each evid value read.     

## HOW TO RUN  
  
```
usage: ./evids2cmd [-f filename] <command> [list of evid arguments ...]

Executes the command specified as the first input argument for one
or more event evid. 

The command argument MUST be surrounded by single-quotes and must have a
variable named $evid in the appropriate place in the command string.

The command is run in a loop for all evid values that either follow the
command argument, or are instead parsed as the first token of lines read
from a file specified with the -f option. The $evid placeholder in the
command string is replaced with each evid value read. 
    
-h        : print this usage info
-v        : verbose - prints the command line executed for each evid

-f file   : parses the evid from the first column of each line in the
            specified file instead of from the command line. Lines that
            begin with a '#' or that are whitespace are skippped over.

example: ./evids2cmd -f idfile.txt 'cattail -d mydb -I $evid' 
            ./evids2cmd 'cattail -d mydb -I $evid' 1234 1234 1236 1237
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues