# psgrep {#psgrep}  
  
Print a list of those process descriptions returned by 'ps -ef' that contain the input string (case insensitive).  

## PAGE LAST UPDATED ON

2021-06-14  
  

## NAME  

psgrep.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Print a list of those process descriptions returned by 'ps -ef' that contain the input string (case insensitive).
  

## HOW TO RUN  
  
```
usage: ./psgrep [-h] <string>

Print a list of those process descriptions returned by 'ps -ef'
that contain the input string (case insensitive). Input string
should be a command, username, pid, or command argument.

-h               : print usage info

example: ./psgrep "some processname"
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues