# hypolocate {#hypolocate}

Locate this event with a [SolServer](https://aqms-swg.gitlab.io/aqms-docs/man-solserver.html)  

## PAGE LAST UPDATED ON

2021-06-09  

## NAME

hypolocate.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Locate the event found in the db with input evid using a solserver.

## HOW TO RUN

```
usage: ./hypolocate -[h][-P] [-d dbase] [-o|O]  [-f arcfile] [-u URL] [-p port] [-x] <evid>
     
Locate the event found in the db with input evid using a solserver.

-h         : print usage info
-d dbase   : alias to use for db connection (defaults to "archdbe")
-f arcfile : read specified ARC input file (default: query db for location/phase data).
-u URL     : URL of remote SolServer (default = lhotse.gps.caltech.edu)
-p port    : port number for the remote SolServer (default = 6501)
-P         : Hypoinverse results in PRT format (default = ARC)
-o         : print output to ./<evid>.hinvout
-O file    : print output to the named file, trumps -o
-x         : debug, prints solserver URL and input string sent to solserver

example: ./hypolocate -d k2db -u luxor.usgs.gov -p 6501 9277430
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


