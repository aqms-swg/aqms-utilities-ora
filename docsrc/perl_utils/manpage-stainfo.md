# stainfo {#stainfo}  
  
Prints summary station info ordered by sta, net, location, seedchan.      
  

## PAGE LAST UPDATED ON

2021-05-28  
  

## NAME  

stainfo.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Prints summary station info ordered by sta, net, location, seedchan.    
  

## HOW TO RUN  
  
```  
usage: ./stainfo -[ahqt] [-d dbase] [-f type] <station string>
```
  

Prints summary station info ordered by sta, net, location, seedchan. All formats include lat,lon, and elevation. Default includes channel ondate, offdate, and site name.  
      

    -h        : print this usage
    -d dbase  : alias to use for db connection (defaults to "archdbe")

    -q        : do not write column header for default format
    -a        : show active stations only
    -f type   : alternative output formats (other than default)
                hypo  = Hypoinverse station list format
                kml   = KML format (one placemark per station)
                gmt   = GMT plotting format 
                adhoc = "adhoc" list format includes site name 


\<Station string\> input argument can be in various forms, case insensitive. All SQL wildcards are allowed, as is "\*"; shell requires you to quote strings with glob wildcards like "*".  
  
1) STA code only  (e.g. "SJE", "S__")  
2) NT.STA         (e.g. "NC.SJE", "CE.", "NP.26%")  
3) NT.STA.CHL     (e.g. "NC.SJE.HHZ", "CE.%.hn_")  
4) NT.STA.CHL.LC  (e.g. "NC.SJE.HHZ.--", "CE.%.hn_.0%")  
      

example: ./stainfo -d mydb "ZY.*"  

  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues