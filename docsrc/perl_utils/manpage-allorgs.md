# allorgs {#allorgs}

Show info about the origins associated with event evid.

## PAGE LAST UPDATED ON

2021-06-09  

## NAME

allorgs.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Show info about the origins associated with event evid.  
The event preferred origin is the one flagged with "<----" after its lddate.  

## HOW TO RUN

```
usage: ./allorgs [-h] [-q] [-e] [-w] [-d dbase] <evid>
     
Show info about the origins associated with event evid.
The event preferred origin is the one flagged with "<----" after its lddate.

The orid values is printed at the end of the output line.

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-e        : include event evid at end of output lines
-w        : include credit attribution (who) commited origin
-q        : do not print title header

example: ./allorgs -d mydb -w 8735346
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


