# showPPcont {#showPPcont}  
  
Lists process table info for configured job-descriptions.    

## PAGE LAST UPDATED ON

2021-06-14  
  

## NAME  

showPPcont.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Lists process table info for configured job-descriptions.    
  

## HOW TO RUN  
  
```
usage: ./showPPcont

Lists process table info for configured job-descriptions:


pgrep -l -u aqms -f "makeDRPcon[t]"
pgrep -l -u aqms -f "makeDRPScon[t]"
pgrep -l -u aqms -f "makeTRGcon[t]"
pgrep -l -u aqms -f "qml2pdlCon[t]"
pgrep -l -u aqms -f "pcsCancelCon[t]"
pgrep -l -u aqms -f "pcsFinalizeCon[t]"
pgrep -l -u aqms -f "exportWFcon[t]"
pgrep -l -u aqms -f "solserve[r]"
pgrep -l -u aqms -f "exportamp[s]"
pgrep -l -u aqms -f "[D]importamps"
pgrep -l -u aqms -f "xdata/bin"
pgrep -l -u aqms -f "[D]RCG_V0"
pgrep -l -u aqms -f "[D]RCG_RT"
pgrep -l -u aqms -f "[D]RCG_TRIG"
pgrep -l -u aqms -f "heli_ewI[I]"
pgrep -l -u aqms -f "eremarkCon[t]"
pgrep -l -u aqms -f "fpfitCon[t]"
pgrep -l -u aqms -f "fmarCon[t]"
pgrep -l -u aqms -f "redoWfsCon[t]"
pgrep -l -u aqms -f "trigAmpsCon[t]"
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues