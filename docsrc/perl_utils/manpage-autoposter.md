# autoposter {#autoposter}

Print the row column data found in AUTOPOSTER database table.  

## PAGE LAST UPDATED ON

2021-06-09  

## NAME

autoposter.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Print the row column data found in AUTOPOSTER database table.  

## HOW TO RUN

```
usage: ./autoposter [-d dbase] [-q] [-s char] [<instancename>]
     
Print the row column data found in AUTOPOSTER database table.
User optional input arguments to filter by column matching input,
interprets a sql wildcard '%' or '_' in input argument strings.

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-q        : do not print title header

example: ./autoposter -d mydb NewEventPoster
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


