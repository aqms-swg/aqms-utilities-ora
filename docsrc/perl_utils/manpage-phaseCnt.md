# phaseCnt {#phaseCnt}  
  
Print count of phases associated with the preferred origin of evid 

## PAGE LAST UPDATED ON

2021-06-14  
  

## NAME  

phaseCnt.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Print count of phases associated with the preferred origin of the event matching the input evid.  
  
By default, prints count of all phases regardless of their weights.  
  

## HOW TO RUN  
  
```
usage: ./phaseCnt [-h] [-d dbase] [-iozpsPS] <evid>
     
Print count of phases associated with the preferred origin of the event 
matching the input evid.

By default, prints count of all phases regardless of their weights.
Use one of the optional switches to filter by weights and/or phase type.

-h        : prints this usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

Use one of the switches below to filter count by phase and/or weight types.
-i        :   phases with in_wgt > 0.
-o        :   phases with wgt > 0.
-z        :   phases with wgt = 0.
-p        : P phases with in_wgt > 0.
-s        : S phases with in_wgt > 0.
-P        : P phases with wgt > 0.
-S        : S phases with wgt > 0.

example: ./phaseCnt -d mydb -o 1343331
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues