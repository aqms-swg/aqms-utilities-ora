# isInsideNetwork {#isInsideNetwork}  
  
Prints 1 if event is located inside event auth network region, 0 otherwise.  

## PAGE LAST UPDATED ON

2021-06-11  
  

## NAME  

isInsideNetwork.pl     


## VERSION and STATUS

status: ACTIVE  
  

## PURPOSE
  
Prints 1 if event is located inside event auth network region, 0 otherwise.  

## HOW TO RUN  
  
```
usage: ./isInsideNetwork [-h] [-d dbase] [-n name] <evid>
     
Prints 1 if event is located inside event auth network region, 0 otherwise.

-h        : print usage info

-d dbase  : alias to use for db connection (defaults to "archdbe")
-n name   : name of region polygon in gazetteer_region table
            default is event.auth code

example: ./isInsideNetwork -d k2db 9277430
```  
  
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues