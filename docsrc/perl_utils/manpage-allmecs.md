# allmecs {#allmecs}

Show info about the mechanisms associated with all event origins.

## PAGE LAST UPDATED ON

2021-06-09  

## NAME

allmecs.pl  

## VERSION and STATUS

status: ACTIVE    

## PURPOSE

Show info about the mechanisms associated with all event origins.  
The event preferred mec is the one flagged with "<----" after its lddate.  

## HOW TO RUN

```
usage: ./allmecs [-d dbase] [-w] [-q] <evid>
     
Show info about the mechanisms associated with all event origins.
The event preferred mec is the one flagged with "<----" after its lddate.

The mecid/oridin/oridout values are printed at the end of the output line.

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-w        : include credit attribution (who) commited mec
-q        : do not print title header

example: ./allmecs -d mydb -w 8735346
```


## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues


