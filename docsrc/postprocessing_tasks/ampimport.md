# %Amplitude Import {#ampimport}  
  
%Amplitude data are imported in [CISN Ground Motion Message format](https://aqms-swg.gitlab.io/aqms-docs/cisn-ground-motion-format.html) format.  

\subpage ampexc

Imported amplitudes are associated with events like so.    
- \subpage amp-association