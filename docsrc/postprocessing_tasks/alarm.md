# alarm {#alarm}  
  
Utilities to  
*  send alarm messages via CMS for a new or modified event  
*  send "cancel" alarm messages for alarm systems  
*  send a  CUBE event(UPDATE) msg to EIDS polling directory
*  "send" a CUBE D event (CANCEL) msg to EIDS polling directory  
  

Requires *CMS*  
  

Sub-directory *bin* has the executable scripts while *cfg* has the configuration files.   
  
*  ``bin/alarmsendCMS.sh`` : Ssend alarm messages via CMS for a new or modified event  
*  ``bin/alarmcancelCMS.sh`` : Send "cancel" alarm message.  
*  ``bin/alarmcancelone.sh`` : Send "cancel" alarm messages for evid on one db host.   
*  ``bin/sendCUBE2EIDS.sh`` : Send a CUBE event(UPDATE) msg to EIDS polling directory.      
*  ``bin/cancelCUBE2EIDS.sh`` : Send a CUBE D event (CANCEL) msg to EIDS polling directory.  
*  ``bin/makeCMScfg.sh`` : Generate *cms.cfg*
*  ``bin/makeAlarmCfgCMS.sh`` :  Generate *cms_dbname.cfg* i.e. *cms.cfg* for an input database.
*  ``bin/rotate-logs.sh`` : Log file rotator  

<br>  

*  ``cfg/cms_dbname.cfg`` : *cms.cfg* for database named *dbname*   
*  ``cfg/sendCMS_dbname.cfg`` : Configuration file for *alarmsendCMS.sh*  
*  ``cfg/cancelCMS_dbname.cfg`` : Configuration file for *alarmcancelCMS.sh*  
*  ``cfg/alarm-cancel.cfg`` : Holds a list of databases, sourced by *alarmCancelCMS.sh*          
*  ``cfg/alarm-user.cfg`` : Holds the logging verbosity and location for configuration file holding database authentication.  
*  ``cfg/db_auth_pp_alarm.d`` : Database username and password.    