# Hypomag {#hypomag}  
  
Hypomag is a Java module for batch relocation and/or magnitude processing of a set of archived datacenter events whose event ids are read from an ASCII file, or are obtained by database query using event selection properties, or are those matching a specified state description in the PCS_STATE table. See PCS and ​hypomag.props for more details.   
  
Sub-directory *bin* has the executable scripts while *cfg* has the configuration files.  

### *bin*

*  `makePickerParms.pl` : Generates the parameter file for Jiggle jar hypomag style picker.  
*  `refreshPickerParms.sh` : Usually run as a cron job. Generate Jiggle picker parameters for active channels in applications table list.  
*  `refreshRcgOnlyRt.sh` : Usually run as a cron job. Replace the RCG-ONLY-RT list of channels in the appchannels table. This is a custom script created for the CI network and may not be applicable to other AQMS network as-is.  
*  `refresh_rcg_only_rt.sql` : SQL for refreshRcgOnlyRt.sh.  
*  `runHypoMag.sh` and `bin/runHypoMag4Posted.sh` : Start a background process that processes `<pcsGroupName> <dbase> <pcsStateName>` : e.g.  `EventStream <dbase> hypomagML`  
*  `startHypoMagCont.sh` : Usually run as a cron job. Start/restart the continuous hypomag jobs.  
*  `startHypoMagJobs.pl`: Usually run as a cron job. Start up all hypomag job.    
*  `stopHypoMagJobs.pl` : Stop HypoMag jobs.  
*  `runAlreadyPosted.sh` : For already posted ids run in background using hypomagML.props.  
  
<br>  
  

### *cfg*   
  
The properties in the configuration files mentioned below are defined here : https://aqms-swg.gitlab.io/jiggle-userdocs/properties/#alphabetical-list-of-all-jiggle-properties  
   

*  `hypomagML.props`,`hypomagMAG.props`,`hypomagLOC.props` : The primary config file for hypomag. Each of the config allows hypomag to be run in a different mode, depending on the property *pcsProcessorModeName*. ML calculcates magnitude and location, MAG calculats only magnitude and LOC calculates only location.  
*  `delegateCISN_ML.props`, `delegateCISN_MAG.props`, `delegateCISN_LOC.props` : The secondary config file name that defines the model and its params like Power Law model. This is set in `hypomag.props` in the property named *pcsEngineDelegateProps*.  
*  `logEng.props` : The config file name that is used for hypoinverse. Similar to the above two config files, users can create different versions of the config file, each containing different location engines.
*  `mlMagEng.props` :  The config file to set properties for calculating the magnitude. Similar to the above config files, users can create different versions of the config file, each containing different magnitude methods. 
*  `pickEW.parms` : Holds the list of channels used when hypomag runs in picker mode. Generated via cron using `picker-parms.sql`.  
*  `picker-parms.sql` : The sql used to generate `pickEW.parms`.  
*  `pickEW.props`: The config file used when hypomag runs in picker mode.    
*  `waveformModel.props` : The config file related to windowing of channel waveforms.  
*  `pickerParmsLocalTargetURL.cfg` : The config file to use to publish `pickEW.parms` for external users. For e.g. at CIT, the parms are available for download via a web page.    
*  `pcs-cfg.props` : The config file that tells hypomag which pcs thread name to look at for processing.  
*  `jobsHypoMag.cfg` : Contains a list of hypomag property files. Used by `bin/startHypoMagJobs.pl`. When multiple versions of hypomag are run at an RSN, this config file allows all of them to be started or stopped simultaneously.  

 
