# How to implement webicorder {#how-to-webicorder}
  
The implementation done at the SCSN is shown below and can be used as an example.  
  
Winston waveservers are hosted on *someserver1* and *someserver2* for BHZ channels only (40sps).   
  
The software that creates the images and publishes them runs on *someserver3* in dir: */app/aqms/tpp/webicorder/bin/* as user *aqms*.   
  
*  ``startWebicorder.sh`` : Chron starter script runs every 15 minutes. This is just a c-shell wrapper to give context for running the Perl script ``heli_maint.pl``.   
*  ``heli_maint.pl`` - Runs each chron interval inside startWebicorder.sh  
  
  *  Checks if ``heli_ewII`` is running. If not, it is started.  
  *  Cleans up the old gif files  
  *  renames welcome.html to index.html  
  *  rsync the latest gif and html files to the webservers by running ``/app/aqms/tpp/webicorder/bin/syncHeli.sh``  
  *  write a log entry to heli_start.log.    

*  ``heli_ewII`` : Official Earthworm plotting program (compiled C code) which runs continuously. Creates .gif image files.   %Configuration file is heli_ewII.d (see below). NOTE: the maximum number of channels allowed is 100.  

  *  **Manual run** : ``/app/aqms/tpp/webicorder/bin/heli_ewII heli_ewII.d``  
  *  **Output Dir** : ``/app/aqms/tpp/webicorder/output`` (Defined by the “GifDir” command)       

*  ``syncHeli.sh`` : The Webicorder application runs on backend server. This script ensures webicorder plots are copied to public web servers for public view, as detailed in *webicorder/cfg/syncHeliDest.cfg*    
  
  
### Webicorder cron job  
  
```
# set of BH channel gifs from heli_ewII+waveservers copied to www/webicorder (uses no db access)
 #NOTE: make interval >= the UpdateInt value in the heli_ewII.d, below is every 5 minutes
 0,5,10,15,20,25,30,35,40,45,50,55 * * * * /app/aqms/tpp/webicorder/bin/startWebicorder.sh -c 7 >/dev/null 2>&1
 # once a day update the BHZ plot_station2.d list
 1 7 * * * /app/aqms/tpp/webicorder/bin/updatePlotStation.sh >/dev/null 2>&1
```
  
```
# Kill the heli_ewII process on the 2nd Sun in March and 1st Sun in Nov
 # for Daylight Savings.  We need to do this so that the timezone will
 # be updated on the webicorder output.
 # right before the mid-day change.
 54 15 * mar sun [ $(date +\%d) -gt 07 ] && [ $(date +\%d) -le 14 ] && /usr/bin/pkill heli_ewII
 54 15 * nov sun [ $(date +\%d) -le 07 ] && /usr/bin/pkill heli_ewII
```

### Notes  

  *  There is a hardwired limit of 100 channels in ``heli_ewII.c``  
  *  You must restart heli_ewII if you change the config (``heli_ewII.d``) file.  
  *  The optional ``indexa.html`` and ``indexb.html`` files must be in the directory defined by the GifDir command.  
  *  The ``indexa.html`` is just pasted, verbatim, on the top of html and so must have all the necessary header syntax. The ``indexb.html`` must have the correct closing syntax. They are reread each time the code runs so edits to them will take effect without restarting the program.  
  *  The local time on the plots is hardwired in the ``heli_ewII.d`` file. It will not switch between standard and daylight savings time automatically.  
  

### Example heli_ewII.d configuration file
  
```
#
# This is the heli_ewII parameter file. This program gets data gulps
# from the waveserver(s), and creates helicorder displays.
# Derived from heli_ew and heli_standalone

# This program runs either standalone or as an Earthworm module.
# If any of the four Earthworm paramaters below are stated, it will asssume
# that it's supposed to be a module. It will beat it's heart into the ring,
# and use the Earthworm logging scheme. Other wise, it runs standalone.
# LogSwitch 1
# MyModuleId MOD_HELI_EWII
# RingName ASSOC_RING
# HeartBeatInt  10

wsTimeout 30 # time limit (secs) for any one interaction with a wave server.

# List of wave servers (ip port comment) to contact to retrieve trace data.
WaveServer     someserver1 ip     portnumber     "someserver1"
WaveServer     someserver2 ip     postnumber     "someserver2"

# Directory in which to store the temporary .gif, .link and .html files.
GifDir   /home/drp/webicorder/output

# Redirect to file containing list of channels to plot (ONLY does first 100)
@/home/drp/webicorder/plot_station.d

    # *** Optional Commands ***

 Days2Save     7    # Number of days to display on web page; default=7
# Actually, "Days2Save" is really the number of GIFs of each SCN to save.
# For example, if HoursPerPlot is 12, you will save 7 gifs for 3.5 days.

 UpdateInt    15    # Number of minutes between updates; default=2
 RetryCount    2    # Number of attempts to get a trace from server; default=2
# Logo    smusgs.gif # Name of logo in GifDir to be plotted on each image

# We accept a command "Clip" which sets trace clipping at this many
# vertical divisions
Clip 5

# We accept a command "SaveDrifts" which logs drifts to GIF directory.
# SaveDrifts

# We accept a command "Make_HTML" to construct and ship index HTML file.
Make_HTML

# We accept a command "IndexFile" to name the HTML file.
# Default is "index.html"
IndexFile welcome.html

# We accept a command "BuildOnRestart" to totally rebuild images on restart.
#BuildOnRestart

# We accept a command "Debug" which turns on a bunch of log messages
Debug
#WSDebug
```