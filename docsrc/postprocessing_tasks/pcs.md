# Process Control %System (PCS) {#pcs}   
  
  *  [Model Overview](#pcs-model-overview)
    *  [APIs](#pcs-apis)  
    *  [Command Line & Scripting](#pcs-cmd-line-scripting)  
  *  [Elements of a PCS State](#pcs-state)
  *  [Example](#pcs-example)  
  *  [Rules of Thumb](#pcs-rules-of-thumb)  
  *  [Current Usage](#pcs-current-usage)  
  *  [The Autoposter](#pcs-autoposter)  
    *  [Add an Autoposter Entry](#pcs-add-autoposter-entry)  
    *  [PCS Transition Definitions](#pcs-trans-def)
  *  [Sending CMS from Post Processing](#pcs-cms-from-post-proc)  
  *  [Post Processing Data Flow at RSNs](#pcs-post-proc-data-flow)  
    *  [DRP Accept button](#pcs-drp-accept-btn)  
    *  [DRP Cancel button](#pcs-drp-cancel-btn)  
    *  [DRP Delete button](#pcs-drp-delete-btn)  
    *  [Jiggle Finalize butto](#pcs-jiggle-finalize-btn)  
      *  [Why do alarms only fire the first time Finalize is clicked?](#pcs-finalize-alarms)  
    *  [Jiggle Delete Event button](#pcs-jiggle-delete-ev-btn)  
    *  [New event arrives in archdb](#pcs-new-event-arrives)  
    *  [New subnet trigger arrives in archdb](#pcs-new-st-arrives)  
  *  [PDL sending example](#pcs-pdl-send-example)  

  
The purpose of the Process Control %System (PCS) is to allow the definition and automation of a series of data processing steps, a processing stream, where the result of each processing step determines the next step to be executed. Thus, the processing stream can be customized to meet a wide range of requirements. The PCS provides a uniform API that can be used by processing modules to determine what work to do and report results of work done.   
  
## Model Overview <a name="pcs-model-overview"></a>   
  
Central to this process control model are the concepts of processing "state" and "state transitions". A numeric id (e.g. EVENT.EVID) is associated with a state that corresponds to a logical processing step. The operator defines a processing stream; a series of processing steps where the result of each step determines the next step to be taken. This is accomplished by inserting an entry into the PCS_STATE table. If multiple state entries exist for a single id, then the rank assigned to will determine the order in which the processing steps are executed, a higher rank value execute first. As a processing step completes it reports a result code to the database, which might automatically transition the id to a new state and rank if such are defined by the operator in the PCS_TRANSITION table.   
  

### APIs <a name="pcs-apis"></a> 
  
The API is in the form of DBMS stored procedures and functions that can be called from SQL or a DBMS API (e.g. perl, Java, PHP)   
  

### Command Line & Scripting <a name="pcs-cmd-line-scripting"></a>   
  
The main PCS actions are encapsulted in command line wrappers command line wrappers???:  
  
**post**  
  
```
Post an event to some state in PCS
    
    usage: /app/postproc/bin/post [-d dbasename] <id> <group> <table> <state> <rank> [<result>]
     
     -h        : print usage info
     -d dbase  : use this particular dbase (defaults to "archdbe")

    example: /app/postproc/bin/post -d mydb  EventProc  Stream1  MakeReport 100
```
  
**postFromList**  
  
```
Posts every event evid found in the input file to the input state description.

The evid must be the first white-space delimited token of each line in the
input file. Lines not begining with a number > 0 will be skipped.  
  
usage: /app/aqms/tpp/bin/postFromList [-d dbase] <id-file> <group> <table> <state> <rank> [result]  
  
-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

-v        : verbose - print a line for each event posted
-s secs   : post each id at interval secs (sleep for specifed seconds)

example: /app/aqms/tpp/bin/postFromList -d mydb eids.txt EventStream archdb MakeGif 100
```
  
**unpost**  
  
```
Un-post an event in PCS. Delete all rows associated with one or more input evids in PCS_STATE table.  
    
    usage: /app/postproc/bin/unpost [-d dbasename] <id>  [ id2 id3 ...] 
     
     -h        : print usage info
     -d dbase  : use this particular dbase (defaults to "archdbe")

    example: /app/postproc/bin/unpost -d mydb 9377694 9377700
```
  
**delstates**  
  
```
Delete PCS_STATE table row entries matching the input state description.

    Input elements can contain a db wildcard (i.e. Oracle %  or _ ).
    Specifying -i <id> without any state elements deletes all postings 
    for that id (like unpost command).
    
    usage: /app/aqms/tpp/bin/delstates [-d dbase] [-i id] | <group> [<table> <state> <rank> <result>]
     
    -h        : print usage info
    -d dbase  : alias to use for db connection (defaults to "archdbe")
    -i id     : all rows that match input id (evid)
    -a #days  : only rows whose lddate is older than specified #days.

    example: /app/aqms/tpp/bin/delstates -d mydb  EventProc  Stream1  MakeReport 100
```
  
**next**  
  
```
Print the next event evid to be processed for input state description.

    The evid is parsed from the oldest row in the PCS_STATE table that matches
    the input description. No evid is returned if the same evid has an active
    post (result=0) for another state description at higher rank than the input
    state description (i.e. it is rank blocked).
    
    usage: /app/aqms/tpp/bin/next [-d dbase] [-s secs] <group> <table> <state>
     
    -h        : print this usage info
    -d dbase  : alias to use for db connection (defaults to "archdbe")

    -s secs   : print an evid only if its posting's age is >= secs

    example: /app/aqms/tpp/bin/next -d k2db -s 30 EventStream archdb MakeGif 
```
  
**getstates**  
  
```
List all postings in PCS_STATE table whose state description matches input arguments. 
When no input arguments, shows all post descriptions.

Unprocessed postings have a result =0, processed events usually have result =1,
and other result codes may flag processing error depending on the application.

usage: /app/aqms/tpp/bin/getstates [-d dbase] [-c|C] [-iI] [-q] [-r] [-n] [[group] [table] [state] [rank] [result]]

-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-c        : list row counts grouped by group,table,state,rank,result (without ids)
-C        : list row counts grouped by group,table,state (without ids)
-i id     : match specified posting id (event evid) 
-I        : sort output in id, group,table,state,rank,result,lddate order
            default sort is by group,table,state,rank,result,lddate,id 
-n count  : max number of rows returned from db query
-q        : do not print column name title line
-r char   : alternative character to flag unprocessed result, (e.g. '#', default=0)

example: /app/aqms/tpp/bin/getstates -d archdb TPP TPP FINALIZE

```
  
**result**  
  
```
Set the RESULT value of rows in PCS_STATE table matching the input state
description. When -r option is absent, result the highest rank row that
matches the input state description arguments.  
  
usage: /app/aqms/tpp/bin/result [-d dbase] [-r rank] <id> <group> <table> <state> <result>

-h        : print this usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")

-r rank   : for a post matching the input state description 
            > 0 set result only for the row with this rank value
            = 0 set result for all rows, any rank,  matching state 
            =-1 set result only for highest ranking row matching state
                (default)

example: /app/aqms/tpp/bin/result -d k2db 992875 EventStream k2db MakeGif 1
```
  
**puttrans**  
  
```
Insert a transition row rule into the PCS_TRANSITION table.
     
Requires at least 4 command line arguments. When the additional 8 or 9 arguments are absent
resulting the old state will causes that old row entry to be deleted from the PCS_STATE table.
If a "newResult" is not specified it will default to null (i.e. 0).
 
usage: /app/aqms/tpp/bin/puttrans [-d dbase] [-s char] <oldGrp> <oldSrc> <oldState> <oldResult> [<newGrp> <newSrc> <newState> <newRank> [<newResult>]]  

-h        : print this usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-s char   : optional character delimiter between the old and new state description groups
            if not specified, default is to allow '=', ':' or ' '

example: /app/aqms/tpp/bin/puttrans -d k2db TPP TPP INTERIM 1 EventStream mydb2 MakeGif 95
```  
  
**deltrans**  
  
```
Delete a PCS transition rows from PCS_TRANSITION table.
Specified string elements can contain db wildcards ( Oracle: % or _ ). 

usage: /app/aqms/tpp/bin/deltrans [-d dbase] [-s char] <oldGrp> [<oldSrc>] [<oldState>] [<oldResult>] [<newGrp>] [<newSrc>] [<newState>] [<newRank>] [<newResult>]  

-h        : print this usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-s char   : optional character delimiter between the old and new state descriptions
            if not specified, default allows '=', ':' or ' '.

example: /app/aqms/tpp/bin/deltrans -d mydb2 TPP TPP INTERIM 1  EventStream mydb2 FocMecAr 100 
```
  
**gettrans**  
  
```
Print the PCS transition row data found in PCS_TRANSITION database table.  

usage: /app/aqms/tpp/bin/gettrans [-d dbase] [-q] [-s char] [<group>] [<table>] [<stateold>]  
  
-h        : print usage info
-d dbase  : alias to use for db connection (defaults to "archdbe")
-q        : do not print title header
-s char   : character separating for old and new state descriptions
            defaults to '=', e.g instead use ':', ',', '+'

example: /app/aqms/tpp/bin/gettrans -d mydb EventStream
```
  

## Elements of a PCS State <a name="pcs-state"></a>   
  
There are five elements to a PCS processing state definition.  
  
  *  group
  *  source
  *  state
  *  rank
  *  result 

The first three are strings to form a "tri-tuple" to uniquely identify a processing thread. Typically, group and source are used to group all processing occurring on a particular host, and State is used by the application to identify the event ids it should process. The fourth attribute, rank, is an integer value > 0 used to control the order in which processing occurs, a higher rank posting blocks the processing of all lower ranked state postings for the same event id. The last attribute, result, is an integer value either updated by the processing application or set upon initial posting. On initial posting, the result value is NULL, since this flags the id as unprocessed; setting its result value using the "result" command could transition the state to other postings, if state-result transitions are defined by row values in the PCS_TRANSITION table.   
  
## Example <a name="pcs-example"></a> 
  
The following is simple hypothetical example of how the PCS can be used.  
  
When a new event is written to the dbase by the RT system we want it to be checked by a quality control (QC) script. The results of the QC step will determine the next step to be taken. If it passes the QC step we want it to send a public alarm. If it fails the QC step it should not send the alarm but write a log entry. This thread of PCS processing is given the arbitrary group name "AlarmCheck" and *source* name "Quakes".   
  
  *  Define state transitions   
  
Note: group, source and state names are case sensitive.   
  
    ```
    puttrans AlarmCheck Quakes NewEvent   1  AlarmCheck Quakes CheckEvent 100   
    puttrans AlarmCheck Quakes CheckEvent 1  AlarmCheck Quakes SendAlarm 100    
    puttrans AlarmCheck Quakes SendAlarm  1                                     
    puttrans AlarmCheck Quakes CheckEvent 2  AlarmCheck Quakes LogBadEvent 100  
    puttrans AlarmCheck Quakes LogBadEvent 1   
    ```
  
  *  Check the transitions   
    
    ```
    $ gettrans
    groupold      sourceold     stateold       result -> groupnew      sourcenew     statenew       rank  result 
    AlarmCheck    Quakes        CheckEvent     1      -> AlarmCheck    Quakes        SendAlarm      100   NULL 
    AlarmCheck    Quakes        CheckEvent     2      -> AlarmCheck    Quakes        LogBadEvent    100   NULL 
    AlarmCheck    Quakes        LogBadEvent    1      -> null          null          null           NULL  NULL 
    AlarmCheck    Quakes        NewEvent       1      -> AlarmCheck    Quakes        CheckEvent     100   NULL 
    AlarmCheck    Quakes        SendAlarm      1      -> null          null          null           NULL  NULL 

    (Note: transitions are listed in alphabetical order NOT execution order.)
    ```

  *  A new event, ID = 1234, is introduced into the PCS with:  
    
    ```
    % post 1234 AlarmCheck Quakes NewEvent 1000 1
    ```

Because it is posted with result code = 1 (rather than NULL) it immediately transtions to the CheckEvent state with an rank of 100.   
  
  *  Quality Control script   
  
The quality control script checks for events to work on at some interval it determines with:  
  
    ```
    % next AlarmCheck Quakes CheckEvent 
    ```

If there are no event in the given state, "0" is returned and the script sleeps until its wakeup interval has elapsed. If there is an event posted the ID of the event is returned. The script does its quality control work and "results" the event to 1 if it is good and to 2 if it is bad. This is a branch in the processing stream.   
  
  *  Downstream scripts   
  
Downstream scripts handle the "LogBadEvent" and "SendAlarm" states. When done the event is resulted to "1" and the event leaves the PCS system.   
  

## Rules of Thumb <a name="pcs-rules-of-thumb"></a>  
  
  *  Scripts should not use the post command to send events to the next processing step. This hides the data flow and puts "business rules" in the scripts rather than in the PCS system where the belong. Instead, scripts should result the event and the next step should be defined within the PCS system with the puttrans command.  
  *  The interval at which scripts call next, polling for work to do, should be based on the time urgency of the task. Don't hammer the database unnecessarily.  
  *  Only define multiple processing steps of equal ranks if they can run simultaneously.  
  *  Make sure you define a "terminal" state so that events are ultimately removed from PCS. Otherwise, entries will accumulate in the PCS table forever.   
    

## Current Usage <a name="pcs-current-usage"></a> 
  
Currently the PCS system is used to:   
  
  *  Update the snapshot seen on the <a href="drp.html">Duty Review %Page (DRP)</a> when an event is added or changed in the dbase.  
  *  Resend notifications and refresh products when an event is approved, cancelled or finalized by on the <a href="drp.html">DRP</a>, [​Jiggle](https://aqms-swg.gitlab.io/jiggle-userdocs/), or from the command-line.  
  *  Notify the %Trigger Review %Page (TRP) that there is a new event trigger.  
  *  Notify the %Amplitude Export that there is a new event.  
  *  Notify the <a href="waveformexport.html">%Waveform Export</a> that there is a new event.  
  *  Do batch location and magnitude processing using Hypomag module.   


## The Autoposter <a name="pcs-autoposter"></a>

The AutoPoster's job is to recognize that a new event or trigger has been added to the database for the first time and post it to the correct state or states for processing. You should always configure Autoposter to post to a generic state with a result code. This will cause it to immediately transition to an state you define with *purtrans. It does NOT operate on updated or deleted events.* It is a continuous, automatic processes that runs an a "job" internal to the Oracle data. There are separate instances running on each database.  
  
AutoPoster jobs are defined in the AutoPoster database table. This table is NOT replicated.       
  
### Add an Autoposter Entry <a name="pcs-add-autoposter-entry"></a>  
  
Two steps are necessary to add a behavior to the autoposter.   
  
  1.  Run an SQL like the following. This example will cause the autoposter to detect new 'lp' events and post them to state EventStream/archdb/NewEventLP/100.    
    
      ```
      insert into AUTOPOSTER (INSTANCENAME ,ACTON, EVENTTYPE,
            CONTROLGROUP,SOURCETABLE, STATE, RANK, RESULT, LASTLDDATE)
            values ('NewEventLPposter', 'EVENT', 'lp', 'EventStream',
            'somedb', 'NewEventLP', 100, 1, SYSDATE);
      ```
  REMINDER: Autoposter should always post to some generic state with a result code. This will cause it to immediately transition to an state you define with *purtrans*.   
    
  2.  Create a transition for the new state, for example:   

      ```
      %  puttrans EventStream archdb NewEventLP 1 EventStream somedb rcg_rt 100
      ```

      This transitions the new event to a state that will generate request cards. 
    
### PCS Transition Definitions <a name="pcs-trans-def"></a>   
  
Below are example PCS state processing transitions specified in the PCS_TRANSITION database table.

Note the redundant EventStream processing group which are qualified with a database host described by the "source" column value. The TPP group(Trinet PostProcessing) is for event ids that are deleted or updated by other post-processing tools like the <a href="drp.html">DRP</a>, ​[Jiggle](https://aqms-swg.gitlab.io/jiggle-userdocs/), or command-line scripts. Below the column value NULL is SQL null whereas value 'null' is the literal text string.   
  
  ```
    groupOld        sourceOld    stateOld        result    groupNew             sourceNew    stateNew        rankNew
    EventStream     archdb       DoTrigHtml      1         null                 null         null            NULL
    EventStream     archdb       ExportAmps      1         null                 null         null            NULL
    EventStream     archdb       MakeGif         1         null                 null         null            NULL
    EventStream     archdb       NewEvent        1         EventStream          archdb       rcg_rt          100
    EventStream     archdb       NewEvent        1         EventStream          archdb       exportWF        100
    EventStream     archdb       NewEvent        1         EventStream          archdb       ExportAmps      100
    EventStream     archdb       NewEvent        1         EventStream          archdb       MakeGif         100
    EventStream     archdb       NewTrigger      1         EventStream          archdb       rcg_trig        100
    EventStream     archdb       NewTrigger      1         EventStream          archdb       DoTrigHtml      100
    EventStream     archdb       exportWF        1         null                 null         null            NULL
    EventStream     archdb       rcg_rt          1         null                 null         null            NULL
    EventStream     archdb       rcg_trig        1         null                 null         null            NULL
    TPP             TPP          DELETED         1         null                 null         null            NULL
    TPP             TPP          FINALIZE        1         null                 null         null            NULL
    TPP             TPP          FINALIZE        1         EventStream          archdb       MakeGif         100
  ```

## Sending CMS from Post Processing <a name="pcs-cms-from-post-proc"></a> 

CMS signals in post processing are fired from PCS procs found in */app/aqms/pcs/bin*

In that directory one sets up scripts to be run from the *startPCSjobs* script using the joblist file. An example is running on most postprocessing boxes at the RSN's and this is how clicks on the duty review page interact with the database and cause actions to be fired through PCS and into these scripts (that then use alarmevent to send CMS signals to alarmdec that an event changed state in some way and needs alarms to be performed).

The 2 scripts provided are for event finalization *pcsFinalizeCont* and event cancellation *pcsCancelCont* and each fires different CMS signals through wrapper programs. The wrapper programs simply fire alarmevent or sendcancel alarming utility programs (each requires a config file in a specified location so the programs are not called directly but through a shell wrapper as a convenience).  
  

## Post Processing Data Flow at RSNs <a name="pcs-post-proc-data-flow"></a>  
  
The post processing data flow at RSN's is down right confusing. This section "attempts" to explain the connection of processes when automatic or manual actions are performed on an event.   
  
### DRP Accept button <a name="pcs-drp-accept-btn"></a>    
  
When the user presses the **Accept** button on the <a href="drp.html">DRP</a> page, this sends a SQL call to the database stored procedure epref.accept_event() with the evid as an argument. This stored procedure epref.accept_event() only upates the orgin.rflag and sets it to H for human reviewed. The PHP script also posts to group=TPP, source=TPP the state FINALIZE and this is then picked up by the pcsFinalizeCont script which is sitting polling for events (using the pcs next command). pcsFinalizeCont triggers by firing the alarmevent_wrapper. The alarmevent_wrapper (found in /app/postproc/bin/alarmevent_wrapper) fires off an alarm CMS signal that is received by alarmdec on postprocessing and alarms may or may not be triggered depending on the event's criteria.   
  
### DRP Cancel button <a name="pcs-drp-cancel-btn"></a> 
  
When the user presses the **Cancel** button on the <a href="drp.html">DRP</a> page, this has a long chain of effects. Like the Accept button, this too activates a stored procedure. The stored procedure is the epref.cancel_event() with the evid as the only argument. The epref.cancel_event stored procedure calls the PCS system by posting DELETED state to group=TPP and source=TPP. The pcsCancelCont script is sitting running in the background polling for this state for new events (using the pcs command next). When it finds a new event, it then runs the CMS signaling program sendcancel (via a shellscript wrapper found in /app/postproc/bin/sendcancel_wrapper). sendcancel_wrapper in turn sends a CMS CANCEL signal to post processing (and also to the 2 RT boxes in case they initiated any alarms). The alarmdist program receives the CANCEL message along with the evid and takes the necessary action to retract any alarm actions that may have been activated.   
  
### DRP Delete button <a name="pcs-drp-delete-btn"></a> 
  
The <a href="drp.html">DRP</a> **Delete** button does two actions, it first deletes the event in the db and then it issues the Cancel actions from above (in case any alarms went out for this event and it was bogus). The first action simply calls the stored procedure epref.delete_event with the evid as the only argument. The epref.delete_event() stored procedure sets the event table selectflag to 0 (this merely marks the event for deletion, nothing actually gets removed). Oddly enough, the epref.delete_event() will also call epref.cancel_event(). In addition, the <a href="drp.html">DRP</a> web pages also calls epref.cancel_event() to make sure that if any alarms were created for the event, they get retracted.   
  
### Jiggle Finalize button <a name="pcs-jiggle-finalize-btn"></a>  
  
Pressing the Finalize button (gavel icon) in the ​[Jiggle](https://aqms-swg.gitlab.io/jiggle-userdocs/) GUI toolbar does the following database transactions:   
  
  1.  Sets ORIGIN.RFLAG='F' where ORIGIN.ORID=EVENT.PREFOR, if not already so, and if RFLAG is updated, it then bumps EVENT.VERSION value by 1.  
  2.  Calls the stored package function PCS.POST_ID('TPP', 'TPP', evid, 'FINALIZE', 100) which adds a new row to the PCS_TABLE if no row matching key arguments exists.  
  3.  Creates or updates ASSOCAMO table rows for those AMPID associated in ASSOCAMO with the event's first origin and the current EVENT.PREFOR.   
   
**Why do alarms only fire the first time Finalize is clicked?** <a name="pcs-finalize-alarms"></a> 
  
This could be caused by a missing state transition. Use gettrans to verify that PCS has a transition rule removing rows from PCS_TABLE once processing has completed:   
  
  ```
  % gettrans TPP TPP FINALIZE 
    groupold      sourceold     stateold       resu -> groupnew      sourcenew     statenew       rank  resu 
    TPP           TPP           FINALIZE          1 -> null          null          null           NULL  NULL 
  %
  ```  
  
If needed create the state transition with puttrans:   
  
  ```
  % puttrans TPP TPP FINALIZE 1
  %
  ```
  
### Jiggle Delete %Event button <a name="pcs-jiggle-delete-ev-btn"></a>   
  
Deleting an event in Jiggle posts the evid for state TPP TPP DELETED which gets processed by the pcsCancelCont script which runs the sendcancel program to cancel alarms on the RT and postproc hosts.   
  
### New event arrives in archdb <a name="pcs-new-event-arrives"></a> 
  
TBD - describe what happens in autoposter/pcs when an event is replicated from an RT box, or see autoposter???.   
  
### New subnet trigger arrives in archdb <a name="pcs-new-st-arrives"></a> 
  
TBD - describe here what happens in autoposter/pcs when a subnet trigger is replicated from an RT box, or see autoposter???  
  

## PDL sending example <a name="pcs-pdl-send-example"></a> 
  
 Below is a PDL sending chain that was set up for the Induced Seismicity AQMS system in Menlo Park by Allan Walter:

If you finalize an event in jiggle it should get posted to "TPP TPP FINALIZE" and the FINALIZE state processing script will make it transition a "TPP TPP PDLsend" posting. Then the script processing those "PDLsend" posts will run the pp2PDL.pl script with its configured option filters. If an event passes the filter tests pp2PDL runs the evid2pdl.sh script, the script which generates the qml file and sends it using the ProductClient.jar. If an event fails the pp2PDL tests it is posted to TPP TPP PDL-REJECT.

There are 4 jobs started by /app/aqms/tpp/pcs/startPcsTasks.sh on isdp01 for handling the various posted states (note that one is for handling the cancel delete events posts).   
  
  ```
  # process for "TPP TPP FINALIZE" (results post to a configured to transition that makes a PDLsend posting for the evid)
    ppicker   1566     1  0 Jul01 ?        00:00:02 /usr/bin/perl /app/aqms/tpp/pcs/bin/pcsCont.pl -l ../logs/pcsFinalizeCont.log ../cfg/pcsFinalizeCont.props

  # process for "TPP TPP DELETED" (runs evid2pdl.sh with -D delete option, evid-D.qml sent by ProductClient)
    ppicker   1608     1  0 Jul01 ?        00:00:02 /usr/bin/perl /app/aqms/tpp/pcs/bin/pcsCont.pl -l ../logs/pcsCancelCont.log ../cfg/pcsCancelCont.props

  # process for "TPP TPP DISTRIBUTE" (post evid manually with "post" command or using a jiggle event menu option for loaded evid, forces run of the evid2pdl.sh to send qml)
    ppicker   1628     1  0 Jul01 ?        00:00:02 /usr/bin/perl /app/aqms/tpp/pcs/bin/pcsCont.pl -l ../logs/pcsDistribCont.log ../cfg/pcsDistribCont.props

  # process for "TPP TPP PDLsend" posts runs pp2PDL.pl and if event passes its filter that script runs evid2pdl.sh
    ppicker   9655     1  0 Jul01 pts/10   00:00:00 /usr/bin/perl /app/aqms/tpp/pcs/bin/pcsCont.pl -l ../logs/pcsPDLsendCont.log ../cfg/pcsPDLsendCont.props
  ```

NOTE: if a processed posting already exists in database pcs_state table you have to manually delete it like "unpost <evid>". Use "getstates" command to see whats in the table.
  
Later I'll setup a db transition for auto deleting the TPP TPP PDLsend posts that have a success result (=10), the processing failures will have result=-1.    
  
The log files for the 4 jobs are in /app/aqms/tpp/pcs/logs, and the pdl log is in /app/aqms/pdl/logs.  
  
Below are unused pp2PDL filter options: -n -N and -u.   
  
  ```
    -n cnt   : min phase count to accept (default 4)

    -N cnt   : min mag #nobs to accept (default 1)

    -u rms   : max rms location residual (default .5)
  ```

  
