# Joyner/Boore Attenuation Model {#jb-attenuation}  
  
The Joyner/Boore? Attenuation model is used within the CISN system to estimate the ground motion at sites for a given event. It is used by ShakeMap and the request card generator [RCG](https://aqms-swg.gitlab.io/aqms-docs/rcg.html) process.     
  
## Attenuation Model  
  
The regression is based on *Boore, Joyner and Fumal, 1997, SRL, Volume 68, p. 128* where:   
  
  ```
    ln (PGA,PSV) = B1 + B2(M-6) + B3(M-6)**2 - B5*ln(R) - Bv*ln(Vs/Va)

    where R = sqrt(Rjb**2 + h**2)
      Vs = S-wave velocity in rock (620 m/sec)
      *
      For Acceleration
        B1 = 4.037
        B2 = 0.572
        B3 = 0
        B5 = -1.757
        Bv = -0.473
        Va = 760.0

      For Velocity
        B1 = 2.223
        B2 = 0.740
        B5 = -1.387
        Bv = -0.669
        Va = 760.0

    NOTE: while this comment says "B5*ln(R)" the actual code used by ShakeMap says 
    "$B5*log_base(10,$R)" which is base-10. 
  ```  

PGV is from *Joyner and Boore, 1988, Proc. Earthq. Eng. & Soil Dyn. II, Park City, Utah, 1988*, in the form:  

        ln (PGV) = a + b(M-6) + c(M-6)2 - dlog(R) + k*R + s
  
PSA, PGA are in "g". PGV in cm/s. PSA is 5% damped pseudo-acceleration. Random horizontal component on rock. Distance is JB definition (see SRL V68, No. 1, p10.) This is horizontal distance (km) from the nearest projection of the fault rupture to the surface.  
  
This is all coded in *org.trinet.jasi.JBModel.java*.   
  
Results from original ShakeMap Joyner/Boore attenuation script.   
  
  ```
    ^  Joyner/Boore Values for a Mag 0.0 event  ^^^^
    ^	D(KM)	^	PGA(cm/ss)	^	PGA(%g)	^	PGV(cm/s)	^
    |	1	|	0.185848194	|	0.02178649	|	0.00056834	|
    |	2	|	0.1735457	|	0.0203443	|	0.00053843	|
    |	3	|	0.156486316	|	0.01834447	|	0.00049619	|
    |	4	|	0.137820754	|	0.01615636	|	0.00044885	|
    |	5	|	0.119787445	|	0.01404236	|	0.00040182	|
    |	6	|	0.103551635	|	0.01213908	|	0.00035817	|
    |	7	|	0.089501236	|	0.01049199	|	0.00031923	|
    |	8	|	0.077593182	|	0.00909604	|	0.0002852	|
    |	9	|	0.0675962	|	0.00792412	|	0.00025578	|
    |	10	|	0.059225622	|	0.00694286	|	0.00023043	|
    |	15	|	0.033404492	|	0.00391592	|	0.00014663	|
    |	20	|	0.02128309	|	0.00249496	|	0.00010272	|
    |	25	|	0.014766466	|	0.00173103	|	0.00007697	|
    |	30	|	0.010878217	|	0.00127522	|	0.00006047	|
    |	40	|	0.006660624	|	0.00078081	|	0.00004106	|
    |	50	|	0.00453188	|	0.00053126	|	0.0000303	|
    |	60	|	0.003302292	|	0.00038712	|	0.0000236	|
    |	80	|	0.001999652	|	0.00023441	|	0.00001588	|
    |	100	|	0.001353482	|	0.00015867	|	0.00001167	|
    |	150	|	0.000664999	|	0.00007796	|	0.00000666	|
    |	200	|	0.000401394	|	0.00004705	|	0.00000447	|
    |	250	|	0.000271283	|	0.0000318	|	0.00000328	|
    |	300	|	0.000196956	|	0.00002309	|	0.00000255	|
  ```  
  
  



