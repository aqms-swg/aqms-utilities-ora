# Strong Motion %Amplitude Exchange {#ampexc}  
  
The purpose of ampexc is to automatically and rapidly exchange selected strong motion amplitude information among CISN centers. The ground motion [amplitude data types](https://aqms-swg.gitlab.io/aqms-docs/amp-data-types.html) includes PGA, PGV, PGD, SP.3, SP1.0 & SP3.0. The primary consumer of these amplitudes is ShakeMap. Each center must use the same message format and message transport interface but is free to implement the rest of the system as it sees fit.   
  
## Building A Complete Data Set  
  
In order to build a complete data set, each center must export the data they produce and import the data they do not.
  
A center is deemed the producer of:  
  
    data it generates from instruments it operates or
    data for which it acts as a gateway to the CISN system.
  
For example, Menlo Park is the gateway for PG&E data. To transport data among centers two things are necessary, common data formats and a common transport protocol.  
  

## What Amps are Exchanged?  
  
This is descriptive rather than prescriptive.
  
Ground motion amplitudes may be exported by a data producer in two circumstances; 
  
  1) when individual stations triggers and 
  2) as a result of a declared event.    

%Station triggers may occur because of local noise and may not be the result of seismic energy.   

**Individual station triggers**  
  
Some datalogger types can be configured to send amplitudes when they detect ground motions exceeding a threshold level set in their local configuration. This trigger level can be set independently at each station. NSMP and NetQuakes stations behave this way. NetQuakes are uniformly set to a trigger level of 2.5 cm/s/s. The trigger level of NSMP stations will vary depending on the local ambient noise. Note that these locally triggered ground motions are not “associated” with an event, that is, the amplitude packet contains no event association information even if they are the result on an earthquake. If only one or two of the components of a triaxial sensor reach the trigger level it is possible only values for those channels, not all three, will be sent.   
  

**%Event declarations**  
  
**AQMS Behavior**  
  
When an AQMS system detects an event and determines a viable location and magnitude, ground motion amplitudes may be created and written to the database according to rules defined in the configuration of the programs RAD and AmpGen. Note that these rules apply only to those stations that stream data continuously into AQMS systems.   
  
1) RAD will make amps if the parameters below are satisfied  
  
    *  NC: for NP stations  
        *  MinAccel 0.60 cm/(sec*sec)
        *  MaxVelocity 1.0 cm/sec
    *  NC: for all other stations  
        *  MinAccel 0.03 cm/(sec*sec)
        *  MaxVelocity 1.0 cm/sec  
    *  CI: for ALL stations  
        *  MinAccel 0.75 cm/(sec*sec)
        *  MaxVelocity 1.0 cm/sec  
  

The NP and CI acceleration channels have a higher trigger levels because they are often in noisy locations. If an amp’s value is out of range, that is the acceleration less than MinAccel, or velocity greater than MaxVelocity, ALL of the amplitudes for that SNCL are considered out of range and will be so tagged.
  
The MinAccel limit is used for amplitudes recorded by accelerometers (e.g. HN_), regardless of whether it is PGA, PGV, or spectral response amplitudes.  
  
The MaxVelocity limit is for amplitudes recorded by broadband velocity sensors (e.g. HH_ channels), regardless of whether they are PGA, PGV, or spectral response amplitudes.   
  
2) AmpGen harvests ground motions created by RAD from the ADA (%Amplitude Data Area) and writes them to the database if certain criteria are met.   
  
AmpGen will:
  
    *  Only operate on events larger than a magnitude threshold set in its config
        *  NC: MinMag = 2.95
        *  CI: MinMag = 2.95
            *  Only save amps for SNCLs in a customized list of channel defined in its “channelGroupName “ configuration value. The default value is “AmpGen”.
            *  Save only on-scale values for a given station. If that station has both broadband (HH_) and accelerometer (HN_) channels, it will select the amplitudes for whichever sensor is on scale. If both are on scale, ampgen will save only the accelerometer data. If on-scale amps can be found, those will be saved to the database and marked as “on scale”. If only off-scale amps can be found for a station, those will be saved to the database and marked as “off scale”.  
              
**NOTE:** There may be two instances of AmpGen running - one “fast” that harvests near-in amps (<300km) and one “slow”. If AmpExport runs before the slow AmpGen has written its amps then, clearly, only the near-in amps will be sent.   
  
  
3) Db2GMP will act only on events that exceed its “minMag “ property. That value is set to 3.4. It will send ALL ground motion amps in the database (PGA, PGV, PGD, SP3.0, SP 1.0, SP0.3) that were associated with an event by AmpGen EXCEPT those that are marked “below noise” or “off scale”. It does no selection or rejection of amps based on rules related to distance, quality, or value. It will send amps only from stations listed in its “netcodeList “ property. Typically, this list contains only those network codes for which the producer is the primary data source. Generally the “amplitude exchange” is only a data transfer mechanism. It does not decide what amps are created or what amps consumers like ShakeMap should use.   
  

**NetQuakes Request Behavior**  
  
When an EIDS message has been received declaring an event, the magnitude is used to estimate of the maximum epicentral distance at which energy might be expected to be recorded by a NetQuakes instrument. Data requests for those instruments within that distance are posted to the NetQuakes servers. The NetQuakes devices usually satisfied these requests within 10-20 minutes. Ground motion parameters are calculated for these time series when they are retrieved and are exported with no additional selection rules.   
  

## Ground Motion %Message Formats  
  
Version one of the transport used the Earthworm [StrongMotionII](https://aqms-swg.gitlab.io/aqms-docs/strong-motion-format.html) format. This format was replaced in late 2010 by the [CISN Ground Motion Message format](https://aqms-swg.gitlab.io/aqms-docs/cisn-ground-motion-format.html).   
  

## CISN File Transport  
  
Messages are exchanged among CISN partners as described here.
  
1)  %Message transport – the message transport mechanism is the 'fileflinger' aka sendfile2/getfile2.  
    a.  Export – message files are sent by putting them in the appropriate sendfile2 directory. There is one directory for each message recipient.  
    b.  Import – received messages appear in a getfile2 directory. There is generally one directory for each message sender, however, it is possible to configure the getfiles to put all received messages in a single directory.  
    c.  Filenames are arbitrary but must be unique. Menlo Park and Berkeley use incrementing 6-digit numbers. Pasadena uses a concatenation of eventNumber/ChannelName. Example: 14242516.JGB.HLZ.NP.- -.smCI. Note that “- -” is used where the location code is “blank-blank”.  
    d.  File suffixes are arbitrary but the current convention is “.smUCB” for Berkeley, “.smMP” for Menlo Park and “.smCI” for Pasadena.  
2)  %Message format – the agreed upon message format, Earthworm [StrongmotionII](https://aqms-swg.gitlab.io/aqms-docs/strong-motion-format.html).  
  

## %Configuration Files  
  
The behavior of the import/export is controlled by values in a configuration file that is read at runtime.   
  
| **Parameter** | **Default** | **Meaning** |
--------------- | ----------- | ------------
| destinationDirs= | none | List of destination directories to write to (export only) |
| sourceDirs= | none | List of directories to read from (import only) |
| tempSubDir | none | Temp subdir in destination dir, needed to make files appear atomically in main dir |
| prefixList= | none | List of file prefixes to be processed (import only) |
| suffixList= |	none | List of file suffixes to be processed (import) or written (export) |
| formatType= | EW2 | %Message file format type. Other option is GMP which is not yet used |
| sleepSeconds= | 60 | Seconds to sleep between checks for new events or files to process |
| delayTime= | 150 | Seconds past event origin time to wait before querying for database amps (export) |
| minMag= | 4.5 | Only mags >= this value will be processed | 
| localNetCode= | ?? | Net code to be inserted into export messages |
| netcodeList= | none | Only channels with 'net' in this list will be processed |
| authcodeList= | none | Only amps with 'auth' in this list will be processed |
| verbose | N/A | No value: if present write verbose output to log |
| **%Database definition** | | | 
| dbaseDriver= | oracle.jdbc.driver.OracleDriver | Java dbase driver |
| dbaseDomain= | none | Domain of database URL | 
| dbaseHost= | none | Dbase host name, appended to dbaseDomain to complete URL | 
| dbaseName= | none | Name of database | 
| dbasePort= | 1521 | Dbase port # |
| dbaseUser= | none | Dbase username (can be readonly for export) |
| dbasePasswd= | none | Dbase password |
  

## Pasadena (SCSN) Implementation  
  
The Pasadena-specific version of the exchange system was designed and implemented to be compatible with its operational environment. It feeds message files to the transport system in StrongmotionII format.  
  
### Pasadena Import/Export Components  
  
**Amp Export**  
  
<img src="https://gitlab.com/aqms-swg/aqms-docs/-/raw/master/docsrc/images/ampexport.gif" />

The amp export has the following components found in */app/rtem/ampexc/bin/*:   
  
*  *startAmpExchange* - cron job (user=doug) that starts or restarts both amp import and export. It also does directory cleanup and initiates checking scripts that report to Big Brother. It runs every 10 minutes.  
      
*  *exportamps* -  perl script that waits for events posted to its PCS state. Check that event meets the mag threshold, makes sure at least 'delayTime' seconds have elapsed (property default=150)(to insure amps have been written to the dbase) then calls *db2gmp*.  
      
*  *db2gmp* - Java program reads amps from dbase, writes ground motion packets and writes them to the fileflinger directories. It's behavior is controlled by a properies file as described above. One instance of the code can write to multiple export directories as long as the magnitude threshold of all targets is the same. This code does the following:  
      
        1)  Poll PCS for new events  
        2)  For each new event:  
            a.  elect all associated amps from the dbase. Only amps that originate with us are sent (e.g. network codes of CI, WR, AZ). Amp types sent are: PGD, PGV, PGA, SP03, SP10, SP30.
            b.  Check that magnitude meets threshold level.
            c.  Compose StrongmotionII format messages.
            d.  Write the message files to the sendfile directories.

Example - to generate amps to a stand alone directory 
```
./db2gmp.sh <properties file> <evid>
```
  

**Amp Import**  
  
See : [Amplitude Association](https://aqms-swg.gitlab.io/aqms-docs/amp-association.html)   
  
  
<img src="https://gitlab.com/aqms-swg/aqms-docs/-/raw/master/docsrc/images/ampimport.gif" />  
  
  
1)   %Message import – incoming messages are processed by the Java program Gmp2Db. It is in the org.trinet.apps package. This code does the following:  
    a. Poll import directories for new files. Note that one instance of the code can poll all the import directories.  
    b.  Parse new message files  
    c.  Write resulting amp records to UnassocAmp table
    d.  Move or delete message  
    e.  Initiate association module
      
2)   %Amplitude association – This process is initiated by Gmp2Db and runs at intervals to associate amplitudes with new and updated events. It is implemented as a thread of Gmp2Db. On each run it prioritizes amps so that recent amps are processed first. This code does the following:

    a.  Attempt to associate amps in the UnassocAmp table with event/origins.
    b.  If an association can be made:
        i.  Transfer amp from the UnassocAmp table to the Amp table
        ii.  Write an AssocAmO row
    c.  Delete unassociated amps after an ’age-out’ time has passed.  
      
  
**Directory Structure**  
  
%Amplitude exchange code runs on two redundant servers, magma1 and magma2,  as a specific OS user, for e.g. rtem. You must login as yourself and do everything with 'sudo -u <user>'. The directory root is at app/rtem/ampexc. Here's the structure:

    /app/rtem/ampexc/
        bin/
        export/
            cfg/
        import/
            cfg/

In addition, there are sendfile/getfile directories specified in the configuration properties destinationDirs, sourceDirs, and tempDir.   
  

**Note on the adhoc station list**  
  
The amp import process needs a current, complete list of all CISN stations that may produce amplitude readings. This is provided by the adhoc list. The adhoc list that is used is defined in the config file to be *app/rtem/ampexc/import/cfg/allnets.adhoc*. It is automatically refreshed 4x per day and pulled over to magma1/2 by the script pullAdhocList.pl which is run every 5 minutes inside the *startAmpExchange* cronjob.   
  
**Example Config Files - Pasadena**
  
Example *export* config:   
  
```
destinationDirs=/app/rtem/cisn/BERKELEY/outdir /app/rtem/cisn/MENLO/outdir /app/rtem/cisn/CGS/outdir
#destinationDirs=/home/tpp/ampexc/export/testout
prefixList=
#Old GroMoPacket out file format:
suffixList=.smCI 
formatType=EW2
#New GroMoPacket out file format:
#suffixList=.gmCI 
#formatType=GMXY
#
sleepSeconds=60
delayTime=150
minMag=3.5
localNetCode=CI
netcodeList=CI AZ WR NP
authcodeList=CI
dbaseHost=huber
dbaseDriver=oracle.jdbc.driver.OracleDriver
dbaseDomain=gps.caltech.edu
dbaseName=database_name
dbasePort=database port number
dbaseUser=database user
dbasePasswd=database password
verbose=true
tempSubDir=/temp.dir
```
  
Example *import* config:   
  
```
#
# Define the behavior of the Gmp2Db (Amp association) process
#
verbose=true

#DEBUG:
debug=false
debugSQL=false
debugCommit=false

# props needed by super class
localNetCode= CI
jasiObjectType= TRINET

# where sendfile/getfile import files are found
sourceDirs=/app/rtem/cisn/BERKELEY/indir /app/rtem/cisn/MENLO/indir /app/rtem/cisn/CGS/indir

# where to put successfully processed files when done (will be purged later)
destinationDirs=/app/rtem/ampexc/import/done

# where to put problem files
errorDirs=/app/rtem/ampexc/import/err

# file prefixes to process
prefixList=

# file suffixes to process note MENLO is NEW packet format
suffixList=.smUCB .gmMP .EW2

#When ALL are NEW format:
#suffixList = .gmBK .gmMP .gmCE

# Default file format type
formatType=EW2

# sleep time between checks for new amps
sleepSeconds=60

# If net codes are listed, only accept amps from those listed
netcodeList=

# If auth codes are listed, only accept amps from those listed
authcodeList=

# If subsource codes are listed, only accept amps from those listed
subsourceList=

# If amptype codes are listed, only accept amps of those listed
ampTypeList=

# dbase connection info
dbaseHost=huber
dbaseDriver=oracle.jdbc.driver.OracleDriver
dbaseDomain=gps.caltech.edu
dbaseName=database_name
dbasePort=database port
dbaseUser=database user
dbasePasswd=databae password
#
# Invoke association thread production = true
doAssociation=true

# does association make an ampset ?
makeAmpSet=true

# Runtime mode, continuous oneshot=false
oneshot=false

# ASCII adhoc station list - dbase doesn't contain all CE, NP, NC, BK, etc.
assocAdHocFile=/app/rtem/ampexc/import/cfg/allnets.adhoc

# retry associations this often
assocSleepSeconds=1800

# How long do amps exist before we give up and delete them? (2 days)
assocExpirationDays=2.0

# use gap this long between amps to delimit amp groups
assocGroupGap=120

# log rotation
logFileName=/app/rtem/ampexc/import/logs/import.log
logRotationInterval=24H

# EventSelection properties to control candidates for association
eventSelectionProperties=/app/rtem/ampexc/import/cfg/candidate.props

#Only accept amps from channels in the channel list
# GMP import doesn't need channel gains so skip load of gains and stacorrections
channelListReadOnlyLatLonZ=true

channelCacheFilename=/app/rtem/ampexc/import/cisnChannelList16.cache
channelDbLookUp=true

# Is there a channel group name in appchannels table corresponding to GMP import channel list?  if not use group RCG-TRINET
channelGroupName=RCG-TRINET
channelListAddOnlyDbFound=true
channelListCacheRead=true
channelListCacheWrite=false

#Velocity model used to calculate energy window for associating events
velocityModel.DEFAULT.modelName = HK_SOCAL
velocityModelList = HK_SOCAL USGS_NC

velocityModel.HK_SOCAL.psRatio    = 1.73
velocityModel.HK_SOCAL.depths     = 0.0 5.5 16. 32.
velocityModel.HK_SOCAL.velocities = 5.5 6.3 6.7 7.8

velocityModel.USGS_NC.psRatio    = 1.78
velocityModel.USGS_NC.depths = 0.0 3.5 15. 25.0
velocityModel.USGS_NC.velocities = 4.0 5.9 6.8 8.05
```
  
Example *candidate.props* config:  
  
```
# Specify properties of events to include as candidates for 
#  Amp Association
#
#
# props needed by super class
localNetCode=CI
#jasiObjectType=TRINET
#
# props defining SQL WHERE condition filter 

eventValidFlag= true
#eventTypes = "local", "quarry", "regional","teleseism", "sonic", "nuclear", "trigger", "unknown" 
eventTypesIncluded=local quarry regional
eventTypesExcluded=teleseism
#
#originProcTypesIncluded=automatic human 
#originProcTypesExcluded=final
originDummyFlag=false
#originAuth=CI
#originSubsource=Jiggle
#
# If you DON'T want to include very small events to be associated with amps set min magnitude range in property below:
#magValueRange= 3.0 9.9
#
# If you don't set = -1 an anoying messages is printed out every time you get a candidate list
maxCatalogRows=-1
```
  
### Monitoring Amp Exchange Processes  
  

Status is monitored by Nagios on the primary magma server.
  
*  webbuilder – builds Duty review and trigger pages
*  fileflinger – [Earthworm](???) sendfile/getfile jobs that send & receive amp exchange files
*  exportamps – process that create export format files to send via fileflingers. A single process exports to all recipients.
*  importamps – accepts imported files and puts them in the database. A single process imports from all remote senders.
  

### The Code  

Sub-directory *bin* has the executable scripts while *cfg* has the configuration files.  

*  ``bin/BBdisable.sh`` : Turn off checking of status on Big Brother (BB) system.  
*  ``bin/bbmsg.sh`` : Send Big Brother report message.  
*  ``bin/checkFileStatus.sh`` : Wrapper script to run *checkampexchange.pl* and *checkfileflingers.pl*.  
*  ``bin/checkampexchange.pl`` : Check AmpExchange jobs. Writes text file for inclusion in BB report.  
*  ``bin/checkautoposter.pl`` : Will check and report on all jobs in User_Jobs table that meet the Select criteria. Report will reflect the worst state of any job selected. Job details will appear on the detail BB webpage.    
*  ``bin/checkfileflingers.pl`` : Check File flinger jobs. Writes text file for inclusion in BB report. 'grep' this file for the word 'red' to look for trouble.  
*  ``bin/checkindirs.sh`` : Checks indir.  
*  ``bin/checkoutdirs.sh`` : Checks outdir.  
*  ``bin/cleanupAllIndir.sh, cleanupExport.sh, cleanupImport.sh, cleanupStartLogs.sh`` : various cleanup logs.  
*  ``bin/db2gmp.sh`` : Runs Db2Gmp.  
*  ``bin/exportamps.pl`` : Check for events posted to group/table/state else sleep for time interval. If new event is found, run db2gmp.sh wrapper around java app code. Java app will decide if it should make packet files or not.  
*  ``bin/importamps.sh`` : Run the amplitude import java code.  
*  ``bin/javarun_opt_jar.sh`` : Runs java application class code found in the jiggle.jar file.  
*  ``bin/pullAdhocList.pl`` : Pulls the AdHoc list from the web.  
*  ``bin/startAmpExchange.sh`` : Start/restart the Strong Motion amplitude Exchange processes.  
*  ``bin/statusAmpExchange.sh`` : Check job status of the Strong Motion amplitude Exchange processes.  
*  ``bin/stopAmpExchange.sh`` : Stop the Strong Motion amplitude Exchange processes.  

<br>
  
* ``cfg/adhocURL.cfg`` : URL to get the adhoc list from.  
*  ``cfg/allnets.adnoc`` : Sample adhoc list.  
*  ``cfg/candidate.props`` : Specify properties of events to include as candidates for Amp Association.  
*  ``cfg/db2gmp.props`` : Property file for db2gmp.  
*  ``cfg/exportamps.props`` : Property file for exportamps.sh  
*  ``cfg/importamps.props`` : Property file for importamps.sh  











