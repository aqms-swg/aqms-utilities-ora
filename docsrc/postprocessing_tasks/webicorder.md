# Webicorder {#webicorder}  
  
The “webicorder” is a web page with helicorder-like images of 12-24 hours of a seismic channel. Because this code is an Earthworm module it only works for channels that are available via a Winston or Earthworm Waveserver.   
  
<img src="https://gitlab.com/aqms-swg/aqms-utilities-ora/-/raw/master/docsrc/postprocessing_tasks/bbs_bhz_ci_--.2011011812.gif" />  
  

*  **Overview doc**  
  
http://folkworm.ceri.memphis.edu/ew-doc/ovr/heli_ewII_ovr.html  
  

*  **%Configuration doc**  
  
http://folkworm.ceri.memphis.edu/ew-doc/cmd/heli_ewII_cmd.html  
  

*  **Link to USGS Webicorder page**  
  
http://earthquake.usgs.gov/earthquakes/helicorders.php   

  
## Implementation  

For an example implementation, see <a href="how-to-webicorder.html">how to implement webicorder</a>


    






