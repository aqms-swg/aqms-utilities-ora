# Autoposter {#post-proc-autoposter}  
  
*  [Controlling the Autoposter Job](#control-autoposter-job)  
    *  [Defining Jobs](#define-autoposter-jobs)  
*  [The Code](#autoposter-code)  
    *  [Starting the job ](#autoposter-start-job)  
    *  [Checking the job](#autoposter-check-job)  
    *  [Stopping the job](#autoposter-stop-job)  
*  [Define Autoposter Actions](#definte-autoposter-actions)  
*  [Define PCS Transitions](#define-pcs-trans)  
  

The Autoposter is part of the [Process Control System](https://aqms-swg.gitlab.io/aqms-docs/pcs.html). Its job is to recognize that a new event or trigger has been added to the database for the first time and post it to the correct state or states for processing. It can operate on updated events if configured to do so. It does NOT operate on deleted events. The source of the new event doesn't matter; it can be from RT processing, a Jiggle "clone", or some other source.  
  
Autoposter is a continuous, automatic processes that runs as a “job” internal to the Oracle data. There are separate instances running on each database. The Autoposter job detects when an event row is inserted in the database with a LDDATE value after (later than) the last time the job ran. By default the check interval is 1 minute.  
  

## Controlling the Autoposter Job <a name="control-autoposter-job"></a> 
  
### Defining Jobs <a name="define-autoposter-jobs"></a> 
  
Autoposter jobs are defined in the AutoPoster table. There is no simple tool for adding these rows since it is done very rarely. This table is NOT replicated.  
  
Here is an example of operational definitions that detect both new events and new triggers. (This is a partial list of fields.)   
  
```
INSTANCENAME            ACTON     EVENTTYPE     CONTROLGROUP     SOURCETABLE     STATE       RANK     RESULT    
 ----------------------  --------  ------------  ---------------  --------------  ----------  -------  --------- 
 NewEventPosterArchdb    EVENT     le            EventStream      archdb          NewEvent    100      1         
 NewTriggerPosterArchdb  EVENT     st            EventStream      archdb          NewTrigger  100      1       
```  
  
Where:  
  
*  InstanceName - is any unique string describing the instance  
*  ActOn - may be either "EVENT" or "ORIGIN"  
*  EventType - is any valid event type.  
*  CONTROLGROUP, SOURCETABLE, STATE, RANK, RESULT - are how new events will be posted. NOTE: that using an non-NULL result code will cause the entry to transition immediately to any defined transitional states.   
  
<span style="color:blue">**NOTE** : he PCS_STATE table is replicated. Therefore, each autoposter (database) instance must post new events to **different** CONTROLGROUP, SOURCETABLE, STATE, RANK signatures. Otherwise, they will create duplicate entries that may cause replication errors in the PCS_state table. Uniqueness can be insured by setting the SOURCETABLE to any unique string (e.g. the name of the host dbase).</span>  
  
The first Autoposter definition above will cause any new Event row of EventType = 'le' (local event) to be posted to EventStream/makalu/NewEvent/100/1. In this example, the non-null "result" value causes any transitions defined to occur immediately. This allows a new event to be immediately posted to several states if multiple transitions are defined. For more on state processing see [Process Control System](https://aqms-swg.gitlab.io/aqms-docs/pcs.html).  
  
When ACTON = "EVENT" the Autoposter job detects EVENT rows with a LDDATE value after (later than) the last time the job checked for new rows. When ACTON = "ORIGIN" the AutoPoster job detects updated origins; "new" preferred ORIGINs with a LDDATE value after (later than) the last time the job checked for new rows. By default the check interval is 1 minute.   
  

## The Code <a name="autoposter-code"></a> 
  
The PCS.AutoPostNew stored procedure reads the AutoPoster table every time it runs so new entries will work immediately and do not require that the job be restarted.   
  
```
PROCEDURE AutoPostNew(p_instanceName VARCHAR2) AS
        v_actOn    VARCHAR2(6);
        v_statement VARCHAR2(500);
        v_lddateClause varchar(50);
--
  BEGIN
  -- Check InstanceName, get PCS group/table/state/rank params
        SELECT actOn
          INTO v_actOn
                FROM Autoposter
                WHERE InstanceName LIKE p_instanceName;
        v_statement := 'SELECT Event.evid, Event.lddate FROM Event
                        WHERE Event.etype LIKE :p_eventType AND
                        SELECTflag = 1';
        IF (v_actOn = 'EVENT') THEN
          v_statement := v_statement ||
                        ' AND lddate > :v_lastTime
                         ORDER BY lddate';
        ELSIF (v_actOn = 'ORIGIN') THEN
          v_statement := v_statement ||
                        ' AND Event.prefor IN (SELECT orid FROM origin WHERE lddate > :v_lastTime)
                         ORDER BY lddate';
        END IF;
        postEvidsFromStatement(v_statement, p_instanceName);
  END AutoPostNew;
```
  

### Starting the job <a name="autoposter-start-job"></a> 
  
The job starts automatically when the database is started. You need dba privilage to start or stop the job. This job should be created as the trinetdb user. The code to start the job is something like:   
  
```
---------------------------------------------------------------------------
-- Schedule the job using the DB Job System. 
---------------------------------------------------------------------------
declare
  v_job number;
  v_jobname Varchar2(80) := 'PCS.DoAutoPostJobs();';
begin
  select job into v_job from user_jobs where what like v_jobname;
  dbms_output.put_line('Removing old job No.= '||v_job);
  dbms_job.remove(v_job);
  dbms_job.submit(v_job, v_jobname, sysdate,
                        'SYSDATE+(59/(24*60*60))');  
  dbms_job.run(v_job);
  dbms_output.put_line('Job '||v_jobname||' RE-submitted. No.= '||v_job);
exception
  when NO_DATA_FOUND then
       dbms_job.submit(v_job, v_jobname, sysdate,
                      'SYSDATE+(59/(24*60*60))');  
       dbms_job.run(v_job);
       dbms_output.put_line('Job '||v_jobname||' submitted. No.= '||v_job);
end;
/

--- note the / at the end is critical!
```
  
The job's run interval is set to 1 minute with the string 'SYSDATE+(59/(24*60*60))'.   
  
  
### Checking the job <a name="autoposter-check-job"></a>   

```
SQL> SELECT * FROM DBA_JOBS Where WHAT like '%DoAutoPostJobs%';

--or if checking as trinetdb user

SQL> select job, broken, what from user_jobs;

       JOB B
---------- -
WHAT
--------------------------------------------------------------------------------
	 1 N
PCS.DoAutoPostJobs();


SQL> 
```
  

### Stopping the job <a name="autoposter-stop-job"></a>  
  
```
SQL> Execute DBMS_JOB.Remove(<Job_Number>);
```  
Where *<Job_Number>* is the value returned from the SQL statement above.   
  
   
## Define Autoposter Actions <a name="definte-autoposter-actions"></a>  
  
Use sqlplus or some other SQL editor to define the Autoposter behavior.  
  
The definitions below post newly inserted local ('le') and trigger ('st') events to the state EventStream/archdb/NewEvent/100 and results to 1 which causes initiates transitions. The "InstanceName" is just a human-readable, unique string for keeping track of the definitions.   
  
```
SQL> insert into AUTOPOSTER (INSTANCENAME ,ACTON, EVENTTYPE, CONTROLGROUP,SOURCETABLE, STATE, RANK, RESULT, LASTLDDATE) 
   values ('NewEventPoster', 'EVENT', 'le', 'EventStream', 'archdb', 'NewEvent', 100, 1, SYS_EXTRACT_UTC(SYSTIMESTAMP));

SQL> insert into AUTOPOSTER (INSTANCENAME ,ACTON, EVENTTYPE, CONTROLGROUP,SOURCETABLE, STATE, RANK, RESULT, LASTLDDATE) 
   values ('NewTriggerPoster', 'EVENT', 'st', 'EventStream', 'archdb', 'NewTrigger', 100, 1, SYS_EXTRACT_UTC(SYSTIMESTAMP));
```
  

<span style="color:blue">Note: the example above sets LASTLDDATE=SYS_EXTRACT_UTC(SYSTIMESTAMP) which will start posting events from NOW forward. You could set it retroactive.</span>   
  
<span style="color:orange">**WARNING** : f you don't set LASTLDDATE the system will post **every event** in the table - not what you want. </span>   
  

## Define PCS Transitions <a name="define-pcs-trans"></a> 
  
The Autoposter states defined above set the result value to "1". Therefore, new events posted by the Autoposter will automatically invoke [[postproc:process_control_system|PCS]] transition rules and immediately change to the states defined in this next step.  
  
The puttrans command is executed from the command line.   
  
```
% puttrans EventStream archdb NewEvent   1 EventStream archdb MakeGif 100
% puttrans EventStream archdb NewEvent   1 EventStream archdb exportWF 100
% puttrans EventStream archdb NewTrigger 1 EventStream archdb DoTrigHtml 100
```
  
The state transitions along with the autoposter config defined above will cause a new event to immediately transition to two new states ; EventStream/archdb/MakeGif/100 and EventStream/archdb/exportWF/100/. A new trigger will transition to EventStream/archdb/DoTrigHtml/100.   
  
<span style="color:lightblue">Question: So why didn't you just set Autoposter to post to these states in the first place? </span>  
  
<span style="color:blue">Answer: You could, but the processing steps in a production system may change. If they do, you only need to redefine the transitions using *puttrans* and don't need to touch or even think about the Autoposter. Also, Autoposter can only post to a single state, this technique allows you to branch the processing into multiple states as in the new event example shown here.</span> 