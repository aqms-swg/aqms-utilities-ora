# redo_wfs {#man-redo_wfs}

Redo triggered waveform archival  
  
## PAGE LAST UPDATED ON  
  
2022-02-03
  
## NAME  
  
redo_wfs
  
## VERSION and STATUS

status : ACTIVE  
    
## PURPOSE
  
Deletes from database all existing waveform related database table rows, moves or removes the waveform files, and deletes request_card table rows associated with the input event evid. If successful, either forks a java job to create new waveform requests or with '-p' option posts the input evid to a new state, and if pp_db2 is specified, it checks whether dbase or pp_db2 is primary and posts or writes requests to primary database.  
  
## HOW TO RUN
  
```
./redoWfs.pl -h

Deletes from database all existing waveform related database table rows, moves or removes
the waveform files, and deletes request_card table rows associated with the input event evid.
If successful, either forks a java job to create new waveform requests or with '-p' option
posts the input evid to a new state, and if pp_db2 is specified, it checks whether dbase
or pp_db2 is primary and posts or writes requests to primary database.

Syntax: ./redoWfs.pl [-h] [-c file] [-d dbase] [-p state ] [-r] [-w] <evid> [pp_db2]

Options:

  -h         : this usage

  -c file    : read waveform file directories from file instead of the WAVEROOTS table. 

  -d dbase   : alias name for primary database connection, defaults to archdbe.

  -p state   : If the waveforms and requests are successfully deleted (and no -w option),
               do not run the "tpp/rcg/bin/rcgone.pl" script to generate new waveform requests,
               instead do a posting:

                 post <evid> EventStream archdbe <state> 100

               For example, state = "rcg_rt" would queue the evid for the continuously running
               request generator application polling the database, which defaults to archdbe.

  -r         : remove event waveform file directories, default action is to rename the file
               directories, move them to <waveroot-dir>/dumpster/x<evid>
               If dumpster directory already exists for event, it is deleted.

  -w         : delete all waveform data, but DO NOT run "tpp/rcg/bin/rcgone.pl script to make 
               new waveform requests, NOR post the event evid to a state specified by -p option.

 Arguments:

  evid       : all waveform data associated with this event id will be deleted from the
               database, which defaults to the current master database = "archdbe".

  pp_db2     : optional, deletes all request_card table rows associated with input event
               in the specified secondary database too.
```
  
## CONFIGURATION FILE  
  
*redo_wfs* uses two configuration files; *redoWfsCont.props* and *wfdirs.cfg*. Their parameters are explained below.  
Format is   

```
<parameter name> <data type> <default value>
<description>
```  

***redoWfsCont.props***  
  
This config file is used by the continuously running redo_wfs process.  
  
**pcsAppName** *string*  
The pcs state name that the continuously running redo_wfs process listens for in order to trigger re-archival.  

**pcsAppProps** *string*  
Accessory property file  

**noop** *boolean*  
Set to 0 or 1   
  
**reconfig** *integer*  
???
  
**sleepTime** *integer*  
???  
  
**archivalTime** *integer*  
???  
  
**minMag** *integer*  
Minimum magnitude value of event to process for re-archival.  
  
**maxDaysBack** *integer*  
Ignores events that are older than this value set here. Set to 0 to process all events.  
  
**pcsStallTime** *integer*  
???  
  
**pcsDbase** *string*  
???  
  
**pcsThreadName** *string*  
???
    
**pcsGroupName** *string*   
???  
  
**pcsThreadName** *string*  
  
**pcsStateName** *string* redoWfs  
???  

**pcsResult** *integer*  
???  
  
**rotateLogsDaily** *integer*  
0 = do not rotate, 1 = rotate daily  


***wfdirs.cfg***  

Lists one or more locations, with path, where the waveforms should be saved to.    
  

## ENVIRONMENT  
  
NA  
  
## DEPENDENCIES

*  PCS

## MAINTENANCE

NA
  
## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-utilities-ora/-/issues  
  
## MORE INFORMATION  
  
NA


