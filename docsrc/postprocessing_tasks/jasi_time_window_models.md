# JASI Time Window Models {#jasi-time-window-models}  
  
*  [Channel Time Window Models](#jasi-chan-time-win-models)  
    *  [List of models](#jasi-list-of-models)   
    *  [Properties Common to All Models](#jasi-props)  
    *  [PowerLawTimeWindowModel](#jasi-powerlawtimewindowmodel)  
      *  [Model-Specific Properties](#jasi-model-specfic-props)  
      *  [Notes on tuning](#jasi-tuning-notes)  
    *  [JBChannelTimeWindowModel](#jasi-jbchanneltimewindowmodel)  
      *  [Model-Specific Properties](#jasi-model-specific-props-2)  
    *  [Channels with seismic energy](#jasi-seismic-channels)  
    *  [TriggerChannelTimeWindowModel](#jasi-trigchanneltimewindowmodel)  
      *  [Model-Specific Properties](#jasi-model-specific-props-3)  
    *  [NCChannelTimeWindowModel](#jasi-ncchanneltimewindowmodel)  
    *  [Channels Name Exceptions](#jasi-chan-name-exceptions)  


<br><br>  
## Channel Time Window Models <a name="jasi-chan-time-win-models"></a> 
  
A channel time-window model is used to calculate a set of channels and time windows for some purpose: e.g. scanning for peak amplitudes, archiving, viewing, etc. These are implemented in the Java as part of the JASI libraries and all extend the ChannelTimeWindow class. There are run-time properties that are common to all models plus model-specific properties.  
  
A major use of these models is for determining channel sets for archiving for an event by the [Java request card generator](https://aqms-swg.gitlab.io/aqms-docs/rcg.html).    
  

### List of models <a name="jasi-chan-time-win-models"></a>  
  
<table>
  <tr style="background-color:grey;"><th><b>Model/class name</b></th><th><b>Description</b></th></tr> 
  <tr><td>DataSourceChannelTimeModel</td><td>all waveforms saved in dbase for the event</td></tr>
  <tr><td>PicksOnlyChannelTimeModel</td><td>all channels with picks</td></tr>
  <tr><td>TriggerChannelTimeWindowModel</td><td>all channels for triggered stations (CarlSubTrig)</td></tr>
  <tr><td>CTMampsOnly</td><td>all channels with amps</td></tr>
  <tr><td>NamedChannelTimeWindowModel</td><td>all channels in a named list</td></tr>
  <tr><td>ListBasedChannelTimeWindowModel</td><td>all channels specified in a file</td></tr>
  <tr><td>SimpleChannelTimeModel</td><td>all stations within distance = 170.0 * magnitude - 205.0</td></tr>
  <tr><td>JBChannelTimeWindowModel</td><td>based on Joyner/Boore? attenuation and instrument sensitivity</td></tr>
  <tr><td><a name="jasi-powerlawtimewindowmodel">PowerLawTimeWindowModel</a></td><td>a power law magnitude distance relationship</td></tr>
  <tr><td><a name="jasi-ncchanneltimewindowmodel">NCChannelTimeWindowModel</a></td><td>a log-linear magnitude distance relationship</td></tr>   
</table>  

<img src="https://gitlab.com/aqms-swg/aqms-docs/-/raw/master/docsrc/images/models.gif" />
  
**Key**  
  
  *  Vel(obs) - Observation of distance out to which you see vel energy for a few events  
  *  Acc(obs) - Observation of distance out to which you see acc energy for a few events  
  *  Power (Vel(obs)) - power law fit to Vel(obs)  
  *  Power (Acc(obs)) - power law fit to Acc(obs)  
  *  Power (vel) - tweaked power law coefficients to include all vel channels with energy  
  *  Power (acc) - tweaked power law coefficients to include all acc channels with energy  
  *  Amp Cutoff - Kanaomori's model, distance = 170.0 * magnitude - 205.0 (SimpleChannelTimeModel)  
  *  NC Model(vel) - NCChannelTimeWindowModel log-linear magnitude distance relationship  
  *  NC Model(acc) - NCChannelTimeWindowModel log-linear magnitude distance relationship   


### Properties Common to All Models <a name="jasi-props"></a> 
  
The behavior of the model is determined by the values of run-time properties. If a property is not explicitly set, its default value will be used. Note that each of these properties is prefixed by the name of the specific model class used. For example: *org.trinet.jasi.JBChannelTimeWindowModel.maxDistance*.  
  
All models extend the abstract class [ChannelTimeWindowModel​](https://gitlab.com/aqms-swg/aqms-jiggle/-/blob/master/src/org/trinet/jasi/ChannelTimeWindowModel.java)     
  
   
<table>
  <tr style="background-color:grey;"><th><b>Property</b></th><th><b>Explanation</b></th></tr>  
  <tr><td>minDistance</td><td>all channels within this distance are saved for every event (default = 0km)</td></tr> 
  <tr><td>maxDistance</td><td>no channels are saved that are beyond this horizontal distance from the event (default = 1000km)</td></tr> 
  <tr><td>maxChannels</td><td>only the first 'maxChannels' channels are included (default = unlimited)</td></tr> 
  <tr><td>includeAllComponents</td><td>if true, include all components (orientations) of a multicomponent sensor if any of its components is included (default = true)</td></tr> 
  <tr><td>includeAllMag</td><td>save all channels if the magnitude >= this value (default = 3.0)</td></tr> 
  <tr><td>includePhases</td><td>include all channels that have phase readings (default = true)</td></tr> 
  <tr><td>includeMagAmps</td><td>include all channels that have magnitude-amplitude readings (default = true)</td></tr> 
  <tr><td>includePeakAmps</td><td>include all channels that have peak-amplitude readings (default = true)</td></tr> 
  <tr><td>includeCodas</td><td>include all channels that have coda readings (default = true)</td></tr> 
  <tr><td>includeAllMag</td><td>adjust this value, above which all channels are saved. Setting this to 0.0 would result in all candidate channels being saved for all events.</td></tr> 
  <tr><td>minWindowSize</td><td>always save at least this many seconds (defaults to 60)</td></tr> 
  <tr><td>maxWindowSize</td><td>Do not save any more than this number of seconds (defaults to 300)</td></tr> 
  <tr><td>preEventSize</td><td>the number of seconds to pre-append to the window (defaults to 20)</td></tr> 
  <tr><td>postEventSize</td><td>the number of seconds to add at the end of the window (defaults to 20)</td></tr>  
</table>
  

### PowerLawTimeWindowModel <a name="jasi-powerlawtimewindowmodel"></a>  
  
This uses a power law to determine the maximum distance out to which energy may be seen for a given event. It is base on an empirical fit to a modest data set. The parameters can be tuned to capture fewer or more channels.   
  
See source at: [PowerLawTimeWindowModel](https://gitlab.com/aqms-swg/aqms-jiggle/-/blob/master/src/org/trinet/jasi/PowerLawTimeWindowModel.java)   
  
  
#### Model-Specific Properties <a name="jasi-model-specfic-props"></a> 
  
<table>
  <tr style="background-color:grey;"><th><b>Property</b></th><th><b>Default</b></th></tr>  
  <tr><td>velConstant</td><td>120.0</td></tr>
  <tr><td>velExponent</td><td>1.3</td></tr>
  <tr><td>accConstant</td><td>40.0</td></tr>
  <tr><td>accExponent</td><td>1.8</td></tr>
</table>  
  
  
#### Notes on tuning <a name="jasi-tuning-notes"></a>  
  
Tuning is best done interactively in Excel where you can adjust the curve to a summary data set or some other view of you desired behavior.  
  
That spread sheet is attached to this page : ​https://gitlab.com/aqms-swg/aqms-utilities-ora/-/blob/master/docsrc/postprocessing_tasks/powerlawmodel.xls  
  
The xxxConstant tends to affect the position of the curve. Larger values move it upward, lower values downward. The xxxExponent tends to change the slope of the curve. Smaller values flatten it out (rotate it clockwise) while larger values make it steeper (rotate conterclockwise) yielding more close-in chanels and fewer distant channels if you don't adjust xxxConstant.   
  

### JBChannelTimeWindowModel <a name="jasi-jbchanneltimewindowmodel"></a> 
  
The JBChannelTimeWindowModel is an implementation of the [Joyner/Boore](https://aqms-swg.gitlab.io/aqms-docs/jb-attenuation.html) attenuation model as coded in (*org.trinet.jasi.JBChannelTimeWindowModel*). This attenuation model is used to estimate the ground motion at each station for an event. A station's channels are selected for archiving if the channel's sensitivity (gain or reponse) is such that it is likely that seismic energy above its noise level by some factor.

See source at: [JBChannelTimeWindowModel](https://gitlab.com/aqms-swg/aqms-jiggle/-/blob/master/src/org/trinet/jasi/JBChannelTimeWindowModel.java)  ​
  
**In practice this model was found to select too many stations for archiving.**  


#### Model-Specific Properties <a name="jasi-model-specific-props-2"></a> 
  
<table>
  <tr style="background-color:grey;"><th><b>Property</b></th><th><b>Explanation</b></th></tr>  
  <tr><td>defaultNoiseThreshold</td><td>default background noise level, in counts, to use if none is available in dbase or code. (default = 25)</td></tr>
  <tr><td>thresholdFactor</td><td>factor by which ground motion must exceed background noise level to be saved. For example, if this value is 2.0 seismic energy must be twice the background noise level to be saved. (default = 1.0) Increasing this value will result in fewer stations being archived.</td></tr> 
</table>
  

### Channels with seismic energy <a name="jasi-seismic-channels"></a> 
  
A channel is archived if it is likely to have seismic energy that rises above the background noise level. This is calculated for each site using the event's magnitude and the Joyner-Boore attenuation relationship. This is the relationship used by ShakeMap for small events.  
  
First, the expected **ground motion** at the site is calculated. This may be acceleration or velocity depending on the sensor type. Then that estimated ground motion is converted to digital counts using the gain of the channel. If the channel's gain is unknown (not in the database) a 'nominal' gain based on the channel description string is used.  
  
Ideally the average background noise of each channel (in counts) would be known, but since it isn't a nominal value for each channel type is used. Finally, the calculated ground motion is compared to the noise level. If the **seismic signal-to-noise ratio (SNR) is greater than one, the channel is saved.**   
  

### TriggerChannelTimeWindowModel <a name="jasi-trigchanneltimewindowmodel"></a>  
  
This model returns a set of Channel Time Windows for "triggered" stations. It uses the contents of the Trig_Channel table (referenced by the ntid in the assocnte table) for the event which is populated by the EW carlsubtrig process. Like other models it can be configured with several model-specific parameters. See source at: [TriggerChannelTimeWindowModel ](https://gitlab.com/aqms-swg/aqms-jiggle/-/blob/master/src/org/trinet/jasi/TriggerChannelTimeWindowModel.java)  
  

#### Model-Specific Properties <a name="jasi-model-specific-props-3"></a>  
  
<table>
  <tr style="background-color:grey;"><th><b>Property</b></th><th><b>Explanation</b></th></tr> 
  <tr><td>defMinDistance</td><td>Include stations within this distance of the earliest triggered station. [default = 20]</td></tr> 
</table>
  

### NCChannelTimeWindowModel <a name="jasi-ncchanneltimewindowmodel"></a> 
  
For NCChannelTimeWindowModel an acceptable channel is one whose energy threshold value is greater than 0 in the formulation described below:   
  

  ```
  if (isVelocity) {
          threshold = mag - velMagDecrement * log(dist) - velThreshold;
      } else { // accelerometer
          threshold = mag - accMagDecrement * log(dist) - accThreshold;
      }
      if (threshold > 0.0) return true;
      else false;
  ```

See source at: [NCChannelTimeWindowModel](https://gitlab.com/aqms-swg/aqms-jiggle/-/blob/master/src/org/trinet/jasi/NCChannelTimeWindowModel.java)   
  

<img src="https://gitlab.com/aqms-swg/aqms-docs/-/raw/master/docsrc/images/NCChannelTimeWindowModel.jpg" />   
  

```
    # default properties set in java code, user should change values to those appropriate for network 
    # (note: accelerometer channels by default are undefined, so these were set below)

    org.trinet.jasi.NCChannelTimeWindowModel.accMagDecrement=2.56
    org.trinet.jasi.NCChannelTimeWindowModel.accThreshold=1.67
    org.trinet.jasi.NCChannelTimeWindowModel.velMagDecrement=2.56
    org.trinet.jasi.NCChannelTimeWindowModel.velThreshold=1.67
    org.trinet.jasi.NCChannelTimeWindowModel.timeChannelNames=
    org.trinet.jasi.NCChannelTimeWindowModel.accelerometerChannels= HNN HNE HLN HLE HGN HGE ELN ELE
    org.trinet.jasi.NCChannelTimeWindowModel.minDistance=20
    org.trinet.jasi.NCChannelTimeWindowModel.maxDistance=500
    org.trinet.jasi.NCChannelTimeWindowModel.minWindowSize=40
    org.trinet.jasi.NCChannelTimeWindowModel.maxWindowSize=600
    org.trinet.jasi.NCChannelTimeWindowModel.preEventSize=20
    org.trinet.jasi.NCChannelTimeWindowModel.postEventSize=20
    org.trinet.jasi.NCChannelTimeWindowModel.includeAllComponents=true
    org.trinet.jasi.NCChannelTimeWindowModel.includeAllMag=3.0
    org.trinet.jasi.NCChannelTimeWindowModel.includeMagAmps=true
    org.trinet.jasi.NCChannelTimeWindowModel.includePeakAmps=true
    org.trinet.jasi.NCChannelTimeWindowModel.includePhases=true
    org.trinet.jasi.NCChannelTimeWindowModel.includeCodas=true
    org.trinet.jasi.NCChannelTimeWindowModel.maxChannels=9999
    org.trinet.jasi.NCChannelTimeWindowModel.maxNoEnergySta=10
    org.trinet.jasi.NCChannelTimeWindowModel.useMasterListCandidateList=false
    org.trinet.jasi.NCChannelTimeWindowModel.candidateListName=RCG-TRINET
    org.trinet.jasi.NCChannelTimeWindowModel.synchCandidateListBySolution=false
    org.trinet.jasi.NCChannelTimeWindowModel.filterWfListByChannelList= false
    org.trinet.jasi.NCChannelTimeWindowModel.includeDataSourceWf=false
    org.trinet.jasi.NCChannelTimeWindowModel.allowedStaTypes=
    org.trinet.jasi.NCChannelTimeWindowModel.allowedSeedChanTypes=
    org.trinet.jasi.NCChannelTimeWindowModel.allowedNetTypes=
    org.trinet.jasi.NCChannelTimeWindowModel.rejectedStaTypes=
    org.trinet.jasi.NCChannelTimeWindowModel.rejectedSeedChanTypes=
    org.trinet.jasi.NCChannelTimeWindowModel.rejectedNetTypes=
```
  
  
### Channels Name Exceptions <a name="jasi-chan-name-exceptions"></a> 
  
*  timeChannelNames – list of timecode channel types delimited by “,:;” that will always be included. (e.g. “ATT”) The time window for these channels is from the earliest start time to latest end time of all the seismic channels.   
  
*  accelerometerChannels – list of channel types delimited by “,:;” that will always be treated as accelerometers rather than velocity transducers. This is only necessary for ambiguous or aberrant channel names (e.g. “EL_” is a low-gain velocity not an acceleration channel). Can treat as pseudo-wildcards because it does matching with ‘startsWith’.   
  

  

