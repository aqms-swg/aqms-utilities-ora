# ampgen {#ampgen}  
  
Calculate ground motions for one event. Runs the AmpGenPp application found in jiggle jar to create a database ampset having amptypes: PGD, PGV, PGA, and SP. Then executs the approprite alarm scripts.  
      
Requires *jiggle.jar*  
  
Sub-directory *bin* has the executable scripts while *cfg* has the configuration files.     
  

*  ``bin/ampgenpp.pl`` : Calculate ground motions for one event.  
*  ``bin/ampgenpp.sh`` : Wrapper around ampgennpp perl script to setup environment variables.  
*  ``bin/ampgen2shake.pl`` : Calculate ground motions for one event, if successful, then executes ShakeMap alarm scripts.  
*  ``bin/ampgenPPcont.pl`` : Continuously running script to generate strong motion amps for events. Acts on events posted to the configured PCS state description (e.g ampgenPP)  
*  ``bin/startAmpgenPPcont.sh`` : Start/restart the process that generates strong motion ampsets in database. This is run at intervals by a cron job to insure that the jobs start at boot time and are always present.  
  
<br>  
  
*  ``cfg/ampgen-generic.props`` : AmpGenPP properties file. It is included by a database specific prop file. 
*  ``cfg/ampgenpp.props`` : The main property file that includes database specific property file as well as AmpGenPP properties file.  
*  ``cfg/ampgenpp-dbname.props`` : The above file created for a database named *dbname*. Use corresponding *dbd-host-dbname.props* file for *auxPropFile.dbhost* property.  
*  ``cfg/ampgenPPcont.props`` : Property file for ampgenPPcont.pl.  
 
