# Waveform Export {#waveformexport}  

Event waveforms are exported in COSMOS V0 format. The AQMS repo contains a couple of options to write and distribute V0 files. 

1.  ms_to_v0 - C program that takes miniseed files as input and produces COSMOS V0 files as output.  
2.  mkv0.pl  - A Perl program that converts miniseed files to COSMOS V0 files.  
   
Both approaches follow similar processing steps and use the PCS system as a trigger to do their work.    
  
## ms_to_v0  
  
[ms_to_v0](https://aqms-swg.gitlab.io/aqms-docs/man-ms_to_v0.html) is a C program that takes miniseed files as input and produces COSMOS V0 files as output. The [source code](https://gitlab.com/aqms-swg/aqms-tools/-/tree/main/ms_to_v0) for ms_to_v0 is part of the AQMS repo

ms_to_v0 can be run continously to facilitate automatic waveform export. 
    
### An example PCS client implementation  
  
A sample implementaton using the PCS system, where you can run the client continuously. 

  *  startExportWF.sh    
    1.  Cron job that starts things on reboot or failure  
    2.  Writes log to ${root}/startCron.log“  

  *  exportWFcont.pl      
    1.  Continuously running script (autostarted by cron). Acts on events posted to the “exportWF” state in “EventStream”. Logs are written to stdout.  
    2.  uses: exportWF (see below).   
      
  *  exportWF.pl    
    1.  Takes event ID as command-line argument  
    2.  Check that event meets waveform export criteria  
    3.  Create list of waveforms that meet export criteria (HL_ or HN_ amp exceeds PGA threshold value)  
    4.  Retrieve waveform files in mSEED format using [STP](???) client.  
    5.  Operate on those files:  
      *  time align the traces ([qmerge](https://aqms-swg.gitlab.io/aqms-docs/man-qmerge.html))  
      *  create XML of station/channel info ([create_v0xml](https://aqms-swg.gitlab.io/aqms-docs/create_v0xml.html))  
      *  convert XML & mSEED files into v0 ([ms_to_v0](https://aqms-swg.gitlab.io/aqms-docs/man-ms_to_v0.html))  
      *  concatenate 3-components into a single file with the correct file name  
    
**Notes:**  
  
  *  If there is known latency in incoming data, exportWFcont should be setup to wait accordingly for the latent data, before calling exportWF.  
    

## mkv0.pl  
  
[mkv0.pl](https://aqms-swg.gitlab.io/aqms-docs/man-mkv0.html) is a Perl program that generates and publishes COSMOS V0 waveforms files.  
  
 
### An example PCS client implementation  
  
A sample implementaton using the PCS system, where you can run the client continuously. 
  
*  startExportWF.sh 
  1.  Cron job that starts things on reboot or failure   
  2.  Writes log to logs/startexportWF.log    
  
*  mkv0Cont.pl   
  1.  Continuously running script (autostarted by cron).  

*  mkv0.pl   
  1.  Takes event ID as command-line argument  
  2.  Makes database queries for information about the event, and for the list of stations for which one or more channels have peak ground acceleration (PGA) at least 0.1 %g (configurable). It determines the distance from station to event hypocenter, computes P traveltime, and sets the time window for waveforms for that station. Requested waveform start is P arrival time minus configured PreEventTime; waveform end is start time plus event magnitude times configured SecsPerMag.  
  3.  Using this list of stations, queries the database for the list of SNCLs that are elegible for use by *mkv0*. Then *mkv0* tries to obtain waveforms for these SNCLs, using the “DataSource” methods given in mkv0.conf. The available methods are SQL query using the [Wave stored procedure package](https://aqms-swg.gitlab.io/aqms-docs/wave-stored-package.html#md_docsrc_details_stored_packages_wave), and the external program [wvc](???). mkv0 uses each DataSource method in the order listed in the configuration file. After each method is tried, *mkv0* evaluates the retrieved waveform. If its start and end times meet or exceed the requested times, that waveform is accepted and no further DataSource methods are tried for that SNCL. At this point the waveform for each SNCL has been read into *mkv0* arrays.  
  4.  Queries the database for all the header information reuired for the COSMOS V0 file. Then for each SNL (station-network-location code) it finds the time period for which each channel's waveforms has data. A COSMOS requirement is that each of the waveforms for one S-N-L must have the same start and end times, with no gaps or missing data. The waveforms are now trimmed in *mkv0's* internal arrays. A COSMOS V0 file is written for each SCNL; these are then concatenated for each station (SN).    
  5.  Finally the COSMOS v0 files are distributed as specified by command-line options, with the details set in the mkv0 configuration file.    


*mkv0* saves all its files in a directory named by the event ID under the configured WorkDir
  
Other programs used by *mkv0*:
  
  *  wvc  
  *  qmerge  
  *  /usr/bin/scp  
  *  /usr/bin/mailx  
   
  
The parameters in makeV0cont are sufficient for the initial run of *mkv0*. For larger events of interest, it is likely that *mkv0* wll need to be run manually in the hours or days following the earthquake. It is helpful to do a preliminary manual run of *mkv0* without any of the publishing options:    
  
```
mkv0 -c ...conf/mkv0.conf eventID
```
  
If this reports producing more V0 files than the previous run, then consider running mkv0 again with the appropriate publishing options.     




      





    
  
    

 






  














