#!/bin/bash

# Kills pcsCont jobs on host

pids=`pgrep -u aqms -l -f 'pcsCont.pl'`
if [ -n "$pids" ]; then
  echo "Killing processes:"
  echo "$pids"
fi

pkill -u aqms -f 'pcsCont.pl'
