#!/bin/bash 
# For tests: not usually invoked standalone, instead use startPcsCont.sh 
# this should be run in background with output redirected to a log file
# gets configuration from <jobname>.props : ../cfg/pcsCancelCont.props
#
# For crontab job we need to source the environment variables, does oracle too
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

$rootdir=$ENV{TPP_HOME}/pcs;
${rootdir}/bin/pcsCont.pl ${rootdir}/cfg/pcsCancelCont.props
