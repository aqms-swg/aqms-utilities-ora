#!/bin/bash
#
# run the processes that checks the number of PCS state rows waiting to be processed
#0,10,20,30,40,50 * * * * /app/aqms/tpp/pcs/runWatchdog.sh > /dev/null 2>&1
#
# For crontab job we need the environment variables defined
# below defines aqms runtime environment variables if needed 
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi
# working dir needed for relative cfg filename paths
cd $TPP_HOME/pcs/bin
./pcsWatchdog.pl "$@"
