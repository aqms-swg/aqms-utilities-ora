#!/bin/bash
#
# Start/restart the processes that process TPP PCS states
# crontab -e 
#0,10,20,30,40,50 * * * * /app/aqms/tpp/pcs/startPcsTasks.sh > /dev/null 2>&1
#
# Need to define runtime environment first
# For crontab job we need to source the environment variables, does oracle too
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

appHome="${TPP_HOME}/pcs"
joblist="${appHome}/cfg/pcsJobList.cfg"

usage() {
  echo "Syntax: $0 [-c] [-j] -[v|n]
    -c days     : max days back to keep log files
    -h          : this usage
    -j          : name of job list file [default = $joblist]
    -n          : do not check the pcs table posting counts for backlogged posts
    -v          : if do pcs table check, print the OK pcs states found 

  A chron job runs this script at intervals to insure that the jobs in the
  list are always running.

INPUT:
 Expects "../cfg/pcsJobList.cfg" a text file that lists jobs to start-up (one per line) like:
 pcsFinalizeCont
 pcsCancelCont

 The configuration file for each listed job defaults to ../cfg/<jobname>.props 

 Unless -n option, the pcsWatchdog script is also run; it reads state descriptions from ../cfg/pcsWatchdog.cfg
 using the -v option prints the state descriptions in the cfg that are OK, that don't have a reportable backlog.
 If postings for a specified state description exceed the configured count, an email is sent to TPP_ADMIN_MAIL

OUTPUT:
  This script redirects output to ../logs/start/startPcsTasks_<timetag>.log
  and each job task redirects output to to log file called  ${appHome}/logs/${jobname}.log
"
  exit $1
}

# Email address if job is restarted
mailaddr=${TPP_ADMIN_MAIL}
host=`uname -n | cut -d'.' -f1`

logdir="${appHome}/logs"

timetag=`date -u +"%Y%m%d%H%M"`
#one log per calendar day UTC
startLog="${logdir}/start/startPcsTasks_${timetag:0:8}.log"
restartcnt=0
startedcnt=0

cd ${appHome}/bin

# By default check pcs state table postings specified in ../cfg/pcsWatchdog.cfg
opt_n=1

# By default, don't print the OK pcs states found in cfg list 
opt_v=''

maxDaysBack=''

# Use options
while getopts ":c:hj:nv" opt; do
  case $opt in
    c)
      maxDaysBack="$OPTARG"
      ;;
    h)
      usage 0
      ;;
    j)
      joblist="$OPTARG"
      ;;
    n)
      opt_n=0   # don't check the pcs table posting counts
      ;;
    v)
      opt_v="-v"
      ;;
    \?)
      echo "====================================================" >> ${startLog}
      echo "Error: Invalid command switch option: -$OPTARG" | tee -a ${startLog}
      usage 1
      ;;
    :)
      echo "====================================================" >> ${startLog}
      echo "Error: Command option -$OPTARG requires an argument." | tee -a ${startLog}
      usage 2
      ;;
  esac
done

#Here use shift to offset input $@ to rest of args following the switch options
shift $(($OPTIND - 1))

# Parse optional input args, after switches?
#myarg1=$1

# Block wrapper for redirection to startLog
{
  echo "======================================================================="
  echo "Checking for PCS tasks at `date -u +'%F %T %Z'`"
  
  # check all jobs (scripts) in the list
  job="${appHome}/bin/pcsCont.pl"
  
  # Moved joblist var to top of script to allow option override
  #joblist="${appHome}/cfg/pcsJobList.cfg"
  if [ ! -f "$joblist" ]; then
      echo "Input joblist file not found: $joblist" | mailx -s "FAILED $0 on ${host}" ${mailaddr}
      usage 3
  fi
  for jobname in $(grep "^[^#]" ${joblist} | grep "^[^ ]") 
  do
    jobcfg=../cfg/${jobname%.*}.props 
    grepStr="/${jobname}[.]"
  
    # look for the pid of an active job
    pid=`pgrep -f "${grepStr}"`
  
    # if no job running for this host, START ONE
    if [ -z "${pid}" ]
    then
        let restartcnt=(restartcnt + 1)
  
        logfile="../logs/${jobname%.*}.log"
  
        ${job} -l ${logfile} ${jobcfg} >>${startLog} 2>&1 &
  
        # send e-mail after waiting for background process to write output
        if [ -n "$mailaddr" ]; then
            sleep 3
            pid=`pgrep -f "${grepStr}"`
            if [ -n "${pid}" ]; then
                let startedcnt=(startedcnt + 1)
                echo "Started PCS job ( ${job} -l ${logfile} ${jobcfg} ) on $host (pid=${pid})"
                tail -100 ${logfile} | mailx -s "Restart ${jobname} on ${host} (pid=$pid)"  ${mailaddr}
            else 
                echo "ERROR: Unable to restart PCS job ( ${job} -l ${logfile} ${jobcfg} ) on $host"
                tail -6 ${startLog} | mailx -s "FAILED restart ${jobname} on ${host}"  ${mailaddr}
            fi
        fi
  
    else 
        echo "PCS job ${jobname} ${jobcfg} running as pid=${pid}"
    fi
  done
  
  echo "PCS jobs needing restart=${restartcnt}, started=${startedcnt}"

  echo "-----------------------------------------------------------------------"
  if [ $opt_n -eq 1 ]; then
    # Check for stuck PCS states
    echo "Checking for PCS state table posting backlogs (using ../cfg/pcsWatchdog.cfg)"
    ${appHome}/bin/pcsWatchdog.pl $opt_v ${appHome}/cfg/pcsWatchdog.cfg
  else
    echo "No check done for a PCS state table posting backlog"
  fi
  
  # Do log cleanup
  if [ -n "$maxDaysBack" ]; then 
    echo "-----------------------------------------------------------------------"
    if [[ "$maxDaysBack" =~ ^[0-9]+$ && "$maxDaysBack" > "0"  ]]; then
      echo "Deleting files in $logdir older than $maxDaysBack days"
      find $logdir -mtime "+$maxDaysBack" -print -exec \rm '{}' \;
    else
      echo "Invalid max days back to keep logs: $maxDaysBack"
    fi
  fi 
} >> ${startLog} 2>&1
