#!/usr/bin/perl
use strict;
use warnings;

# NOTE: pcsAppProps property in the pcsCont input properties file for DELETED or DISTRIBUTE job
# is evaluated in the command line specified by the pcsCommand property as the input argument
# for this script

# Requires a property file to parse config values 
if ( @ARGV < 2 ) {
    print "Usage: $0 <evid> <prop-file>\n";
    die "$0 Fatal Error: Missing the event id and/or properties filename input arguments\n";
}

my $evid = $ARGV[0];
die "Input event id must be a number\n" if $evid !~ /^\d+$/;

my $propFile = $ARGV[1];
die "$0 Unable to run, properties file: $propFile DOES NOT EXIST\n" if ! -e $propFile;

print "Loading properties from file: $propFile\n";

############################################################

# Variables whose values are parsed from the input properties file

my $hostAppCmd;
# e.g. pcsFinalizeCont => alarmsendCMS and pcsCancelCont => alarmcancelCMS
#hostAppCmd=$ENV{TPP_HOME}/alarm/bin/alarmsendCMS.sh
#hostAppCmd=$ENV{TPP_HOME}/alarm/bin/alarmcancelCMS.sh

my @hostdbpairs;
my $hostLogFile = undef; 
my $hostLogTail = 3; 

# hash for mapping properties
my %myprops;
eval {
    loadProperties(\%myprops, $propFile);
    printConfig();
};
if ( $@ ) {
    die "$0 Fatal config error: $@";
}

# need to check this host to prop specified alarm host 
my $myhost = `uname -n`;
chomp $myhost;
$myhost =~ s/\..*$//; # grab first element

# MAIN work done here after configuration 
foreach my $val (@hostdbpairs) {
       my ($_host, $_db) = split(/[,>:]/,$val);

       my $cmd;
       if ( ($myhost =~ m/^$_host/) ) {
         #$cmd = eval "$hostAppCmd";
         $cmd ="$hostAppCmd $evid $_db";
       }
       else {
         $cmd = "ssh $_host $hostAppCmd $evid $_db";
       }
       print "==== $cmd\n";
       print `$cmd`;

       # NOTE: dump result of application command from the log file on the host server
       if ( defined($hostLogFile) ) {
           if ( ($myhost =~ m/^$_host/) ) {
               $cmd = "tail -$hostLogTail $hostLogFile";
           }
           else {
               $cmd = "ssh $_host tail -$hostLogTail $hostLogFile";
           }
           #$date = `date -u +'%F %T %Z'`;
           #print "==== Tail $hostLogTail lines from $hostLogFile on $_host @ $date"; 
           print "==== $cmd\n";
           print `$cmd`; 
           print "==== End of tail ====\n";
       }
}
      
exit 0;

####################################################################################

sub loadProperties {

    my ($defPropsRef, $propFile) = @_;

    open CFGFILE, $propFile or die "$! : unable to open properties file $propFile";
    
    my $pat = "[=]"; # config file key value separator characters, e.g. only one "="
    
    while (<CFGFILE>)
    {
       chomp;
       next if /^(\s)*#/; # skip comments
       next if /^(\s)*$/; # skip blank lines
       my ($key, $val) = split /$pat/;
       $key =~ s/^\s+//;  # remove leading spaces key
       $key =~ s/\s+$//;  # remove trailing spaces key
       $val =~ s/^\s+//;  # remove leading spaces value
       $val =~ s/#.*$//;  # remove any trailing comment text from value
       $val =~ s/\s+$//;  # remove trailing spaces from value
       $myprops{$key} = $val;
    }
    close CFGFILE;

    # ###########################################################

    my $tmp = $myprops{"hostAppCmd"};
    $hostAppCmd = "$tmp"  if ( defined ($tmp) );
    defined ($hostAppCmd) or die "Required property 'hostAppCmd' undefined in $propFile\n";

    my $hostcfg = $myprops{"hostDbPairList"};
    defined ($hostcfg) or die "Required property 'hostDbPairList' undefined in $propFile\n";

    @hostdbpairs = split(/ /,$hostcfg);
    my $idx = 0;
    foreach my $val2 (@hostdbpairs) {
       #print "$val2\n"; # DEBUG
       my ($host, $db) = split(/[,>:]/,$val2);
    }

    $tmp = $myprops{"hostLogFile"};
    $hostLogFile = "$tmp" if ( defined ($tmp) );
    #defined ($hostLogFile) or die "Required property 'hostLogFile' undefined in $propFile\n";

    $tmp = $myprops{"hostLogTail"};
    $hostLogTail = "$tmp" if ( defined ($tmp) );

} # end of sub loadProperties

sub printConfig {
    print "#### Current Configuration ###\n";
    print "hostAppCmd = $hostAppCmd\n";
    print ("hostLogFile = ",(defined($hostLogFile) ? $hostLogFile : "UNDEF"),"\n");
    print "hostLogTail = $hostLogTail\n";
    print "Application hosts are:\n@hostdbpairs\n"; 
    print "##############################\n";
}

