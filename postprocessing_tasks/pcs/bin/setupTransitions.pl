#!/usr/bin/perl
#
# Create the standard PCS transitions for a CISN system 
#
# parse optional switches
use strict;
use warnings;

use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use MasterDbs;

use Getopt::Std;
use vars qw/$opt_h/;  #prevent "only used once" error
getopts('h');

if ( $opt_h ) { usage(); }

my $dbasename = "$masterdb";
if ( @ARGV > 0) { $dbasename = $ARGV[0] };

print "Note: This script is DEPRECATED: use tpp/dbinfo/bin/setupTransitions.pl\n"
print "Database stream name : $dbasename\n";
# Events
print `$TPP_BIN_HOME/puttrans.pl EventStream ${dbasename} NewEvent    1  EventStream ${dbasename} MakeGif    100 `;
print `$TPP_BIN_HOME/puttrans.pl EventStream ${dbasename} NewEvent    1  EventStream ${dbasename} exportWF   100 `; 
print `$TPP_BIN_HOME/puttrans.pl EventStream ${dbasename} NewEvent    1  EventStream ${dbasename} rcg_rt     100 `;  
print `$TPP_BIN_HOME/puttrans.pl EventStream ${dbasename} NewEvent    1  EventStream ${dbasename} ExportAmps 100 `;  
print `$TPP_BIN_HOME/puttrans.pl EventStream ${dbasename} MakeGif     1 `;
print `$TPP_BIN_HOME/puttrans.pl EventStream ${dbasename} exportWF    1 `; 
print `$TPP_BIN_HOME/puttrans.pl EventStream ${dbasename} rcg_rt      1 `;
print `$TPP_BIN_HOME/puttrans.pl EventStream ${dbasename} ExportAmps  1 `;

# Triggers
print `$TPP_BIN_HOME/puttrans.pl EventStream ${dbasename} NewTrigger  1  EventStream ${dbasename} MakeGifTrg 100 `;
print `$TPP_BIN_HOME/puttrans.pl EventStream ${dbasename} NewTrigger  1  EventStream ${dbasename} rcg_trig   100 `;  
print `$TPP_BIN_HOME/puttrans.pl EventStream ${dbasename} MakeGifTrg  1 `;
print `$TPP_BIN_HOME/puttrans.pl EventStream ${dbasename} rcg_trig    1 `;

# -----------------------------------------------------------
# Help message about this program and how to use it
#
sub usage {
    print <<"EOF";

    Note: This script is DEPRECATED: use tpp/dbinfo/bin/setupTransitions.pl

    Setup standard PCS_TRANSITION table rules
    usage: [$0 [ old and new db source thread (defaults to "$masterdb")] 
    -h  : print usage info
    example: $0 mydb

EOF
    exit;
}
