#!/usr/bin/perl
#
# Run by a cronjob script to continuously process any event ids posted to a PCS configured state e.g.:
# Script uses configuration files found in $ENV{TPP_HOME}/pcs/cfg :
#
# TPP TPP FINALIZE
# TPP TPP DELETED
#
# pcsCont [-i or -I] [-o]  <propFile> 
#    defaults:                     none
#    -i            : re-read propFile properties after loop sleep
#    -I            : re-read propFile properties after loop sleep and print their values too
#    -o            : off, skip further processing of event, just result its posting (for testing, or shadow?)
#
# The configuration file defines the application command, the application hosts and the databases to be used,
# Its name should contain the jobname string so that "pgrep" can use to easily discriminate the processing stream.
# E.g.  pcsCancelCont.props
# Some property options can be set by command line switch or by a property in the configuration file.
# As currently implemented, a property defined in the configuration file OVERRULES its command switch value.
#
# Example:
# pcsCont.pl pcsCancelCont.props
# pcsCont.pl pcsFinalizeCont.props
#
#NOTE for alarming action in above properties files hostAppCommand is a CMS message script found  in "$ENV{TPP_HOME}/alarm/bin" 
# the CMS scripts are wrappers that run these binaries:
# $ENV{RT_HOME}/utils/alarm/sendcancel -c ${propFile} ${evid}
# $ENV{RT_HOME}/utils/alarm/alarmevent -c ${propFile} ${evid}

use strict;
use warnings;

use File::Basename;

use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use MasterDbs;
use Utils;

# parse optional switches
use Getopt::Std;

our ( $opt_c, $opt_d, $opt_h, $opt_l, $opt_v );
getopts('cd:hl:v');

# Make STDOUT "hot" e.g. autoflush
# By default perl running in background flushes its buffers only once/min
# so monitoring the log file makes it appear the script isn't working!
select STDERR; $| = 1;
select STDOUT; $| = 1;

# Environmental variables supplied by a bash wrapper sourcing /usr/local/etc/aqms.env
# First check runtime environment
defined $ENV{TPP_HOME} or die "$0 Error: Required aqms.env environment vars are not defined\n$!";

$ENV{PCS_HOME} = "$ENV{TPP_HOME}/pcs";
die "Missing application directory\n$!" if ( ! -d $ENV{PCS_HOME} ) ;
chdir "$ENV{PCS_HOME}/bin";

usage() if ( $opt_h || @ARGV == 0 );

# Host machine running this script
my $host = `hostname`;
chomp $host;
$host =~ s/\..*$//; # grab first element

my $logFile  = "../logs/" . basename($0,'.pl') . '.log';
if ( $opt_l ) { # override default
  my ($fname, $path, $suffix) = fileparse("$opt_l");
  die "Unknown log file path: \"$path\"" if ( $path  && ! -e $path); 
  $logFile = $opt_l;
}

# Direct all output to log file, rename existing log if present
print "All output will be directed to $logFile\n";
rotateLogs($logFile);

my $date = `date -u +'%F %T %Z'`;
chomp $date;
print "Starting $0 on $host at $date\n";

# ==========================================================================
# Behavior variables 
# ==========================================================================
# Hash array to hold configuration properties, like object attributes
my ( %myprops );
# Generic PCS processing property settable vars:
my ( $pcsCommand );
my ( $pcsAppName, $pcsAppProps, $pcsDbase, $pcsGroup, $pcsTable, $pcsState, $pcsResult, $pcsStallTime );
my ( $sleepTime, $archivalTime, $minMag, $maxDaysBack );
my ( $propFile, $debug, $verbose, $reconfig, $noop );

# Rotate logs UTC daily if true, default is no rotation
my $rotateLogsDaily = 0;

# Below props specific to pcsCont TPP TPP DELETED FINALIZE should be delegated to a pcsCommand script invoked by processEvent in a pcsAppProps
#my ($hostAppCmd, $hostLogFile);
#my $hostLogTail = 3; 
#my (@hostdbpairs);
#my @hostdbpairs;

# Define default runtime properties 
setDefaultProperties(\%myprops);

# Check for properties file to override the default config
$propFile = "$ARGV[0]" if (@ARGV > 0);
die "$0 Error: missing property file: $propFile \n$!" if ! -e $propFile; 
print "Loading properties from file: $propFile\n";

loadProperties(\%myprops, $propFile);

# Report starting configuration
printConfig();

# Are all props valid ?
checkProperties();

# set verbose mode if option set, and not in props
$verbose = 1 if $opt_v;

# Declare dynamic variables used inside infinite while loop
# that may need to be accessed outside block scope

my $amEnabledForPCS = 'true'; # reset in loop by checking with db
my ($evid, $age, $mag, $wait, $cutoff, $status);

# ==========================================================================

my $dayNumber = `date -u '+%d'`; #  note LF is not stripped

# ---- MAIN CONTINUOUS LOOP ----
while (1) {

    # code block here is executed on every check interval
    # test for change of day, do some chores
    # since 'redo' redoes the iteration we put this code at top of loop
    if ( $rotateLogsDaily && ($dayNumber != `date -u '+%d'`) ) {
        print "Executing change-of-day chores @ ". `date -u +'%F %T %Z'`;
        rotateLogs($logFile);
        printConfig();
        $dayNumber = `date -u '+%d'`;
    }
    else { # keep file timestamp current (proof of redo looping)
        my $now = time();
        utime $now, $now, $logFile;
    }

    # See if an event id is posted to PCS_STATE table in db
    $evid = `$ENV{TPP_BIN_HOME}/next.pl -d $pcsDbase -s $pcsStallTime $pcsGroup $pcsTable $pcsState`;
    chomp $evid;

    if ($evid eq '' || $evid eq '0') { # No event: sleep - zzzz
        sleep $sleepTime;
        if ($reconfig) { # reload props file
            if ( ! loadProperties(\%myprops, $propFile) ) {
              checkProperties();
              printConfig() if ( $reconfig > 1);
            }
        }
        redo;
    }

    # Have event id
    $date = `date -u +'%F %T %Z'`;
    chomp $date;
    print "\nNEXT: $evid \@ $date using primary db:" . `$ENV{TPP_BIN_HOME}/amPrimaryDb.pl`;

    # check db host app processing status
    $amEnabledForPCS = `$ENV{TPP_BIN_HOME}/enabledForPCS.pl $pcsAppName $pcsState`;  
    chomp $amEnabledForPCS; # may have newline at end

    if ( "$amEnabledForPCS" ne 'true' ) {
        print "SKIP: Not primary app host on $host for $pcsAppName $pcsState, a no-op for $evid\n";
        next; # result it
    } 

    # check age
    $age = getEventAge($evid); # age of event in seconds
    if ( $age <= 0 ) {  # Bogus FUTURE event (yes, this happened)
        print "SKIP: No valid event found in db, age = $age secs for $evid\n";
        next; # result it
    }

    # check cutoff age only if input property for maxDaysBack was specified
    $cutoff = (86400 * $maxDaysBack);
    if ( $cutoff > 0 ) {
        if ( $age > $cutoff ) {
            printf "SKIP: Too old, %8.2f > $maxDaysBack days for %d\n", ($age/86400), $evid;
            next; # result it
        }
    }

    # allow at least archivalTime secs post-origin for data to be generated (e.g. amps, mags, waveforms ...)
    if ( $age < $archivalTime ) {
        $wait = ($archivalTime - $age);  # wait for data to be archived to db
        print "WAIT: $wait secs to age $evid, for its data to be archived ...\n";
        sleep $wait;
    }

    # check for qualifying magnitude
    $mag = getEventMag($evid);
    if (  $mag < $minMag ) {
        print "SKIP: Too small, $mag < $minMag (minMag) for $evid\n";
        next; # result it
    }

    print "PROC: $evid Mag = $mag \@ " . `date -u +'%F %T %Z'`;
    $status = processEvent($evid);
    print "STAT:" . ($status >> 8) . " for $evid  \@ " . `date -u +'%F %T %Z'` if $status && $verbose;

} continue { # after iteration, done with event
    # !!! NOTICE: Result the event even though the forked process is not done
    #             else next time through the loop we'll get the SAME evid
    #
    # Implement a RESULT sub here to to override actions in subclasses
    #
    # Done - result it, get value to use
    my $result = $pcsResult;
    #$result = ($status >> 8) if $status; #  reset to returned result from processing event, like error code ?
    # ------------------------------------------------------------------------
    # Result all posted ranks for id using -r 0 switch:
    # Transitions defined in PCS for that result should remove this posting from PCS 
    # ------------------------------------------------------------------------
    $status = `$ENV{TPP_BIN_HOME}/result.pl -d $pcsDbase -r 0 $evid $pcsGroup $pcsTable $pcsState $result`;
    print $status;
    print "DONE: $evid \@ " .  `date -u +'%F %T %Z'`;

}  # end of infinite while loop

# ####################################################
# Subroutines
# ####################################################

# Return event magnitude, or -9 if undefined
sub getEventMag {
    my ($evid) = @_;
    my $emag = `$ENV{TPP_BIN_HOME}/eventMag.pl -d $pcsDbase $evid`;
    chomp $emag;
    if ( $emag !~ m/\d/ ) {
       print "NOTE: No magnitude found for $evid\n";
       return -9;
    }
    $emag = -9 if $emag < -9; 
    return $emag;
}

#
# open log file.
#
sub openLogs {
    my ($logFile) = @_;   # get arg

    open STDOUT, "> $logFile" or die "Can't redirect STDOUT to $logFile: $!\n";
    open(STDERR, ">&STDOUT") || die "Can't dup err to stdout";

    select(STDERR); $| = 1; # make unbuffered
    select(STDOUT); $| = 1; # make unbuffered

    print "Log file opened: " . `date -u +'%F %T %Z'`;

  }

# ###################################################
# Rotate log files
#
# rotateLogs ();
#
# You can call this at startup to rename an old existing log
# and open a new one.
sub rotateLogs {
    my ($logFile) = @_;   # get arg

    if (-e $logFile) {
        my $timestamp = `date -u '+%Y%m%d%H%M'`;
        chomp $timestamp;
        #my $cmd = "mv $logFile ${logFile}.$timestamp";
        my ($fname,$path,$suffix) = fileparse("$logFile",qr"\..[^.]*$");
        my $cmd = "mv $logFile ${path}${fname}_${timestamp}${suffix}";
        `$cmd`;
    }

    # open new
    openLogs($logFile);
}

# ------------------------------
# Send mail about a problem
# Sends last 25 lines of the log
#
sub sendMail {
    my ($subject) = @_;

    # send email
    my $mailaddr = $ENV{"TPP_ADMIN_MAIL"};
    if ( $mailaddr ) { print `tail -25 $logFile | mailx -s "$subject"  "$mailaddr"`; }
}

####################################################################################
sub processEvent {

    my ($evid) = @_;   # get arg

    #my $cmd = "$ENV{PCS_HOME}/bin/ppAlarm.pl ${evid} $pcsAppProps 2>&1";
    
    my $cmd;
    # must force substitution of variables from $pcsCommand and need quotes around expanded string
    my $command = '$cmd = ' . "\"$pcsCommand\"";
    eval $command;

    if ( $noop ) {
      print "EXEC: $cmd\n" if ( $debug ne '0' );
      print "NOOP: $evid\n" if $verbose;
      return 0; # assumes 0 is the same as system success
    }
    print "EXEC: $cmd\n" if $verbose;

   # need EXTERNAL command script declared by pcsCommand app, invoked with pcsAppProps for host props like hostdbpairs hostLogFile hostLogTail!!!
   # script would do the ssh alarming functions
    # Note /bin/sh shell forked by system does not have the ENV vars
    my $result = `$cmd`;
    my $status = $?;
    sendMail("FAILED $host: $cmd ") if ($status >> 0) != 0;
    print $result if defined $result;
    return $status;
    #return 0;
}

# Return the age of the given event in integer seconds
# Returns "0" if no such event.
sub getEventAge {
   my ($evid) = @_;
   my $secsOld = `$ENV{TPP_BIN_HOME}/eventAge.pl -d $pcsDbase $evid`;  #age of event in secs
   chomp $secsOld;
   return $secsOld;
}

# Read properties into hash from properties file, merge with defaults
sub loadProperties {
    my ($defPropsRef, $propFile) = @_;
    my $fileHashRef = {};

    die "Invalid input, properties file D.N.E.\n $!" if ! -e "$propFile";

    $date = `date -u +'%F %T %Z'`;
    chomp $date;
    print "Loading properties from file: $propFile \@ $date\n"  if ( $reconfig > 1 );

    eval {
      # Optional regex for split delimiter for key value is a string like "[=: ]+" 
      $fileHashRef = Utils::->readPropsFromFile($fileHashRef, $propFile, " *[=] *");
    };
    die "Error loading properties from $propFile\n $!" if ( $@ );

    # Check if properties file changed
    my $retval = equalsHash($fileHashRef, $defPropsRef);
    #print "retval=$retval\n";

    if ( scalar( keys %$fileHashRef ) != 0 ) {
        setProperties( $fileHashRef );
        # Use slice assignment to merge prop file hash into the default props by refernce:
        @{$defPropsRef}{ keys %$fileHashRef } = values %$fileHashRef;
    }

    $debug = ( $defPropsRef->{'debug'} eq 'true' || $defPropsRef->{'debug'} eq '1' ) if exists $defPropsRef->{'debug'};
    if ( $debug ) {
        $verbose = 1;
        print "$0 Properties:\n";
        Utils::->printProps($defPropsRef, " = ");
    }

    return $retval;  # 0 or 1
}

# Lookup property in input hash, return default if no key
sub getProperty {
    my ($propsRef, $propName, $defaultVal) = @_;
    my $val = $propsRef->{$propName};
    $val = $defaultVal if ! defined $val;
    return $val;
}

####################################################################################

# Populate input hash with current config
sub getProperties {

    my ($propsRef) = @_;

    $propsRef->{pcsAppName} = $pcsAppName;
    $propsRef->{pcsAppProps} = $pcsAppProps;
    $propsRef->{pcsDbase} = $pcsDbase;
    $propsRef->{pcsGroup} = $pcsGroup;
    $propsRef->{pcsTable} = $pcsTable;
    $propsRef->{pcsState} = $pcsState;
    $propsRef->{pcsResult} = $pcsResult;
    $propsRef->{pcsStallTime} = $pcsStallTime;
    $propsRef->{pcsCommand} = $pcsCommand;
    $propsRef->{noop} = $noop;
    $propsRef->{reconfig} = $reconfig;
    $propsRef->{sleepTime} = $sleepTime;
    $propsRef->{archivalTime} = $archivalTime;
    $propsRef->{maxDaysBack} = $maxDaysBack;
    $propsRef->{minMag} = $minMag;

    $propsRef->{rotateLogsDaily} = $rotateLogsDaily;

    return $propsRef;
}

# set internal config to property hash values
sub setProperties {

    my ($propsRef) = @_;

    my $str = "";

    # PCS config props
    $str = getProperty($propsRef, 'pcsAppName');
    if ( defined $str ) { $pcsAppName = $str; }

    $str = getProperty($propsRef, 'pcsAppProps');
    if ( defined $str ) { $pcsAppProps = $str; }

    $str = getProperty($propsRef, 'pcsDbase'); 
    if ( defined $str ) {
       $pcsDbase = $str;
       $pcsTable = $str;
    }

    $str = getProperty($propsRef, 'pcsGroup'); 
    if ( defined $str ) { $pcsGroup = $str; }

    $str = getProperty($propsRef, 'pcsTable');
    if ( defined $str ) { $pcsTable = $str; }

    $str = getProperty($propsRef, 'pcsState'); 
    if ( defined $str ) { $pcsState = $str; }

    $str = getProperty($propsRef, 'pcsResult'); 
    if ( defined $str ) { $pcsResult = $str; }

    $str = getProperty($propsRef, 'pcsStallTime'); 
    if ( defined $str ) { $pcsStallTime = $str; }

    # app args are specific to implementation like where to insert props and $evid in cmd string
    $str = getProperty($propsRef, 'pcsCommand'); 
    if ( defined $str ) { $pcsCommand = $str; }

    # Other props
    $str = getProperty($propsRef, 'verbose');
    if ( $str ) { $verbose = $str; }
    else { $verbose = 0; }

    $str = getProperty($propsRef, 'noop');
    if ( defined $str ) { $noop = $str; }

    $str = getProperty($propsRef, 'reconfig');
    if ( defined $str ) { $reconfig = $str; }

    $str = getProperty($propsRef, 'sleepTime');
    if ( defined $str ) { $sleepTime = $str; }

    $str = getProperty($propsRef, 'archivalTime');
    if ( defined $str ) { $archivalTime = $str; }

    $str = getProperty($propsRef, 'maxDaysBack');
    if ( defined $str ) { $maxDaysBack = $str; }

    $str = getProperty($propsRef, 'minMag');
    if ( defined $str ) { $minMag = $str; }
        
    $str = getProperty($propsRef, 'rotateLogsDaily');
    if ( defined $str ) { $rotateLogsDaily = $str; }

}

# print current internal config
sub printConfig {
    #
    #dumpArgs();
    #
    print "!!!!!!!! NOTICE !!!!!!!!\n";
    print "Output from multiple forked processes may be intermingled in this log!\n";
    print "\n";

    print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
    print "Property file=$propFile\n";
    print "App $pcsAppName on $host connects to $pcsDbase and processes PCS state posts of:\n";
    print "  $pcsGroup $pcsTable $pcsState for result $pcsResult where\n";
    print "  pcsStallTime=$pcsStallTime secs, minimum seconds age of state posting\n";
    print "  sleepTime=$sleepTime secs of hibernation between iterations when no posted id\n";
    print "  reconfig=$reconfig (=1 reloads properties after sleep)\n";
    print "  verbose=$verbose (=1 prints extra processing output and app properties)\n";
    print "  archivalTime=$archivalTime secs, minimum post-origin aging before processing\n";
    print "  maxDaysBack=$maxDaysBack days, maximum event age eligible for processing (any age if <= 0 )\n";
    print "  minMag=$minMag, minimum magnitude eligible for processing\n";
    print "  rotateLogsDaily=$rotateLogsDaily, where =0 no, =1 yes\n";
    print "\nProcessing $pcsCommand\n";

    print "\n";

    if ( $pcsAppProps ) {
      print "Using pcsAppProps file $pcsAppProps\n";
      if ( $verbose ) {
        print "\nProperties:\n";
        open(my $fh, $pcsAppProps);
        while (my $line = <$fh>) { print $line; }
        close($fh);
      }
    }
    print "\n" ;

    print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
}

sub setDefaultProperties {

    my ($propsRef) = @_;

    $propFile = "unknown";
    $pcsAppName = "unknown";
    $pcsCommand = '$ENV{PCS_HOME}/bin/ppAlarm.pl ${evid} $pcsAppProps 2>&1';
    $pcsAppProps = "$ENV{PCS_HOME}/cfg/ppAlarm.props";  # Java properties

    #Default to master else parse command line arg option
    $pcsDbase = $masterdb;
    if ($opt_d) { $pcsDbase = $opt_d; }

    # NOTE: pcs_transition table sourceTable could be some string other than the $pcsDbase
    # default PCS state description to operate on:
    $pcsGroup = "EventStream";
    $pcsTable = "$pcsDbase";
    $pcsState = "unknown";
    $pcsResult = 1;
    $pcsStallTime = 0;

    $noop = 0;              # =0 always process, =1 do not process posts, for testing
    $rotateLogsDaily = 0;   # =0 no daily rotation, =1 daily rotation
    $reconfig = 0;          # do not reload properties after sleep between next post poll
    $sleepTime  = 30;       # secs to sleep between checks for new events
    $archivalTime = 30;     # secs post-origin to wait for data to be archived
    $minMag = -9;           # minimum magnitude to process evid

    #NOTE: maxDaysBack special value 0 implies do not check age for cutoff 
    $maxDaysBack = 7;    # Don't process events older than this
    if ($opt_c) { $maxDaysBack = $opt_c; }
    
    getProperties($propsRef);

}

sub checkProperties {
    # 2 aux app properties file are required 
    die "$0 Error: missing property file: $pcsAppProps\n$!" if ($pcsAppProps && ! -e $pcsAppProps); 
}

# This belongs in the utils pm ?
sub equalsHash {
    my ($a, $b) = @_;
    my @ka = keys %$a;
    my @kb = keys %$b;
    return 0 if ( @ka != @kb );
    my %cmp = map { $_ => 1 } @ka;
    for my $key (@kb) {
      last unless exists $cmp{$key};
      last unless $a->{$key} eq $b->{$key};
      delete $cmp{$key};
    }
    return ( scalar( keys %cmp ) ? 0 : 1 );
}

sub usage {

  print <<'EOF';

Usage $0  [-d -c -v] [prop-file]
   -d dbase, default for PCS when not in props
   -c cutoff maxDays when not in props
   -v verbose output (e.g command strings and current properties)
   -h usage

DEFAULT propFile when not specified on command line is: 
  $ENV{TPP_HOME}/pcs/cfg/pcsCancelCont.props
  $ENV{TPP_HOME}/pcs/cfg/pcsFinalizeCont.props

Default values are used when not defined in property file:
 pcsDbase     $masterdb value
 pcsGroup     TPP
 pcsTable     TPP 
 pcsState     DELETED or FINALIZE 
 pcsResult    1
 pcsStallTime 0   seconds
 sleepTime    30  seconds
 archivalTime 30 seconds
 maxDaysBack  7   (set 0 for no cutoff age)
 minMag       -9  no min 
 noop         0   always process, =1 do not process posts, only for testing
 reconfig     0   do not reload properties after sleep between next post polls

 pcsAppName     pcsFinalizeCont or pcsCancelCont
 pcsCommand     '$ENV{TPP_HOME}/pcs/bin/ppAlarm.pl $evid $pcsAppProps 2>&1'
 pcsAppProps    $ENV{TPP_HOME}/pcs/cfg/ppAlarm.props

EOF
  exit;
}
