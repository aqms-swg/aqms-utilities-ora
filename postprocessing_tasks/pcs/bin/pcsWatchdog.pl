#!/usr/bin/perl
# ###########################################################
# 
# Check the SOH of the PCS system
#
# Syntax : pcsWatchdog [-h [-v -n] [-d dbname] [config_file]
#
# Input file syntax (white-space delimited): 
#
# # Group Table State Age(secs) Count Message
# TGroup Ttable Tstate  120  1  "Test state is hung!"
#
# If the number of events pending in the given state for the given
# time interval exceeds the value of "Count" raise a warning.
#
# Valid SQL wildcards for the 1st 3 state strings are allowed.
# Strings are Case-Sensitive.
# If you want to count all evids in a state just use an Age=0.
# If you want to alarm on any events posted set count=0
#
# EXAMPLES:
#
# X Y Z 60 0 "There are XYZs event not processed after 60secs"
# X Y Z 60 5 "Six or more XYZ events not processed after 60secs"
# X Y Z  0 5 "Six or more XYZ event awaiting processing"
#
# ###########################################################


use strict;
use warnings;

use DBI;            # Load the DBI module
# get masterdb and username/password
use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use DbConn;
use MasterDbs;

# parse optional switches
use Getopt::Std;
our ($opt_h, $opt_d, $opt_n, $opt_v);
getopts('d:hvn:');

my $configFile = "$ENV{TPP_HOME}/pcs/cfg/pcsWatchdog.cfg";
my $maxknt = 25;

usage() if $opt_h;

# check command line for arg
$configFile = "$ARGV[0]" if ( $ARGV[0] ); 

$maxknt = $opt_n if ( defined $opt_n && $opt_n >= 0 );

# check file exists
if (!-e $configFile) {
    print "$0 Error: PCS row signature config file does not exist: $configFile\n";
    exit;
}

my $dbasename = $masterdb;        #dbase not spec'ed in command :. default
chomp $dbasename;
if ( $opt_d ) {
    $dbasename = $opt_d;
}

# ToDo: dynamic switching of dbase?

my $thisHost = `uname -n | cut -d'.' -f1`;
chomp $thisHost;

# Direct all output to log file, rename existing log if present

testRowSignatures($configFile);

exit;

# ####################################################
# Subroutines
# ####################################################

# ------------------------------
# Read the file that defines the PCS row signatures to test
# Returns an array
# Example: @testarray = &readTargets ("filename")
#
sub testRowSignatures {

    my ($sigfile) = @_;
    # read in list of possible rt dbases
    open FILE, $sigfile or die $!;
    my @list;
    my @filestream = <FILE>;
    for my $sig (@filestream) {
        chomp $sig;
        $sig =~ s/\s+$//;            # trim trailing whitespace
        $sig =~ s/^\s+//;            # trim leading whitespace
        if ( $sig =~ m/^\#/ || length($sig) == 0 ) { next; }  # skip "#" comments
        #print "cfg sig = $sig\n";

        my ($grp, $tbl, $sta, $secs, $num) = split (/\s+/, $sig);
        #print "splits to: $grp $tbl $sta $secs $num \n";
        $num = 0 if ! defined $num;

        my $knt = pcsRowCount($grp, $tbl, $sta, $secs);
        #print "pcs row knt: $knt\n";

        # final test
        if ($knt > $num) { # freek out
            my $err = "PCS alert: $grp $tbl $sta = $knt";
            print "$err\n";
            sendMail ($grp, $tbl, $sta, $err);
        } else {
            if ($opt_v) { print "PCS ok: $grp, $tbl, $sta, $secs, $num = $knt\n"; }
        }
    }

}  #end of sub


# ------------------------------
# Returns count of rows that meet the critera
# Example: $count = pcsRowCount ($grp, $tbl, $sta, $secs)
#
sub pcsRowCount {

  my ($grp, $tbl, $sta, $secs) = @_;

  my $knt = 0;

  my $fracDay = $secs/86400.0;

  my $db = $dbasename;
  my $dbconn = DbConn->new($db)->getConn();

  # get a list of candidate phases
  my $statement = q{Select count(*) from pcs_state where CONTROLGROUP like ? and SOURCETABLE like ? and STATE like ?
                    and ( CAST(SYS_EXTRACT_UTC(SYSTIMESTAMP) AS DATE) - LDDATE ) > ?};
  #print "sql: $statement\n";       
  my $sql = $dbconn->prepare($statement) or die "Bad SQL statment : $DBI::errstr\n";

  #print "execute(grp, tbl, sta, fracDay) -> execute($grp, $tbl, $sta, $fracDay)\n";
  $sql->execute($grp, $tbl, $sta, $fracDay);

  while (my @data = $sql->fetchrow_array()) {
      $knt = $data[0] || 0;
  }
  return $knt;
}

# ------------------------------
# Send mail about a problem
#
sub sendMail {
  my ($grp, $tbl, $sta, $subject) = @_;
  # send email with the maxknt state postings piped to the body from getstates
  my $mailaddr = $ENV{"TPP_ADMIN_MAIL"};
  #print "getstates -n $maxknt $grp $tbl $sta | mailx -s $thisHost/$dbasename $subject $mailaddr\n";
  if ( $mailaddr ) { print `getstates -n $maxknt $grp $tbl $sta | mailx -s "$thisHost/$dbasename $subject"  "$mailaddr"`; }
}

sub usage {
    print <<EOF;
Syntax: $0 [-h] [-v] [-n] [-d dbname] [config_file]

 -h        : this help usage
 -d dbname : alias name for database connection, default=$masterdb
 -n maxknt : max number of postings to send in email body, default=$maxknt
 -v        : log (no mail) the state signatures that were within configured limits

  Log all PCS_STATE table signatures whose row counts exceeds configured limits,
  and send an email with those counts to the TPP_ADMIN_MAIL group.

  If a configuration filename with the state signature data is not specified,
  the configuration filename defaults to: $ENV{TPP_HOME}/pcs/cfg/pcsWatchdog.cfg

  The configuration file line format is (# ok to start a comment line):

     group source state age(secs) max_row_count

  where valid SQL wildcards (% _) are allowed for the first 3 state strings,
  and these state strings are case-sensitive.

  Mail is sent if the count of rows matching a state description with an "age"
  exceeding the configured seconds and exceeds the configured maximum count.

  If you want to count all evids in a state just use an Age=0.
  If you want to alarm on any posting exceeding age set count=0

  EXAMPLES:

    X Y Z 60 0 "There are XYZs event not processed after 60secs"
    X Y Z 60 5 "Six or more XYZ events not processed after 60secs"

  To report all postings with >1000 rows older than 10min the signature would be:
    % % % 600 1000

EOF
    exit;
}
