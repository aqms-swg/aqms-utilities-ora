# Process Control System  
  
Sub-directory *bin* has the executable scripts while *cfg* has the configuration files.  
  
### *bin*  
  
*  `pcsCont.pl` : Run by a cron job script to continuously process any event ids posted to a PCS configured state.  
*  `pcsFinalizeCont.sh` : Uses `pcsCont.pl` to send a CMS SEND message for events in the FINALIZE state.   
*  `pcsCancelCont.sh` : Uses `pcsCont.pl` to send a CMS CANCEL message for events in the DELETED state.  
*  `pcsDistribCont.sh` : Uses `pcsCont.pl` to send a CMS SEND message for events in the DISTRIBUTE state. 
*  `setupTransitions.pl` : Create the standard PCS transitions.    
*  `ppAlarm.pl` : Used by `pcsCont.pl` to send the appropriate CMS message.  
*  `pcsWatchdog.pl` : Check the state of health of the PCS system. If the number of events pending in the given state for the given time interval exceeds the value of "Count" raise a warning.  
*  `checkPCSstates.sh` : Run as a cron job, checks the number of PCS state rows waiting to be processed.  
*  `startPcsTasks.sh` : Start/restart the processes that process PCS states.  
*  `stopPCS.sh` : Kills pcsCont jobs on host.  
  

<br>  
  
### *cfg*  
  
*  `pcsFinalizeCont.props` : The config file for `pcsFinalizeCont.sh`.  
*  `pcsDistribCont.props` : The config file for `pcsDistribCont.sh`.   
*  `pcsCancelCont.props` :  The config file for `pcsCancelCont.sh`.   
*  `pcsJobList.cfg` :  List of PCS jobs that need to be started/restarted.  
*  `pcsWatchdog.cfg` : The config file for `pcsWatchdog.pl`.  


  