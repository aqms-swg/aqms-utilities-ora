# Post-processing scripts for DRP  
  
These are scripts associated with creating and distributing Duty Review web page gifs. Depends on [pcs](???).  

Sub-directory *bin* has the executable scripts while *cfg* has the configuration files. All configuration files are templates and should be modified as needed.  
  

### *bin*  
  
* ``makeDRPcont.pl`` : Continuously running script to create & publish SnapShot .gifs for Duty Review web pages. Acts on events posted to the given state. For each new event this script forks so that multiple events may be processed symultaneously and a "hung" event will not stop the whole processing thread.  Script uses configuration files found in $ENV{TPP_HOME}/drp/cfg; event.props/synopsis.props java properties for the gif generation, targetURL.cfg list the host destinations for the event gif files.  
* ``makeDRPScont.pl`` : Simiar to ``makeDRPcont.pl`` but for creating scaled gifs. Script uses configuration files found in $ENV{TPP_HOME}/drp/cfg; event-scaled.props/synopsis-scaled.props java properties for the gif generation, targetURL.cfg list the host destinations for the event gif files.  
* ``makeTRGcont.pl`` : Similar to above two but for creating gifs for subnet triggers. Script uses configuration files found in $ENV{TPP_HOME}/drp/cfg ; trigger.props java properties for the gif generation, targetURL.cfg list the host destinations for the event gif files.  
* ``startDRP.sh`` : Start/restart the processes that make Duty Review web page gifs. This is run at intervals by a cron job to insure that the jobs start at boot time and are always present. Starts processes for event, scaled event, subnet trigger gif creation.  
* ``stopDRP.sh`` : Kill/stop all DRP processes.  
* ``checkDRPlog.sh`` : check the ``makeDRPcont.log`` for any connection errors.  
* ``checkDRPlog.sh`` : Generic directory cleaner meant for use in DRP snapshot cleanup. Deletes *.gif files or *.log.* files below drp root directory.  
* ``repost.sh`` : Post an event to the *MakeGif* pcs state. It is recommended to created copies of this script to repost to other states in your system related to creating gifs for duty review.  
* ``scpDRPgif.sh`` : Generic scp sender meant for use for DRP snapshots.  



  

### *cfg*  

* ``event.props`` :  Contains list of destinations for archival of GIF files, including the cloud as well as hosts on the local netowrk.  
* ``makeDRPcont.props`` : Used by ``makeDRPcont.pl`` to create gifs. It is recommended to create a copy of this configuration file if the user wants to create a gif for a different pcs state.    
* ``makeDRPScont.props`` : Used to create the scaled gifs or scaled view via the pcs states of *ScaledGif*. It is recommended to create a copy of this configuration file if the user wants to use a different *ScaledGif* state. Update the values for parameters ``pcsAppProps`` and ``pcsState`` accordingly.   
* ``synopsis.props`` : Used to create the synonsis gif or synopsis view for an event.  
* ``makeTRGcont.props`` : Used the create gifs for subnet triggers. It is recommended to create a copy of this configuration file if the user wants to create a gif for a different pcs state or database configuration.  
* ``trigger.props`` : 
*  ``snapfig.props`` : NOT used in production. TEST file for time sorting of subnet trigger without the trigger times phase markers plotted.    
*  ``targetURL.cfg`` : Lists locations, both cloud and local network, where the gifs should be copied for distribution to the outside world.  
*  ``archiveURL.cfg`` : Lists locations, both cloud and local network, where the gifs should be copied for backup or archival.





 
  
