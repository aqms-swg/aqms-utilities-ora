#!/bin/bash
# Generic scp sender meant for use for DRP snapshots
#
# arg1 - file source location
# arg2 - file destination location

# First check runtime environment
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

root=${TPP_HOME}/drp
remoteGIFDir="backup@onyx.gps.caltech.edu:/backups/snapshots/rift"
archiveDir="/app/aqms/www/review_eventfiles/gifs/toarchive/*.gif"


timetag=`date -u +"%Y%m%d%H%M"`
#one log per calendar day UTC
logfile="${root}/logs/scpFile_${timetag:0:8}.log"

{
  echo "/usr/bin/scp $archiveDir $remoteGIFDir"
  /usr/bin/scp $archiveDir $remoteGIFDir
  if [ $? -eq 0 ];
  then
      echo "rm $archiveDir"
      rm $archiveDir
  fi
  echo "Finished scp $archiveDir $remoteGIFDir \@ $timetag"
} >>$logfile 2>&1
