#!/usr/bin/perl
# Continuously running script to create & publish SnapShot .gifs for 
# Duty Review web pages. Acts on events posted to the given state.
#
# For each new event this script forks so that multiple
# events may be processed symultaneously and a "hung" event
# will not stop the whole processing thread.
#
# Script uses configuration files found in $ENV{TPP_HOME}/drp/cfg :
# trigger.props java properties for the gif generation
# targetURL.cfg list the host destinations for the event gif files
#
# Documentation:
# http://vault.gps.caltech.edu/trac/cisn/wiki/DutyReview
#

use strict;
use warnings;

use POSIX qw(:signal_h :errno_h :sys_wait_h);   # Required by REAPER
use File::Basename;

use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use MasterDbs;
use Utils;

# parse optional switches
use Getopt::Std;

our ( $opt_c, $opt_d, $opt_h, $opt_v );
getopts('c:d:hv');

# Trigger gifs are not monitored by BB (non-essential)
my $BBError = "";
my $sendBB = 0;    # set =1 to send BigBrother messages, set to 0 not to send

# Make STDOUT "hot" e.g. autoflush
# By default perl running in background flushes its buffers only once/min
# so monitoring the log file makes it appear the script isn't working!
select STDERR; $| = 1;
select STDOUT; $| = 1;

# Environmental variables supplied by a bash wrapper sourcing /usr/local/etc/aqms.env
# First check runtime environment
defined $ENV{TPP_HOME} or die "$0 Error: Required aqms.env environment vars are not defined\n$!";

$ENV{DRP_HOME} = "$ENV{TPP_HOME}/drp";
die "Missing application directory\n$!" if ( ! -d $ENV{DRP_HOME} ) ;
chdir "$ENV{DRP_HOME}/bin";

usage() if ( $opt_h );

# Host machine running this script
my $host = `hostname`;
chomp $host;
$host =~ s/\..*$//; # grab first element

#my $logFile  = "$ENV{DRP_HOME}/logs/makeTRG.log";
my $logFile  = '../logs/' . basename($0,'.pl') . '.log';

# Direct all output to log file, rename existing log if present
print "All output will be directed to $logFile\n";
rotateLogs($logFile);

my $date = `date -u +'%F %T %Z'`;
chomp $date;
print "Starting $0 on $host at $date\n";

# ==========================================================================
# Behavior variables 
# ==========================================================================
# Hash array to hold configuration properties, like object attributes
my ( %myprops );
# Generic PCS processing property settable vars:
my ( $pcsCommand );
my ( $pcsAppName, $pcsAppProps, $pcsDbase, $pcsGroup, $pcsTable, $pcsState, $pcsResult, $pcsStallTime );
my ( $sleepTime, $archivalTime, $minMag, $maxDaysBack );
my ( $propFile, $debug, $verbose, $reconfig, $noop );

# ==========================================================================
# DRP specific variables overrides of defaults should be read from config file
# ==========================================================================
my ($gifDir, $maxChildren, $maxWindowSecs, $maxChannels, $targetCfgFile);

# Define default runtime properties 
setDefaultProperties(\%myprops);

# Check for properties file to override the default config
$propFile = "$ARGV[0]" if (@ARGV > 0);

die "$0 Error: missing property file: $propFile \n$!" if ! -e $propFile; 
print "Loading properties from file: $propFile\n";

loadProperties(\%myprops, $propFile);

# Report starting configuration
printConfig();

# Are all props valid ?
checkProperties();

# set verbose mode if option set, and not in props
$verbose = 1 if $opt_v;

# Declare dynamic variables used inside infinite while loop
# that may need to be accessed outside block scope

my $amEnabledForPCS = 'true'; # reset in loop by checking with db
my ($evid, $age, $mag, $wait, $cutoff, $status);

# ==========================================================================

my @gifTargets;
my $dayNumber = `date -u '+%d'`; #  note LF is not stripped
my $knt = 0;
my $childKnt = 0;
my %isChild;

# install a SIGCHLD handler to kill zombie children created by forking
$SIG{CHLD} = \&REAPER;   # see 'sub REAPER()'

my $mpid = $$; # parent main loop process id
print "Main loop pid = $mpid\n";
# ---- MAIN CONTINUOUS LOOP ----
my $lastReportTime = 0;
while (1) {

    # code block here is executed on every check interval
    # test for change of day, do some chores, send BB
    # since 'redo' redoes the iteration we put this code at top of loop
    {
      if ($dayNumber != `date -u '+%d'`) {
        print("Executing change-of-day chores \@ ". `date -u +'%F %T %Z'`);
        rotateLogs($logFile);
        printConfig();
        $dayNumber = `date -u '+%d'`;
      }
      else {
        my $now = time();
        utime $now, $now, $logFile;  # keep file timestamp current (proof of redo looping)
      }

      # send Big Brother message, disable since nagios should be monitoring 2014/08/19 -aww
      if ($sendBB) {
        my $myTime = time();
        if ( ($myTime - $lastReportTime) > 600 ) {
          BBreport(); # tell BB we're alive at least every 30 min. or it will page
          $lastReportTime = $myTime;
        }
      }
    }

    # See if an event id is posted to PCS_STATE table in db
    $evid = `$ENV{TPP_BIN_HOME}/next.pl -d $pcsDbase -s $pcsStallTime $pcsGroup $pcsTable $pcsState`;
    chomp $evid;

    if ($evid eq '' || $evid eq '0') { # No event: sleep - zzzz
        sleep $sleepTime;
        if ($reconfig) { # reload props file
            if ( ! loadProperties(\%myprops, $propFile) ) {
              checkProperties();
              printConfig() if ( $reconfig > 1 );
            }
        }
        redo;
    }
    elsif ($childKnt >= $maxChildren) {    # population control
        my $msg = "* $pcsAppName on $host reached maxChildren = $maxChildren; sleeping and retrying $evid";
        print("$msg\n");
        #sendMail($msg);
        sleep $sleepTime;   
        redo;
    }

    # Have event id
    $date = `date -u +'%F %T %Z'`;
    chomp $date;
    print "\nNEXT: $evid \@ $date using primary db:" . `$ENV{TPP_BIN_HOME}/amPrimaryDb.pl`;

    # check db host app processing status
    $amEnabledForPCS = `$ENV{TPP_BIN_HOME}/enabledForPCS.pl $pcsAppName $pcsState`;  
    chomp $amEnabledForPCS; # may have newline at end

    if ( "$amEnabledForPCS" ne 'true' ) {
        print "SKIP: Not primary app host on $host for $pcsAppName $pcsState, a no-op for $evid\n";
        next; # result it
    } 

    # check age
    $age = getEventAge($evid); # age of event in seconds
    if ( ! $age or "$age" eq '' or $age <= 0 ) {  # Bogus FUTURE event (yes, this happened)
        print "SKIP: No valid event found in db, age < 0 for $evid\n";
        next; # result it
    }

    # check cutoff age only if input property for maxDaysBack was specified
    $cutoff = (86400 * $maxDaysBack);
    if ( $cutoff > 0 ) {
        if ( $age > $cutoff ) {
            printf "SKIP: Too old, %8.2f > $maxDaysBack days for %d\n", ($age/86400), $evid;
            next; # result it
        }
    }

    # allow at least archivalTime secs post-origin for data to be generated (e.g. amps, mags, waveforms ...)
    if ( $age < $archivalTime ) {
        $wait = ($archivalTime - $age);  # wait for data to be archived to db
        print "WAIT: $wait secs to age $evid, for its data to be archived ...\n";
        sleep $wait;
    }

    # check for qualifying magnitude
    $mag = getEventMag($evid);
    if ( $mag < $minMag ) {
        print "SKIP: Too small, $mag < $minMag (minMag) for $evid\n";
        next; # result it
    }

    print "PROC: $evid Mag = $mag \@ " . `date -u +'%F %T %Z'`;
    $status = processEvent($evid);
    print "STAT:" . ($status >> 8) . " for $evid  \@ " . `date -u +'%F %T %Z'` if $status && $verbose;

} continue { # after iteration, done with event
    # !!! NOTICE: Result the event even though the forked process is not done
    #             else next time through the loop we'll get the SAME evid
    #
    # Implement a RESULT sub here to to override actions in subclasses
    #
    # Done - result it, get value to use
    my $result = $pcsResult;
    #$result = ($status >> 8) if $status; #  reset to returned result from processing event, like error code ?
    # ------------------------------------------------------------------------
    # Result all posted ranks for id using -r 0 switch:
    # Transitions defined in PCS for that result should remove this posting from PCS 
    # ------------------------------------------------------------------------
    $status = `$ENV{TPP_BIN_HOME}/result.pl -d $pcsDbase -r 0 $evid $pcsGroup $pcsTable $pcsState $result`;
    print($status);
    print "DONE: $evid \@ " .  `date -u +'%F %T %Z'`;
    
}  # end of infinite while loop

# ####################################################
# Subroutines
# ####################################################

# This override returns -9 every time 
sub getEventMag {
    my ($evid) = @_;
    # NOTE: SUBNET TRIGGERS HAVE NO MAGNITUDE
    #my $emag = `$ENV{TPP_BIN_HOME}/eventMag.pl -d $pcsDbase $evid`;
    #chomp $emag;
    #if ( $emag !~ m/\d/ ) {
    #   print "NOTE: No magnitude found for $evid\n";
    #}
    #return $emag; 

    return -9;
}


# ####################################################
# Tell Big Brother this process is alive
# If no report in 30 minutes, process goes purple and pages someone
#
sub BBreport {

  my $color = "green";

  if ($BBError) {
    $color = "orange";
  }
  my $user = $ENV{USER} || $ENV{LOGNAME} || "PostProc";
  my $str = "This process makes waveform gifs and publishes them to web pages<br>" .
            "Process name = $0  (pid = $$)<br />" .
            "Running on = $host<br />" .
            "As user = $user<br />" .
            "Dbase = $pcsDbase<br />" .
            "$BBError <br />";

  # send report
  # To DEBUG str enable print here:
  #print("$ENV{DRP_HOME}/bin/BBreport $pcsDbase \"$str\"\n");

  print(`$ENV{DRP_HOME}/bin/BBreport.sh $pcsDbase \"$str\"`);
  $BBError = "";   # reset the Error code in case the error cleared up.
}

# #####################################################
# Kill completed child processes - weirdly if you don't all forks turn into zombies!!
#
# http://www.unix.org.ua/orelly/perl/cookbook/ch16_20.htm

sub REAPER {

    my $ppid = $$; # process calling this sub

    my $flag = 0;
    for my $key ( keys %isChild ) {
        $flag = 1 if ($ppid == $key);
    }
    if ( $flag  ) {
        #print "DEBUG REAPER: caller pid [$ppid] found child of $mpid, return no-op\n";
        return;
    }
    if ( $ppid != $mpid ) {
        #print "DEBUG REAPER: caller pid $ppid != $mpid , return no-op\n";
        return;
    }

    # Assume main parent here
    my $cpid = waitpid(-1, &WNOHANG);

    if ($cpid == -1) {
        # no child waiting.  Ignore it.
        #print "REAPER: No child pid waiting, ignore signal.\n";
    } elsif (WIFEXITED($?)) {
        #print "REAPER: [$cpid] Child of [$mpid] exiting.\n";
        if ( $isChild{$cpid} ) {
            $childKnt-- if ( $childKnt > 0 ); # only if child is result of explicit fork
            $isChild{$cpid} = 0;
            print "REAPER: reaped [$cpid], there are now $childKnt child processes for $mpid\n";
        }
    } else {
        #print "REAPER: False alarm on $cpid for $mpid.\n";
    }
    $SIG{CHLD} = \&REAPER;          # in case of unreliable signals
}

# ####################
# Make gif
#
#  makeGif ($_evid)
#
#Usage: snapgif <evid> [ntraces] [max_secs] [property-file] [out-file]
#           defaults:  [15]      [120]      [properties]    [<evid>.gif]
sub makeGif {
    ($evid) = @_;   # get arg

    my $gifFile = "${gifDir}/${evid}.gif";

    #my $cmd = "$ENV{TPP_HOME}/makegif/bin/triggergif.sh ${evid} ${maxChannels} ${maxWindowSecs} $pcsAppProps $gifFile 2>&1";
    
    my $cmd;
    # must force substitution of variables from $pcsCommand and need quotes around expanded string
    my $command = '$cmd = ' . "\"$pcsCommand\"";
    eval $command;

   if ( $noop ) {
     print "EXEC: $cmd\n" if ( $debug ne '0' );
     print "NOOP: $evid\n" if $verbose;
     return 0; # assumes 0 is the same as system success
   }
   print "EXEC: $cmd\n" if $verbose;

   # Note /bin/sh shell forked by system does not have the ENV vars
   my $result = `$cmd`;
   print($result);

   publish ($gifFile, @gifTargets);

   #Notify admin about missing waveforms 
   my $size = ( -s "$gifFile");
   sendMail("$pcsAppName on $host: $evid gif has 0 waveforms? check waveservers!") if (! defined $size || $size < 3000);

   return 0;

}

# ###################################################
# Publish (scp) file(s) to web servers
#
# publish ($file, @targetURLs);
# 
sub publish {
    my ($file, @targetURLs) = @_;   # get arg

    # no-copy when empty input array
    return if ( @targetURLs == 0 );

    print("------- Copy $file to web servers -------\n");

    my @pfile = fileparse($file); # get path of gif

    foreach my $target (@targetURLs) {

        $target =~ s,/$,,; # trim any trailing slash

        print("/usr/bin/scp $file $target \@ " . `date -u +'%F %T %Z'`);

        if ( "$target" eq substr("$pfile[1]", 0, -1) ) {
            print "---> GIF FILE ALREADY AT TARGET <---\n";
            next;
        }

        # Perhaps need a local $SIG{CHLD} = 'DEFAULT' ? -aww 2012/04/28
        #local $SIG{CHLD} = 'DEFAULT';
        my $result = `/usr/bin/scp $file $target 2>&1`;
        print($result);
        print("Finished scp $file $target \@ " . `date -u +'%F %T %Z'`);
    }

    #NOT DONE FOR TRIGGER:  send QDDS link to waveforms
    #NOT DONE FOR TRIGGER: print(`$ENV{DRP_HOME}/bin/sendLink.sh $evid`);
}

# ###################################################
# Run (ssh) cleanup script on target web servers
#
# cleanup (@targetURLs);
# 
sub cleanup {
    my (@targetURLs) = @_;   # get arg

    print("* Cleaning up local and remote directories \@" . `date -u +'%F %T %Z'`);

    # remote dirs
    foreach my $target (@targetURLs) {
        my ($url, $dir) = split /:/, $target;  # break out host and dir
        my $cmd = "/bin/ssh $url ${dir}/cleanup.sh 2>&1";
        print("$cmd\n");
        my $result = `$cmd`;

        if ($?) {
        }
        print("$result");
    }
}

# ###################################################
#
# open log file.
#
sub openLogs {
    my ($logFile) = @_;   # get arg

    open STDOUT, "> $logFile" or die "Can't redirect STDOUT to $logFile: $!\n";
    open(STDERR, ">&STDOUT") || die "Can't dup err to stdout";

    select(STDERR); $| = 1; # make unbuffered
    select(STDOUT); $| = 1; # make unbuffered

    print("Log file opened: " . `date -u +'%F %T %Z'`);

  }

# ###################################################
# Rotate log files
#
# rotateLogs ();
#
# You can call this at startup to rename an old existing log
# and open a new one.
sub rotateLogs {
    my ($logFile) = @_;   # get arg

    if (-e $logFile) {
        my $timestamp = `date -u '+%Y%m%d%H%M'`;
        chomp $timestamp;
        #my $cmd = "mv $logFile ${logFile}.$timestamp";
        my ($fname,$path,$suffix) = fileparse("$logFile",qr"\..[^.]*$");
        my $cmd = "mv $logFile ${path}${fname}_${timestamp}${suffix}";
        `$cmd`;
    }

    # open new
    openLogs($logFile);
}

# ------------------------------
# Read the file that defines the target URLs
# Returns an array of URLs
# Example: @urlarray = readTargets ("filename")
#
sub readTargets {

    my ($targetfile) = @_;

    my @targetlist = ();

    # A blank input filename results empty array of target destinations => no-copy of gifs, 
    if ( ! $targetfile ) { return @targetlist; }

    # read in list of possible rt dbases
    open FILE, $targetfile or die $!;
    my @filestream = <FILE>;

    for my $target (@filestream) {

        chomp $target;
        $target =~ s/\s+$//;            # trim trailing whitespace
        $target =~ s/^\s+//;            # trim leading whitespace
        if ($target =~ m/^\#/) {next;}  # skip "#" comments

        print("> $target\n");
        push @targetlist, $target;
    }

    return @targetlist;
}

# ------------------------------
# Send mail about a problem
# Sends last 25 lines of the log
#
sub sendMail {
    my ($subject) = @_;

    # send email
    my $mailaddr = $ENV{"TPP_ADMIN_MAIL"};
    if ( $mailaddr ) { print `tail -25 $logFile | mailx -s "$subject"  "$mailaddr"`; }
}

# Store up message text for sending to BigBrother
sub storeBBMessage {
    $BBError .= shift();
}

####################################################################################
sub processEvent {
   my ($evid) = @_;
   # What the fork?
   ++$knt;
   my $child_pid = fork(); 
   if ( ! defined($child_pid) ) {
       die "cannot fork: $!";
   }
   elsif ($child_pid == 0) { # this code block executes & terminates in the child
       # I'm the child
       my $t0 = time;  
       my $cpid = $$;      # perl way to get pid running this code
    
       print("[$cpid] Child of [$mpid] now processing event $evid (# $knt total since restart)\n");
       @gifTargets = readTargets($targetCfgFile);  # read these each time, in case they change
       makeGif($evid);   # make gif (and synopsis if big enough)

       my $secsOld2 = getEventAge($evid);
       # Final elapse time
       my $t = time - $t0;
       print("<<<<$cpid] Finished ${evid}: $secsOld2 secs after origin. Script took $t secs.\n");

       exit (0);

    }
    else {
        # Parent process continues here...
        $childKnt++;             #parent keeps track of children
        $isChild{$child_pid} = 1;
        print("PARENT: There are $childKnt child processes.\n");
    }
    return 0;
}

# Return the age of the given event in integer seconds
# Returns "0" if no such event.
sub getEventAge {
   my ($evid) = @_;
   my $secsOld = `$ENV{TPP_BIN_HOME}/eventAge.pl -d $pcsDbase $evid`;  #age of event in secs
   chomp $secsOld;
   return $secsOld;
}

# Read properties into hash from properties file, merge with defaults
sub loadProperties {
    my ($defPropsRef, $propFile) = @_;
    my $fileHashRef = {};

    die "Invalid input, properties file D.N.E.\n $!" if ! -e "$propFile";

    $date = `date -u +'%F %T %Z'`;
    chomp $date;
    print "Loading properties from file: $propFile \@ $date\n"  if ( $reconfig > 1 );

    eval {
      # Optional regex for split delimiter for key value is a string like "[=: ]+" 
      $fileHashRef = Utils::->readPropsFromFile($fileHashRef, $propFile, " *[=] *");
    };
    die "Error loading properties from $propFile\n $!" if ( $@ );

    # Check if properties file changed
    my $retval = equalsHash($fileHashRef, $defPropsRef);
    #print "retval=$retval\n";

    if ( scalar( keys %$fileHashRef ) != 0 ) {
        setProperties( $fileHashRef );
        # Use slice assignment to merge prop file hash into the default props by refernce:
        @{$defPropsRef}{ keys %$fileHashRef } = values %$fileHashRef;
    }

    $debug = ( $defPropsRef->{'debug'} eq 'true' || $defPropsRef->{'debug'} eq '1' ) if exists $defPropsRef->{'debug'};
    if ( $debug ) {
        $verbose = 1;
        print "$0 Properties:\n";
        Utils::->printProps($defPropsRef, " = ");
    }

    return $retval;  # 0 or 1
}

# Lookup property in input hash, return default if no key
sub getProperty {
    my ($propsRef, $propName, $defaultVal) = @_;
    my $val = $propsRef->{$propName};
    $val = $defaultVal if ! defined $val;
    return $val;
}

####################################################################################

# Populate input hash with current config
sub getProperties {

    my ($propsRef) = @_;

    $propsRef->{pcsAppName} = $pcsAppName;
    $propsRef->{pcsAppProps} = $pcsAppProps;
    $propsRef->{pcsDbase} = $pcsDbase;
    $propsRef->{pcsGroup} = $pcsGroup;
    $propsRef->{pcsTable} = $pcsTable;
    $propsRef->{pcsState} = $pcsState;
    $propsRef->{pcsResult} = $pcsResult;
    $propsRef->{pcsStallTime} = $pcsStallTime;
    $propsRef->{pcsCommand} = $pcsCommand;
    $propsRef->{noop} = $noop;
    $propsRef->{reconfig} = $reconfig;
    $propsRef->{sleepTime} = $sleepTime;
    $propsRef->{archivalTime} = $archivalTime;
    $propsRef->{maxDaysBack} = $maxDaysBack;
    $propsRef->{minMag} = $minMag;
    $propsRef->{sendBB} = $sendBB;

    # DRP properties for both EVENT and TRIG
    $propsRef->{gifDir} = $gifDir;
    $propsRef->{maxChildren} = $maxChildren;
    $propsRef->{maxWindowSecs} = $maxWindowSecs;
    $propsRef->{numChannels} = $maxChannels;
    $propsRef->{targetCfgFile} = $targetCfgFile;

    return $propsRef;
}

# set internal config to property hash values
sub setProperties {

    my ($propsRef) = @_;

    my $str = "";

    # PCS config props
    $str = getProperty($propsRef, 'pcsAppName');
    if ( $str ) { $pcsAppName = $str; }

    $str = getProperty($propsRef, 'pcsAppProps');
    if ( $str ) { $pcsAppProps = $str; }

    $str = getProperty($propsRef, 'pcsDbase'); 
    if ( $str ) { 
      $pcsDbase = $str;
      $pcsTable = $str;
    }

    $str = getProperty($propsRef, 'pcsGroup'); 
    if ( $str ) { $pcsGroup = $str; }

    $str = getProperty($propsRef, 'pcsTable');
    if ( $str ) { $pcsTable = $str; }

    $str = getProperty($propsRef, 'pcsState'); 
    if ( $str ) { $pcsState = $str; }

    $str = getProperty($propsRef, 'pcsResult'); 
    if ( $str ) { $pcsResult = $str; }

    $str = getProperty($propsRef, 'pcsStallTime'); 
    if ( defined $str ) { $pcsStallTime = $str; }

    # Not here since app args are specific to implementation like where to insert props and $evid in cmd string
    $str = getProperty($propsRef, 'pcsCommand'); 
    if ( $str ) { $pcsCommand = $str; }

    # Other props
    $str = getProperty($propsRef, 'verbose');
    if ( defined $str ) { $verbose = $str; }
    else { $verbose = 0; }

    $str = getProperty($propsRef, 'noop');
    if ( defined $str ) { $noop = $str; }

    $str = getProperty($propsRef, 'reconfig');
    if ( defined $str ) { $reconfig = $str; }

    $str = getProperty($propsRef, 'sleepTime');
    if ( defined $str ) { $sleepTime = $str; }

    $str = getProperty($propsRef, 'archivalTime');
    if ( defined $str ) { $archivalTime = $str; }

    $str = getProperty($propsRef, 'maxDaysBack');
    if ( defined $str ) { $maxDaysBack = $str; }

    $str = getProperty($propsRef, 'minMag');
    if ( defined $str ) { $minMag = $str; }
        
    # DRP specific properties for EVENT and TRIG
    $str = getProperty($propsRef, 'gifDir');
    if ( $str ) { $gifDir = $str; }

    $str = getProperty($propsRef, 'maxChildren');
    if ( defined $str ) { $maxChildren = $str; }

    $str = getProperty($propsRef, 'maxWindowSecs');
    if ( defined $str ) { $maxWindowSecs = $str; }

    $str = getProperty($propsRef, 'maxChannels');
    if ( defined $str ) { $maxChannels = $str; }

    $str = getProperty($propsRef, 'targetCfgFile');
    # Note "" is special case, so test if defined here:
    if ( defined $str ) { $targetCfgFile = $str; }

    $str = getProperty($propsRef, 'sendBB');
    if ( defined $str ) { $sendBB = $str; }

}

# print current internal config
sub printConfig {
    #
    #dumpArgs();
    #
    print("!!!!!!!! NOTICE !!!!!!!!\n");
    print("Output from multiple forked processes may be intermingled in this log!\n");
    print("\n");

    print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
    print "Property file=$propFile\n";
    print "App $pcsAppName on $host connects to $pcsDbase and processes PCS state posts of:\n";
    print "  $pcsGroup $pcsTable $pcsState for result $pcsResult where\n";
    print "  pcsStallTime=$pcsStallTime seconds\n";
    print "  sleepTime=$sleepTime secs hibernation when no evid is posted\n";
    print "  reconfig=$reconfig (=1 reloads properties after sleep)\n";
    print "  verbose=$verbose (=1 prints extra processing output and app properties)\n";
    print "  archivalTime=$archivalTime min secs of event aging before processing\n";
    print "  maxDaysBack=$maxDaysBack max days age eligible for processing (any age if <= 0 )\n";
    print "  minMag=$minMag is min magnitude eligible for processing, to disable set -9\n";
    print "  sendBB=$sendBB\n";
    print "\nProcessing $pcsCommand\n";

    #DRP EVENT/TRIG specific
    print(" Max forked children = $maxChildren\n");
    print(" Gif directory       = $gifDir\n");
    print(" Max seconds/gif     = $maxWindowSecs\n");
    print(" Max channels/gif    = $maxChannels\n"); 
    print(" Target URL file     = $targetCfgFile\n");
    print("\n");

    if ( $pcsAppProps ) {
       print "Using pcsAppProps file $pcsAppProps\n";
       if ( $verbose ) {
         print "\nProperties:\n";
         open(my $fh, $pcsAppProps);
         while (my $line = <$fh>) { print $line; }
         close($fh);
       }
    }

    print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
}

sub setDefaultProperties {

    my ($propsRef) = @_;

    $pcsAppName = "makeTRGcont";
    $pcsAppProps = "$ENV{DRP_HOME}/cfg/trigger.props";  # Java properties

    $pcsCommand = '$ENV{TPP_HOME}/makegif/bin/triggergif.sh ${evid} ${maxChannels} ${maxWindowSecs} $pcsAppProps $gifFile 2>&1';
    $propFile = "$ENV{DRP_HOME}/cfg/makeTRGcont.props";

    #Default to master else parse command line arg option
    $pcsDbase = $masterdb;
    if ($opt_d) { $pcsDbase = $opt_d; }

    # NOTE: pcs_transition table sourceTable could be some string other than the $pcsDbase
    # default PCS state description to operate on:
    $pcsGroup = "EventStream";
    $pcsTable = "$pcsDbase";
    $pcsState = "MakeGifTrg";
    $pcsResult = 1;
    $pcsStallTime = 0;

    $noop = 0;              # =0 always process, =1 do not process posts, for testing
    $reconfig = 0;          # do not reload properties after sleep between next post poll
    $sleepTime  = 30;       # secs to sleep between checks for new events
    $archivalTime = 30;     # secs post-origin to wait for data to be archived
    $minMag = -9;           # minimum magnitude to process evid
    $sendBB = 0;            # do not send BB message

    #NOTE: maxDaysBack special value 0 implies do not check age for cutoff 
    $maxDaysBack = 7;    # Don't process events older than this
    if ($opt_c) { $maxDaysBack = $opt_c; }
    
    # DRP EVENT and TRIG properties
    $maxChildren = 2;       # max allowed simultaneous child processes
    $targetCfgFile = "$ENV{DRP_HOME}/cfg/targetURL.cfg";
    # output dir for gifs
    $gifDir = "$ENV{DRP_HOME}/output/triggers";

    # width of channel timewindow
    $maxWindowSecs = 120; 
    $maxChannels = 15;

    getProperties($propsRef);

}

sub checkProperties {
    # aux app properties file are required 
    die "$0 Error: missing property file: $pcsAppProps\n$!" if ($pcsAppProps && ! -e $pcsAppProps); 
}

# This belongs in the utils pm ?
sub equalsHash {
    my ($a, $b) = @_;
    my @ka = keys %$a;
    my @kb = keys %$b;
    return 0 if ( @ka != @kb );
    my %cmp = map { $_ => 1 } @ka;
    for my $key (@kb) {
      last unless exists $cmp{$key};
      last unless $a->{$key} eq $b->{$key};
      delete $cmp{$key};
    }
    return ( scalar( keys %cmp ) ? 0 : 1 );
}

sub usage {

  print <<'EOF';

Usage $0  [-d -c -v] [prop-file]
   -d dbase, db alias when not specified by property pcsDbase in properties files
   -c cutoff maxDays when not in props
   -v verbose output (e.g command strings and current properties)
   -h usage

DEFAULT prop-file when not specified on command line is:
  $ENV{TPP_HOME}/drp/cfg/makeTRGcont.props

Default values are used when not defined in property file:
 pcsDbase     $masterdb value
 pcsGroup     EventStream
 pcsTable     the pcsDbase value
 pcsState     MakeGifTrg 
 pcsResult    1
 pcsStallTime 0   seconds
 sleepTime    30  seconds
 archivalTime 30 seconds
 maxDaysBack  7   (set 0 for no cutoff age)
 minMag       -9  no min 
 noop         0   always process, =1 do not process posts, only for testing
 reconfig     0   do not reload properties after sleep between next post polls
 sendBB       0   do NOT notify Big Brother webbuilder status

 pcsAppName     makeTRGcont
 pcsCommand     $ENV{TPP_HOME}/makegif/bin/triggergif.sh $evid $maxChannels $maxWindowSecs $pcsAppProps $gifFile 2>&1
 pcsAppProps    $ENV{TPP_HOME}/drp/cfg/trigger.props
 gifDir         $ENV{TPP_HOME}/drp/output/triggers
 targetCfgFile  $ENV{TPP_HOME}/drp/cfg/targetURL.cfg
 maxChildren    2
 maxWindowSecs  120
 maxChannels    15

EOF
  exit;
}
