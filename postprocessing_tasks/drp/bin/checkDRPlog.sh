#!/bin/bash
if [ -z "$TPP_HOME" ]; then source /usr/local/etc/aqms.env; fi
cnt=`tail -10000 $TPP_HOME/drp/logs/makeDRPcont.log | egrep -c -e  'io error reconnecting to Socket|Read timed out for TCPConn.receive\(\) where timeout'`
if [ $cnt -ge 1000 ]; then
  host=`uname -n`
  str="Found count=$cnt socket errors at end of makeDRPcont.log on $host, check waveserver status (CWB)"
  echo $str 
  if [ -n "$TPP_ADMIN_MAIL" ]; then
    echo $str | mailx -s "ALERT: check waveservers for DRP client, socket error count: $cnt" $TPP_ADMIN_MAIL 
  fi
else
  echo "Found count=$cnt waveserver client socket errors at end of makeDRPcont.log"
fi
