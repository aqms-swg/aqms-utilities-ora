#!/bin/bash
if [ $# -lt 2 ]; then
  echo "Post an evid for MakeGif at rank 101"
  echo "Syntax:  repost <evid> <dbstream>"
  echo "Example: repost 1000001 mydb"
else
  post $1 EventStream $2 MakeGif 101
fi
