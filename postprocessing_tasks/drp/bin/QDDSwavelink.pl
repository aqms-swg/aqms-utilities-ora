#!/usr/bin/perl
#
# NOTE: Script code is CI network specific has local network dependencies
#
# Create the "standard" Waveform link (LI message) for the Simpson Map
# that's transported by QDDS
#
# OK, here's the deal. I'd like the gif file creator (processOneEvent) to do this but
# it writes the gif to agent86 & fang. They serve Akamai and don't
# know the Akamai server address. So, the actual external URL ia encapsulated
# here. If it ever changes this script must change.
#

use strict;
use warnings;

if ( @ARGV == 0) {
  print " Syntax: $0 <evid> \n"; 
  exit;
}

# Build up the URL string (where the waveform snapshot is)
# EXAMPLE:
# http://pasadena.wr.usgs.gov/review_eventfiles/9988381.html

my $evid = $ARGV[0];
my $url ="http://pasadena.wr.usgs.gov/eqview/cgi-bin/makePublicView.php?EVID=$evid";

my $addonType = "Waveform_CI";
my $linkText = "Waveforms";

# The QDDSlink script handles the formatting of the addon message.
# Will put message in QDDS dir of current master RT computer
# Note: use "print" so script output goes to stdout - cmd is executed
my $str = sprintf "LI%8.8dCI01 %s %s %s " , $evid, $addonType, $url, $linkText;

print $str, "\n";

exit;

#
#LI - Link to Addon
#    The LI format is used to issue a link to additional information (addons) about an event 
# that are available on some accessible web site. Addons can be updated by using higher version 
# numbers. The file name gives the data source, the eventid, the version number, the addon type,
# and is followed by .add or .del (if it is a delete addon message). 
# The format is:
#
#TpEidnumbrScVV message........ must be on one line.
#123456789012345678901234567890 # 1 2 3
#
#a2 * Tp =  Message type = "LI" text message
#a8 * Eid = Event identification number of event to delete
#a2 * Sc  = Data Source
#a2 * VV  = Version # of the addon comment.
#a    ... = message contains three strings separated by spaces:
#           1. a string that defines the type of addon, this string should be unique amoung
#              the addons being used.  See the recenteqs system for more information.
#           2. the url where the addon information is available
#           3. the text that will be used to describe what is available at the url.
#              This third string only may contain spaces.  If the text is "delete" the
#              addon is deleted.

#Example:
#LI 006729 NC01 fm http://whatever/whoknows This is a test
#LI 006729 NC01 fm http://whatever/whoknows delete
