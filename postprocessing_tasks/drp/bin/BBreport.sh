#!/bin/bash
# Report status to Big Brother system
# uses environmental variables from bbdef.sh

# Arg #1 is the host name of the reporting task
# Arg #2 is an optional string, in quotes
#
# Must do this because /bbdef.sh steps on the command line args

# Bail if test system
if [ -e "/app/cluster/I_AM_TEST" ]; then
  echo "$0 found /app/cluster/I_AM_TEST file exists, a no-op `date -u +'%F %T %Z'`"
  exit
elif [ "$AQMS_SYSTEM_TYPE" !=  'PROD' ]; then
  echo "$0 AQMS_SYSTEM_TYPE=${AQMS_SYSTEM_TYPE}, not PROD, a no-op `date -u +'%F %T %Z'`"
  exit
fi

MYHOST=${1}
CMDTEXT=${2}
HOST=`uname -n`

# NOTE BB code uses BBHOME env dir def
if [ -z "${BBHOME}" ]; then
    echo "ERROR: BBreport.sh BBHOME env variable for home directory is undefined." 
    exit
fi

. ${BBHOME}/etc/bbdef.sh

# stamp originating host name on message
MYTEXT="Report from $HOST<br>$CMDTEXT"

# NOTE: color status is not passed in as arg, the color is always green ?
LINE="status webbuilder.${MYHOST} green `date` <br>${MYTEXT}"

$BB $BBDISP "$LINE" 

echo $CMDTEXT > /app/aqms/tpp/drp/logs/webbuilder.${MYHOST}.status
