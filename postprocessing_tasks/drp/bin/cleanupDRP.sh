#!/bin/bash
# Generic directory cleaner meant for use in DRP snapshot cleanup 
# Deletes *.gif files or *.log.* files below drp root directory 
#
# arg1 - how long to keep files (optional)- default=7 days

# First check runtime environment
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

root=${TPP_HOME}/drp

daysBack=7

# check command line args 
if [ "$1" == "-h" ]; then
  echo "  Usage: $0 [max days age to keep (default=$daysBack)]"
  echo "  removes the 'gif' or 'log' files below ${root} older than maxDaysAge"
  exit
fi

if [ $# -gt 0 ]; then
  daysBack=$1
fi
if [[ ! $daysBack =~ [0-9]+ ]]; then
 echo "Input max days age to keep: '$daysBack', must be an integer value"
fi

cd $root

timetag=`date -u +"%Y%m%d%H%M"`
#one log per calendar day UTC
logfile="${root}/logs/cleanupDRP_${timetag:0:8}.log"

# NOTE: Must add -print option at end (not at start) of find  to see what files are being deleted
{
  echo '********************************************************************************'
  echo "$0 start at `date -u +'%F %T %Z'`"

  cnt=`find logs -name "*.log*" -mtime +${daysBack} | wc -l | tr -d ' '`
  echo "Deleting $cnt log files older than $daysBack days";
  find logs  -name "*.log*" -mtime +${daysBack} -print -exec \rm -f '{}' \;

  cnt=`find output -name "*.log*" -mtime +${daysBack} | wc -l | tr -d ' '`
  echo "Deleting $cnt gif files older than $daysBack days"
  find output -name "*.gif" -mtime +${daysBack} -print -exec \rm -f '{}' \;

  echo "$0 done  at `date -u +'%F %T %Z'`";
  echo '********************************************************************************'
} >>$logfile 2>&1
