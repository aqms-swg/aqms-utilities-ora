#!/bin/bash
# 
# Start/restart the processes that make Duty Review web page gifs
# This is run at intervals by a cron job to insure that the jobs start at 
# boot time and are always present.
#
# #####################################################################
# IMPORTANT: the DataBase name must be specified on the crontab line!
#            It should match that set in ${TPP_HOME}/drp/cfg/event.props
#example:
#0,10,24,30,40,50 * * * * ${TPP_HOME}/drp/bin/startDRP -d archdb -S -c 7 >/dev/null 2>&1 

# First check runtime environment
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

usage() {
  cat <<EOF

Usage:

  Start/restart the processes that make Duty Review web page gifs
  This is run at intervals by a cron job to insure that the jobs start at 
  boot time and are always present.

  Syntax: ${0} [-h] [-c maxDaysBack] [-l tag] [-d db] [-e or -t] <dbname>
       -h        : this usage
       -c days   : maximum age of local log/gif files to keep (default=forever)
       -d db     : the database alias

  NOTE: db alias, maxsecs, maxchans can be overridden by properties values.
  By default both the event and trigger drp make gif streams are started.

       -S also startup the event scaled gif stream

       -e startup ONLY the event make gif stream
       -t startup ONLY the trigger make gif stream
       -s startup ONLY the event scaled gif stream

       -l tag : By default, or -e, start a makeDRPcont process using properties: makeDRPcont<tag>.props.
                if -S start a process using properties: makeDRPScont<tag>.props.
                The logfile names includes tag. 
EOF
  exit $1
}

appHome=${TPP_HOME}/drp
logdir=${appHome}/logs
timetag=`date -u +"%Y%m%d%H%M"`
#one log per calendar day UTC
startLog="${logdir}/startDRP_${timetag:0:8}.log"

mailaddr=${TPP_ADMIN_MAIL}
host=`uname -n | cut -d'.' -f1`

cd ${appHome}

# by default start both, else check for command switch option do event or trigger only
# Note assignment value "1" is "true" when used in a logical (( )) condition
doTrigger=0
doEvent=1
doScaled=0
doTag=0
tag=""
dbase=""
maxDaysBack=''
while getopts ":c:d:ehl:Sst" opt; do
  case $opt in
    c)
      maxDaysBack="$OPTARG"
      ;;
    d)
      dbase="-d $OPTARG"
      ;;
    e)
      doTrigger=0  # disable trigger stream start
      doScaled=0  # disable scaled stream start
      ;;
    h)
      usage 0
      ;;
    l)
      tag="$OPTARG"
      doTag=1
      ;;
    S)
      doScaled=1  # enable scaled start also
      ;;
    s)
      doScaled=1   # enable only the scaled start
      doTrigger=0  # disable trigger stream start
      doEvent=0    # disable event stream start
      ;;
    t)
      doEvent=0   # disable event stream start
      doScaled=0  # disable scaled stream start
      ;;
    \?)
      echo "====================================================" >> ${startLog}
      echo "============================================================"
      echo "Error: Invalid command switch option: -$OPTARG" | tee -a ${startLog}
      usage 1
      ;;
    :)
      echo "====================================================" >> ${startLog}
      echo "Error: Command option -$OPTARG requires an argument." | tee -a ${startLog}
      usage 1
      ;;
  esac
done

#Here use shift to offset input $@ to rest of args following the switch options
shift $(($OPTIND - 1))

# Parse optional input args, after switches?
#myArg1=${1}

if !(($doTrigger)) && !(($doEvent)) && !(($doScaled)); then
    echo "Error: -e, -s, -t command line options were specified disabling all streams, that's a no-op" | tee -a ${startLog}
    exit 1
fi

# Block wrapper for output redirection to startLog
{
  if (( $doEvent )) ; then
    # <><><><><><><><><><><><><><><><><><><><><><><><>
    # string to look for in the process list
    # WARNING: a less specific string could fine processes editing the script, etc.
    startJob="bin/makeDRPcont.pl $dbase ../cfg/makeDRPcont.props"
  
    # look for the pid of the active job, if any
    grepStr="makeDRPcon[t].props"
    pid=`pgrep -f "$grepStr"`
  
    echo "============================================================"
    # if no job running, START ONE
    if [ -z "${pid}" ]; then
  
      echo Starting: ${startJob} at `date -u +'%F %T %Z'` on ${host}
  
      # fork the job
      ${startJob} >>${startLog} 2>&1 &   
  
      # send email
      if [ -n "$mailaddr" ]; then
        sleep 3
        pid=`pgrep -f "${grepStr}"`
        if [ -n "${pid}" ]; then
            tail -3 $startLog | mailx -s "Restart makeDRPcont on ${host} (pid=$pid)" "$mailaddr"
        else 
            echo "ERROR: Unable to restart ${startJob} on ${host}"
            tail -6 $startLog | mailx -s "FAILED restart makeDRPcont on ${host}" "$mailaddr"
        fi
      fi
  
    else
      echo ${startJob} running as pid ${pid} at `date -u +'%F %T %Z'` on ${host}
    fi
  fi
  
#  if (( $doTrigger )); then 
    # <><><><><><><><><><><><><><><><><><><><><><><><>
    # string to look for in the process list
    # WARNING: a less specific string could fine processes editing the script, etc.
#    startJob="bin/makeTRGcont.pl $dbase ../cfg/makeTRGcont.props"
  
    # look for the pid of the active job, if any
#    grepStr="makeTRGcon[t].props"
#    pid=`pgrep -f "$grepStr"`
  
    # if no job running, START ONE
#    if [ -z "${pid}" ]; then
  
#      echo "============================================================"
#      echo Starting: ${startJob} at `date -u +'%F %T %Z'` on ${host}
  
      # fork the job
#      ${startJob} >>${startLog} 2>&1 &   
  
      # send email
#      if [ -n "$mailaddr" ]; then
#        sleep 3
#        pid=`pgrep -f "${grepStr}"`
#        if [ -n "${pid}" ]; then
#            tail -3 $startLog | mailx -s "Restart makeTRGcont on ${host} (pid=$pid)"  "$mailaddr"
#        else 
#            echo "ERROR: Unable to restart ${startJob} on ${host}" >>${startLog} 2>&1
#            tail -6 $startLog | mailx -s "FAILED restart makeTRGcont on ${host}"  "$mailaddr"
#        fi
#      fi
  
#    else
#      echo ${startJob} running as pid ${pid} at `date -u +'%F %T %Z'` on ${host}
#    fi
#  fi

  if (( $doScaled )) ; then
    # <><><><><><><><><><><><><><><><><><><><><><><><>
    # string to look for in the process list
    # WARNING: a less specific string could fine processes editing the script, etc.
    startJob="bin/makeDRPScont.pl $dbase ../cfg/makeDRPScont.props"
  
    # look for the pid of the active job, if any
    grepStr="makeDRPScon[t].props"
    pid=`pgrep -f "$grepStr"`
  
    echo "============================================================"
    # if no job running, START ONE
    if [ -z "${pid}" ]; then
  
      echo Starting: ${startJob} at `date -u +'%F %T %Z'` on ${host}
  
      # fork the job
      ${startJob} >>${startLog} 2>&1 &   
  
      # send email
      if [ -n "$mailaddr" ]; then
        sleep 3
        pid=`pgrep -f "${grepStr}"`
        if [ -n "${pid}" ]; then
            tail -3 $startLog | mailx -s "Restart makeDRPScont on ${host} (pid=$pid)" "$mailaddr"
        else 
            echo "ERROR: Unable to restart ${startJob} on ${host}"
            tail -6 $startLog | mailx -s "FAILED restart makeDRPScont on ${host}" "$mailaddr"
        fi
      fi
  
    else
      echo ${startJob} running as pid ${pid} at `date -u +'%F %T %Z'` on ${host}
    fi
  fi
  
  if (( $doTag )) ; then
    # <><><><><><><><><><><><><><><><><><><><><><><><>
    # string to look for in the process list
    # WARNING: a less specific string could find processes editing the script, etc.
    startJob="bin/makeDRPcont.pl -l $tag $dbase ../cfg/makeDRPcont${tag}.props"
    # look for the pid of the active job, if any
    grepStr="makeDRPcon[t]${tag}.props"
    pid=`pgrep -f "$grepStr"`
    echo "============================================================"
    # if no job running, START ONE
    if [ -z "${pid}" ]; then
      echo Starting: ${startJob} at `date -u +'%F %T %Z'` on ${host}
      # fork the job
      ${startJob} >>${startLog} 2>&1 &   
      # send email
      if [ -n "$mailaddr" ]; then
        sleep 3
        pid=`pgrep -f "${grepStr}"`
        if [ -n "${pid}" ]; then
            tail -3 $startLog | mailx -s "Restart makeDRPcont$tag on ${host} (pid=$pid)" "$mailaddr"
        else 
            echo "ERROR: Unable to restart ${startJob} on ${host}"
            tail -6 $startLog | mailx -s "FAILED restart makeDRPcont$tag on ${host}" "$mailaddr"
        fi
      fi
    else
      echo ${startJob} running as pid ${pid} at `date -u +'%F %T %Z'` on ${host}
    fi
    if (( $doScaled )); then 
      # <><><><><><><><><><><><><><><><><><><><><><><><>
      # string to look for in the process list
      # WARNING: a less specific string could find processes editing the script, etc.
      startJob="bin/makeDRPScont.pl -l $tag $dbase ../cfg/makeDRPScont${tag}.props"
      # look for the pid of the active job, if any
      grepStr="makeDRPScon[t]${tag}.props"
      pid=`pgrep -f "$grepStr"`
      echo "============================================================"
      # if no job running, START ONE
      if [ -z "${pid}" ]; then
        echo Starting: ${startJob} at `date -u +'%F %T %Z'` on ${host}
        # fork the job
        ${startJob} >>${startLog} 2>&1 &   
        # send email
        if [ -n "$mailaddr" ]; then
          sleep 3
          pid=`pgrep -f "${grepStr}"`
          if [ -n "${pid}" ]; then
              tail -3 $startLog | mailx -s "Restart makeDRPScont$tag on ${host} (pid=$pid)" "$mailaddr"
          else 
              echo "ERROR: Unable to restart ${startJob} on ${host}"
              tail -6 $startLog | mailx -s "FAILED restart makeDRPScont$tag on ${host}" "$mailaddr"
          fi
        fi
      else
        echo ${startJob} running as pid ${pid} at `date -u +'%F %T %Z'` on ${host}
      fi
    fi
  fi
  
  # Do log cleanup
  if [ -n "$maxDaysBack" ]; then 
    echo "----------------------------------------------------"
    if [[ "$maxDaysBack" =~ ^[0-9]+$ && "$maxDaysBack" > "0"  ]]; then

      echo "Deleting files in $logdir older than $maxDaysBack days"
      find $logdir -mtime "+$maxDaysBack" -print -exec \rm '{}' \;

      echo "----------------------------------------------------"
      echo "Deleting .gif files in ${appHome}/output older than $maxDaysBack days"
      find output -name "*.gif" -mtime +${maxDaysBack} -print -exec \rm -f '{}' \;

    else
      echo "Invalid max days back to keep local logs/gifs: $maxDaysBack"
    fi
  fi 

  # check for waveserver errors in DRP log
  echo "============================================================"
  bin/checkDRPlog.sh
  echo "============================================================"

} >> ${startLog} 2>&1
