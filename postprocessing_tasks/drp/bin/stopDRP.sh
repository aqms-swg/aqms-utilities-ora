#! /bin/bash
# kill a bunch of drp processes
#
killprocs () {
  let count=0
  #  grep -v ${0} so the script doesn't kill itself
  #for pid in `~/utils/ss ${pattern}| grep $USER | grep -v "${0}" | cut -c9-15 | sort | uniq`
  for pid in `pgrep -f -- "${pattern}"`
  do
    kill -9 ${pid}
    let count++ 
    echo killed ${count} $pattern process, pid=$pid 
  done

}

pattern="/makeDRPcon[t]"
killprocs "$pattern"

pattern="/makeDRPScon[t]"
killprocs "$pattern"

pattern="/makeTRGcon[t]"
killprocs "$pattern"

