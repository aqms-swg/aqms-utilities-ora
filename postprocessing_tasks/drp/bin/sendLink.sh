#!/bin/bash

evid=$1
tmpdir="/tmp/$LOGNAME"
#echo "$0 DEBUG tmpdir: $tmpdir"

if [ -e "/app/cluster/I_AM_TEST" ]; then
  echo "$0 found /app/cluster/I_AM_TEST file exists, a no-op `date -u +'%F %T %Z'`"
  exit
fi

if [ -d "$tmpdir" ]; then
    rm $tmpdir/${evid}.LI
else 
    mkdir $tmpdir
    chmod 700 $tmpdir
    echo " $tmpdir created"
fi

${TPP_HOME}/drp/bin/QDDSwavelink.pl $evid > $tmpdir/${evid}.LI
${TPP_HOME}/qdds/bin/QDDSdispatch.pl $tmpdir/${evid}.LI
