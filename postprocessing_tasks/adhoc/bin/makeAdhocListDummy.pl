#!/usr/bin/perl
#
# Download and concatinate all the Ad Hoc lists from all CISN partners.
# Print results to stdout
#
# Requires 'wget' and 'sort' 
#
# Gets the source URLs for station lists from "$ENV{TPP_HOME}/adhoc/cfg/adhocSourceURL.cfg";
#
# Writes raw info to file 'adhocDummyTmp.lis' and 
# dumps to STDOUT alpha sorted by stations/net/channel
# 
# Background:
# The AdHoc station/channel format was developed by CISN in 2003 as 
# a way to exchange the minimum channel information required 
# for simple data processing.
# Each network operator is responsible for maintaining their own 
# AdHoc list and make it available via ftp or html. 
# Typically an e-mail is sent when a list changes. 
# Alternatively, lists may be built from scratch each time the html 
# list is accessed or refreshed.
 
use strict;      
use warnings;

my $URL;
my $result;
my $outfile1 = "adhocDummyTmp.lis";
# file with URLS 
my $urlFile = "$ENV{TPP_HOME}/adhoc/cfg/adhocDummySourceURL.cfg";
open(MYURLFILE, "< $urlFile") or die "Couldn't open URL list file: $urlFile: $!";
my @urlList = <MYURLFILE> ; # read file into list, need to have 1 or URLs
( @urlList > 0) or die "Empty URL file unable to process adhoc list"; 
# more robust test would be for valid URL syntax?

# delete old working file
unlink $outfile1;

# wget - GNU command to download a file from a web server via http: or ftp:
# "-O -" directs output to STDOUT
foreach $URL (@urlList) {
        chomp $URL;
        # remove comment text and all leading,trailing spaces, then skip if empty string
        $URL =~ s/\#.*$//;
        $URL =~ s/^\s+//;
        $URL =~ s/\s+$//;
        next if ! $URL ;
        #
        # NOTE: aqms.env needs to define correct PATH to find wget command, differs between OS types
        #`wget -o adhoc.log -O - $URL >> $outfile1`;
        #`wget -O - $URL >> $outfile1 2>&1`; # the 2> stderr redirect comingles cmd messages with station list
        # Just use default wget message logging to STDERR and send list to STDOUT
        print STDERR "  wget -nv -O - $URL \>\> $outfile1\n"; # cmd messages/errors are logged to STDERR of invoking shell
        `wget -nv -O - $URL >> $outfile1`; # cmd messages/errors are logged to STDERR of invoking shell
}

# Kludge sed to fix the CE listing, sort remove duplicates, strip any html tags ("<*>") and comments
print `sed 's/CE ... --  ../CE DUM --  99/' $outfile1 | sort | uniq | grep -v "<*>" | grep -v "^#" | tr -d '\r'`;

# print trailer with timestamp
my $date = `date -u +'%F %T %Z'`;
chomp $date;
print "#\n";
print "# Updated : $date\n";

# delete old working file
unlink $outfile1;
