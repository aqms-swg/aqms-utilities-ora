#!/usr/bin/perl
#
# Script has hard-coded host network dependencies
# Create current CISN "Ad Hoc" list for Pasadena
# Format
#------------------------------------------------------------------
#....+....1....+....2....+....3....+....4....+....5....+....6....+...
#STAT  NC CHN LC  SC  LATITUDE  LONGITUDE  ELEV STATIONNAME
#1745  NP HN8 --  10  37.45697 -122.17028     4 CA: Menlo Park; USGS Bldg
#DEV   CI VHZ 01   4  33.93597 -116.57794   337 Devers
#DGR   CI BHE 01   4  33.65001 -117.00947   650 Domenigoni Reservoir
#------------------------------------------------------------------
      
use strict;
use warnings;

use DBI;            # Load the DBI module

# get masterdb and username/password
use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use DbConn;
use MasterDbs;

# parse optional switches (N.B. this 'shift's the @ARGV array) 
use Getopt::Std;
use vars qw/ $opt_d $opt_h /;  #prevent "only used once" error

getopts('d:h');  # "d:" means a value must follow

# test for help option or command args
if ( $opt_h ) { usage(); }

my $db = $masterdb;
if ($opt_d) { $db = $opt_d };  

# !!! LOCAL KLUDGE !!!!
# Database has no concept of "my stations". If we don't filter by net code
# we'd get all the imported stations, too.
my $netList = "('CI', 'AZ', 'FA', 'TA', 'EN', 'SB', 'ZY', 'BC', 'NN') ";

# COSMOS site codes are not yet implemented in the CISN schema so...
# Exclude structures
my $excludeList = "('MIK', 'CBC')";

my $selectCmd =
"select distinct s.net, s.sta, 'DUM', '--', s.lat, s.lon, s.elev, INITCAP(s.staname) 
 from station_data s, channel_data c
 where (s.net = c.net and s.sta = c.sta) and
 ( c.net in $netList or (c.net='NP' and c.sta in ( '5030', '5062', '5081', '5271', '5402', '5403', '5444', '5492')))
 and (c.seedchan like 'H%' OR c.seedchan like 'E%') and c.sta not in $excludeList and 
 (c.OFFDATE >= SYS_EXTRACT_UTC(SYSTIMESTAMP) OR c.OFFDATE = NULL) and
 (c.ONDATE  <= SYS_EXTRACT_UTC(SYSTIMESTAMP) OR c.ONDATE  = NULL) 
 order by s.net, s.sta";

my $dbconn = DbConn->new($db)->getConn();

my $sql = $dbconn->prepare($selectCmd) 
         or die "Prepared statement failed: $DBI::errstr\n";

# print the header
print "#SITE NC CHN LC  SC  LATITUDE LONGITUDE   ELEV DESCRIPTION\n"; 
print "#\n";

#print "start = $start   end = $now \n";
# execute the prepared statment. The evid goes in where the "?" is in the statement
#  $sql->execute($net, $sta, $chan);
   $sql->execute(); 
   
my @data;
my $type = 99;    # default will be unknown for dummy channel

# Read the column results into separate variables          
while (@data = $sql->fetchrow_array()) {

    my $net  = $data[0] || '?';
    my $sta  = $data[1] || '?';
    my $chan = $data[2] || '?';
    my $loc  = $data[3] || '  ';
    my $lat  = $data[4] || 0;
    my $lon  = $data[5] || 0;
    my $z    = $data[6] || 0;
    my $name = $data[7] || '';

# implement "--" convention - DDG 5/8/06
    if ($loc eq "  ") {
      $loc = "--";
    }

# format
#sssssssSSSSSssFFFFFFF.FFfffffffff.ffiiiiixxxx
#ABL    VHZ  CI     34.85     -119.22 1975    Mount Abel      
printf "%-5s %-2s %-3s %2s %3s %9.5f %10.5f %5d %-s\n", 
         $sta, $net, $chan, $loc, $type, $lat, $lon, $z, $name ;

}    # end of "while" loop 

# print trailer with timestamp
my $date = `date -u +'%F %T %Z'`;
chomp $date;
print "#\n";
print "# Updated by CI: $date\n";

  if ($sql->rows == 0) { 
    print "No match.\n\n";
  }
  $sql->finish;
  
### Now, disconnect from the database
$dbconn->disconnect
  or warn "Disconnection failed: $DBI::errstr\n";

exit;

# -----------------------------------------------------------------
sub usage {
    print <<"EOF";

    Lists info for SCEDC stations in AdHoc list format for 1 dummy channel.
    for nets: 'CI', 'AZ', 'FA', 'TA', 'EN', 'SB', 'ZY', 'BC', 'NN',
    excluding sta: 'MIK' and 'CBC'.

    usage: $0 [-d dbase]

     -h        : print usage info
     -d dbase  : alias to use for database connection

EOF

    exit;
}

