#!/bin/bash 
#
# Script gets URLs from files found in cfg directory
#
# For crontab job we need to source the environment variables, does oracle too
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

appHome=${TPP_HOME}/adhoc
bindir=${appHome}/bin
cfgdir=${appHome}/cfg
logdir=${appHome}/logs

# Create one refreshAdHoc log per UTC day
logTimeStamp=$(date -u +"%Y%m%d")
startLog="${logdir}/refreshAdHoc_${logTimeStamp}.log"

usage() {

  cat <<EOF

Usage:

  Start the process that recreates the local and combined adhoc dummy channel list
  and copies the list to designated web service url destination directories
  The local and remote URL configuration are read from files in cfg subdirectory

  This script is usually run at regular intervals by a cron job.

  Syntax: $0 [ -c maxDaysBack ] [ -h ]

  -c days     : max days back to keep logs
  -h          : this help usage

EOF
    exit $1
}

# default value keeps all log files
maxDaysBack=''

while getopts ":c:h" opt; do
  case $opt in
    h)
      usage 0 # non-error exit
      ;;
    c)
      maxDaysBack="$OPTARG"
      ;;
    \?)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Invalid command switch option: -$OPTARG" | tee -a ${startLog}
      usage 1 # error exit
      ;;
    :)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Command option -$OPTARG requires an argument."  | tee -a ${startLog}
      usage 1 # error exit
      ;;
  esac
done

#Here use shift to offset input $@ to any remaining args that follow this shell's  switch options
shift $(($OPTIND - 1))

# Block wrapper to redirect message output to log
{

  echo "=========================================================================="

  echo "START: $0 at `date -u +'%F %T %Z'`"

  host=`uname -n`

  # Make adhoc lists and push them out to web servers
  #
  # First make the local list and to put on servers
  #
  ${bindir}/makeAdhocDummy_CI.pl > ${cfgdir}/adhocDummy_CI.lis

  # Push the CI list to local hosts that need it using scp
  while read target
  do
    if [[ "${target}" =~ ^# ]]; then continue; fi;
    if [[ "${target}" =~ ^\ *$ ]]; then continue; fi

    echo "  scp ${cfgdir}/adhocDummy_CI.lis ${target}"
    scp ${cfgdir}/adhocDummy_CI.lis ${target}

    if (( $? ))
    then
      mailx -s "refreshAdHocDummy scp to $target failed on $host" $TPP_ADMIN_MAIL <<EOT
Check user ssh keys for $host on $target 
EOT
    fi

  done < ${cfgdir}/adhocDummyLocalTargetURL.cfg;

  #
  #
  # Next make the all-CISN list (which depends on local list)
  #
  ${bindir}/makeAdhocListDummy.pl > ${cfgdir}/adhocDummy.lis

  #
  # Push the all-CISN list to all hosts that need it using scp
  # loop over targets read from an input cfg file 
  while read target
  do
    if [[ "${target}" =~ ^# ]]; then continue; fi;
    if [[ "${target}" =~ ^\ *$ ]]; then continue; fi

    echo "  scp ${cfgdir}/adhocDummy.lis ${target}"
    scp ${cfgdir}/adhocDummy.lis ${target}

    if (( $? ))
    then
      mailx -s "refreshAdHocDummy scp to $target failed on $host" $TPP_ADMIN_MAIL <<EOT
Check user ssh keys for $host on $target 
EOT
    fi

  done < ${cfgdir}/adhocDummyTargetURL.cfg;


  # Run log cleanup
  if [ -n "$maxDaysBack" ]; then 
    echo "--------------------------------------------------------------------------"
    if [[ "$maxDaysBack" =~ ^[0-9]+$ && "$maxDaysBack" > "0"  ]]; then
      echo "Deleting files in $logdir older than $maxDaysBack days" >> ${startLog} 2>&1
      find $logdir -mtime "+$maxDaysBack" -print -exec \rm '{}' \;
    else
      echo "Invalid max days back to keep logs: $maxDaysBack" >> ${startLog} 2>&1
    fi
    echo "--------------------------------------------------------------------------"
  fi 

  echo "DONE: $0 at `date -u +'%F %T %Z'`"

} >>${startLog} 2>&1
