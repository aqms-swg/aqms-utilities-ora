# Waveform export using ms_to_v0  

**Required binaries must be compiled on compatible  OS (sparc,x86,linux)**    
  
*  stp : ??? <-- Provide URL of source code once checked in  
*  [ms_to_v0](https://aqms-swg.gitlab.io/aqms-docs/man-ms_to_v0.html) is a C program that takes miniseed files as input and produces COSMOS V0 files as output.        
*  [qmerge](https://aqms-swg.gitlab.io/aqms-docs/man-qmerge.html)  
*  [create_v0xml.pl](???) : It is not a compiled binary but is required for V0 creation. It is available [here](https://gitlab.com/aqms-swg/aqms-tools/-/blob/main/ms_to_v0/create_v0xml.pl)  
  
