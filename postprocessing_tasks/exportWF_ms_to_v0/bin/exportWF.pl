#!/usr/bin/perl
# VERSION: 2012/05/12:
#
# exportWF - Export strong motions waveforms for an event to the EDC
# Script has hard-coded host network dependencies, like $stpTrigNets and stp client connection flags
# scp destination hosts are mapped to web directories read from from file ../cfg/ftpHostDir.cfg
# Assume scp destination "user" has originating user's (e.g aqms) public key in authorized_keys 
# NOTE: should be run on same host that has sendfile for amp export
#
# Adds channels into final v0c file ordered by DEPTH (finally!)
#
# Given an event id do the following:
#  - Check that event meets waveform export criteria
#  - Create list of waveforms that meet export criteria
#  - Retrieve waveform files in mseed format from STP
#  - convert mseed files into v0
#  - Concatinate 3-components into a single file with the correct naming
#  - SCP the files to <host>.gps.caltech.edu (requires keys be correct)
#
# prerequisites:
#  - stp-- retrieve mseed files from datacenter
#  - compiled utility ms_to_v0-- to convert mseed into v0 format
#  - perl script create_v0xml-- used by ms_to_v0
#
# RULES (agreed upon with NC, CE, & BK)
#
# - Waveforms will be sent in COSMOS V0 format
# - All three components of a triaxial set will be in a single file.
# - Only associated waveforms will be sent.
# - Waveforms will be sent only for events > 4.0
# - Channels "HL_" & "HN_" (100-80 sps, acceleration channels) will be sent
#
#
#Exchange files will be named in the following fixed format:
#
#  NNSSSSSA.###.v0c
#
#Where:
#
#  NN     = The 2-char FDSN network code of the station owner
#assumesi  SSSSS  = 5-char station code or number. If shorter than 5-char
#           left justify and pad with "-" (e.g. "ABC--")
#  A      = Domain of the event ID. The 1st character of the 
#           network code of the ID originator.
#           (ie, 'n' for NC, 'c' for CI, 'u' for NEIC)
#  ###    = The last three digits/characters of the event ID
#  .v0c   = A literal string ".v0c" suffix to aid recognition of 
#           V0 files
#
# FTP Directories are of the form: 
#              YYYYMMDDHHMMSS_EventName
#    Example: "20050112081046_DesertHotSprings"
#
# Automatic process doesn't KNOW event "name" so we use closest town.
#

use strict;
use warnings;

use Getopt::Std;
use Benchmark;

# get masterdb and username/password
use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use DbConn;
use MasterDbs;

# glibc on RedHat machine croaks on ms_to_v0, this fixes it
#   possible values:
#      0 - Do not generate an error message, and do not kill the program
#      1 - Generate an error message, but do not kill the program
#      2 - Do not generate an error message, but kill the program
#      3 - Generate an error message and kill the program
$ENV{MALLOC_CHECK_} = 0;

# global package option variables
our ($opt_a, $opt_d, $opt_f, $opt_h, $opt_k, $opt_n, $opt_t, $opt_v, $opt_x, $opt_z);
# parse optional switches
getopts('a:d:fhkn:t:vxz'); # ":" means a value must follow

# Make STDOUT "hot" e.g. autoflush
# By default perl running in background flushes its buffers 
# so monitoring the log file makes it appear the script isn't
select STDERR; $| = 1;
select STDOUT; $| = 1;

# Check for valid arg list
if ( @ARGV == 0 || $opt_h ) { usage(); }

# Requires local network code used for create_v0xml.pl command line argument
die "Undefined environment variable 'AQMS_NET_AUTH', check /usr/local/etc/aqms.env\n$!" if ( ! defined $ENV{AQMS_NET_AUTH} );
my $localNet = $ENV{AQMS_NET_AUTH};

if (! defined $ENV{EXPORTWF_HOME} ) { $ENV{EXPORTWF_HOME} = "$ENV{TPP_HOME}/exportwf"; }
die "Missing application directory\n$!" if ( ! -d $ENV{EXPORTWF_HOME} ) ;

my $evid = $ARGV[0];
chomp $evid;

# Default data dir
my $datadir = "$ENV{EXPORTWF_HOME}/data"; # data directory in same root as src

# Alternative directory locations for generated data 
if ( @ARGV > 1 ) {
  $datadir = "$ARGV[1]";
}
elsif ( defined $ENV{EXPORTWF_DATADIR} ) {
  $datadir = "$ENV{EXPORTWF_DATADIR}";
}
die "\"$datadir\" data directory for output files D.N.E." if ! -d "$datadir";

# Read script property configuration
my $propsFile = "$ENV{EXPORTWF_HOME}/cfg/exportWF.props";
my %props = readConfig( $propsFile, "=" );

my @destinationNets;
# Command option for destination net codes overrides any cfg file property
if ( $opt_n ) {
 @destinationNets = split( ':', $opt_n ) 
}
else {
 @destinationNets = split( '[: ,]+', $props{'destinationNets'} );
}

# get remote host scp destination dirs and their http URL mapping from input file
# cfg file directory is in same root as src
my $ftpCfgFile = "$ENV{EXPORTWF_HOME}/cfg/ftpHostDir.cfg";
my %ftpServerList = readConfig( $ftpCfgFile, "=>" );

#Default to master else parse command line arg option
my $dbase = $masterdb;
if ($opt_d) { $dbase = $opt_d; }

# Make a dbase connection - for use in subroutines
my $dbconn =  DbConn->new($dbase)->getConn() 
   or die "Can't connect to database $dbase\n";

# start benchmark timer
my $bm_start = new Benchmark;

# Change working dir for new stpin files 
chdir $datadir;

if ( $opt_z ) {
  print "INFO exportWF: run with opt_z => no STP processing, channel mseed files must already exist\n";
}
else {
  my $stpIn = "$datadir/${evid}.stpin";  # stores channel information for stp to read and download
  makeSTPchanList($dbconn, $evid, $stpIn);
  doSTP($evid, $stpIn);
}

# Run STP - should have created a subdir named for the evid, so if doSTP was successful...
$datadir = "$datadir/${evid}";
if ( ! -d $datadir) {
    # handling $datadir doesn't exist error
    print "ERROR exportWF: STP unable to create the event subdirectory -> $datadir\n";
    exit 1;
}

# change working dir to event subdir
chdir $datadir;
print "\nINFO exportWF: begin processing for event $evid ===========>>\n\n";

# we'll store the result files here before transfering them to ftp
print `mkdir $datadir/export`;

# Flag effects file deletions after processing, deletes if still true
my $eventSuccess = 1; # set to 0 on file error

# Save and report event time
my $etime = getOriginTime($evid);  # requires STP generated <evid>.evnt file
my @torigin = split( '[,]', $etime );
print "\nINFO exportWF: event datetime: $etime\n\n";

# Read into list the STP generated mseed files found in directory
opendir(THISDIR, ".") or die "couldn't open directory: $!";
my @seedFiles = grep /.*\.mseed$/, readdir THISDIR;
closedir THISDIR;

# Loop over mseed files found in list
my (%alreadyDone, $newfile);
foreach my $file (@seedFiles) {

    my ($id, $net, $sta, $seed, $locid) = split (/\./, $file);

    # flag sta processing state in hash
    if ( $alreadyDone{$sta} ) {
        next;
    } else { 
        $alreadyDone{$sta} = "done";
    }

    # Create xml file using data available in db for channels active on the input event origin date
    my $cmd = "$ENV{EXPORTWF_HOME}/bin/create_v0xml.pl -t $torigin[0] -n $net -s $sta $localNet $dbase";
    print "$cmd\n";
    my $result = `$cmd`;
    print "$result\n"; # note not if $opt_v;

    if ( $? ) {
      $eventSuccess = 0;
      print "ERROR exportWF: create_v0xml failed for $net $sta, skipping to next...\n";
      next;
    }

    # determine order from xml file created by create_v0xml.
    my @chanOrder = orderChans("./${net}-${sta}.xml");

    # ordered by location id increasing
    if (! @chanOrder || ! defined $chanOrder[0] ) {
      print "WARNING exportWF: missing channel meta-data? check xml file contents for: $net $sta, skipping to next sta\n";
      next;
    }

    print "INFO exportWF: processing mseed files for next sta: $sta\n";

    # First handle missing loc code in filename for blank-blank case (weird STP naming)
    $locid =~ s/mseed/--/;

    # Get the minimum datetime_on found in database for station's archived waveforms 
    # add 10 secs to move into pre-event, hoping that the signal follows later,
    # then round resulting value to nearest whole sec.
    my $sql = "select translate(truetime.getStringf(round(min(w.datetime_on)+10.0, 0)),' ',','),
               round(min(w.datetime_on)+10.0, 0), max(w.datetime_on)
               from assocwae a, waveform w where
               a.evid=? and w.wfid=a.wfid and w.sta=? and w.net=? and substr(w.seedchan,1,2)='HN'"; 
    #print "$sql\n";  # debug
    
    my $sth = $dbconn->prepare($sql) or die "Prepared statement failed\n";
    $sth->execute($evid,$sta,$net); 

    my @data;  
    my $datetime = 'unknown';
    my $minTime = 0;
    my $maxTime = 0;
    while (@data = $sth->fetchrow_array()) {
      $datetime = $data[0] || 'unknown';
      $minTime  = $data[1] || '0';
      $maxTime  = $data[2] || '0';
    }
    $sth->finish;

    print "INFO exportWF: $net $sta alignment time: $datetime\n";
    
    # Create time-aligned mseed channel files for the channels of net sta
    # Use the datetime derived from the channels db waveform's datetime_on values
    # Align the start time of 1st mseed record written by qmerge output file: *.ali
    # Note $locid and $seed channel are reversd in output filename,
    # for glob sort order by location,seedchan
    my @stafiles = glob("$id.$net.$sta.*.mseed");
    foreach my $sfile (@stafiles) {
      my ($id, $net, $sta, $seed, $locid, $mseed) = split (/\./, $sfile);
      $locid =~ s/mseed/--/;
      my $cmd;
      if ( $datetime ne 'unknown' ) {
        $cmd = "$ENV{EXPORTWF_HOME}/bin/qmerge -T -f $datetime -o $id.$net.$sta.$locid.$seed.mseed.ali $sfile";
        print "Warning: $sta $net misaligned, max-min datetime_on > 5s\n" if ($maxTime - $minTime) > 5;
      }
      else {
        print "Warning: datetime unknown, waveform data is unaligned for $net $sta\n";
        $cmd = "cp -p $sfile $id.$net.$sta.$locid.$seed.mseed.ali";
      }
      print "$cmd\n";
      my $result = `$cmd`;
      if ( $? ) {
        $eventSuccess = 0;
        print "ERROR exportWF: mseed.ali failure? result= $result \n";
      }
      else {
        # remove pre-alignment STP generated mseed files to save space
        if ( ! $opt_k ) {
          `\\rm -f $sfile`;
        }
      }
    }

    # convert to v0 requires 1 or more mseed.ali channel files
    @stafiles = glob("$id.$net.$sta.*mseed.ali");
    my $countFiles = scalar @stafiles;
    if ($countFiles == 0) {
        print "WARNING exportWF: no time-aligned (qmerge) channels found $net $sta, skipping to next sta...\n";
        next;
    }
    elsif ($countFiles < 3) {
        print "WARNING exportWF: found < 3 ($countFiles) time-aligned channels for $net $sta\n";
        #next;
    }

    #NOTE: construct files string not from stafiles glob order, but instead from chanOrder xml file order
    my $strfiles;
    for (my $i = 0; $i < @chanOrder; $i++) {
        my $chan = $chanOrder[$i][0];
        if (defined $chan) {
            my $loc = $chanOrder[$i][1];
            $strfiles .= "$id.$net.$sta.$loc.$chan.mseed.ali "; # need space separator between filenames
        }
    }

    # ms_to_v0 uses the xml files and seed files to generate cosmos style v0 files.
    $cmd = "$ENV{EXPORTWF_HOME}/bin/ms_to_v0 -ei ${net}-${sta}.xml $strfiles 2>&1";
    print "$cmd\n";  # if $opt_v;
    $result = `$cmd`;
    if ($?) {
        $eventSuccess = 0;
        print "ERROR exportWF: ms_to_v0 failed with result= $result\n";
        next;
    }
    else {
      # remove xml and .ali mseed files
      if ( ! $opt_k ) {
        `\\rm -f ${net}-${sta}.xml $strfiles`;
      }
    }

    # Build the filename like: NNSSSSSA.###.v0c
    my $sta5 = substr("$sta"."-----", 0, 5);   #"-" fill 5-char station code
    my $id3  = substr($id, -3);                # last 3 digits of event ID
    my $domain = "c";                          # data originator - 1st char of network

    # concatenate the 3 components into a single file down in the 'export' dir
    # NOTE: channels are ordered by location, seedchan
    $newfile = "${net}${sta5}${domain}.${id3}.v0c";
    print "INFO exportWF: new V0 file is: $newfile\n";
    for (my $i = 0; $i < @chanOrder; $i++) {
        my $chan = $chanOrder[$i][0];
        if (defined $chan) {
            my $loc = $chanOrder[$i][1];
            print "INFO exportWF: $i concatenating files: $id.$net.$sta.$loc.$chan.mseed.ali.v0\n" if $opt_v; 
            `cat $id.$net.$sta.$loc.$chan.mseed.ali.v0 >> $datadir/export/$newfile`;
            if ( $? ) {
              $eventSuccess = 0;
              print "ERROR exportWF: summary v0 concatenation failed for $net $sta\n"; 
            }
            else {
              # remove ali.v0 files
              if ( ! $opt_k ) {
                `\\rm -f $id.$net.$sta.$loc.$chan.mseed.ali.v0`;
              }
            }
        }
    }
}

# remove the STP generated evnt summary and stpin file after processing v0
if ( ! $opt_k ) {
  if ( $eventSuccess ) { 
    `\\rm -f ${evid}.evnt` if -f "${evid}.evnt";
    `\\rm -f ../${evid}.stpin` if -f "../${evid}.stpin";
  }
}

print "\nINFO exportWF: V0 creation took ", timestr(timediff(new Benchmark, $bm_start)), "\n";

if ( $opt_x ) {
    print "WARNING exportWF: running option -x mode: No V0 files copied to destinations or email sent!\n";
    exit 0;
}

# First cleanup by removing 0-byte size v0C files from dir
print `find $datadir/export -name "*.v0c" -size 0 -print -exec \\rm '{}' \\;`;

# Copy concatenated event v0c into to export temp.dir 
my $targetHost = '';
if ( $opt_t ) {
  if ( index($opt_t, ':') >= 0 ) {
    my @thosts = split(':', $opt_t);
    foreach my $hst ( @thosts ) {
      my $host = substr($hst, index($hst, '@')+1);
      my $tf = `$ENV{TPP_BIN_HOME}/amPrimaryHost.pl $host`;
      chomp $tf;
      if ( $tf eq "true" ) {
        $targetHost = $hst;
        last;
      }
    }
  }
  else {
    $targetHost = $opt_t;
  }
  die "exportWF Error: Unable to find a primary target host in -t $opt_t\n" if ! $targetHost;
}

foreach my $netc ( @destinationNets ) {
    my $target = ( ${targetHost} ) ? "${targetHost}:" : '';
    print "scp -p $datadir/export/*.v0c ${target}$ENV{PP_HOME}/xdata/io/${netc}/outdir/temp.dir\n";
    print `scp -p $datadir/export/*.v0c ${target}$ENV{PP_HOME}/xdata/io/${netc}/outdir/temp.dir`;
    $target = ( ${targetHost} ) ? "ssh ${targetHost} " : '';
    print "${target}mv $ENV{PP_HOME}/xdata/io/${netc}/outdir/temp.dir/*.v0c $ENV{PP_HOME}/xdata/io/${netc}/outdir\n";
    print `${target}mv $ENV{PP_HOME}/xdata/io/${netc}/outdir/temp.dir/*.v0c $ENV{PP_HOME}/xdata/io/${netc}/outdir`;
}

my $urllist = "\nV0 waveform files are available:\n";
if ($opt_f) {
  # Build the sub dir name 
  # FTP Directories are of the form: 
  #              YYYYMMDDHHMMSS_EventName
  #    Example: "20050112081046_DesertHotSprings"
  #
  #$newfile = "CISVD--c.260.v0c";
  #
  # event date/time is from the 'evnt' file written by STP evnt has the following format:
  # 10059745 le 2004/11/13,17:39:16.930   34.3535  -116.8447   9.64   4.19  l 1.0

  my $str = join("", split('[./,:]', $etime)); # $etime lookup already done above 
  my $town = getClosestTownStr($dbconn, $evid);
  my $subdir = "${str}_${town}";
  print "INFO exportWF: export subdirectory renamed for ftp to: $subdir\n";
  # Delete previous versions and rename the export dir to ftp-style name
  print `rm -rf $subdir`;
  print `mv $datadir/export $subdir`;
  
  # Copy files to ftp mirror hosts
  foreach my $destination (keys %ftpServerList) {
    print `scp -r $datadir/$subdir $destination`;
    $urllist .= "http://" . $ftpServerList{$destination} . $subdir . "\n";
  }
}

send_completion_email($evid, $urllist);

print "INFO exportWF: complete processing took ", timestr(timediff(new Benchmark, $bm_start)), "<<==========\n";

exit 0; # Exit at end of main exportWF code below are subroutine declarations

# ==================================================================================
# ==================================================================================

# Retrieve V0 files for channels defined in a file.
# args (evid, inputfile)
# Notes:
#  - requesting a bogus station is harmless, no file is created
sub doSTP {
    #OVERRIDE: a network host specific stp for a test system e.g.:
    # stp -a stp2 -p 9997"; # stp -v -a calcite2";
    my $runstp;
    if ( $opt_a ) { # User provided server name:port
      my @stpCfg = split( ':', $opt_a );
      if ( @stpCfg == 2 ) { 
        $runstp = "$ENV{EXPORTWF_HOME}/bin/stp -a $stpCfg[0] -p $stpCfg[1]";
      }
      elsif ( @stpCfg == 1 ) { 
        $runstp = "$ENV{EXPORTWF_HOME}/bin/stp -a $stpCfg[0]";
      }
    }
    else { # PRODUCTION DEFAULT:
      $runstp = "$ENV{EXPORTWF_HOME}/bin/stp";
    }
    print "$runstp\n";

    # resulting files are written to a dir named for the evid
    # files have the format: 10060101.CI.WES.$chan.$loc.mseed

    my $evid   = shift @_;
    my $infile = shift @_;

    # <> Run stp to create files
    my $run1;
    open ($run1, "| ${runstp}") or die "Unable to doSTP\n$!";
    print $run1 <<"EndofRun";
mseed
input $infile
quit
EndofRun

    close $run1;
}

# =================================================================
# Get the closest town to the event
#
#  $town = getClosestTownStr ($dbconn, $evid);

sub getClosestTownStr {

    # Look-up closest town
    # Returns a string like "Mt. San Gorgonio, CA"
    my $sql = "Select Wheres.getNearestTown(o.lat, o.lon)
              from event e, origin  o
              where o.orid = e.prefor and e.evid = $evid" ;

    my $sth = $dbconn->prepare($sql) or die "Prepared statement failed\n";
    $sth->execute(); 

    my $town     = "unknown";
    my @data;  
    while (@data = $sth->fetchrow_array()) {
        $town = $data[0] || 'unknown';
    }
    print "INFO exportWF: closest TOWN is $town\n";
    $sth->finish;

    # clip off ", CA" & loose whitespace
    # "Mt. San Gorgonio, CA" -> "MtSanGorgonio"
    $town = substr($town, 0, -3);

    $town =~ s/[., ()]//g;   #chuck unwanted chars

    return $town;
}

# =============================================
# Make the file containing the channels with big amps
#
# makeSTPchanList($dbconn, $evid, $stpIn);

sub makeSTPchanList {

    print "\nINFO exportWF: building STP Channel Set\n";

    # Define behavior variables
    #
    # Only send waveforms from listed networks TODO: put acceptable network values for stp query in cfg file
    # NOTE: -aww 2012/09/25
    #   Add BC net when metadata is reliable
    #   And what about 'PB' net ?
    #   Do not include CE net, assume other networks have that CGS data.
    #   Remove AZ net from list until we have reliable logger metadata
    #my $stpTrigNets = "('CI', 'AZ', 'NP', 'ZY', 'FA', 'SB')";
    #my $stpTrigNets = "('CI', 'NP', 'ZY', 'FA', 'SB')";

    my @nets = split( '[: ,]+', $props{"stpSourceNets"} );
    my $stpTrigNets .= "(";
    for ( my $i = 0; $i < @nets; $i++) {
      $stpTrigNets .= "\'$nets[$i]\'";
      if ( $#nets > 1 && $i < $#nets ) {  $stpTrigNets .= ', '; };
    }
    $stpTrigNets .= ")";

    # Only send waveforms if one component exceeds this acceleration
    # Agreed value = 0.5%g -- g = 980.665cm/sec2
    #$pgaThreshold = 0.005 * 980.665;    # ~5 cmss dbase units are cm/sec2 - DDG 6/13/07
    my $pgaThreshold = 0.001 * 980.665;    # as per DIWG request -aww 2011/08/17

    my $dbconn = shift @_;
    my $evid   = shift @_;
    my $stpIn  = shift @_;

# Changed sql query to use valid ampgen ampsets from assocevampset,ampset,amp table join -aww 2011/08/31
# removed ampset filter:  AND SUBSTR(UPPER(e.subsource),1,3) != 'GMP' -aww 2012/08/07
#                         AND (a.subsource != 'GMP' OR a.net='CI')
#
# Need location to discriminate multiple seedchan types with different locations -aww
# AND c.edepth < 5.  # CESMD wants all channels, regardless of edepth
#
    my $sql =  "SELECT DISTINCT a.net, a.sta, SUBSTR(a.seedchan,1,2), a.location
                 FROM assocevampset e, ampset s, amp a, channel_data c
                 WHERE e.evid = ${evid}
                   AND e.isvalid=1
                   AND s.ampsetid = e.ampsetid
                   AND a.ampid = s.ampid
                   AND a.amptype='PGA'
                   AND a.amplitude >= ${pgaThreshold}
                   AND UPPER(a.cflag) = 'OS'
                   AND a.net IN ${stpTrigNets}
                   AND SUBSTR(a.seedchan,1,2) IN ('HH', 'HN','HL')
                   AND c.sta=a.sta AND c.net=a.net AND c.seedchan=a.seedchan AND c.location=a.location
                   AND util.nominal2date(a.datetime) BETWEEN c.ondate AND c.offdate";

    my $sth = $dbconn->prepare($sql) or die "Prepared statement failed\n";
    # execute the prepared statement. 
    $sth->execute(); 

    # open the STP control file
    open(StpInFile, ">${stpIn}");

    # Parse the result and ...
    while (my @data = $sth->fetchrow_array()) {
        my $net = $data[0] || 'null';
        my $sta = $data[1] || 'null';
        my $ch2 = $data[2] || 'null';
        my $loc = $data[3] || 'null';
        # replace blanks with dash
        $loc =~ s/ /-/g;

        # ... write an STP line to retreive this channel
        # Only export HN types for HH
        # Need location to discriminate multiple seedchan types with different locations -aww
        if ($ch2 eq "HH") {
            print StpInFile "TRIG -net $net -sta $sta -chan HN_ -loc $loc ${evid}\n";
            #
            # NOTE: Removed HL type request below -aww 2012/09/20
            #print StpInFile "TRIG -net $net -sta $sta -chan HL_ -loc $loc ${evid}\n";
        }
        else {
            print StpInFile "TRIG -net $net -sta $sta -chan ${ch2}_ -loc $loc ${evid}\n"; 
        }
    }

    close StpInFile;

    # Remember: $sth->rows will = 0 UNTIL you call fetchrow_array()
    if ($sth->rows == 0) {
        print "INFO exportWF: No strong-motion amps met selection criteria.\n";
        exit 0;
    }

    $sth->finish;
}

sub send_completion_email {
    my $evid = shift;
    my $URLlist = shift;
    my $mailaddr = $ENV{"TPP_EXPORTWF_MAIL"};

    # only send e-mail notification once by checking semaphore file (in the evid data directory)
    if (-e "./.noEmail") {
        print "INFO exportWF: Not sending e-mail notification\n";
        return;
    }
    open MAILER, "| mailx -s \"V0 files for event $evid\" $mailaddr" or die "can't fork: $!";

    print MAILER $URLlist;

    close(MAILER);

    # create the semaphore file
    `date -u +'%F %T %Z' >> ./.noEmail`;
     print "INFO exportWF: sent e-mail notification\n";
}

sub getOriginTime {
    my $event = shift;
    my $evtFile = "./$event.evnt";

    open EVNT, "< $evtFile"  or die("couldn't open $evtFile: $!");

    my $line = <EVNT>;
    my ($null0, $null1, $time) = split(/ +/, $line);
    $time = 0 if ! defined $time;
    $time = substr($time, 0, -4);   # chop off fractions of second
    close EVNT;
    return $time;
}

sub orderChans {

    my $fileName = shift;
    my @ordered = ();
    open XMLFILE, "< $fileName" or return undef();

    my @contents = <XMLFILE>;
    close(XMLFILE);

    my @channelLines = grep /seedchan/, @contents;

    # NOTE: create_v0xml query should order by 'edepth' 
    for (my $i = 0; $i < @channelLines; $i++) {
        my $line = $channelLines[$i];
        if ( $line =~ m/seedchan="(H[LN].)"/ ) {
            my $chan = $1;
            my $loc = '??';
            if ( $line =~ m/loc="(..)"/ ) {
              $loc  = $1;
            }
            push( @ordered, [$chan, $loc] );
        }
    }

    print "INFO exportWF orderChans: @ordered\n" if $opt_v;
    return @ordered;

}


sub readConfig {

    my $configFile = shift @_;

    #e.g. $pat = "=>"; # config file delimiter to split key value strings
    my $pat = shift @_;

    open (CFGFILE, "<$configFile") or die "$! : unable to open configuration file $configFile";
    
    my %cfghash;

    while (<CFGFILE>)
    {
       chomp;
       next if /^(\s)*#/; # skip comments
       next if /^(\s)*$/; # skip blank lines
       my ($key, $val) = split /$pat/;
       $key =~ s/^\s+//;  # remove leading spaces key
       $key =~ s/\s+$//;  # remove trailing spaces key
       defined $val or die "Cannot split value from key using \"$pat\". Check $configFile line: $_\n";
       $val =~ s/^\s+//;  # remove leading spaces value
       $val =~ s/#.*$//;  # remove any trailing comment text from value
       $val =~ s/\s+$//;  # remove trailing spaces from value
       $cfghash{$key} = $val;
    }
    close CFGFILE;

    return %cfghash;
}

sub usage {
   print << "EOF";
Usage: $0 [-h -a <stp>  -d <name> -f -n -t <host> -v] <evid>  [data-path, defaults to exportwf/data or EXPORTWF_DATADIR if defined]
        h               : this usage
        a name:port     : alternative stp server (stp2 stp3) and port number (default stp:9999)
        d name          : db for channel list and wheres package town lookup, defaults to masterdb
        f               : copy V0 files to ftp mirrors as well as sendfile export 
        k               : do not delete (keep) generated work files (for debugging)
                          (e.g. individual channel's *v0,*ali,*mseed plus <evid>.evnt and <evid>.stpin)

        t host1[:host2] : remote scp target host(s) having sendfile export directory, when option is absent
                          code assumes the sendfile dir exists on localhost; if more than one target host
                          is specified (colon delimited), one of them must be flagged as primary in the
                          database AQMS_HOST_ROLE table, otherwise code fails with error.  
                          Use this option in lieu of a virtual host target to avoid the ssh key conflict
                          when a virtual IP is reassigned to another sendfile host after a role change.

        n               : one or more destination net codes for sendfile directory root (colon delimited)
                          overrides value parsed from ../cfg/exportWF.props "destinationNets" property
        v               : verbose output
        x               : test mode no V0 files copy (no emails)
        z               : no STP processing, used with x option STP generated files already exist.
Example:
$0 -d mydb -t ampexc2.geo.edu -n BK:CE:NC 

EOF
   exit;
}
