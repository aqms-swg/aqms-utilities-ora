#!/bin/bash
#
# Start/restart the processes that perform waveform exchange
# This is run at intervals by a cron job to insure that the jobs start at
# boot time and are always present.
#
# #####################################################################
#
# crontab -e
#0,10,20,30,40,50 * * * * ${TPP_HOME}/exportwf/bin/startExportWF

# For crontab job we need to source the environment variables, does oracle too
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

usage() {

  cat <<EOF

Usage:

  Start/restart the process that sends waveform V0 files to destination networks 
  This is run at intervals by a cron job to insure that the jobs start at
  boot time and are always present.

  Syntax: $0 [ -c maxDaysBack] [ ]

  -c days     : max days back to keep logs
  -h          : this help usage

EOF
    exit $1
}

export EXPORT_WF_HOME=${TPP_HOME}/exportwf
logdir=${EXPORT_WF_HOME}/logs

# One startExportWF_YYYYMMDD.log per UTC day
logTimeStamp=$(date -u +"%Y%m%d")
startLog="${logdir}/startExportWF_${logTimeStamp}.log"

maxDaysBack=''
while getopts ":c:h" opt; do
  case $opt in
    h)
      usage 0 # non-error exit
      ;;
    c)
      maxDaysBack="$OPTARG"
      ;;
    \?)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Invalid command switch option: -$OPTARG" | tee -a ${startLog}
      usage 1 # error exit
      ;;
    :)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Command option -$OPTARG requires an argument." | tee -a ${startLog}
      usage 1 # error exit
      ;;
  esac
done

#Here use shift to offset input $@ to any remaining args that follow this shell's  switch options
shift $(($OPTIND - 1))

#outputLog="${logdir}/exportWF.log"
startJob="${EXPORT_WF_HOME}/bin/exportWFcont.pl"

mailaddr=${TPP_ADMIN_MAIL}
host=`uname -n | cut -d'.' -f1`

cd $EXPORT_WF_HOME

#FLAGFILENAME=/app/cluster/I_AM_PRIMARY # only start if host system is PRIMARY for send/get files
# force it to run ( allows app no-op and result of pcs posts)
FLAGFILENAME="/etc/profile"

#Block wrapper to redirect echo output to startLog
{
  echo "===================================================="

  if [ -f "$FLAGFILENAME" ]; then

    # look for the pid of the active job, if any
    pid=`pgrep "[e]xportWFcont"`

    # if no job running, START ONE
    if [ -z "$pid" ]
    then
      # NOTE: Moved logfile creation and rotation to perl script
      #if [ -e "$outputLog" ]
      #then
      #  timeStamp=`date -u +"%Y.%m.%d.%H%M"`
      #  mv $outputLog ${outputLog}-$timeStamp
      #fi
    
      echo "Starting: ${startJob} at `date -u +'%F %T %Z'` on $host"
    
      # fork the job, put in background
      # NOTE: Use command line args to specify perl script options
      # -d for the db name (defaults to value of $masterdb) also default for pcs table
      #${startJob} $@ >$outputLog 2>&1 &
      ${startJob} $@ >>$startLog 2>&1 &
    
      # send email of success or failure
      if [ -n "$mailaddr" ]; then

        sleep 5   # give it time to start run then check for pid
        pid=`pgrep "[e]xportWFcont"`

        if [ -n "$pid" ]; then
            echo "exportWFcont now running as pid = $pid"
            tail -3 $startLog | mailx -s "Restart exportWFcont on $host (pid=$pid)"  "$mailaddr"
        else
            echo "exportWFcont restart failed, no matching process found"
            tail -3 $startLog | mailx -s "FAILED restart exportWFcont on $host"  "$mailaddr"
        fi
      fi
    
    # noop but gives error if no 'else' block!
    else
      echo "exportWFcont running as pid=$pid on $host `date -u +'%F %T %Z'`"
    fi
  else
    echo "$host on standby, NOT starting exportWFcont: `date -u +'%F %T %Z'`"
  fi

  # Run log cleanup
  if [ -n "$maxDaysBack" ]; then 
    echo "----------------------------------------------------"
    if [[ "$maxDaysBack" =~ ^[0-9]+$ && "$maxDaysBack" > "0"  ]]; then
      echo "Deleting files in $logdir older than $maxDaysBack days"
      find $logdir -mtime "+$maxDaysBack" -print -exec \rm '{}' \;
    else
      echo "Invalid max days back to keep logs: $maxDaysBack"
    fi
  fi 
} >> ${startLog} 2>&1
