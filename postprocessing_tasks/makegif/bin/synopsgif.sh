#!/bin/bash

#########################################################################
# Java 1.5 MUST HAVE "X Virtual Frame Buffer"  (Xvfb) for graphics context
# export DISPLAY and Xvfb are not required by Java 1.6 with -D headless
#########################################################################

if [ $# -lt 1 ]; then
 echo "Usage: $0 <evid> [propfile: cfg/synopsgif.props] [outfile: <evid>.gif]"
 exit
fi

evid=$1

cd $TPP_HOME/makegif/bin
propfile="../cfg/synopsgif.props"
outfile="../gifs/${evid}_synops.gif"

if [ $# -ge 2 ]; then
  propfile=$2
fi

if [ $# -ge 3 ]; then
  outfile=$3
fi

# check for Java 1.6 +

vers=`java -version 2>&1  | grep "openjdk version" | awk -F\. '{print $2}'`

if [[ ! ( $vers =~ [[:digit:]] ) ]]; then
    echo "Program requires java version number, found: $vers,  exiting"
    exit
fi

if [ -n "$vers" -a "$vers" -ge 6 ]; then
  # must use headless graphics option -H
  ${TPP_HOME}/jiggle/bin/javarun_jar.sh -H org.trinet.apps.SnapShotFromList $evid $propfile $outfile
else
  xvfb=`pgrep -f '[X]vfb'`
  if [ -z "$xvfb" ]; then
    echo "Program requires Xvfb process to be running on host... exiting"
    exit
  fi

  export DISPLAY=127.0.0.1:1.0
  ${TPP_HOME}/jiggle/bin/javarun_jar.sh org.trinet.apps.SnapShotFromList $evid $propfile $outfile
fi
