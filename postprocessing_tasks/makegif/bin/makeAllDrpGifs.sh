#!/bin/bash

# Make ithe gif, scaled gif, and synopsis gif  for an event

if [ $# -eq 0 ]; then
  echo "Syntax: $0 <evid> [gif-output-dir]"
  exit
fi

evid=$1
if [[ ! ($evid =~ ^[[:digit:]]+$ ) ]]; then
  echo "$0 error: input evid must be integer: $evid"
  exit 1
fi

# Ok figure out relative vs. absolute gifDir
#gifDir=`pwd`
if [ -n "$2" ]; then
  gifDir=$2
  if [ ! -d "$gifDir" ]; then
    echo "$0 error: Output gif dir does not exist: $gifDir"
    exit 1
  fi
fi
# convert to absolute path
cd $gifDir
gifDir=`pwd`
cd - >/dev/null
echo "Running $(basename $0):
  $evid .gif files are written to: $gifDir"

# Defaults:
maxChannels=20
maxWindowSecs=120
echo "  channels=$maxChannels
  window=$maxWindowSecs secs"

appDir=$TPP_HOME/makegif
logFile=$(basename $0)
logFile=$appDir/logs/${logFile%.sh}_${evid}.log
echo "  Output logged to: $logFile"

{
# Make event gif:
#echo "$appDir/bin/eventgif.sh $evid $maxChannels $maxWindowSecs $appDir/cfg/eventgif.props $gifDir/${evid}.gif"
$appDir/bin/eventgif.sh $evid $maxChannels $maxWindowSecs $appDir/cfg/eventgif.props $gifDir/${evid}.gif

# Make event scaled gif:
#echo "$appDir/bin/eventgif.sh $evid $maxChannels $maxWindowSecs $appDir/cfg/eventgif-scaled.props $gifDir/${evid}_scaled.gif"
$appDir/bin/eventgif.sh $evid $maxChannels $maxWindowSecs $appDir/cfg/eventgif-scaled.props $gifDir/${evid}_scaled.gif

# Make event synopsis gif:
#echo "$appDir/bin/synopsgif.sh $evid $appDir/cfg/synopsgif.props $gifDir/${evid}_synops.gif"
$appDir/bin/synopsgif.sh $evid $appDir/cfg/synopsgif.props $gifDir/${evid}_synops.gif
} >$logFile 2>&1

exit
