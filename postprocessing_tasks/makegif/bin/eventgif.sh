#!/bin/bash

#########################################################################
# Java 1.5 MUST HAVE "X Virtual Frame Buffer"  (Xvfb) for graphics context
# export DISPLAY and Xvfb are not required by Java 1.6 with -D headless
#########################################################################

if [ $# -lt 1 ]; then
 echo "Usage: $0 <evid> [ntraces: 15] [maxsecs: 120] [propfile: cfg/eventgif.props] [outfile: <evid>.gif]"
 exit
fi


evid=$1
ntraces=15
maxsecs=120

cd $TPP_HOME/makegif/bin
propfile="../cfg/eventgif.props"
outfile="../gifs/${evid}.gif"

if [ $# -ge 2 ]; then
  ntraces=$2
fi

if [ $# -ge 3 ]; then
  maxsecs=$3
fi

if [ $# -ge 4 ]; then
  propfile=$4
fi

if [ $# -ge 5 ]; then
  outfile=$5
fi

# check for Java 1.6 +

vers=`java -version 2>&1  | grep "openjdk version" | awk -F\. '{print $2}'`

if [[ ! ( $vers =~ [[:digit:]] ) ]]; then
    echo "Program requires java version number, found: $vers,  exiting"
    exit
fi
if [ -n "$vers" -a "$vers" -ge 6 ]; then
  # must use headless graphics option -H
  ${TPP_HOME}/jiggle/bin/javarun_jar.sh -H org.trinet.web.SnapGif $evid $ntraces $maxsecs $propfile $outfile
else
  xvfb=`pgrep -f '[X]vfb'`
  if [ -z "$xvfb" ]; then
    echo "Program requires Xvfb process to be running on host... exiting"
    exit
  fi

  export DISPLAY=127.0.0.1:1.0
  ${TPP_HOME}/jiggle/bin/javarun_jar.sh org.trinet.web.SnapGif $evid $ntraces $maxsecs $propfile $outfile
fi
