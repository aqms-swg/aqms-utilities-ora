#!/usr/bin/perl

# Makes an gif file image of an event snapshot view panel using the Java SnapGif class generator
# configured with the properties file "../cfg/event_<dbname>}.props", see usage() options below

use strict;
use warnings;
use File::Path 'mkpath';

# set masterdb/masterrt variable db aliases
use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use MasterDbs;

# declare allowed command line switch options
use Getopt::Std;
our ($opt_c, $opt_d, $opt_h, $opt_o, $opt_p, $opt_r, $opt_s );
getopts('c:d:ho:p:rs:');

# check for command line args
if ($opt_h || @ARGV == 0) {
  usage(0);
}

# application home
my $appHome = "$ENV{TPP_HOME}/makegif";

# check validity of input event id arg
my $evid = $ARGV[0];
if ( $evid !~ m/^\d+$/ ) {
  print "ERROR: First input argument, the evid, must be an integer\n";
  usage(1);
}

# set db name alias to be used for props file name
# default to masterdb else masterrt, if option -r
my $dbase = ( $opt_r ) ?  ${masterrt} : ${masterdb};
if ( $opt_d ) {
 print "Note: -d option override of -r option\n" if ( $opt_r );
 $dbase = $opt_d;  # No check for valid db alias
}
else {
  print "Using database : ${dbase}\n";
}

# set seconds width of time window in view panel 
my $secs = 120;
if ( $opt_s ) {
  $secs = $opt_s;
  if ( $secs !~ m/^\d+$/ ) {
    print "ERROR: -s, the seconds in window, must be an integer\n";
    usage(2);
  }
}
else {
  print "Seconds width of time window in view panel: $secs\n";
}

# set max number of channels in view panel
my $numChannels = 15;
if ( $opt_c ) {
  $numChannels = $opt_c;
  if ( $numChannels !~ m/^\d+$/ ) {
    print "ERROR: -s, the channels in window, must be an integer\n";
    usage(3);
  }
}
else {
  print "Max number of channels in view panel: $numChannels\n";
}

# check for database properties files
my $propFile = "${appHome}/cfg/event_${dbase}.props";  # Java properties for Snapshot
if ( $opt_p ) {
  $propFile = $opt_p;
}
die "Error missing properties file: $propFile\n" if ! -f $propFile; 

# set output dir for gif
my $gifDir = "$ENV{HOME}/tpp/makegif/gifs";
if ( $opt_o ) {
  $gifDir = $opt_o;
}
else {
  print "Gif file is written to output directory: ${gifDir}\n";
}
# check for or make output dir for gif
if ( ! -d $gifDir ) {
  eval {
    mkpath $gifDir;
  };
  if ( $@ ) {
    print "Error $0: $@\n"; 
    print "Unable to create: $gifDir\n";
    exit 9;
  }
}
my $gifFile = "${gifDir}/${evid}.gif";

# now make the snapshot gif
my $cmd = "${appHome}/bin/eventgif.sh ${evid} ${numChannels} ${secs} ${propFile} ${gifFile}";
print "${cmd}\n"; # print command line
print `${cmd} 2>&1`;   # Execute the command: "print" its output to stdout
my $status = $?;
# check for cmd success
if ( ! $status ) {
  print "Success generating: ${gifFile}\n";
}
else {
  # arghh ... java returns negative exit number so below returns 255 instead of -1
  #print "Exit status:" . $status . " : " . ($status >> 8) . "\n";
  # Mess with returned java status bits to recover sign ( 8 bit shift first)
  #my $sstatus = ($status >> 8);
  #print "Exit status:" . ( ($sstatus & 0x80) ? -(0x100 - ($sstatus & 0xFF)) : $sstatus ) . "\n";
  print "Error generating: ${gifFile}\n";
}

# return shell exit status
exit ($status >> 8);

#####################################################################################
# print help info, exit with called exit status value
sub usage {
    my $status = shift;
    print <<"EOF";
Usage:
  Make an event gif snapshot using the the gif generator app in jiggle.jar
  with properties file ../cfg/event_<dbname>.props

Syntax:
$0 [-h] [-c #chan] [-s #sec] [-o gifDir] [-d dbname] <evid>
  -h               : this usage
  -c               : maximum # of sorted channels to show in snapshot view panel
  -s               : integer seconds width of snapshot view window panel
  -o               : destination directory for your output gif
                     if -o path does not exist, it tries to create the directory
                     when -o option is absent path defaults to '\$HOME/tpp/makegif/gifs'
  -p propFileName  : properties file used instead of default ../cfg/event_<dbname>.props

  -d               : db alias, default to master db ($masterdb) unless -r
  -r               : if no -d option db defaults to master rt ($masterrt)

  example: $0 -c 15 -s 60 -d mydbname 12345
EOF
    exit $status;
}
