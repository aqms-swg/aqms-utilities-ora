# makegif 

Makes an gif file image of an event snapshot view panel using the Java SnapGif class generator.  
  
Requires *jiggle.jar*.  

Sub-directory *bin* has the executable scripts while *cfg* has the configuration files.   

*  ``bin/eventgif.sh`` : Create event snapshot gif for an event.  
*  ``bin/makeOneGif.pl`` : Makes an gif file image of an event snapshot view panel using the Java SnapGif class generator.  
*  ``bin/makeAllDrpGifs.sh`` : Makes the gif, scaled gif, and synopsis gif  for an event.  
*  ``bin/synopsgif.sh`` : Makes event snapshot gifs from list.
*  ``bin/triggergif.sh`` : Makes subnet trigger snapshot gif.  
  
<br>  
  
*  ``cfg/event_\<dbname\>.props`` : database connection information.  
*  ``cfg/eventgif_*.props`` : various property files for eventgif.sh.  
*  ``cfg/picksmodel.props`` : Example properties for DRP synopsis (declare in synopsis.props to override defaults). A reference, not read by any apps.  
*  ``cfg/synopsis-*.props`` : Various property files for synopsisgif.sh  
*  ``cfg/synopsisChannel.cfg`` : Channel list for synopsisgif.sh  
*  ``cfg/triggergif.cfg`` : property file for triggergif.sh  
*  ``cfg/waveserverList.props`` : property file for waveserver connection 
