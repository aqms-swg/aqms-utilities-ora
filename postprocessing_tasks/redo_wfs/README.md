# Redo triggered waveform archival {#redo_wfs}  
  
redo_wfs deletes existing waveforms, associations, and request cards from databases for a given event evid. If successful with deletions, then  runs a job to recreate waveform request_card rows which are then fulfilled by the waveform archiver.    


### Dependencies  

  *  **pcs** : Once deletion of existing waveforms is successful, redo_wfs posts the event to the appropriate state in the pcs system in order to create new request cards.    
    

The sub-directory *bin* contains the executable scripts and *cfg* contains the corresponding configuration files.  


### *bin*  
  
*  ``redoWfs.pl`` : The main program. Run ``./redoWfs.pl -h`` for usage.  
*  ``redoWfs.sh`` : A wrapper shell script that sets up the environment and runs ``redoWfs.pl``  
*  ``redoWfsCont.pl`` : Run by a cronjob script to continuously process all event ids posted to the configured state description.    
*  ``startRedoWfsCont.sh`` : A Wrapper to define aqms environment for continuous perl script job start/restart the continuous job that deletes all waveform associated data from the master database (both tables rows and files, also includes request_card) as well as any request_cards in the secondary stovepipe db.  


### *cfg*  

*  ``redoWfsCont.props `` : The config file used by ``redoWfs.pl``  
*  ``wfdirs.cfg`` : Location, on the local network, where there waveforms are stored.  


  

  
