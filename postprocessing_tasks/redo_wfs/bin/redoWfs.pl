#!/usr/bin/perl
#-------------------------------
# Deletes existing waveforms, associations, and request cards from databases for a given event evid
# if successful with deletions, then  runs a job to recreate waveform request_card rows.
#------------------------------------------

use strict;
use warnings;

use DBI; 

use lib "$ENV{TPP_BIN_HOME}/perlmodules";

use MasterDbs;

use Getopt::Std;
our ( $opt_c, $opt_d, $opt_h, $opt_p, $opt_r, $opt_w );
getopts('c:d:hp:rw');

if ( $opt_h ) { usage(0); }

print "==============================================================\n";

defined $ENV{TPP_HOME} or die "$0 Error: Required aqms.env environment vars are not defined\n$!";

# check for required args count
if ( @ARGV < 1 ) {
  print STDERR "$0 : Error, too few input arguments\n";
  usage(1);
}

# first arg is the event evid
my $evid = $ARGV[0];
if ( ! ($evid =~ m/[0-9]+$/) ) {
  print STDERR "$0 : Error, first arg, the event id must be integer\n";
  usage(1);
}

# connect to the master db configured for this host
my $dbase = $masterdb;
$dbase = $opt_d if $opt_d;

# user must specify the alternative stovepipe db alias as input arg
# (needed to delete request_cards from other stovepipe)
my $dbase2 = '';
if ( @ARGV > 1) {
  $dbase2 = $ARGV[1];
}


# define connection string using the Oracle driver
my $data_source = "dbi:Oracle:" . "$dbase";
print "Connecting to $dbase to delete waveform associated rows\n";
my $dbh = DBI->connect($data_source, $masterdbuser, $masterdbpwd, {AutoCommit=>0})
            or die "Error: connecting to Oracle database: $DBI::errstr\n";

my $sql = "select truetime.getstring(o.datetime) MY_DATE from event e, origin o where e.evid=? and o.orid=e.prefor";
my $sth = $dbh->prepare("$sql") or die "Error: preparing statement : " . $dbh->errstr;
$sth->execute($evid);
my ($date) = $sth->fetchrow_array();  
$date =~ s/\///g if $date;
$date = substr $date, 0, 6 if $date; 
$sth->finish;

print "deleting filename rows for evid: $evid";
$sql = "delete from filename where fileid in ( select fileid from waveform where wfid in ( select wfid from assocwae where evid = ?))";
$sth = $dbh->prepare("$sql") or die "Error: preparing statement : " . $dbh->errstr;

my $success = 1;

$success &&= $sth->execute($evid) or die "Error: preparing statement : " .  $sth->errstr;
print " deleted: " . $sth->rows . "\n";

$sth->finish;

print "deleting waveform rows for evid: $evid";
$sth = $dbh->prepare(q{ delete from waveform where wfid in ( select wfid from assocwae where evid = ?) }) 
         or die "Error: preparing statement : " . $dbh->errstr;

$success = 1;
$success &&=$sth->execute($evid) or die "Error: executing statement : " .  $sth->errstr;
print " deleted: " . $sth->rows . "\n";

$sth->finish;

print "deleting association rows for evid: $evid";
my $sth1 = $dbh->prepare(q{ delete from assocwae where evid = ?}) or die "Error: preparing statement : " .  $dbh->errstr;

$success &&=$sth1->execute($evid) or die "Error: executing statement : " .  $sth1->errstr;
print " deleted: " . $sth1->rows ."\n";
$sth1->finish;

print "deleting request_card rows in $dbase for evid: $evid";
my $sth2 = $dbh->prepare(q{ delete from request_card where evid = ?})
    or die "Error: preparing statement: " .  $dbh->errstr;

$success &&=$sth2->execute($evid) or die "Error: executing statement: " .  $sth2->errstr;
print " deleted: " . $sth2->rows ."\n";
$sth2->finish;

my $result = ($success ? $dbh->commit : $dbh->rollback);
unless ($result) { 
    die "Error: finishing transaction: " . $dbh->errstr 
}

my @wfdirs;
$sql = "select fileroot from waveroots where wavetype='T' order by wcopy,status desc";
$sth = $dbh->prepare("$sql") or die "Error: preparing statement : " . $dbh->errstr;
$sth->execute() or die "Error: executing statement : " .  $sth->errstr;
my $dbrowsref = $sth->fetchall_arrayref(); 
$sth->finish;
for my $rowref ( @{$dbrowsref} ) {
  push( @wfdirs, @{$rowref}[0] );
}

my $pdate = gmtime();
print "Disconnecting from $dbase at $pdate\n";
$dbh-> disconnect;

############################################################################
if ( $dbase2 ) {
  print "Connecting to $dbase2 to delete request cards\n";

  $data_source = "dbi:Oracle:" . "$dbase2";
  my $dbh2 = DBI->connect($data_source, $masterdbuser, $masterdbpwd, {AutoCommit=>0})
          or die "Error: connecting to Oracle database: $DBI::errstr\n";
  print "deleting request_card rows for evid: $evid";
  $sth2 = $dbh2->prepare(q{ delete from request_card where evid = ?}) or die "Error: preparing statement: " .  $dbh2->errstr;

  $success &&=$sth2->execute($evid) or die "Error: executing statement: " .  $sth2->errstr;
  print " deleted: " . $sth2->rows ."\n";
  $sth2->finish;

  $result = ($success ? $dbh2->commit : $dbh2->rollback);
            unless ($result) { die "Error: finishing transaction: " . $dbh2->errstr }

   $pdate = gmtime();
   print "Disconnecting from $dbase2 at $pdate\n";
   $dbh2-> disconnect;

}

#############################################################################
if ( $opt_c ) {
  # read list of directory locations for event waveform files
  my $filename = $opt_c;
  ( -e $filename ) or die "Error: Missing waveform directory cfg file: $filename\n";
  my $FH;
  open($FH, "<$filename");
  @wfdirs = ();
  while ( my $path = <$FH> ) {
    next if $path =~ /^#/;
    chomp $path;
    push(@wfdirs, $path);
  }
  close($FH);
}
#############################################################################
my $knt = 0;
$pdate = gmtime();
print "Starting waveform file delete at $pdate\n";
#foreach my $wf_dir (@wf_dirs) {
foreach my $wf_dir ( @wfdirs ) {
    last if ! $wf_dir;
    $wf_dir .= '/' unless ($wf_dir =~ m/\/$/);
    my $ev_dir = $wf_dir . $evid;
    my $dumpster = $wf_dir . 'dumpster/x' . $evid;
    if ( -d $ev_dir ) {
        if ( $opt_r ) {
          print "rm -r  $ev_dir\n";
          system("rm -r $ev_dir");
        }
        else {
          if ( -d $dumpster ) {
            print "Removing previous dumpster rm -r $dumpster\n";
            system("rm -r $dumpster");
          }
          print "mv $ev_dir $dumpster \n";
          system("mv $ev_dir $dumpster");
        }
        if ( ($? >> 8) == 0 ) { $knt++; }
    }
    else {
        if ( $date ) {
          $ev_dir = $wf_dir . substr($date, 0, 4) . '/' . $date;
          if ( -d $ev_dir ) {
            if ( $opt_r ) {
                print "rm -r  $ev_dir/$evid\n";
                system("rm -r $ev_dir/$evid");
             }
             else {
                if ( -d $dumpster ) {
                  print "Removing previous dumpster rm -r $dumpster\n";
                  system("rm -r $dumpster");
                }
                print "mv $ev_dir/$evid $dumpster \n";
                system("mv $ev_dir/$evid $dumpster");
             }
             if ( ($? >> 8) == 0 ) { $knt++; }
          }
        }
    }
}

$pdate = gmtime();
print "Done deleting $knt waveform directory files at $pdate\n";

#############################################################################

# below forks java job create new request_cards
# default is to first check for existing wfs
if ( $success  && ! $opt_w ) {

  my $postDb = $dbase;

  # is secondary db the primary db? If so, make it the posting db 
  if ( $dbase2 ) {
    my $primaryDb =`$ENV{TPP_BIN_HOME}/amPrimaryDb -d $dbase2`;
    chomp $primaryDb;
    $postDb = $dbase2 if $primaryDb eq "true";
  }

  # do a standalone rcg run
  if ( $opt_p ) {
    print "Posting $evid EventStream $dbase $opt_p 100\n";
    # send print output to stdout -o; wait for java job exit code
    $success = system("$ENV{TPP_BIN_HOME}/post.pl -d $postDb $evid EventStream $postDb $opt_p 100");
  }
  else {
    print "creating new requests for $evid...\n";
    print "$ENV{TPP_HOME}/rcg/bin/rcgone.pl -d $postDb -o -q $evid\n";
    # send print output to stdout -o; wait for java job exit code
    $success = system("$ENV{TPP_HOME}/rcg/bin/rcgone.pl -d $postDb -o -q $evid");
  }

}

print "======================================================================\n";
exit $success;

sub usage {
  my ($status) = @_;
  print <<EOF;

Deletes from database all existing waveform related database table rows, moves or removes
the waveform files, and deletes request_card table rows associated with the input event evid.
If successful, either forks a java job to create new waveform requests or with '-p' option
posts the input evid to a new state, and if pp_db2 is specified, it checks whether dbase
or pp_db2 is primary and posts or writes requests to primary database.

Syntax: $0 [-h] [-c file] [-d dbase] [-p state ] [-r] [-w] <evid> [pp_db2]

Options:

  -h         : this usage

  -c file    : read waveform file directories from file instead of the WAVEROOTS table. 

  -d dbase   : alias name for primary database connection, defaults to $masterdb.

  -p state   : If the waveforms and requests are successfully deleted (and no -w option),
               do not run the "tpp/rcg/bin/rcgone.pl" script to generate new waveform requests,
               instead do a posting:

                 post <evid> EventStream $masterdb <state> 100

               For example, state = "rcg_rt" would queue the evid for the continuously running
               request generator application polling the database, which defaults to $masterdb.

  -r         : remove event waveform file directories, default action is to rename the file
               directories, move them to <waveroot-dir>/dumpster/x<evid>
               If dumpster directory already exists for event, it is deleted.

  -w         : delete all waveform data, but DO NOT run "tpp/rcg/bin/rcgone.pl script to make 
               new waveform requests, NOR post the event evid to a state specified by -p option.

 Arguments:

  evid       : all waveform data associated with this event id will be deleted from the
               database, which defaults to the current master database = "$masterdb".

  pp_db2     : optional, deletes all request_card table rows associated with input event
               in the specified secondary database too.
EOF
  exit $status;
}
