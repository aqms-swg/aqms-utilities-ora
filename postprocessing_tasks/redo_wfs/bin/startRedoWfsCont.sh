#!/bin/bash

# Wrapper to define aqms environment for continuous perl script job
# start/restart the continuous job that deletes all waveform associated data
# from the master database (both tables rows and files, also includes request_card)
# as well as any request_cards in the secondary stovepipe db.
# 
# crontab -e 
#
#0,10,20,30,40,50 * * * * $ENV{TPP_HOME}/redo_wfs/bin/startRedoWfsCont.sh >/dev/null 2>&1


# For crontab job we need to source the environment variables, does oracle too
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

usage() {

  cat <<EOF

Usage:

  Start/restart the process that deletes all waveforms table data and related files for an event evid,
  including request_cards in both the master and secondary databases. The event evid must be posted
  to state description specified by properties, which defaults to TPP TPP redoWfs 

  Script can be run at intervals by a cron job to insure that the job is always present.

  After checking job runtime status, script optionally purges logs files found in logs directory
  that are older than maximum days age value that can be specified with command line
  option -c.
  
  Syntax: $0 [ -c maxDaysBack ] [ -h ]

  -c days     : max days back to keep logs (default = forever)
  -v          : verbose logging
  -h          : this help usage

EOF
    exit $1
}

appHome=${TPP_HOME}/redo_wfs

cd ${appHome}/bin

jobStr="redoWfsCont"

# one Cont_YYYYMMDD.log per UTC day
logdir="../logs"
logTimeStamp=$(date -u +"%Y%m%d")
startLog="${logdir}/start_${jobStr}_${logTimeStamp}.log"

maxDaysBack=0

while getopts ":c:hv" opt; do
  case $opt in
    h)
      usage 0 # non-error exit
      ;;
    c)
      maxDaysBack="$OPTARG"
      ;;
    v)
      verbose="-v"
      ;;
    \?)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Invalid command switch option: -$OPTARG" | tee -a ${startLog}
      usage 1 # error exit
      ;;
    :)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Command option -$OPTARG requires an argument." | tee -a ${startLog}
      usage 1 # error exit
      ;;
  esac
done

#Do not use shift to offset input $@ to any remaining args that follow this shell's  switch options
#shift $(($OPTIND - 1))

{
  date=`date -u +'%F %T %Z'`

  host=`uname -n`
  host=${host%%\.*}

  # Report status
  echo "============================================================"
  echo "Checking for $jobStr job on $host $date"

  # This starter script must check and only start if NO current instance is running.
  # (assumes environmental variables are defined)
  progName="./${jobStr}.pl $verbose -c $maxDaysBack"

  echo "------------------------------------------------------------"

  # Check process list for existing jobs 
  # pgrep -l option returns name of process after pid
  pids=( `pgrep -f "$jobStr"` )
  failed=0
  started=0
  if [ ${#pids[@]} -eq 0 ]; then 

    # check for job properties file
    propsFile="../cfg/${jobStr}.props"
    if [ ! -f "$propsFile" ]; then
      echo "$0 Error: Missing $propsFile" 
      exit 1
    fi

    # Fork job to background
    echo "Starting: $progName $propsFile &"
    $progName $propsFile &

    # Wait a bit for startup
    sleep 3

    # Check for job in process table
    pids=( `pgrep -f "$jobStr"` )
    if [ ${#pids[@]} -gt 0 ]; then 
        jobStr="Started $jobStr $date pid=(${pids[@]})";
        started=1
    else 
        jobStr="Failed to start $jobStr $propsFile $date";
        failed=1
    fi
  else 
     jobStr="$jobStr is running on $host $date as pid: ${pids[@]}"
  fi

  # Report results
  echo "$jobStr"
  if [ $started -gt 0 -o $failed -gt 0 ]; then
    mailaddr=$TPP_ADMIN_MAIL
    if [ -n "$mailaddr" ]; then
      subject="Restart of $jobStr on $host"
      if [ $failed -gt 0 ]; then
        subject="$subject, failed"
      fi 
      mailx -s "$subject" "$mailaddr" <<< "$jobStr"
    fi
  fi

  # do log cleanup
  echo "------------------------------------------------------------"
  if [[ $maxDaysBack =~ ^[0-9]+$ ]]; then 
    if [ $maxDaysBack -gt 0 ]; then 
      echo "Deleting files in $logdir older than $maxDaysBack days"
      echo "find $logdir -type f -mtime +$maxDaysBack -print -exec \\rm '{}' \\;"
      find "$logdir" -type f -mtime "+$maxDaysBack" -print -exec \rm '{}' \;
    fi
  else 
      echo "Invalid -c max days back to keep logs: $maxDaysBack"
  fi

  echo "============================================================"

} >> ${startLog} 2>&1

exit 0
