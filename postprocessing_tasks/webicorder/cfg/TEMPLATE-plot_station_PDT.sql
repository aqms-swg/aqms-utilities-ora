set lines 256
set pages 0
set verify off
set feedback off
select 'Plot ' || a.sta || ' ' || seedchan || ' ' || a.net || ' ' || decode(location, '  ', '--',location)
 || ' 12 12 -7 PDT 1 0 800 600 15 0.01 1.5 1 "' || s.staname || '"'
from active_channels_view a, station_data s where a.seedchan='BHZ' and a.net='CI'
and s.net=a.net and s.sta=a.sta order by a.sta;
exit
