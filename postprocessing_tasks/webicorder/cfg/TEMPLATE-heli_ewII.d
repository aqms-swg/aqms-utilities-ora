#
# This is the heli_ewII parameter file. This program gets data gulps
# from the waveserver(s), and creates helicorder displays.
# Derived from heli_ew and heli_standalone, which were derived from
# Jim Luetgert's original efforts.

# This program runs either standalone or as an Earthworm module.
# If any of the four Earthworm paramaters below are stated, it will asssume
# that it's supposed to be a module. It will beat it's heart into the ring,
# and use the Earthworm logging scheme. Other wise, it runs standalone.
# LogSwitch 1
# MyModuleId MOD_HELI_EWII
# RingName ASSOC_RING
# HeartBeatInt  10

wsTimeout 30 # time limit (secs) for any one interaction with a wave server.

# List of wave servers (ip port comment) to contact to retrieve trace data.
#WaveServer     server1 IP       server1 PORT     "server1 HOSTNAME"
#WaveServer     server2 IP       server2 PORT     "server2 HOSTNAME"
#...
#
#If your AQMS installation uses CWB for data acquisition, please use the section below
# CWB waveserverV
#WaveServer     CWB server1 IP     CWB server1 PORT     "CWB server1 HOSTNAME"
#WaveServer     CWB server2 IP     CWB server2 PORT     "CWB server2 HOSTNAME"

# Directory in which to store the temporary .gif, .link and .html files.
GifDir /app/aqms/tpp/webicorder/output

# Redirect to file containing list of channels to plot (ONLY does first 100)
##@/app/aqms/tpp/webicorder/cfg/plot_station.d
# does below do 200 with updated binary for max nplots?
@/app/aqms/tpp/webicorder/cfg/plot_station2.d

# *** Optional Commands ***

Days2Save     14    # Number of days to display on web page; default=7
# Actually, "Days2Save" is really the number of GIFs of each SCN to save.
# For example, if HoursPerPlot is 12, you will save 7 gifs for 3.5 days.

UpdateInt     5    # Number of minutes between updates; default=2
RetryCount    1    # Number of attempts to get a trace from server; default=2
# Logo    smusgs.gif # Name of logo in GifDir to be plotted on each image

# We accept a command "Clip" which sets trace clipping at this many
# vertical divisions
Clip 5

# We accept a command "SaveDrifts" which logs drifts to GIF directory.
# SaveDrifts

# We accept a command "Make_HTML" to construct and ship index HTML file.
Make_HTML

# We accept a command "IndexFile" to name the HTML file.
# Default is "index.html"
IndexFile index.html

# We accept a command "BuildOnRestart" to totally rebuild images on restart.
#BuildOnRestart

# We accept a command "Debug" which turns on a bunch of log messages
Debug
WSDebug
