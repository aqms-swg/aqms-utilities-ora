#!/bin/bash

#source $HOME/.bash_profile
if [ -z "$PP_HOME" -o -z "$LD_LIBRARY_PATH" ]; then source /usr/local/etc/aqms.env; fi

if [ -n "$AQMS_HOME" ]; then
  home="$AQMS_HOME"
else
  home="$PP_HOME"
fi

cd $home/tpp/webicorder/bin

timestamp=`date -u +'%F %T %Z'`

cleanupDays=7
cfgfile="../cfg/helictl.cfg" 
if [ -f "$cfgfile" ]; then source $cfgfile; fi

# declare functions
start() {
    pid=`pgrep -l -x heli_ewII`
    if [ -n "$pid" ]; then
      echo "heli_ewII binaries already running @ $timestamp"
      echo "`pgrep -l -f 'heli_ewII'`"
      exit
    fi

    ./startWebicorder.sh -c $cleanupDays &

    sleep 6
    status
}

stop() {
    for i in `pgrep -x heli_ewII`
    do
      echo "killing heli_ewII pid $i @ $timestamp"
      kill $i
      sleep 2
    done
    # in case SIGTERM did not work:
    for i in `pgrep -x heli_ewII`
    do
      kill -9 $i
    done
}

restart() {
    stop
    sleep 3
    start
}

status() {
    #pids=`pgrep -l -x 'heli_ewII'`
    pids=`ps -ef | egrep -e 'heli_ewII' | grep -v 'egrep -e' | sort -k 9`
    timestamp=`date -u +'%F %T %Z'`
    if [ -n "$pids" ]; then
       echo "heli_ewII processes running @ $timestamp"
       echo "$pids"
    else
       echo "No heli_ewII processes found @ $timestamp"
    fi
}

usage() {
    echo "Webicorder (heli_ewII) init script"
    echo "Usage $(basename $0) [start|stop|restart|status|help]"
    exit $1

}
# End of function declarations

# Execute input action via function call
case $1 in
  start)
    start ;;
  stop)
    stop ;;
  restart)
    restart ;;
  status)
    status ;;
  help)
    usage 0 ;;
  *)
    echo "Unknown or missing input argument: $1"
    usage 2 ;;
esac

