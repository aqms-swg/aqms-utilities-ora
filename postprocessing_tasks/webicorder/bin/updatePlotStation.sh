#!/bin/bash
if [ -z "$PP_HOME" -o -z "$LD_LIBRARY_PATH" ]; then source /usr/local/etc/aqms.env; fi

if [ -n "$AQMS_HOME" ]; then
  home="$AQMS_HOME"
else
  home="$PP_HOME"
fi

#
# The 'Local Time Diff' and 'Local Time Zone' parameters (columns 6 & 7,
# respectively) need to be adjusted twice a year, so we have two '.sql'
# scripts - one for PST and PDT.
#
TIMEZONE=`date +%Z`

cd $home/tpp/webicorder/bin
dbase=`$home/tpp/bin/masterdb`
sqlplus -R 3 -S SOME DB USER/SOME DB PASSWORD@$dbase @../cfg/plot_station_$TIMEZONE.sql >../cfg/plot_station2.d
