#!/bin/bash
#

# For crontab job we need to source the environment variables, does oracle too
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

usage() {

  cat <<EOF

Usage:

  Start/restart the process that creates the "helicorder" gif images for a
  list of BH channels.

  This is run at intervals by a cron job to insure that the jobs start at
  boot time and are always present.

  Syntax: $0 [ -c maxDaysBack ] [ -h ]

  -c days     : max days back to keep logs
  -h          : this help usage

EOF
    exit $1
}

appHome=${TPP_HOME}/webicorder
logdir=${appHome}/logs

# One startCron_YYYYMMDD.log per UTC day
logTimeStamp=$(date -u +"%Y%m%d")
startLog="${logdir}/startWebicorder_${logTimeStamp}.log"

maxDaysBack=''

while getopts ":c:h" opt; do
  case $opt in
    h)
      usage 0 # non-error exit
      ;;
    c)
      maxDaysBack="$OPTARG"
      ;;
    \?)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Invalid command switch option: -$OPTARG" | tee -a ${startLog}
      usage 1 # error exit
      ;;
    :)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Command option -$OPTARG requires an argument." | tee -a ${startLog}
      usage 1 # error exit
      ;;
  esac
done

#Here use shift to offset input $@ to any remaining args that follow this shell's  switch options
shift $(($OPTIND - 1))

# Block wrapper to redirect output to startLog
{

  jobStr=${appHome}/bin/heli_maint.pl $@

  # start the job
  echo START: ${jobStr} `date -u +'%F %T %Z'`

  ${jobStr}

  # Run log cleanup
  if [ -n "$maxDaysBack" ]; then 
    if [[ "$maxDaysBack" =~ ^[0-9]+$ && "$maxDaysBack" > "0"  ]]; then
      echo "Deleting files in $logdir older than $maxDaysBack days"
      find $logdir -mtime "+$maxDaysBack" -print -exec \rm '{}' \;
    else
      echo "Invalid max days back to keep logs: $maxDaysBack"
    fi
  fi 

  echo DONE : ${jobStr} `date -u +'%F %T %Z'`
  echo "=========================================================================="

} >>${startLog}  2>&1

