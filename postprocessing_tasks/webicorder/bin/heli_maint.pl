#!/usr/bin/perl
# heli_maint.pl - maintain webicorder files.  Run by cron about every 15 minutes:
#  1) Is heli_ewII running? If not restart with file $ENV{TPP_HOME}/webicorder/cfg/heli_ewII.d 
#  2) remove old gif files
#  3) sync files to web server[s]

use strict;
use warnings;

use Sys::Hostname;

select STDERR; $| = 1;
select STDOUT; $| = 1;

# Change these variables for local network configuration
my $rootdir = "$ENV{TPP_HOME}/webicorder";
my $mailaddr = $ENV{"TPP_ADMIN_MAIL"};

my $heliLog = "${rootdir}/logs/heli_ewII.log";

# get short host name, behavior of "hostname()" varies with OS
my $hostlong = hostname();
my @parts = split ('\.',$hostlong);  # insure we have the short hostname
my $host = $parts[0];
my $date = gmtime();

my $amEnabledForPCS=`$ENV{TPP_BIN_HOME}/enabledForPCS.pl heli_ewII none`;
chomp $amEnabledForPCS; # may have newline at end

# only start if running on primary (not shadow) host 
if ( ! defined $ENV{FLAGFILENAME} ) { 
  $ENV{FLAGFILENAME} = "/app/cluster/I_AM_PRIMARY";
}
my $pid;
if ( -f "$ENV{FLAGFILENAME}" || "$amEnabledForPCS" eq 'true' ) { 
    # check that GIF-making process is running
    # NOTE: this check is "weak": a process tailing the log or editing the
    #       script or .d file would cause the pgrep to return 'true'.
    $pid = `pgrep -f '[/]heli_ewII'`;
    chomp $pid;

    if ( "$pid" ) {
        print "Job heli_ewII is running on $host as pid=$pid \@ $date\n";
    } else {
        print "Job heli_ewII is not running, starting job with log ${heliLog} \@ $date\n";

       # rename previous log with current time stamp 
       if ( -e ${heliLog} ) {
         my $stamp = `date -u +"%Y%m%d%H%M"`;
         chomp $stamp;
         my $oldLog = ${heliLog};
         $oldLog =~ s/\.log/_${stamp}.log/;
         rename ( ${heliLog}, ${oldLog} );
       }

       # restart the process
       `${rootdir}/bin/heli_ewII ${rootdir}/cfg/heli_ewII.d >> ${heliLog} 2>&1 &`;

       # send mail after wait for process startup
       if ( "$mailaddr" ) {
          sleep(3);
          $pid = `pgrep -f '[/]heli_ewII'`;
          chomp $pid;
          if ( "$pid" ) {
              print "Job heli_ewII now running as pid = ${pid}\n";
              sendMail($mailaddr, "Restart webicorder on $host (pid=$pid)", ${heliLog}, 10);
          }
          else {
              print "Restart of heli_ewII job failed, check ${heliLog}\n";
              sendMail($mailaddr, "Restart webicorder on $host (pid=$pid)", ${heliLog}, 10);
          }
       }
    }
}
else {
    print "Not primary host, no-op, heli_ewII restart skipped \@ $date\n"; 
    $pid=`pgrep -f /heli_ewII`;
    if ( $pid ) {
      print "Killing running heli_ewII job pid: $pid\n";
      system("kill -TERM $pid");
    }

}

# Do maintenance, delete old files
print "--------------------------------------------------------------------------\n";
print "Deleting ../output/*.gif files older than DaysToSave (in ../cfg/heli_ewII.d)\n";
my $numRemoved = removeOldGifs("${rootdir}/output");
print "Removed $numRemoved .gif files\n";

# Move active files to the web server
# Comment this out if you have a permanent rsync running or this
# is running on the server and putting files in the right dirs.
# Replace this function logic if you want to do something like 'scp'.
# Don't rsync if host is not running heli_ewII, avoid multihost rsync 
if ( "$amEnabledForPCS" eq 'true' ) { 
  print "Synching .gif files with web servers (in ../cfg/syncHeliDest.cfg)\n";
  syncfiles("${rootdir}/bin");
}

exit;

# --------------------------------------------------------------------------
sub removeOldGifs {
# <argument> is directory where gif files are located.
#            Files NOT being displayed by webicorder are deleted
# method: heli_ewII keeps a ".hist" file for each SNCL.
#         Inside the .hist file are the dates that are displayed on 
#         the welcome.html page.  Combining the SNCL with the dates
#         yields the name of active gif images.
# NOTE: this does NOT clean up the files on the remote servers.
    my $dir = shift;

    opendir(GIFDIR, "$dir") or die("couldn't open $dir $!");
    my @allFiles = readdir GIFDIR;
    closedir GIFDIR;

    my @histories = grep /.*\.hist$/ && -f "$dir/$_", @allFiles;
    my @gifFiles = grep /.*\.gif$/, @allFiles;

    my %activeFiles;
    foreach my $history (@histories) {
        my ($sncl, $null) = split(/\./, $history);
        open(INFILE, "<$dir/$history") or die "couldn't open file\n$!";
        while (<INFILE>) {
            chomp($date = $_);
            my $gifFile = $sncl . "." . $date . ".gif";
            $activeFiles{$gifFile} = 1;
        }
        close(INFILE);
    }

    for (my $i = 0; $i <= $#gifFiles; $i++) {
        if ($activeFiles{$gifFiles[$i]} ) {
            $gifFiles[$i] = 0;  # if $gifFiles[$i] is in %activeFiles, remove 
                                # it from the list which will be unlinked
        } else {
             $gifFiles[$i] = "${dir}/$gifFiles[$i]";
        }
    }

    return unlink(@gifFiles);
}
# ------------------------------
# Synch gif & html files with the web servers
# Delegates this work to the script "syncHeli.sh"
#
sub syncfiles {
    my $dir = shift; 

    # Remove existing rsync output file, if any.
    unlink "${rootdir}/logs/syncHeli.log" if -f "${rootdir}/logs/syncHeli.log";

    # Open new file with redirect at the end the command 
    # Output file is read in the loop below.
    system( "${dir}/syncHeli.sh >${rootdir}/logs/syncHeli.log" ) == 0 
            or die("Could not open/run syncHeli.sh system return: $?");

    # count gif files sent
    open(SYNC, "<${rootdir}/logs/syncHeli.log");
    my $numGif = 0;
    while (my $line = <SYNC>) {
        #print "$line\n";           # debug
        if ($line =~ m/\.gif$/) {   # count ".gif" lines in output of command
            $numGif++;
        }
    }
    close(SYNC);

    print "Sent $numGif .gif files total (#servers x files)\n";
}
# ------------------------------
# Send mail about a problem, tails a file (log?) as body of email.
#  sendMail (mailaddr, subject, logfile, lines)
sub sendMail {
    my ($mailaddr, $subject, $log_File, $lines) = @_;
    if ( $mailaddr ) { print `tail -$lines $log_File | mailx -s "$subject"  "$mailaddr"`; }
}

