# Webicorder  
  
The “webicorder” is a web page with helicorder-like images of 12-24 hours of a seismic channel. Because this code is an Earthworm module it only works for channels that are available via a Winston or Earthworm Waveserver.   
  
The executable scripts are under *bin* and corresponding configuration files are under *cfg*.  
  
## Pre-requisites    

Required binaries must be compiled on compatible  OS (sparc,x86,linux)  
  *   ``heli_ewII``  
    


### *bin*  
  
*  ``heli_ewII`` : The main program that actually makes the waveform record plots.  
*  ``helictl.sh`` : ``heli_ewII`` job control options are available with helictl.sh script: [status|stop|start|restart]
*  ``startWebicorder.sh`` : Can be run as aqms crontab job task. Scripts checks to see if heli_maint.pl job is running, and if not, restarts it.  
*  ``heli_maint.pl`` : Script that forks the background job: ``heli_ewII.pl cfg/heli_ewII.d > webicorder/logs/heli_ewII.log 2>&1 &``    
*  ``syncHeli.sh`` : The Webicorder application runs on backend server. This script ensures webicorder plots are copied to public web servers for public view, as detailed in *webicorder/cfg/syncHeliDest.cfg*   
*  ``updatePlotStation.sh`` :  
  

### *cfg*  

*  ``heli_ewII.d`` : This is the heli_ewII parameter file used by ``heli_ewII``.   
*  ``plot_station.d`` : This configuration file contains plot parameters. It is designed such that each SCN creates it's own helicorder display; one per day of data.  
*  ``heli_ewII_doc.txt`` : A short document describing how to create ``plot_station.d``  
*  ``helictl.cfg`` : Config file for ``helictl.sh``  
*  ``syncHeliDest.cfg`` : Read by ``syncHeli.sh``. lists target destinations to rsync output files for web server directory.  
*  ``plot_station_PDT.sql`` : SQL statement to query the database and populate ``plot_station.d`` for pacific daylight savings time.  
*  ``plot_station_PDT.sql`` : SQL statement to query the database and populate ``plot_station.d`` for pacific standard time.  
*  ``indexa.html``, ``indexb.html`` : These are optional files. The ``indexa.html`` is just pasted, verbatim, on the top of html and so must have all the necessary header syntax. The ``indexb.html`` must have the correct closing syntax. They are reread each time the code runs so edits to them will take effect without restarting the program.     



