#!/bin/bash
#
# Instead use javarun_jar.sh with -H command line option switch
#
# Runs application class code in the  jiggle.jar file
# user must specify application class and args on commandline

if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

jiggleHome=${TPP_HOME}/jiggle/bin
cp=${jiggleHome}/jiggle.jar:${jiggleHome}/ojdbc8.jar:${jiggleHome}/acme.jar

if [ -d "${JAVA_BINDIR}" ]; then
  ${JAVA_BINDIR}/java -showversion -Djava.awt.headless=true -Xms128m -Xmx512m -cp ${cp} "$@"
else
  java -showversion -Djava.awt.headless=true -Xms128m -Xmx512m -cp ${cp} "$@"
fi

# pass along the java exit status
exit $?
