#!/bin/bash
#[User] [Passwd] [dbHost ip] [dbName] [date (yyyy-MM-dd)][cacheFile (channelList.cache)]
cache=$1
if [ -z "$cache" ]; then
  read -p 'Name of cache file: ' cache
fi
$TPP_HOME/jiggle/bin/javarun_jar.sh -v -B org.trinet.apps.ChannelListCacheDumper $cache
