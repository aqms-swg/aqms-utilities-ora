#!/bin/bash
if [ "$1" == '-h' ]; then
  echo "args: [dbuser] [dbpasswd] [dbHost.domain] [dbname] [active date (yyyy-MM-dd)] [cacheFile (default channelList.cache)]"
  exit
fi
if [ $# -eq 0 ]; then
  read -p 'Db user: ' dbuser
  read -p 'Db passwd: ' dbpasswd
  read -p 'Db host.domain: ' dbhost
  read -p 'Db name: ' dbname
  read -p 'Active date (yyyy-MM-dd): ' dbdate
  read -p 'Cache filename: ' cache
fi
$TPP_HOME/jiggle/bin/javarun_jar.sh -v -B org.trinet.apps.ChannelListCacheWriter $dbuser $dbpasswd $dbhost $dbname $dbdate $cache
