#!/bin/bash
#
# Instead use javarun_jar.sh with -B command line option switch
#
# Forks background job for application class code in jiggle.jar file
# user must specify application class in args on commandline

if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

jiggleHome=${TPP_HOME}/jiggle/bin
cp=${jiggleHome}/jiggle.jar:${jiggleHome}/ojdbc8.jar:${jiggleHome}/acme.jar

if [ -d "${JAVA_BINDIR}" ]; then
  ${JAVA_BINDIR}/java -showversion -Xms128 -Xmx512m -cp ${cp} "$@" &
else
  java -showversion -Xms128m -Xmx512m -cp ${cp} "$@" &
fi
