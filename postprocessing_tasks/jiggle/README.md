# Post-processing utilities related to Jiggle  
  

These utilities are a set of shell scripts that allow users to use Jiggle on the command line. The scripts and jar files including jiggle.jar are present under sub-directory *bin*. The config files consisting of database connection information are present under *cfg*.  

### Dependencies  
  
**Please install dependencies BEFORE using any of the scripts**  
  
The following binaries must be compiled to be compatible with Java runtime  
  
  *  bin/acme.jar  
  *  bin/ojdbcNN.jar : Get from your oracle installation  
  *  bin/jiggle.jar : Compiled from https://gitlab.com/aqms-swg/aqms-jiggle
    

### *bin*  
  
*  ``javarun_jar.sh`` : Runs java application class code found in the jiggle.jar file.  
*  ``javarun_fork_jar.sh`` : Forks background job for application class code in jiggle.jar file. It is recommended to use ``javarun_jar.sh`` with *-B* command line option switch instead.  
*  ``javarun_headless_jar.sh`` : Runs application class code in the  jiggle.jar file in headless mode. It is recommended to use ``javarun_jar.sh`` with *-H* command line option switch instead.  
*  ``cacheWrite.sh`` :  Creates a channel cache file for use by Jiggle and other applications. Channel data are obtained from a database account that is specified by command line parameters.   
*  ``cacheDump.sh`` : Dumps the contents of the channel cache file used by Jiggle and other applications to a file named after the cache file name with the suffix “Dump” appended to its file extension (e.g. scedcChannelList.cacheDump). The channel data are written in channel name sorted order. Files are relative to the local directory from which the “java” command is invoked. 


### *cfg*  

*  ``dbd-host.props`` : Contains database connection description parameters. If multiple databases need to be accessed, a separate copy of this file must be created for each database. For e.g. dbd-host-mydb1.props for a database named mydb1.  
*  ``dbd-dbuser.props`` : Contains database database username and password. If mutltipel users need to access a database, a separate copy of this file must be created for each such user. For e.g. dbd-user1.props for a user named user1.  

