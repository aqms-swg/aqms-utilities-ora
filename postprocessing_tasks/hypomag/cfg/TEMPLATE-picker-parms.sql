set pages 0
set lines 256
set trimspool on
set termout off
set feedback off
spool new_parms.txt
prompt #                          MinBigZC     RawDataFilt     LtaFilt
prompt #             Pflg     MinSmallZC  MaxMint         StaFilt         RmavFilt
prompt #SNCL               Itr1       MinPeakSize    CharFuncFilt    EventThresh
prompt #==========================================================================
select lpad(a.net||'.'||a.sta||'.'||a.seedchan||'.'|| decode(a.location,'  ','--',a.location), 16) || ' ' ||
decode(abs(c.dip),90,1,2) || '  3  10  3   20  500 0.85 3.00 0.40 0.015' ||
 to_char(decode(abs(c.dip),90,5.0,4.5),'9.9') || ' 0.9961'
from appchannels a, channel_data c where a.progid=25 and c.offdate>sysdate and
substr(a.seedchan, 1,2) in ('HH','HN','HL','HG','EH','EL','SH') and c.offdate>sysdate
and c.sta=a.sta and c.seedchan=a.seedchan and c.location=a.location and c.net=a.net
order by a.sta,a.seedchan,a.location,a.net
/
exit
