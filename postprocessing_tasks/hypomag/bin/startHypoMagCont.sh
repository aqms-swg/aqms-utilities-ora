#!/bin/bash
# 
# Wrapper to define aqms environment for continuous perl script job
# start/restart the continuous hypomag jobs
# crontab -e 
#
#0,10,20,30,40,50 * * * * $ENV{TPP_HOME}/hypomag/bin/startHypoMagCont.sh >/dev/null 2>&1
#

# For crontab job we need to source the environment variables, does oracle too
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

usage() {

  cat <<EOF

Usage:

  Start/restart processes that run the pcs controlled java application HypoMag
  (it can configured to relocate and/or recalculate event location and/or ML/MD magnitudes)
  This is run at intervals by a cron job to insure that the jobs start at
  boot time and are always present.

  After checking job status script optionally purges logs files found in logs directory
  that are older than maximum days age value that can be specified with command line
  option -c.
  
  Syntax: $0 [ -c maxDaysBack ] [ -h ]

  -c days     : max days back to keep logs (default = forever)
  -h          : this help usage

EOF
    exit $1
}

appHome=${TPP_HOME}/hypomag

# One startHypoMagCont_YYYYMMDD.log per UTC day
logdir=${appHome}/logs
logTimeStamp=$(date -u +"%Y%m%d")
startLog="${logdir}/start/startHypoMagCont_${logTimeStamp}.log"

cd ${appHome}

maxDaysBack=''

while getopts ":c:h" opt; do
  case $opt in
    h)
      usage 0 # non-error exit
      ;;
    c)
      maxDaysBack="$OPTARG"
      ;;
    \?)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Invalid command switch option: -$OPTARG" | tee -a ${startLog}
      usage 1 # error exit
      ;;
    :)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Command option -$OPTARG requires an argument." | tee -a ${startLog}
      usage 1 # error exit
      ;;
  esac
done

#Don't use shift to offset input $@ to any remaining args that follow this shell's  switch options
#shift $(($OPTIND - 1))

# Redirect print output from script to start log
${appHome}/bin/startHypoMagJobs.pl $@ >>${startLog} 2>&1 &
