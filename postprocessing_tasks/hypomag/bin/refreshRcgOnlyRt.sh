#!/bin/bash 
#
# CI network custom script not generic applicable to other AQMSnetworks
#
# For a crontab job we need to source the environment variables, does oracle too
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

appHome=${TPP_HOME}/hypomag
bindir=${appHome}/bin
cfgdir=${appHome}/cfg
logdir=${appHome}/logs

# Create one refreshPickerParms log per UTC day
logTimeStamp=$(date -u +"%Y%m%d")
startLog="${logdir}/refreshRcgOnlyRt${logTimeStamp}.log"

usage() {

  cat <<EOF

Usage:


  Syntax: $0 [ -c maxDaysBack ] [ -h ]

  Replace the RCG-ONLY-RT list of channels in the appchannels table
  This script is usually run at regular intervals by a cron job.

  -c days     : max days back to keep logs, default forever 
  -h          : this help usage

EOF
    exit $1
}

# default value keeps all log files
maxDaysBack=''
name=''

# Script gets destination URLs from file found in cfg directory
while getopts ":c:hn:" opt; do
  case $opt in
    h)
      usage 0 # non-error exit
      ;;
    c)
      maxDaysBack="$OPTARG"
      ;;
    \?)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Invalid command switch option: -$OPTARG" | tee -a ${startLog}
      usage 1 # error exit
      ;;
    :)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Command option -$OPTARG requires an argument."  | tee -a ${startLog}
      usage 1 # error exit
      ;;
  esac
done

#Here use shift to offset input $@ to any remaining args that follow this shell's  switch options
shift $(($OPTIND - 1))

host=`uname -n`
# Block wrapper to redirect message output to log
{

  echo "=========================================================================="

  echo "START: $0 at `date -u +'%F %T %Z'`"

  d=`grep 'masterdb ' /app/aqms/tpp/dbinfo/cfg/masterinfo.cfg | cut -d' ' -f 2`
  u=`grep masterdbuser /app/aqms/tpp/dbinfo/cfg/masterinfo.cfg | cut -d' ' -f 2`
  p=`grep masterdbpwd /app/aqms/tpp/dbinfo/cfg/masterinfo.cfg | cut -d' ' -f 2`
  u=`hexify -r $u`
  p=`hexify -r $p`
  sqlplus -S $u/$p@$d @${bindir}/refresh_rcg_only_rt.sql
  stat=$?
  if (( $stat ))
  then
      str= "$0 ERROR status $stat returned from sqlplus"
      echo $str
      echo "$str" | mailx -s "$0 failure on $host" $TPP_ADMIN_MAIL 
      exit 1
  fi

  # Run log cleanup
  if [ -n "$maxDaysBack" ]; then 
    echo "--------------------------------------------------------------------------"
    if [[ "$maxDaysBack" =~ ^[0-9]+$ ]]; then
      if [[ "$maxDaysBack" > "0" ]]; then
        echo "Deleting refreshRcgOnlyRt log files in $logdir older than $maxDaysBack days" >> ${startLog} 2>&1
        find $logdir -type f -name 'refreshRcgOnlyRt*.logs' -mtime "+$maxDaysBack" -print -exec \rm '{}' \;
      fi
    else
      echo "Invalid max days back to keep logs: $maxDaysBack" >> ${startLog} 2>&1
    fi
    echo "--------------------------------------------------------------------------"
  fi 

  echo "DONE: $0 at `date -u +'%F %T %Z'`"

} >>${startLog} 2>&1
