#!/bin/bash
#
# EDIT THIS SCRIPT TO USE DESIRED ARGUMENTS and "propsHomeDir" environment variable below
#
# For already posted ids run in background using hypomagML.props 
#
# The java app command args are: [userAppName] [propFile] [dbuser, can be empty string] [dbpwd, can be empty string] [modeFlag] [postOnly] 
# the last 2 input args are runtime input options where:
#
# modeFlag = TRUE (ids are already posted in table)
#          = FALSE (post ids table by EventProperties filter query)
#          = idFileName in local dir (ascii file 1 id per line)
#          = id (number) one-shot event
#
# postOnly = TRUE (only post, don't process ids)
#
# defaults:  hypomag hypomag.props "" "" true false
#
# #####################################################################
 
# NOTE: below variable value is used by the java code to find the dir
#       where props files are found. The variable is: <prog-name>_HOME_DIR
#       which should match the appname property in the cfg/*.props file
#       and the databse APP_HOST_ROLE table, if there's one
propsHomeDir="HYPOMAG_USER_HOMEDIR=${TPP_HOME}/hypomag/cfg"
 
# Script below does background fork with -B option, and -T option is for setting java property for a pgrep tag id
# The -- separates the shell options from the java command line options before the classname
${TPP_HOME}/jiggle/bin/javarun_jar.sh -v -B -Thypomag -- -D$propsHomeDir org.trinet.pcs.PcsProcessingController hypomag hypomagML.props "" "" true false
