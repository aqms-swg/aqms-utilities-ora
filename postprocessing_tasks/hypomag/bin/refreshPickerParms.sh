#!/bin/bash 
#
# For a crontab job we need to source the environment variables, does oracle too
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

appHome=${TPP_HOME}/hypomag
bindir=${appHome}/bin
cfgdir=${appHome}/cfg
logdir=${appHome}/logs

# Create one refreshPickerParms log per UTC day
logTimeStamp=$(date -u +"%Y%m%d")
startLog="${logdir}/refreshPickerParms_${logTimeStamp}.log"

usage() {

  cat <<EOF

Usage:


  Syntax: $0 [ -c maxDaysBack ] [ -h ]

  Generate Jiggle picker parameters for active channels in applications table list.
  This script is usually run at regular intervals by a cron job.

  -c days     : max days back to keep logs, default forever 
  -h          : this help usage
  -n name     : applications list name

EOF
    exit $1
}

# default value keeps all log files
maxDaysBack=''
name=''

# Script gets destination URLs from file found in cfg directory
while getopts ":c:hn:" opt; do
  case $opt in
    h)
      usage 0 # non-error exit
      ;;
    c)
      maxDaysBack="$OPTARG"
      ;;
    n)
      name="-n $OPTARG"
      ;;
    \?)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Invalid command switch option: -$OPTARG" | tee -a ${startLog}
      usage 1 # error exit
      ;;
    :)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Command option -$OPTARG requires an argument."  | tee -a ${startLog}
      usage 1 # error exit
      ;;
  esac
done

#Here use shift to offset input $@ to any remaining args that follow this shell's  switch options
shift $(($OPTIND - 1))

host=`uname -n`
# Block wrapper to redirect message output to log
{

  echo "=========================================================================="

  echo "START: $0 at `date -u +'%F %T %Z'`"

  # Make picker parameter list
  filename=pickEW.parms
  if [ -f  "$filename" ]; then
    dtime=$(date -u +"%Y%m%d%H%M")
    mv $filename ${logdir}/pickEW.parms_${dtime}
  fi

  ${bindir}/makePickerParms.pl $name > $cfgdir/$filename
  stat=$?
  if (( $stat ))
  then
      echo "$makePickerParms.pl $name returned error status $stat creating $cfgdir/$filename" | mailx -s "$0 failure on $host" $TPP_ADMIN_MAIL 
      exit 1
  fi

  if [ ! -s  $cfgdir/$filename ]; then
      echo "$makePickerParms.pl $name file $cfgdir/$filename has 0 bytes, is the db data available?, check logs" | mailx -s "$0 failure on $host" $TPP_ADMIN_MAIL 
      exit 1
  fi

  #scp picker parameter list to local web servers
  while read target
  do
    if [[ "${target}" =~ ^# ]]; then continue; fi;
    if [[ "${target}" =~ ^\ *$ ]]; then continue; fi

    echo "  scp -p $cfgdir/$filename $target"
    scp -p $cfgdir/$filename ${target}

    if (( $? ))
    then
      str="$0 scp to $target failed on $host"
      echo "Check user ssh keys for $host on $target" | mailx -s "$str" $TPP_ADMIN_MAIL 
    fi

  done < ${cfgdir}/pickerParmsLocalTargetURL.cfg;

  # Run log cleanup
  if [ -n "$maxDaysBack" ]; then 
    echo "--------------------------------------------------------------------------"
    if [[ "$maxDaysBack" =~ ^[0-9]+$ ]]; then
      if [[ "$maxDaysBack" > "0" ]]; then
        echo "Deleting refreshPickerParms log files in $logdir older than $maxDaysBack days" >> ${startLog} 2>&1
        find $logdir -type f -name 'refreshPickerParms*.log' -mtime "+$maxDaysBack" -print -exec \rm '{}' \;
      fi
    else
      echo "Invalid max days back to keep logs: $maxDaysBack" >> ${startLog} 2>&1
    fi
    echo "--------------------------------------------------------------------------"
  fi 

  #echo "  Killing all hypomag jobs running on $host ..."
  #${bindir}/stopHypoMagJobs.pl

  echo "DONE: $0 at `date -u +'%F %T %Z'`"

} >>${startLog} 2>&1
