--
-- hypomag needs to either synch the candidate list for each event or use refreshed channel cache list to get changes 
--
col progid new_value my_pid
set verify off
--
select progid from applications where name='RCG-ONLY-RT';
--
delete from appchannels where progid=&my_pid;
--
insert into appchannels (progid,net,sta,seedchan,location,ondate,offdate) 
select &my_pid,t.*,trunc(sysdate-1,'DDD'),to_date('3000/01/01')
from (
SELECT acv.net, acv.sta, acv.seedchan, acv.location
    FROM  active_channels_view acv, channel_data, appchannels
    WHERE
      acv.net=channel_data.net AND acv.sta=channel_data.sta
      AND substr(acv.seedchan,1,2) != 'BH'
      AND acv.seedchan=channel_data.seedchan
      AND acv.location=channel_data.location
      AND acv.net=appchannels.net
      AND acv.sta=appchannels.sta
      AND acv.seedchan=appchannels.seedchan
      AND acv.location=appchannels.location
      AND appchannels.progid IN (SELECT progid FROM applications WHERE name = 'FLAG_T')
      AND appchannels.offdate > CURRENT_DATE
      AND channel_data.ondate <= CURRENT_DATE
      AND channel_data.offdate > CURRENT_DATE
minus
(SELECT acv.net, acv.sta, acv.seedchan, acv.location
FROM   active_channels_view acv,  appchannels
WHERE
acv.net=appchannels.net
      AND acv.sta=appchannels.sta
      AND acv.seedchan=appchannels.seedchan
      AND acv.location=appchannels.location
      AND appchannels.progid IN (SELECT progid FROM applications WHERE name = 'NO-PICK')
      AND appchannels.offdate > CURRENT_DATE
)
union
(
SELECT net,sta, seedchan, location
FROM   appchannels
WHERE
      appchannels.progid IN (SELECT progid FROM applications WHERE name = 'ONLY-PP-PICK')
      AND appchannels.offdate > CURRENT_DATE
)
) t;
--
commit;
--
exit;
