#!/usr/bin/perl
#
# Start up all hypomag jobs
#
# ***** IMPORTANT *****
# Bash wrapper run in crontab defines TPP_HOME user environment and
# logfile redirect inherited by this script
#
#0,10,20,30,40,50 * * * * $ENV{TPP_HOME}/hypomag/bin/startHypoMagJobs.sh >/dev/null 2>&1

use strict;
use warnings;

use Getopt::Std;

defined ($ENV{TPP_HOME}) or die "TPP_HOME undefined, check aqms.env\n";

our ( $opt_c, $opt_h );
getopts('c:h');

# ? perl gmtime() function instead of shell ?
my $date = `date -u +'%F %T %Z'`;
chomp $date;

my $host=`uname -n`;
chomp $host;
$host =~ s/\..*\z//;

# Report status
print "============================================================\n";
print "Checking HypoMag jobs on $host $date\n";

my $appHome = "$ENV{TPP_HOME}/hypomag";

# Jobs are distinguished by their use of different named properties files
my @joblist;
# Get job propfile names from cfg file, else default
if ( -e "$appHome/cfg/jobsHypoMag.cfg" ) { # override defaults
  open (my $jobfile, "<", "$appHome/cfg/jobsHypoMag.cfg");
  foreach my $line (<$jobfile>) {
    $line =~ s/#.*//;
    next if $line eq '';
    chomp($line);
    push(@joblist, split(' ',$line));
  }
  close $jobfile;
}
else { # default
  @joblist = ("hypomagML.props");
}

# This starter script must check and only start if NO current instance is running.
my $progName = "$appHome/bin/runHypoMag4Posted.sh";

# Note: we ps -ef | grep the string "-D<prop_file_name>" from joblist to determine if there's a current instance
my $jobStr = "";

my @pids;
my @startedList=();
my @failedList=();

foreach my $job (@joblist) {

  print "------------------------------------------------------------\n";

  # Check process list for existing jobs 
  @pids = `pgrep -f -- "-[D]${job}"`; # -l option returns name of process after pid
 
  #print "$job pids = @pids \n"; # debug

  if ( @pids == 0 ) {

    # Fork job to background
    system("$progName $job &");

    # Wait a bit for startup
    sleep 2;

    # Check for job in process table
    @pids = `pgrep -f -- "-[D]${job}"`;
    if ( @pids > 0 ) {
        chomp(@pids);
        $jobStr = "Started $progName $job $date pid=($pids[0])";
        push (@startedList, $jobStr);
    }
    else {
        $jobStr = "Failed to start $progName $job $date";
        push (@failedList, $jobStr);
    }
  }
  else {
     chomp(@pids);
     $jobStr = "HypoMag $job is running on $host $date as pid: @pids";
  }

  # Report results
  print "$jobStr\n";
}
if ( @startedList > 0 || @failedList > 0 ) {
    my $mailaddr = $ENV{"TPP_ADMIN_MAIL"};
    if ( $mailaddr ) {
        my $startCnt = scalar @startedList; 
        my $failCnt = scalar @failedList;
        my $subject = "Restart of HypoMag jobs on $host: $startCnt running";
        if ( $failCnt > 0 ) {
           $subject .= ", $failCnt failed";
        }
        open (MAIL, "|-","mailx -s \"$subject $host\" $mailaddr");
        foreach (@failedList) { print MAIL "$_\n"; }
        foreach (@startedList) { print MAIL "$_\n"; }
        close MAIL;
    }
}

# do log cleanup
if ( $opt_c ) {
  my $maxDaysBack = $opt_c;
  print "------------------------------------------------------------\n";
  if ( $maxDaysBack =~ m/^[0-9]+$/ && $maxDaysBack > 0 ) {
      my $logDir = "$ENV{TPP_HOME}/hypomag/logs";
      print "Deleting files in $logDir older than $maxDaysBack days";
      print `find "$logDir" -type f -mtime "+$maxDaysBack" -print -exec \\rm '{}' \\;`;
      print "\n";
  }
  else {
      print "Invalid -c max days back to keep logs: $maxDaysBack\n";
  }
}

print "============================================================\n";

exit;

sub usage() {

  print <<"EOF";

  Start/restart processes that runs the java app HypoMag
  (it can configured to relocate and/or recalculate event location and/or ML/MD magnitudes)
  This is run at intervals by a cron job to insure that the jobs start at
  boot time and are always present.

  After checking job status script optionally purges logs files found in logs directory
  that are older than maximum days age value that can be specified with command line
  option -c.

  After checking job status script optionally purges logs files found in logs directory
  that are older than maximum days age value that can be specified with command line
  option -c.
  
  Syntax: $0 [ -c maxDaysBack ] [ -h ]

  -c days     : max days back to keep logs (default = forever)
  -h          : this help usage

EOF
  exit;
}
