#!/bin/bash
#
# EDIT THIS SCRIPT TO USE DESIRED ARGUMENTS and "propsHomeDir" environment variable below
#
# Start a background process that processes <pcsGroupName> <dbase> <pcsStateName> : e.g.  EventStream <dbase> hypomagML
# NOTE: posts event evids using configured eventSelectionProps if defined and named file exists.
#
# The java app command args are: [userAppName] [propFile] [dbuser, can be empty string] [dbpwd, can be empty string] [modeFlag] [postOnly] 
# the last 2 input args are runtime input options where:
#
# modeFlag = TRUE (ids are already posted in table)
#          = FALSE (post ids table by EventProperties filter query)
#          = idFileName in local dir (ascii file 1 id per line)
#          = id (number) one-shot event
#
# postOnly = TRUE (only post, don't process ids)
#
# defaults:  hypomag hypomag.props "" "" true false
#
# #####################################################################

if [ $# -eq 0 ]; then
  echo "Usage: ${0} <name of prop_file found in ../cfg directory>"
  exit 1
fi

# Setup environment, if undefined, does oracle too
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

# Process identifier string - need some unique string visible to 'ps', so we use the props file name.
propFile=${1}

# NOTE: below variable value is used by the java code to find the dir
#       where props files are found. The variable is: <prog-name>_HOME_DIR
#       which should match the appname property in the cfg/*.props file
#       and the databse APP_HOST_ROLE table, if there's one
propsHomeDir="HYPOMAG_USER_HOMEDIR=${TPP_HOME}/hypomag/cfg"

# Script below does background fork with -B option, and -T option is for setting java property for a pgrep tag id
# The -- separates the shell options from the java command line options before the classname
echo "${TPP_HOME}/jiggle/bin/javarun_jar.sh -v -B -T${propFile} -- -D$propsHomeDir org.trinet.pcs.PcsProcessingController hypomag ${propFile} \"\" \"\" false false"
${TPP_HOME}/jiggle/bin/javarun_jar.sh -v -B -T${propFile} -- -D$propsHomeDir org.trinet.pcs.PcsProcessingController hypomag ${propFile} "" "" false false
