#!/usr/bin/perl

#------------------------------------------------------------------
# Generates the parameter file for Jiggle jar hypomag style picker
#------------------------------------------------------------------
      
use strict;
use warnings;

use DBI;

# get masterdb and username/password
use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use DbConn;
use MasterDbs;

# parse optional switches (N.B. this 'shift's the @ARGV array) 
use Getopt::Std;
our ( $opt_d, $opt_h, $opt_n, $opt_x );
getopts('d:hn:x');

# test for help option or command args
if ( $opt_h ) { usage(0); }

my $db = $masterdb;
if ($opt_d) { $db = $opt_d };  

my $name = 'RCG-TRINET';
my $name2 = 'ONLY-PP-PICK';
$name = $opt_n if $opt_n; 

my $sql =
"select lpad(c.net||'.'||c.sta||'.'||c.seedchan||'.'|| decode(c.location,'  ','--',c.location), 16) || ' ' ||
case when abs(c.dip) > 80 then 1 else 2 end || '  3  10  3   20  500 0.85 3.00 0.40 0.015' ||
case when ( abs(c.dip) > 80 ) then to_char(5.0,'9.9') else to_char(5.0,'9.9') end || ' 0.9961'
from channel_data c, appchannels a, applications p where p.name=? and a.progid=p.progid and
a.offdate>sys_extract_utc(systimestamp) and c.offdate>sys_extract_utc(systimestamp) and 
substr(c.seedchan, 1,2) in ('HH','HN','HL','HG','EH','EL','SH') and
c.sta=a.sta and c.seedchan=a.seedchan and c.location=a.location and c.net=a.net
order by c.sta,c.location,c.seedchan,c.net";

my $sql2 = 
"select lpad(c.net||'.'||c.sta||'.'||c.seedchan||'.'|| decode(c.location,'  ','--',c.location), 16) || ' ' ||
case when abs(c.dip) > 80 then 1 else 2 end || '  3  10  3   20  500 0.85 3.00 0.40 0.015' ||
case when ( abs(c.dip) > 80 ) then to_char(5.0,'9.9') else to_char(5.0,'9.9')
end || ' 0.9961' from channel_data c, appchannels a, applications p where p.name=?
and a.progid=p.progid and a.offdate>sys_extract_utc(systimestamp) and c.offdate>'2019/07/20' and
substr(c.seedchan, 1,2) in ('HH','HN','HL','HG','EH','EL','SH') and
c.sta=a.sta and c.seedchan=a.seedchan and c.location=a.location
and c.net=a.net
order by c.sta,c.location,c.seedchan,c.net";

my $dbconn = DbConn->new($db)->getConn();

my $stm = $dbconn->prepare($sql) or die "Prepared statement failed: $DBI::errstr\n";

if ( $opt_x ) {
  print "$sql\n";
  exit;
}
# execute the prepared statment.
$stm->execute($name); 

my @data;
my @data2;

# print the header
my $date = `date -u +'%F %T %Z'`;
chomp $date;
print "#Updated picker parms for $name list from $db $date\n";
print "#                          MinBigZC     RawDataFilt     LtaFilt\n";
print "#             Pflg     MinSmallZC  MaxMint         StaFilt         RmavFilt\n";
print "#SNCL               Itr1       MinPeakSize    CharFuncFilt    EventThresh\n";
print "#==========================================================================\n";
# Read the column results into separate variables          
while (@data = $stm->fetchrow_array()) {
    my $str  = $data[0] || '#';
    printf "%s\n", $str; 
}

$stm->finish;

if ( $opt_x ) {
  print "$sql2\n";
  exit;
}
# execute the prepared statment.
my $stm2 = $dbconn->prepare($sql2) or die "Prepared statement failed: $DBI::errstr\n";
$stm2->execute($name2);

while (@data2 = $stm2->fetchrow_array()) {
    my $str  = $data2[0] || '#';
    printf "%s\n", $str;
}
  
$stm2->finish;
# disconnect from the database
$dbconn->disconnect or warn "Disconnection failed: $DBI::errstr\n";

exit;

# -----------------------------------------------------------------
sub usage {

    my $status = shift @_;

    print <<"EOF";

    usage: $0 [-d dbase] 

    Generates the Jiggle picker parameters for active channels in appchannels table
    for named application (default = 'RCG-TRINET').
     
     -h         : print usage info
     -d dbname  : name alias for the database connection 
     -n appname : applications table name for appchannels list
     -x         : debug print sql query text and exit
    
    example: $0 -d k2db 
EOF

    exit $status;
}
