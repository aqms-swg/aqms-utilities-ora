#!/usr/bin/perl
#
# Calculate ground motions for one event. Does: PGD, PGV, PGA and spectral amps
# By default, AmpGenPp java class uses property file found in ../cfg subdirectory
     
use strict;
use warnings;

use File::Basename;

if ( ! defined $ENV{TPP_HOME} ) {
  print "TPP environment variables not setup correctly, check file /usr/local/etc/aqms.env\n";
  exit 1
}

my $appHome = "$ENV{TPP_HOME}/ampgen";

# change current directory so that input property file opt can relative to script bin dir
chdir "$appHome/bin";

# default property file
my $propFile = "${appHome}/cfg/ampgenpp.props";

# parse optional switches
use Getopt::Std;
our ($opt_h, $opt_p);
getopts('hp:');

if ( $opt_h || @ARGV == 0 ) { usage(); }

my $evid = $ARGV[0];

if ( $evid !~ m/^\d+$/ ) {
  print "\nInvalid first input arg: event evid must be a number\n";
  usage();
}

my $secsOld = `$ENV{TPP_BIN_HOME}/eventAge.pl $evid`;
chomp $secsOld;
if ( $secsOld == 0 ) {
  print "WARNING: event $evid not found in host default master database\n";
}

if ($opt_p) { $propFile = $opt_p };

# check prop file exists
if ( ! -e $propFile ) {
  print "Error: properties file does not exist: $propFile\n";
  exit;
}

# setup default logfile
my $logFile = "${appHome}/logs/ampgenpp_$evid.log";

# rename old log if any with timestamp
my $timestamp = `date -u '+%Y%m%d%H%M'`;
chomp $timestamp;
if ( -f "$logFile" ) {
  my ( $fname, $path, $suffix ) = fileparse( "$logFile", qr"\..[^.]*$" );
  `mv $logFile ${path}${fname}_${timestamp}${suffix}`;
}

print "Starting java app, print output is redirected to\n $logFile\n";

print `$ENV{TPP_HOME}/jiggle/bin/javarun_jar.sh org.trinet.apps.AmpGenPp $evid $propFile >$logFile 2>&1 &`;

# Wait a few seconds for job to start
sleep 3;

# Lookup job pid
my $jobpid = `pgrep -f "AmpGenP[p]"`;
chomp $jobpid;

if ( $jobpid ) {
  print "AmpGenPp running as background java process with pid: $jobpid " . gmtime() . "\n"; 
}
else {
  print "AmpGenPp job for $evid failed? : check log\n"; 
}

exit;

# ------------------------------------------------------
# Info on this program and how to use it
#
sub usage {

    print <<"EOF";

Calculate ground motions for one event.

Runs the AmpGenPp application found in jiggle jar to create
a database ampset having amptypes: PGD, PGV, PGA, and SP.

Print output is redirected to file:
  ${appHome}/logs/ampgenpp_<evid>.log

usage: $0 -[h] [-p property-file] <event-id>

  -h          : print this usage info
  -p filename : property file (default is $propFile)

  example: ./ampgenpp -p ../cfg/ampgenpp.props 7395710

EOF
    exit;
}
