#!/usr/bin/perl
#
# Calculate ground motions for one event. Does: PGD, PGV, PGA and spectral amps
# By default, AmpGenPp java class uses property file found in ../cfg subdirectory
     
use strict;
use warnings;

use File::Basename;

if ( ! defined $ENV{TPP_HOME} ) {
  print STDERR "\n$0 Error: TPP environment variables not setup correctly, check file /usr/local/etc/aqms.env\n";
  exit 1
}

my $appHome = "$ENV{TPP_HOME}/ampgen";

# change current directory so that input property file opt can relative to script bin dir
chdir "$appHome/bin";

# default property file
my $propFile = "${appHome}/cfg/ampgenpp.props";


# parse optional switches
use Getopt::Std;
our ($opt_a, $opt_d, $opt_h, $opt_p, $opt_s, $opt_z);
getopts('ad:hp:sz');

my $dbase = ( $opt_d ) ? $opt_d : `$ENV{TPP_BIN_HOME}/masterdb.pl`;
chomp $dbase;

usage(0) if $opt_h;

if ( @ARGV == 0 ) {
  print STDERR "\n$0 Error: Missing input evid argument\n";
  usage(1);
}

my $evid = $ARGV[0];

if ( $evid !~ m/^\d+$/ ) {
  print STDERR "\n$0 Error: Invalid first input arg: event evid must be a number\n";
  usage(1);
}

if ($opt_p) { $propFile = $opt_p };

# check prop file exists
if ( ! -e $propFile ) {
  print STDERR "\n$0 Error: properties file does not exist: $propFile\n";
  exit 1;
}

my $esum = `catone -d $dbase $evid`; 
chomp $esum;
my @fields = split(' ', $esum);
if ( "$fields[0]" eq "No" ) {
   print STDERR "$0 Error: event: $evid not found in database: $dbase\n";
   exit 1;
} 

my $ampCount = `$ENV{TPP_BIN_HOME}/ampdumpsm.pl -d $dbase -c -r $evid`;
chomp $ampCount;
if ( $ampCount !~ m/^\d+$/ ) {
      print STDERR "$0 Error: ampdumpsm count failure for $evid : $ampCount\n";
      exit 1;
}
if ( $ampCount > 0 ) {
    print "$0 INFO: $evid already has $ampCount strong motion amps from local subsource in $dbase\n";
    if ( $opt_z ) {
        print "$0 INFO: option -z, no-op when amps already exist in db\n";
        exit 2;
    }
}

#my $result = system("$ENV{TPP_HOME}/jiggle/bin/javarun_jar.sh org.trinet.apps.AmpGenPp $evid $propFile 2>&1");
my $result = 0;
if ( $opt_s ) {
    print "$0 INFO: option -s, skipping amp generation for $evid\n";
}
else {
    #print "$ENV{TPP_HOME}/jiggle/bin/javarun_jar.sh -H -v -TampgenPP org.trinet.apps.AmpGenPp $evid $propFile\n";
    print "$0 INFO: generating amps for $evid ...\n";
    $result = system("$ENV{TPP_HOME}/jiggle/bin/javarun_jar.sh -H -v -TampgenPP org.trinet.apps.AmpGenPp $evid $propFile 2>&1");
}

if ( $opt_a ) {
    print "$0 INFO: option -a, skipping ShakeMap alarm execution for $evid\n";
}
else {
  if ( $result == 0 ) {
    my $inNetwork = `$ENV{TPP_BIN_HOME}/isInsideNetwork.pl -d $dbase $evid`;
    chomp $inNetwork;
    # do shake script here
    if ( $inNetwork == 0 ) {
      print "$0 INFO: Event $evid is located outside of network: $ENV{AQMS_NET_AUTH}, skipping shakemap notification.\n";
    }
    else {
      my $vers = `$ENV{TPP_BIN_HOME}/eventVersion.pl -d $dbase $evid`;
      chomp $vers;
      if ( $vers !~ m/^\d+$/ ) {
          print STDERR "$0 Error: eventVersion failure for $evid\n";
          exit 1;
      }

      my $shakeresult = 0;
      if ( $vers > -1 ) {
        print "$0 INFO: running A_ShakeMap_V2 ...\n";
        my $status = system("$ENV{PP_HOME}/alarm/alarms/A_ShakeMap_V2/A_ShakeMap_V2 $evid $vers 2>&1");
        $shakeresult += 2 if $status; 
        print "$0 INFO: running A_ShakeMap_V3 ...\n";
        $status = system("$ENV{PP_HOME}/alarm/alarms/A_ShakeMap_V3/A_ShakeMap_V3 $evid $vers 2>&1");
        $shakeresult += 3 if $status; 
        print "$0 INFO: running A_ShakeMap_V4 ...\n";
        $status = system("$ENV{PP_HOME}/alarm/alarms/A_ShakeMap_V4/A_ShakeMap_V4 $evid $vers 2>&1");
        $shakeresult += 4 if $status;
        if ( $shakeresult > 0 ) {
           print STDERR "\n$0 Error: A_ShakeMap alarm # of script failures: $shakeresult\n";
           $result = 9;
        }
        else {
            print "$0 INFO: A_ShakeMap script returned success\n";
        }
      }
    }
  }
}

exit $result;

# ------------------------------------------------------
# Info on this program and how to use it
#
sub usage {

    my $status = shift;

    print <<"EOF";

usage: $0 -[h] [-p propfile] [-s] <evid>

  Calculate ground motions for one event, if successful,
  then executes ShakeMap alarm scripts.

  Uses the AmpGenPp application found in jiggle jar to create
  a new set of database amps of amptypes: PGD, PGV, PGA, and SP.

  Does event data db lookup in database: $dbase

  -h          : print this usage info

  -p propfile : property file used by the Java AmpGenPp db amp generator
               (default file is $propFile)

  -a          : generate amps only, skip running of the ShakeMap alarm scripts 
  -s          : skip amp generation, only run the ShakeMap alarm scripts
  -z          : no-op if count of local subsource strong motion amps in db > 0

  Example:

      Generate amps and run alarm scripts only if no amps for event in db:
      $0 -p ../cfg/ampgenpp.props -z 7395710

      Always generate new amps and run alarm scripts:
      $0 -p ../cfg/ampgenpp.props 7395710


EOF
    exit $status;
}
