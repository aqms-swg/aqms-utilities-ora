#!/bin/bash

# Wrapper around ampgennpp perl script to setup environment variables

# Setup AQMS environment
if [[ -z "${TPP_HOME}" ||  -z "${LD_LIBRARY_PATH}" ]]; then 
  if [ ! -f "/usr/local/etc/aqms.env" ]; then 
    echo
    echo "Missing environment setup file: /usr/local/etc/aqms.env, exiting..."
    exit 1
  fi
  source /usr/local/etc/aqms.env
fi

# just pass all switch options and arguments to perl script
${TPP_HOME}/ampgen/bin/ampgenpp.pl $@

