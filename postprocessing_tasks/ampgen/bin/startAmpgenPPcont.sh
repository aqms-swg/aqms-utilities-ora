#!/bin/bash
# #####################################################################
#
# crontab -e
#0,10,20,30,40,50 * * * * ${TPP_HOME}/ampgen/bin/startAmpgenPPcont

# For crontab job we need to source the environment variables, does oracle too
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

usage() {

  cat <<EOF

Usage:

  Start/restart the process that generates strong motion ampsets in database
  This is run at intervals by a cron job to insure that the jobs start at
  boot time and are always present.

  Syntax: $0 [ -c maxDaysBack]

  -c days     : max days back to keep logs
  -h          : this help usage

EOF
    exit $1
}

logdir=${TPP_HOME}/ampgen/logs

# One log per UTC day
logTimeStamp=$(date -u +"%Y%m%d")
startLog="${logdir}/startAmpgenPPcont_${logTimeStamp}.log"

maxDaysBack=''
while getopts ":c:h" opt; do
  case $opt in
    h)
      usage 0 # non-error exit
      ;;
    c)
      maxDaysBack="$OPTARG"
      ;;
    \?)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Invalid command switch option: -$OPTARG" | tee -a ${startLog}
      usage 1 # error exit
      ;;
    :)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Command option -$OPTARG requires an argument." | tee -a ${startLog}
      usage 1 # error exit
      ;;
  esac
done

#Here use shift to offset input $@ to any remaining args that follow this shell's  switch options
shift $(($OPTIND - 1))

cd $TPP_HOME/ampgen/bin

appName="ampgenPPcont"
#outputLog="${logdir}/${appName}.log"
startJob="./${appName}.pl"

mailaddr=${TPP_ADMIN_MAIL}
host=`uname -n | cut -d'.' -f1`

#[a]mpgenPPcont"
grepstr=$startJob

#Block wrapper to redirect echo output to startLog
{
  echo "===================================================="

    # look for the pid of the active job, if any
    pid=`pgrep -f "$grepstr"`

    # if no job running, START ONE
    if [ -z "$pid" ]
    then
      # NOTE: Moved logfile creation and rotation to perl script
      #if [ -e "$outputLog" ]
      #then
      #  timeStamp=`date -u +"%Y.%m.%d.%H%M"`
      #  mv $outputLog ${outputLog}-$timeStamp
      #fi
    
      echo "Starting: ${startJob} at `date -u +'%F %T %Z'` on $host"
    
      # fork the job, put in background
      # NOTE: Use command line args to specify perl script options
      # -d for the db name (defaults to value of $masterdb) also default for pcs table
      #${startJob} $@ >$outputLog 2>&1 &
      ${startJob} $@ >>$startLog 2>&1 &
    
      # send email of success or failure
      if [ -n "$mailaddr" ]; then

        sleep 5   # give it time to start run then check for pid
        pid=`pgrep -f "$grepstr"`

        if [ -n "$pid" ]; then
            echo "${appName} now running as pid = $pid"
            tail -3 $startLog | mailx -s "Restart ${appName} on $host (pid=$pid)"  "$mailaddr"
        else
            echo "${appName} restart failed, no matching process found"
            tail -3 $startLog | mailx -s "FAILED restart ${appName} on $host"  "$mailaddr"
        fi
      fi
    
    # noop but gives error if no 'else' block!
    else
      echo "${appName} running as pid=$pid on $host `date -u +'%F %T %Z'`"
    fi

    # Run log cleanup
    if [ -n "$maxDaysBack" ]; then 
        echo "----------------------------------------------------"
        if [[ "$maxDaysBack" =~ ^[0-9]+$ ]]; then
            if [[ "$maxDaysBack" > "0"  ]]; then
                echo "Deleting files in $logdir older than $maxDaysBack days"
                find $logdir -mtime "+$maxDaysBack" -print -exec \rm '{}' \;
            fi
        else
            echo "Invalid max days back to keep logs: $maxDaysBack"
        fi
    fi 
} >> ${startLog} 2>&1
