# Input args are the class's properties file ($1) and an event evid ($2)
# The -T shell option is value used for a -D java property tag

if [ $# -lt 2 ]; then
  echo "Error: Missing input arg, 2 required, have $#:"
  echo "Syntax: $0 <props-filename> <evid>"
  exit 1
fi

if [ -z "$TPP_HOME" ]; then . /usr/local/etc/aqms.env; fi

${TPP_HOME}/jiggle/bin/javarun_jar.sh -Texport org.trinet.apps.Db2Gmp ${1} ${2}
