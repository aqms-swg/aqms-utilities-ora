#!/bin/bash
# Turn off checking of status on Big Brother system
# gets environmental variables from bbdef.sh

# Arg #1 is the host name of the reporting task
# Arg #2 #of minutes
#
# To see BigBro logs look in rift:/home/htdocs/bb/logs

if [ "$1" == "" ]
then
    echo "Disable Big Brother monitoring of an item for some number of minutes."
    echo " "
    echo "Syntax: $0 <row> <column> [<minutes>]"
    echo "                      default time = forever"
    echo "<row>    is usually the host name on the BB web page."
    echo "<column> is usually the process hame."
    echo "[<minutes>] time for which reports are disabled (default=999999)"
    exit

fi

# Must do this because /bbdef.sh steps on the command line args
minutes=9999999
column=${2}
row=${1}

if [ "$3" != "" ]
then
  minutes=${3}
fi

# NOTE BB code uses BBHOME env dir def
#BBHOME=/usr/local/bb
#BBHOME=/usr/local-sys/bb
#BBHOME=/opt/bb
if [ -z "${BBHOME}" ]; then
    echo "ERROR: BBdisable.sh BBHOME env variable for home directory is undefined." 
    exit
fi

. ${BBHOME}/etc/bbdef.sh

LINE="disable ${row}.${column} ${minutes}"

$BB $BBDISP "$LINE"
