#!/usr/bin/perl
#
# Check for events posted to group/table/state else sleep for time interval
# If new event is found, run db2gmp.sh wrapper around java app code.
# Java app will decide if it should make packet files or not.
# Usage $0 [prop-file]
# If a prop-file is unspecified, it defaults to ../cfg/exportamps.props
# This script expects db2gmp.sh script to be in same bin directory.
#
# Logging is via a pipe in the original started script 'startAmpExchange'

use strict;
use warnings;

use File::Basename;

use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use Utils;
use MasterDbs;

# parse optional switches
use Getopt::Std;
our ( $opt_h, $opt_H, $opt_v );
getopts('hHv');


# Make STDOUT "hot" e.g. autoflush
# By default perl running in background flushes its buffers only once/min
# so monitoring the log file makes it appear the script isn't working!
select STDERR; $| = 1;
select STDOUT; $| = 1;

# First check runtime environment
defined $ENV{TPP_HOME} or die "$0 Error: Required aqms.env environment vars are not defined\n$!";
if (! defined ($ENV{AMPEXC_HOME}) ) { $ENV{AMPEXC_HOME} = "$ENV{TPP_HOME}/ampexc"; }
die "Missing application directory\n$!" if ( ! -d $ENV{AMPEXC_HOME} ) ;

chdir "$ENV{AMPEXC_HOME}/bin";

usage() if $opt_h || $opt_H;

# Host machine running this script
my $host = `hostname`;
chomp $host;
$host =~ s/\..*$//; # grab first element

# Direct all output to log file, rename existing log if present
my $logFile  = '../logs/export/' . basename($0,'.pl') . '.log';
print "All output will be directed to $logFile\n";
rotateLogs($logFile);

# Environmental variables supplied by a bash wrapper sourcing /usr/local/etc/aqms.env
my $date = `date -u +'%F %T %Z'`;
chomp $date;
print "Starting $0 on $host at $date\n";

# ==========================================================================
# Behavior variables 
# ==========================================================================
# Hash array to hold configuration properties, like object attributes
my ( %myprops );
# Property set vars:
my ( $pcsCommand );
my ( $pcsAppName, $pcsAppProps, $pcsDbase, $pcsGroup, $pcsTable, $pcsState, $pcsResult, $pcsStallTime );
my ( $sleepTime, $archivalTime, $minMag, $maxDaysBack );
my ( $propFile, $debug, $verbose, $reconfig, $noop );

# Define default runtime properties 
setDefaultProperties(\%myprops);

# Check for properties file to override the default config
$propFile = "$ARGV[0]" if (@ARGV > 0);
die "$0 Error: missing property file: $propFile\n$!" if (! -e $propFile); 
print "Loading properties from file: $propFile\n";

loadProperties(\%myprops, $propFile);

# Report starting configuration
printConfig();

# Are all props valid ?
checkProperties();

# set verbose mode if option set, and not in props
$verbose = 1 if $opt_v;

# Declare dynamic variables used inside infinite while loop
# that may need to be accessed outside block scope

my $amEnabledForPCS = 'true'; # reset in loop by checking with db
my ($evid, $age, $mag, $wait, $cutoff, $status);

my $dayNumber = `date -u '+%d'`; #  note LF is not stripped

# ---- MAIN CONTINUOUS LOOP ----
while (1) {

      if ($dayNumber != `date -u '+%d'`) {
        print("Executing change-of-day chores @ ". `date -u +'%F %T %Z'`);
        rotateLogs($logFile);
        printConfig();
        $dayNumber = `date -u '+%d'`;
      }
      else {
        my $now = time();
        utime $now, $now, $logFile;  # keep file timestamp current (proof of redo looping)
      }

    # check db host app processing status
    $amEnabledForPCS = `$ENV{TPP_BIN_HOME}/enabledForPCS.pl $pcsAppName $pcsState`;  
    chomp $amEnabledForPCS; # may have newline at end

    # See if an event id is posted to PCS_STATE table in db
    $evid = `$ENV{TPP_BIN_HOME}/next.pl -d $pcsDbase -s $pcsStallTime $pcsGroup $pcsTable $pcsState`;
    chomp $evid;

    if ($evid eq '' || $evid eq '0') { # No event: sleep - zzzz
        sleep $sleepTime;
        if ($reconfig) { # reload props file
            if ( ! loadProperties(\%myprops, $propFile) ) {
              checkProperties();
              printConfig() if ( $reconfig > 1 );
            }
        }
        redo;
    }

    # Have event id
    $date = `date -u +'%F %T %Z'`;
    chomp $date;
    print "\nNEXT: $evid \@ $date using primary db:" . `$ENV{TPP_BIN_HOME}/amPrimaryDb.pl`;

    if ( "$amEnabledForPCS" ne 'true' ) {
        print "SKIP: Not primary app host on $host for $pcsAppName $pcsState, a no-op for $evid\n";
        next; # result it
    } 

    # check event prefor gtype
    my $gtype = `$ENV{TPP_BIN_HOME}/getGtype.pl $evid`; # origin type 
    chomp $gtype;
    if ( "$gtype" eq 't' ) {  # don't do teleseisms
        print "SKIP: $evid has gtype 't', teleseism\n";
        next; # result it
    }

    # check age
    $age = getEventAge($evid); # age of event in seconds
    if ( $age <= 0 ) {  # Bogus FUTURE event (yes, this happened)
        print "SKIP: No valid event found in db, age = $age secs for $evid\n";
        next; # result it
    }

    # check cutoff age only if input property for maxDaysBack was specified
    $cutoff = (86400 * $maxDaysBack);
    if ( $cutoff > 0 ) {
        if ( $age > $cutoff ) {
            printf "SKIP: Too old, %8.2f > $maxDaysBack days for %d\n", ($age/86400), $evid;
            next; # result it
        }
    }

    # allow at least archivalTime secs post-origin for data to be generated (e.g. amps, mags, waveforms ...)
    if ( $age < $archivalTime ) {
        $wait = ($archivalTime - $age);  # wait for data to be archived to db
        print "WAIT: $wait secs to age $evid, for its data to be archived ...\n";
        sleep $wait;
    }

    # check for qualifying magnitude
    $mag = getEventMag($evid);
    if ( $mag < $minMag ) {
        print "SKIP: Too small, $mag < $minMag (minMag) for $evid\n";
        next; # result it
    }

    print "PROC: $evid Mag = $mag \@ " . `date -u +'%F %T %Z'`;
    $status = processEvent($evid);
    print "STAT:" . ($status >> 8) . " for $evid  \@ " . `date -u +'%F %T %Z'` if $status && $verbose;

} continue { # done with event

    # The transition defined in PCS for result removes posting from PCS 
    # Done - result it
    my $result = $pcsResult;
    # $result = ($status >> 8) if $status; #  reset to returned result, like error code ?
    print `$ENV{TPP_BIN_HOME}/result.pl -d $pcsDbase $evid $pcsGroup $pcsTable $pcsState $result`;
    print "DONE: $evid \@ " .  `date -u +'%F %T %Z'`;

} # end of infinite loop

exit;

####################################################################################

sub processEvent {
   my ($evid) = @_;

   my $cmd;
   # must force substitution of variables from $pcsCommand and need quotes around expanded string
   my $command = '$cmd = ' . "\"$pcsCommand\"";
   eval $command;

   if ( $noop ) {
     print "EXEC: $cmd\n" if ( $debug ne '0' );
     print "NOOP: $evid\n" if $verbose;
     return 0; # assumes 0 is the same as system success
   }

   # Do sm with -r ampgen subsource amp count check here or in db2gmp.sh? send mail and bail if 0 count
   my $ampCnt = `$ENV{TPP_BIN_HOME}/ampdumpsm.pl -c -r -d $pcsDbase $evid`;

   if ( $ampCnt == 0 ) {

     my $msg = "Found 0 ampgen amps in $pcsDbase for $evid"; 
     print "SKIP: $msg\n";

     my $mailaddr = $ENV{"TPP_ADMIN_MAIL"};
     if ( $mailaddr ) {
        print `$ENV{TPP_BIN_HOME}/cattail.pl -I $evid | mailx -s "ExportAmps on $host: $msg M$mag"  "$mailaddr"`;
     }
     return 2; # bail on further processing
   }

   print "EXEC: $cmd\n" if $verbose;

   # Note /bin/sh shell forked by system does not have the ENV vars
   return system($cmd);
}

# Return the age of the given event in integer seconds
# Returns '0' if no such event.
sub getEventAge {
   my ($evid) = @_;
   my $secsOld = `$ENV{TPP_BIN_HOME}/eventAge.pl -d $pcsDbase $evid`;  #age of event in secs
   chomp $secsOld;
   return $secsOld;
}

# Return event magnitude, or -9 if undefined
sub getEventMag {
    my ($evid) = @_;
    my $emag = `$ENV{TPP_BIN_HOME}/eventMag.pl -d $pcsDbase $evid`;
    chomp $emag;
    if ( $emag !~ m/\d/ ) {
       print "ERROR: No magnitude found for $evid\n";
       return -9;
    }
    $emag = -9 if $emag < -9; 
    return $emag;
}

# Read properties into hash from properties file, merge with defaults
sub loadProperties {
    my ($defPropsRef, $propFile) = @_;
    my $fileHashRef = {};

    die "Invalid input, properties file D.N.E.\n $!" if ! -e "$propFile";

    $date = `date -u +'%F %T %Z'`;
    chomp $date;
    print "Loading properties from file: $propFile \@ $date\n"  if ( $reconfig > 1 );

    eval {
      # Optional regex for split delimiter for key value is a string like "[=: ]+" 
      $fileHashRef = Utils::->readPropsFromFile($fileHashRef, $propFile, " *[=] *");
    };
    die "Error loading properties from $propFile\n $!" if ( $@ );

    # Check if properties file changed
    my $retval = equalsHash($fileHashRef, $defPropsRef);
    #print "retval=$retval\n";

    if ( scalar( keys %$fileHashRef ) != 0 ) {
        setProperties( $fileHashRef );
        # Use slice assignment to merge prop file hash into the default props by refernce:
        @{$defPropsRef}{ keys %$fileHashRef } = values %$fileHashRef;
    }

    if ( exists $defPropsRef->{'debug'} ) {
      $debug = $defPropsRef->{'debug'};
      if ( $debug  eq 'true' ) {
         $debug = '1';
      } elsif ( $debug eq 'false' ) {
         $debug = '0';
      }
    }
    else {
      $debug = '0';
    }

    if ( $debug ne '0' ) {
        $verbose = 1;
        print "DEBUG $0 Properties:\n";
        Utils::->printProps($defPropsRef, " = ");
    }

    return $retval; # 0 or 1
}

# Lookup property in input hash, return default if no key
sub getProperty {
    my ($propsRef, $propName, $defaultVal) = @_;
    my $val = $propsRef->{$propName};
    $val = $defaultVal if ! defined $val;
    return $val;
}

####################################################################################

# Populate input hash with current config
sub getProperties {

    my ($propsRef) = @_;

    $propsRef->{pcsAppName} = $pcsAppName;
    $propsRef->{pcsAppProps} = $pcsAppProps;
    $propsRef->{pcsDbase} = $pcsDbase;
    $propsRef->{pcsGroup} = $pcsGroup;
    $propsRef->{pcsTable} = $pcsTable;
    $propsRef->{pcsState} = $pcsState;
    $propsRef->{pcsResult} = $pcsResult;
    $propsRef->{pcsStallTime} = $pcsStallTime;
    $propsRef->{pcsCommand} = $pcsCommand;
    $propsRef->{noop} = $noop;
    $propsRef->{reconfig} = $reconfig;
    $propsRef->{sleepTime} = $sleepTime;
    $propsRef->{archivalTime} = $archivalTime;
    $propsRef->{maxDaysBack} = $maxDaysBack;
    $propsRef->{minMag} = $minMag;

    return $propsRef;
}

# set internal config to property hash values
sub setProperties {

    my ($propsRef) = @_;

    my $str = '';

    # PCS config props
    $str = getProperty($propsRef, 'pcsAppName');
    if ( $str ) { $pcsAppName = $str; }

    $str = getProperty($propsRef, 'pcsAppProps');
    if ( $str ) { $pcsAppProps = $str; }

    $str = getProperty($propsRef, 'pcsDbase'); 
    if ( $str ) {
       $pcsDbase = $str;
       $pcsTable = $str;
    }

    $str = getProperty($propsRef, 'pcsGroup'); 
    if ( $str ) { $pcsGroup = $str; }

    $str = getProperty($propsRef, 'pcsTable');
    if ( $str ) { $pcsTable = $str; }

    $str = getProperty($propsRef, 'pcsState'); 
    if ( $str ) { $pcsState = $str; }

    $str = getProperty($propsRef, 'pcsResult'); 
    if ( $str ) { $pcsResult = $str; }

    $str = getProperty($propsRef, 'pcsStallTime'); 
    if ( $str ) { $pcsStallTime = $str; }

    # Note app args are specific to implementation like where to insert props and $evid in cmd string
    $str = getProperty($propsRef, 'pcsCommand'); 
    if ( $str ) { $pcsCommand = $str; }

    # Other props
    $str = getProperty($propsRef, 'verbose');
    if ( $str ) { $verbose = $str; }
    else { $verbose = 0; }

    $str = getProperty($propsRef, 'noop');
    if ( $str ) { $noop = $str; }

    $str = getProperty($propsRef, 'reconfig');
    if ( $str ) { $reconfig = $str; }

    $str = getProperty($propsRef, 'sleepTime');
    if ( $str ) { $sleepTime = $str; }

    $str = getProperty($propsRef, 'archivalTime');
    if ( $str ) { $archivalTime = $str; }

    $str = getProperty($propsRef, 'maxDaysBack');
    if ( $str ) { $maxDaysBack = $str; }

    $str = getProperty($propsRef, 'minMag');
    if ( $str ) { $minMag = $str; }
        
}

# print current internal config
sub printConfig {
    print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
    print "Property file=$propFile\n";
    print "App $pcsAppName on $host connects to $pcsDbase and processes PCS state posts of:\n";
    print "  $pcsGroup $pcsTable $pcsState for result $pcsResult where\n";
    print "  pcsStallTime=$pcsStallTime seconds\n";
    print "  sleepTime=$sleepTime secs hibernation when no evid is posted\n";
    print "  reconfig=$reconfig (=1 reloads properties after sleep)\n";
    print "  verbose=$verbose (=1 prints extra processing output and app properties)\n";
    print "  archivalTime=$archivalTime min secs of event aging before processing\n";
    print "  maxDaysBack=$maxDaysBack max days age eligible for processing (any age if <= 0 )\n";
    print "  minMag=$minMag is min magnitude eligible for processing\n";
    print "\nProcessing $pcsCommand\n";

    if ($pcsAppProps) {
       print "Using pcsAppProps file $pcsAppProps\n";
       if ( $verbose ) {
         print "\nProperties:\n";
         open(my $fh, $pcsAppProps);
         while (my $line = <$fh>) { print $line; }
         close($fh);
       }
    }

    print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
}

sub setDefaultProperties {

    my ($propsRef) = @_;

    #ampexc exportamps:
    $propFile = "$ENV{AMPEXC_HOME}/cfg/exportamps.props";

    $pcsAppName = "exportAmpsCont";
    $pcsAppProps = "$ENV{AMPEXC_HOME}/cfg/db2gmp.props";

    # NOTE: single quotes, var substitution is done at time of execution by "eval" in processEvent function 
    $pcsCommand = '$ENV{AMPEXC_HOME}/bin/db2gmp.sh $pcsAppProps $evid &';

    #Default to master else parse command line arg option
    $pcsDbase = $masterdb;

    # NOTE: pcs_transition table sourceTable could be some string other than the $pcsDbase
    # default PCS state description to operate on:
    $pcsGroup = "EventStream";
    $pcsTable = "$pcsDbase";
    $pcsState = "ExportAmps";
    $pcsResult = 1;         # after processing
    $pcsStallTime = 0;

    $noop = 0;              # =0 always process, =1 do not process posts, for testing
    $reconfig = 0;          # do not reload properties after sleep between next post poll
    $sleepTime = 30;        # secs to sleep between checks for new events
    # NOTE: to capture more distant sta amps from ampgen2 archivalTime at CI should at least as large as
    # ampgen delay ~150s, +30s for processing/replication overhead (only captures primary ampgen amps)
    $archivalTime = 180;    # secs post-origin to wait for data to be archived
    $minMag = 3.4;          # minimum magnitude to export data

    #NOTE: maxDaysBack special value 0 implies do not check age for cutoff 
    $maxDaysBack = 0;    # Don't process events older than this

    getProperties($propsRef);
}

sub checkProperties {
    die "$0 Error: missing property file: $pcsAppProps\n$!" if ! -e $pcsAppProps; 
}

# This belongs in the utils pm ?
sub equalsHash {
    my ($a, $b) = @_;
    my @ka = keys %$a;
    my @kb = keys %$b;
    return 0 if ( @ka != @kb );
    my %cmp = map { $_ => 1 } @ka;
    for my $key (@kb) {
      last unless exists $cmp{$key};
      last unless $a->{$key} eq $b->{$key};
      delete $cmp{$key};
    }
    return ( scalar( keys %cmp ) ? 0 : 1 );
}

sub usage {
  print <<"EOF";
Usage:
  $0 [ -h -v ][prop-file, default=exportamps.props]
  -v    : verbose output
  -h    : usage
  -H    : usage, plus list example properties files setup
EOF
  print <<"EOF" if ( $opt_H );
  ##############################################################################
  #Example CI exportamps.props:
  ##############################################################################
  #
  # PCS lookup data:
  pcsAppName=exportAmpsCont
  pcsDbase=myDbName
  pcsGroup=EventStream
  pcsTable=myDbName
  pcsState=ExportAmps
  pcsResult=1
  pcsStallTime 0   seconds
  
  # Location of properties file for Db2Gmp java jar app:
  pcsAppProps=$ENV{TPP_HOME}/ampexc/cfg/db2gmp.props
  #
  # Set noop=1 to just result, not process the event
  noop=0
  #
  # Set reconfig=1 to reread config file after each loop sleep cycle
  reconfig=0
  #
  #seconds to hiberate before recheck if no next posting id
  sleepTime=30
  #
  # NOTE: to capture more distant sta amps archivalTime
  # should be ampgen delay plus processing/replication overhead
  archivalTime=180
  #
  #max days age of event for Db2Gmp processing, =0 no check for cutoff age
  maxDaysBack=180
  #
  #minimum magnitude for Db2Gmp processing
  minMag=3.4

  ##############################################################################
  #Example CI db2gmp.props file:
  ##############################################################################
  # sendfile directory
  destinationDirs=$ENV{PP_HOME}/xdata/io/BK/outdir
  #
  #dir below destination path to write files before rename into the sendfile dir
  tempSubDir=/temp.dir
  #
  # network authority code for output packet
  authcodeList=CI
  ampGetByTable=ampset
  verbose=true
  #
  prefixList=
  suffixList=.gCI 
  formatType=GMXY
  minMag=3.4
  localNetCode=CI
  netcodeList=CI AZ WR NP FA
  #
  dbaseHost=iron
  dbaseDriver=oracle.jdbc.driver.OracleDriver
  dbaseDomain=gps.caltech.edu
  dbaseName=irondb
  dbasePort=1521
  dbaseUser=username
  dbasePasswd=xxxxx
EOF
  exit;
}
# ###################################################
#
# open log file.
#
sub openLogs {
    my ($logFile) = @_;   # get arg

    open STDOUT, "> $logFile" or die "Can't redirect STDOUT to $logFile: $!\n";
    open(STDERR, ">&STDOUT") || die "Can't dup err to stdout";

    select(STDERR); $| = 1; # make unbuffered
    select(STDOUT); $| = 1; # make unbuffered

    print("Log file opened: " . `date -u +'%F %T %Z'`);

  }

# ###################################################
# Rotate log files
#
# rotateLogs ();
#
# You can call this at startup to rename an old existing log
# and open a new one.
sub rotateLogs {
    my ($logFile) = @_;   # get arg

    if (-e $logFile) {
        my $timestamp = `date -u '+%Y%m%d%H%M'`;
        chomp $timestamp;
        #my $cmd = "mv $logFile ${logFile}.$timestamp";
        my ($fname,$path,$suffix) = fileparse("$logFile",qr"\..[^.]*$");
        my $cmd = "mv $logFile ${path}${fname}_${timestamp}${suffix}";
        `$cmd`;
    }

    # open new
    openLogs($logFile);
}
