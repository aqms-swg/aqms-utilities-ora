#!/bin/bash

echo $0 start at `date -u +"%F %T %Z"`

# First check runtime environment
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

if [ -z "${XDATA_HOME}" ]; then 
    echo "XDATA_HOME env variable not defined check /usr/local/etc/aqms.env"
    exit 1
fi

# Delete older than max days back
maxDays=''
if [ -n "$1" ]; then
    maxDays="$1"
fi

bindir=${XDATA_HOME}/bin
dataRoot=${XDATA_HOME}/io

if [ -n "$maxDays" ]; then
  for adir in $(find ${dataRoot} -type d -name 'indir')
  do
    echo "  Cleaning $adir, maxDays=${maxDays} ..."
    adir=${adir%%/indir*}
    adir=${adir##${dataRoot}/}
    \find ${dataRoot}/${adir}/indir -name "*.g${adir}" -o -name "*.V0C" -type f -mtime +${maxDays} -exec \rm {} \;
    #${bindir}/cleanup_$adir.sh ${maxDays}
    #${bindir}/cleanindir.pl -a ${maxDays} $adir
    echo "  $adir files left: `\find ${dataRoot}/$adir/indir -type f | wc -l`"
  done

  # remove older log files
  echo "  Cleaning ${XDATA_HOME}/logs, maxDays=${maxDays} ..."
  #${bindir}/cleanlogs.sh $maxDays
  \find ${XDATA_HOME}/logs -name "*.log*" -mtime +${maxDays} -exec \rm {} \;
  echo "  log files left: `\find ${XDATA_HOME}/logs -name '*.log*' | wc -l`"

else
  echo "  No cleanup of ${XDATA_HOME}/io data or logs, no maximum days set" 
fi

echo $0 done  at `date -u +"%F %T %Z"`

#if [ ! -f "${XDATA_HOME}/cfg/netsubdir.cfg" ]; then
#    echo "Missing config file ${XDATA_HOME}/cfg/netsubdir.cfg" 
#    exit 1;
#fi
# Read cfg file of directory into array var and loop over elements or?
#source ${XDATA_HOME}/cfg/netsubdir.cfg
#netdirs=( NC BK CE )
#for adir in ${netdirs[@]}
#do
#    echo "Cleaning ${dataRoot}/$adir/indir ..."
#    echo ${bindir}/cleanindir.pl -a ${maxDays} $adir
#    echo $adir files left: `ls ${dataRoot}/$adir/indir | wc -l `
#done 

