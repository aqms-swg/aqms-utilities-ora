#!/usr/bin/perl 
#
# Check AmpExchange jobs
#
# OUTPUT:
#
#  Writes text file for inclusion in BB report
#

use strict;
use warnings;

use Getopt::Std;
our ( $opt_c, $opt_e, $opt_h, $opt_l );
getopts('cehl:');

# FOR PRODUCTION:
my $flagfilename = "/app/cluster/I_AM_PRIMARY";
# FOR TEST SYSTEM:
#my $flagfilename = "/etc/profile";

#$ENV{XDATA_HOME} = "$ENV{PP_HOME}/xdata";
# roots for which to build pathnames 
defined ( $ENV{XDATA_HOME} ) or die "Undefined environment variable XDATA_HOME, check aqms.env";

my $xdataroot = "$ENV{XDATA_HOME}/io";  # for incoming files
my $ampxroot = "$ENV{TPP_HOME}/ampexc"; 
#my $errRoot = "$ENV{TPP_HOME}/ampexc/import/err"; # processed amp files
my $errRoot = "$ENV{XDATA_HOME}/ampexc/import/err"; # moved processed amp files

usage() if $opt_h;

my $tmpErrFile = ( $opt_e ) ? "${errRoot}/$opt_e" : "${errRoot}/checker.tmp";

my %config = readConfig();

#my @dest = ("NC", "BK", "CE");
my @dest = split( "[:, ]+", $config{"xdata.io.dirs"} );

my $maxHbMinutes = 10;
$maxHbMinutes = $config{qq(ampexc.check.maxHbMinutes)} if defined $config{qq(ampexc.check.maxHbMinutes)};
#print "maxHbMin: $maxHbMinutes\n";

my $maxFileMinutes = 60;
$maxFileMinutes = $config{qq(ampexc.check.maxFileMinutes)} if defined $config{qq(ampexc.check.maxFileMinutes)};
#print "maxFileMin: $maxFileMinutes\n";
#
# Check that processes exist
# Nagios does this now, supposedly -aww 2014/08/19
#checkJobs();

#  Temp stopper
#BBreport ("importamps", "clear", "Import not running as of 1/15/04");

# check for stacked up files??

# check of errors in log file
my $importLog = ( $opt_l ) ? "${ampxroot}/logs/import/importamps${opt_l}.log" : "${ampxroot}/logs/import/importamps.log";;

# check for old, jammed up files
checkFileJam();

# check for fresh heart beats
checkHeartBeat();

# check for ORA- errors
`:>$tmpErrFile; tail -200 ${importLog} 2>$tmpErrFile | grep "ORA-" >> $tmpErrFile`;
# grep returns sucess, ie. $?=0, when matching lines 
if ( -s $tmpErrFile ) { # zero-byte size when no match
    print "Ampexc import log missing or has ORACLE errors\n";
    croakMail("Ampexc import: log has ORA errors", $tmpErrFile);
}

# Check for SQLExceptions
`:>$tmpErrFile; tail -200 ${importLog} 2>$tmpErrFile | grep "Exception" >> $tmpErrFile`;
# grep returns sucess, ie. $?=0, when matching lines 
if ( -s $tmpErrFile ) { # zero-byte size when no match
    print "Ampexc import log missing or has Java Exceptions \n";
    croakMail("Ampexc import: log has Java exceptions", $tmpErrFile);
}

# count files in err dir
#`ls -l ${errRoot} > $tmpErrFile`;
#my $errknt = `cat ${tmpErrFile} | wc -l`;
#chomp $errknt;
my @fcnt = glob("${errRoot}/*");
my $errknt = scalar(@fcnt);
if ($errknt > 1) {
    print "Ampexc import err directory contains $errknt files\n";
    `ls -l ${errRoot} &> $tmpErrFile`;
    croakMail("Ampexc import: err directory contains $errknt files", $tmpErrFile);
}

my $date = `date -u +'%F %T %Z'`;
print "DONE $0 \@ $date\n";
exit;

# ===========================================================

sub grepJob 
# grepJob(jobstring)
#
# Uses customized 'ps' output  "ps -eo user,stime,time,args". 
{
    (my $jobStr) = @_;
    my $str = `ps -eo user,stime,time,args  | grep -i "$jobStr" | grep -v "grep"`;
    chomp $str;
    return $str;
}
# -----------------------------------------------------------
# Send report to Big Brother
#
# Syntax:  BBreport(item, state, string);
#
sub BBreport
{   
    (my $myName, my $myState, my $myText) = @_;
    my $host = `hostname`;
    chomp $host;
    $host =~ s/\..*$//; # grab first element

    my $date = `date -u +'%F %T %Z'`;
    chomp $date;

    my $line = "status $myName.$host $myState $date $myText";

    my $bbcmd="${ampxroot}/bin/bbmsg.sh '$line'";
    system( $bbcmd );

}

# -----------------------------------------------------------
#

# Syntax:  checkJob(myName, process-string);
#
# "myName" is the item name as known to BB
# "process-string" is the, hopefully, unique string used to grep for proc existance
#
sub checkJobs
{
    my @proc = split( "[:, ]+", $config{"ampexc.jobs"} );
    my @jobstr; 
    foreach (@proc) {
      checkJob($_, $config{"ampexc.".$_.".grepstr"} );
    }
    #checkJob("exportamps", "exportamp[s]");
    #checkJob("importamps", "[D]importamps");
}
sub checkJob 
{
    (my $myName, my $procStr) = @_;

    my $result;
    my $state = "green";
    my $text = "";

    if (-e $flagfilename) {
      # check job
      $result = grepJob("$procStr");

      if ($result eq "") {
        $text="&red Process missing: $procStr\n";
        $state = "red";
      } else {
        $text = "&green $result\n";
      }
    } else {
      $state = "clear";
      $text = "System on standby";
    }

    BBreport ($myName, $state, $text);

}

# Send mail and die if there's a problem
# args: subject string, file for body
sub croakMail {
  (my $str, my $file) = @_;
  my $mailaddr = $ENV{"TPP_ADMIN_MAIL"};
  my $host= `uname -n`;
  chomp $host;
  $host =~ s/\..*$//; # grab first element
  if ( $mailaddr ) { print `cat ${file} | mailx -s "$str on $host"  "$mailaddr"`; }
  #exit; # bail on further processing
}

# Check for recent heartbeat files
# The time of the heartbeat file should increment once per minute
# (This is probably redundant with checkFileJam()

sub checkHeartBeat {

    if ( ! -e $flagfilename) { return; }  # not primary so heartbeat stale

    # Files are read from cfg because they are not systematic in naming.
    # Example: ('${xdataroot}/BK/indir/9999999.hrtUCB', '${xdataroot}/NC/indir/9999999.hrtMP', '${xdataroot}/CE/indir/hb.cgs');
    my @filename;
    foreach (@dest) { push ( @filename, "${xdataroot}/$_/indir/$config{qq(xdata.io.hb.$_)}" ); }

    my $minutes = 0;
    my @hb_array = ();
    foreach (@filename) {
        #print "$_\n";
        if ( -e $_ ) {
            $minutes = (-M $_) * 1440;  # convert days to minutes
        }
        else {
            print "Ampexc import heartbeat file D.N.E: $_ \n";
        }
        if ($minutes > $maxHbMinutes) {
            my $msg = sprintf("Ampexc import heartbeat file is too old, %6.1f minutes: %s\n", $minutes, $_);
            print $msg;
            push(@hb_array, $msg);
        }
        else {
            printf( "    Ampexc import heartbeat file $_ age is OK < %5.1f mins.\n", $maxHbMinutes);
        }
    }
    if ( @hb_array > 0 ) {
      open( my $errFH, ">$tmpErrFile");
      foreach ( @hb_array ) {
          print $errFH $_;
      }
      close($errFH);

      croakMail("Ampexc import: heartbeat file is too old", $tmpErrFile);
    }
}

# Check for files piling up
sub checkFileJam {

    if ( ! -e $flagfilename) { return; }  # not primary so files stale

    # Look in each sendfile/getfile dir for OLD files
    # Dirs are now systematic in naming.
    #Example: ('${xdataroot}/BK/indir', '${xdataroot}/NC/indir', '${xdataroot}/CE/indir');
    my @dirname;
    foreach (@dest) { push ( @dirname, "${xdataroot}/$_/indir" ); }

    foreach (@dirname) {

        my @file = `find $_  -name '*.g*' -o -name '*.V0C' -o -name '*.v0c'`;    # get list of files
        #my @file = `ls -1 $_/*`;    # get list of files

        my $maxmin = 0;
        my $minutes = 0;
        my $oldfile = "";

        foreach my $nfile (@file) {
          chomp $nfile;

          if (-e $nfile) {
            #print "--- $nfile\n";
            $minutes = (-M $nfile) * 1440;  # convert days to minutes

            # keep track of oldest
            if ($minutes > $maxmin) {
              $maxmin = $minutes;
              $oldfile = $_;
            }
            #print "found a file named $_ : max = $maxmin\n";  #debug

          } # end if -e

        } # end of file loop

        if ($maxmin > $maxFileMinutes) {
          `ls -ltr $oldfile &> $tmpErrFile`;
          my $str = "Ampexc import: " . sprintf("%6.1f", $maxmin). " min. old file: $oldfile, possible hang";
          print "$str\n";
          croakMail("$str", $tmpErrFile);
        }

    } # end of directory loop 

}

sub readConfig {
    # Get site specific configuration for addon web server, path, text
    my $cfgFile = ( $opt_c ) ? "$ENV{XDATA_HOME}/cfg/$opt_c" : "$ENV{XDATA_HOME}/cfg/xdata.cfg";
    open (CONFIG, "<$cfgFile")
        or die "Unable to open file with data exchange config: ${cfgFile}";

    my %aHash;  # for storing configuration

    while ( <CONFIG> )
    {
       chomp;
       next if /^(\s)*#/; # skip comments
       next if /^(\s)*$/; # skip blank lines
       my ($key, $val) = split /[:=,]+/;
       $key =~ s/^\s+//;  # remove leading spaces key
       $key =~ s/\s+$//;  # remove trailing spaces key
       $val =~ s/^\s+//;  # remove leading spaces value
       $val =~ s/#.*$//;  # remove any trailing comment text from value
       $val =~ s/\s+$//;  # remove trailing spaces from value
       $aHash{$key} = $val;
    }
    close CONFIG;

    return %aHash;
}

sub usage {
  print <<"EOF";
Usage:
  $0 [-h] [ -c cfgfile ] [-e tmperrfile] [-l logfile]
  -h            : usage
When running multiple import jobs, you should use options below:
  -c cfgfile    : alternative configuration file found in $ENV{XDATA_HOME}/cfg
                  default: xdata.cfg
  -e tmperrfile : alternative temporary text output file created in  $errRoot
                  default:  checker.tmp
  -l logfile    : alternative logfile scanned in ${ampxroot}/logs/import
                  default: importamps.log
EOF
   exit;
}
