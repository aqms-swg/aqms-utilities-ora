#!/bin/bash 
# 
# Stop the Strong Motion amplitude Exchange processes.
# Arg 1 is a file with a list of hosts for which to run jobs.

# First check runtime environment
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

appHome=${TPP_HOME}/ampexc
outputroot=${TPP_HOME}/ampexc

maxDaysLog=''
if [ -n "$1" ]; then
  maxDaysLog="$1"
fi

maxDaysData=''
if [ -n "$2" ]; then
  maxDaysData="$2"
fi

mydate=`date -u +"%F %T %Z"`

# One startAmpExc_YYYYMMDD.log per UTC day
logTimeStamp=$(date -u +"%Y%m%d")
startLog="${outputroot}/logs/start/startAmpExc_${logTimeStamp}.log"

if [ -e "${startLog}" ]; then
  echo "=========================================================="  >> ${startLog}
else
  echo "=========================================================="
fi

# ###### IMPORT #######
#
# Import definitions
#
importJob="${appHome}/bin/importamps"             # startup command
# string to grep for in the process list
importGrepStr="[D]importamps"             
# grep the pid list to see if the import job is running
#pid=`ps -ef | grep -v grep | grep "${importGrepStr}" | cut -c9-14`
pid=`pgrep -f "${importGrepStr}"`

# if job is running, STOP IT
if [ -n "$pid" ]; then
  if [ -e "${startLog}" ]; then
    echo "${mydate} Stopping ${importJob}" | tee -a ${startLog}
  else
    echo "${mydate} Stopping ${importJob}" 
  fi

  # stop the job
  kill -TERM $pid

fi


# ###### EXPORT #######
#
# Export definitions
#

exportJob="${appHome}/bin/exportamps"              # startup command
# string to grep for in the process list
exportGrepStr="exportamp[s]"
# look for the pid of the export job, if any
#pid=`ps -ef | grep -v grep | grep "${exportGrepStr}" | cut -c9-14`
pid=`pgrep -f "${exportGrepStr}"`

if [ -n "$pid" ]; then
  if [ -e "${startLog}" ]; then
    echo "${mydate} Stopping ${exportJob}" | tee -a ${startLog}
  else
    echo "${mydate} Stopping ${exportJob}"
  fi

  # stop the job
  kill -TERM $pid  

fi

# ========================================================
#
# Cleanup tasks
#
if [ -n "$maxDaysLog" ]; then
  if [ -e "${startLog}" ]; then
    echo "${mydate} Doing import/export cleanup..." | tee -a ${startLog}
    ${appHome}/bin/cleanupImport.sh "$maxDaysLog" "$maxDaysData" 2>&1 | tee -a ${startLog}
    ${appHome}/bin/cleanupExport.sh "$maxDaysLog" 2>&1 | tee -a ${startLog}
    ${appHome}/bin/cleanupStartLogs.sh "$maxDaysLog" 2>&1 | tee -a ${startLog}
    echo "=========================================================="  >> ${startLog}
  else
    echo "${mydate} Doing import/export cleanup..."
    ${appHome}/bin/cleanupImport.sh "$maxDaysLog" "$maxDaysData" 2>&1
    echo "----------------------------------------------------------"
    ${appHome}/bin/cleanupExport.sh "$maxDaysLog" 2>&1
    echo "=========================================================="
    ${appHome}/bin/cleanupStartLogs.sh "$maxDaysLog" 2>&1
    echo "=========================================================="
  fi
fi
