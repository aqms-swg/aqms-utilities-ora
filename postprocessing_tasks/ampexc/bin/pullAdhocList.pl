#!/usr/bin/perl
#
# Pulls the AdHoc list from the web
#
# Requires 'wget' and 'sort' 
#
# Uses files found in $cfgdir directory
#
 
use strict;      
use warnings;
use Getopt::Std;

our( $opt_h, $opt_v, $opt_s );
getopts('hvs');

my $adhocLog = "$ENV{TPP_HOME}/ampexc/logs/start/adhoc-wget.log";

my $cfgdir = "$ENV{TPP_HOME}/ampexc/cfg";
my $cfgfile = "${cfgdir}/adhocURL.cfg";
my $outfile  = "${cfgdir}/adhoc.tmp";
my $permfile = "${cfgdir}/allnets.adhoc";

my $URL;    

if ( $opt_h ) { usage(); }

#SunOS:
my $WGET = ( $^O eq 'solaris') ? "/usr/sfw/bin/wget" : "wget"; # local command path

# list of locations. Will be tried in this order
open(my $fh, "<", "${cfgfile}") or die "$0 : unable to open $cfgfile\n$!"; # open for input
my(@urllist) = <$fh>; # read file into list
close $fh;

# delete old working file
unlink $outfile; 

# wget - GNU command to download a file from a web server via http: or ftp:
# "-O -" directs output to STDOUT
foreach $URL (@urllist) {
    chomp $URL;
    if ($opt_v) {
      print "$0 : trying URL $URL\n";
    }

    if ( $opt_s ) { # send wget output to STDERR of invoker e.g. startup log
      `$WGET -O - $URL >> $outfile`;
    }
    else { # default is to send wget output to separate log
      `$WGET -o $adhocLog -O - $URL >> $outfile`;
    }

    # if 'wget' fails the file is empty
    last if ((-e $outfile) && (-s $outfile));   # bail if file exists and has non-zero length
}
if ( ! (-e $outfile) || ! (-s $outfile) ) {
    print "$0 Error: wget failed for all URLs\n";
    for $URL (@urllist) {
      chomp $URL;
      print " $URL\n";
    }
    exit;
}
# Replace old file with new
print "$0 : mv $outfile $permfile\n" if $opt_v;
`mv $outfile $permfile`;

exit;

sub usage {
    print "Copy an adhoc station list from a URL listed in $cfgfile\n";
    print "to $permfile, short-circuits URL loop upon successful copy\n";
    print "Options:\n";
    print "   -v : verbose, prints the loop URL value\n";
    print "   -h : usage help\n";
    print "   -s : wget output written to STDERR\n";
    print "        default is redirect to $adhocLog\n";

    exit;
}
