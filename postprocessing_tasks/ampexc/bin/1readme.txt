Notes on Amplitude Exchange

Purpose: automatic, near-realtime exchange of strong motion amplitudes among centers.
Primarily for ShakeMap.

Message Transport: 

sendfile2/getfile2 is the transport mechanism. This is done over the CISN ring.

The directory structure is:

    ${dataroot}/
        BERKELEY/indir
        BERKELEY/outdir
        CGS/indir
        CGS/outdir
        MENLO/indir
        MENLO/outdir

Amplitude file suffixes current convention is ".gBK" for Berkely,
".gMP" for Menlo Park and ".gCI" for Pasadena, and ".gCE" for CGS

Format of the files is GroMoPacket.

Export:

amp messages are created for new events by exportamps. This code does the following:

    a. Look for events posted the proper state (see Autoposter)
    b. For each new event
        i. Select all associated amps from the dbase. 
               Amp types sent are: PGD, PGV, PGA, SP03, SP10, SP30.
       ii. Compose GMP format messages.
      iii. Write the message files to the sendfile directories.

Import:

incoming messages are processed importamps. This code does the following:
    a. Poll import directories for new files
    b. Parse new message files
    c. Write amp records to UnassocAmp table
    d. Move processed message file to another directory
    e. Run association module

Association 

This process is initiated by gmp2db and is run at intervals to try to associate 
amp with updated events. It prioritizes amps so that recent amps are processed first. 
This code does the following:

    a. Attempt to associate amps in the UnassocAmp table with event/origins.
    b. If an association can be made:
       i. Transfer amp from the UnassocAmp table to the Amp table 
      ii. Write an AssocAmO row 
    c. Delete unassociated amp after an 'age-out' time has passed.


Autoposter

This runs as an Oracle job inside the dbase. It recognizes new events and posts
them to a state so they can be recognized by down-stream processes.

See: startAutoposterJob.sql

----------------------------------------------------------------------------

Nut & Bolts

root = $PP_HOME/tpp/ampexc (or $TPP_HOME/ampexc)

1) $root/bin/startAmpExchange

    Runs periodically as a cron job to start/restart amp exchange processes.

2) $root/bin/importamps

    continuously running import process

    Program: org.trinet.apps.Gmp2Db.java

    Config file: $root/import/cfg/importamps.props

3) $root/bin/exportamps

    continuously running export process

    Program: org.trinet.apps.Db2Gmp.java
    There is also considerable logic in the perl script.

    Config file: $root/export/cfg/exportamps.props

4) Log files are written to:

    $root/import/logs/import.log
    $root/export/logs/export.log
