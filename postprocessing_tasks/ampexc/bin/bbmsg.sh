#!/bin/bash
# Send BB report message
# NOTE: Called by checkampexchange.pl
export LINE=$1

# NOTE BB code uses BBHOME env dir def
#export BBHOME=/opt/bb
if [ -z "${BBHOME}" ]; then
    echo "ERROR $0 : BBHOME env variable for home directory is undefined." 
    exit
fi
if [ ! -d "${BBHOME}" ]; then
    echo "ERROR $0 : missing ${BBHOME} home directory." 
    exit
fi

. ${BBHOME}/etc/bbdef.sh
cd ${BBHOME}

#echo "$LINE"
bin/bb $BBDISP "$LINE"
