#!/bin/bash
# 
# Check job status of the Strong Motion amplitude Exchange processes.

# First check runtime environment
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

# setup 
appHome=${TPP_HOME}/ampexc
FLAGFILENAME=/app/cluster/I_AM_PRIMARY

# ###### IMPORT #######
importJob="${appHome}/bin/importamps"             # startup command

# string to grep for in the process list
importGrepStr="[D]importamps"             

# grep the pid list to see if the import job is running
pid=`ps -ef | grep "${importGrepStr}" | cut -c9-14`

if [ -e "${FLAGFILENAME}" ]
then
    echo I am primary for amplitude exchange
    if [ -n "${pid}" ]
    then
      echo ${importJob} is running with pid ${pid} 
    else 
      echo ${importJob} not running.
    fi
else
    echo I am on standby for amplitude exchange
    if [ -n "${pid}" ]
    then
      echo ${importJob} is running with pid ${pid}.
    else
      echo ${importJob} not running.
    fi
fi

# ###### EXPORT #######
exportJob="${appHome}/bin/exportamps"              # startup command

# string to grep for in the process list
exportGrepStr="exportamp[s]"

# grep for the pid of the export job, if any
pid=`ps -ef | grep "${exportGrepStr}" | cut -c9-14`

if [ -e "${FLAGFILENAME}" ]
then
    #    echo I am primary for amplitude exchange
    if [ -n "${pid}" ]
    then
      echo ${exportJob} is running with pid ${pid}
    else
      echo ${exportJob} not running.
    fi
else 
    #    echo I am on standby for amplitude exchange
    if [ -n "${pid}" ]
    then
      echo ${exportJob} is running with pid ${pid}.
    else 
      echo ${exportJob} not running.
    fi
fi
