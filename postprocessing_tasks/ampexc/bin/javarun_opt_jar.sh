#!/bin/bash


#!/bin/bash
#
# Runs java application class code found in the jiggle.jar file.
# Requires the application class and its args on the commandline
# after any other options, see usage()
#

# Setup AQMS environment in case of cron job run

if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

usage() {

  cat <<EOF

Usage:

  For testing, instead of using default jiggle jar instead
  run the java using this bin directory's ${TPP_HOME}/ampexc/bin/jiggle.jar 

  Syntax: javarun_jar.sh [-B -H -T -h -p -v] [ -- other java options ] <java class> [ class args ]
  -B          : fork java job to background and exit
  -H          : insert '-Djava.awt.headless=true' in java command string
  -T          : inserts value as a system property in java command string,
                for example, to set a tag so that pgrep can id process
  -v          : verbose, echo java command line
  -h          : this help usage

  NOTE: Must include "--" after this shell's options to add java command options before classname.
    
EOF
    exit $1
}

while getopts ":BHT:hpv" opt; do
  case $opt in
    B)
      opt_B='&'
      ;;
    H)
      opt_H='-Djava.awt.headless=true'
      ;;
    T)
      opt_T="-D$OPTARG"
      ;;
    h)
      usage 0 # non-error exit
      ;;
    v)
      opt_v='t'
      ;;
    \?)
      echo
      echo "$0 Error: Invalid command switch option: -$OPTARG"
      usage 1 # error exit
      ;;
    :)
      echo
      echo "$0 Error: Command option -$OPTARG requires an argument."
      usage 1 # error exit
      ;;
  esac
done

#Here use shift to offset input $@ to any remaining args that follow this shell's  switch options
shift $(($OPTIND - 1))

if [ $# -eq 0 ]; then
  echo
  echo "$0 Error: missing java application class arg"
  usage 2 # error exit
fi

# Define ALTERNATIVE local Jiggle jar for testing only:
cp="${TPP_HOME}/ampexc/bin/jiggle.jar:${TPP_HOME}/jiggle/bin/ojdbc14.jar:${TPP_HOME}/jiggle/bin/acme.jar"

if [ -d "${JAVA_BINDIR}" ]; then
  if [ -n "$opt_v" ]; then
    echo "${JAVA_BINDIR}/java -mx512m $opt_T $opt_H -cp ${cp} $@ $opt_B"
  fi
  if [ "$opt_B" == '&' ]; then
    ${JAVA_BINDIR}/java -mx512m $opt_T $opt_H -cp ${cp} "$@" &
  else
    ${JAVA_BINDIR}/java -mx512m $opt_T $opt_H -cp ${cp} "$@"
  fi
else
  if [ -n "$opt_v" ]; then
    echo "java -mx512m $opt_T $opt_H -cp ${cp} $@ $opt_B"
  fi
  if [ "$opt_B" == '&' ]; then
    java -mx512m $opt_T $opt_H -cp ${cp} "$@" &
  else
    java -mx512m $opt_T $opt_H -cp ${cp} "$@"
  fi
fi

# If not background job (opt_B) return the java command exit status
exit $?
