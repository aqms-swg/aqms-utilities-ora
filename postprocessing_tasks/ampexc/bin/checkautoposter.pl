#!/usr/bin/perl
#
# Syntax:  checkautoposter [dbasehost] [dbase]  [user] [passwd]
#
# Will check and report on all jobs in User_Jobs table that meet the Select criteria.
#
# Report will reflect the worst state of any job selected.
# Job details will appear on the detail BB webpage.

use strict;         # forces local scoping of variables via "my"
use warnings;

use DBI;            # Load the DBI module

# set default values
my $dbasehost = "unknown";
my $dbasename = "unknown";
my $username = "unknown";
my $passwd   = "unknown";

# falues from command line
if ( @ARGV > 0) {
  $dbasehost = $ARGV[0];
  $dbasename = $ARGV[1];  
  $username  = $ARGV[2];  
  $passwd    = $ARGV[3];  
}
else {
  print "Syntax: $0 <dbhost> <dbname> <dbuser> <dbpass>\n";
  exit;
}

my $stmt = "select job, what, last_date, next_date, broken, failures from DBA_JOBS Where WHAT like '%DoAutoPostJobs%'";

### Perform the connection using the Oracle driver
my $dbase    = "dbi:Oracle:$dbasename";
# note: operator and browser do NOT work (not defined on jet & spring?)
my $dbconn = DBI->connect( $dbase, $username, $passwd, {PrintError => 0}  )
  or croak("yellow", "Can't connect to Oracle database $dbasename <br> $DBI::errstr");

my $sql = $dbconn->prepare($stmt)
  or croak ("yellow", "Can't prepare statement:<br> $DBI::errstr");

#debug# print "$stmt\n";

# execute the prepared statment.
$sql->execute();

# data parsing
my @data;
my $jobNo;
my $jobName;
my $lastdate;
my $nextdate;
my $broken;
my $failures;

my $state = "green";
my $badText = "";
my $goodText = "";

while (@data = $sql->fetchrow_array()) {
  $jobNo    = $data[0];
  $jobName  = $data[1];
  $lastdate = $data[2];
  $nextdate = $data[3];
  $broken   = $data[4];
  $failures = $data[5];

  if ($broken eq "Y") {
    $state = "red";
    $badText = $badText . "&red <b>$jobNo  $jobName</b> last run = $lastdate  *BROKEN*\n";
    
  } elsif ($failures > 0) {
    if ($state eq "green") {$state = "yellow"};
    $badText = $badText . "&yellow <b>$jobNo  $jobName</b> last run = $lastdate  *BROKEN*\n";    
  } else {
    $goodText = $goodText . "&green $jobNo  $jobName last run = $lastdate\n";
  }
}

if ($sql->rows == 0) {
  croak ("red", "No autopost jobs in User_Jobs table on $dbasename");
} 

### Now, disconnect from the database
$dbconn->disconnect()
  or warn "Disconnection failed: $DBI::errstr\n";

my $allText = $badText . $goodText;

print "$allText\n";

BBreport ($state, $allText);

exit;

# -----------------------------------------------------------
# Send report to Big Brother
#
# Syntax:  BBreport(state, string);
#
sub BBreport
{   
    (my $myState, my $myText) = @_;
    my $myname = "autoposter";
    # my $localhost = `hostname`;
    # chomp $localhost;
    # $localhost =~ s/\..*$//; # grab first element

    my $date = `date -u +'%F %T %Z'`;
    chomp $date;

    #
    # hardwired BB server name
    my $BBserver = $ENV{"BBSERVER"};
    if ( ! ${BBserver} ) { die "Error: $0, BBSERVER environment variable undefined"; }
    if ( ! -x "$ENV{BBHOME}/bin/bb" ) { die "Error: $0, missing executable \"$ENV{BBHOME}/bin/bb\"\n$!"; }

    my $line = "status $myname.$dbasehost $myState $date  $myText";

    print "$line\n";
    

    my $bbcmd="$ENV{BBHOME}/bin/bb ${BBserver} \"'$line'\" ";
    
 #   print "$bbcmd\n";
    
    system( $bbcmd );
}

# -------------------------------------------------------------
# Send report to BB and exit
#
# Syntax:  croak(state, string);
#
sub croak
{
    (my $myState, my $myText) = @_;
    BBreport($myState, $myText);
    exit;
    
}
