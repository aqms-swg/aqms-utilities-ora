#!/bin/bash 
#
# delete all files older then maxDays back that start with a exportamps.log.
#

#First check runtime environment
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

maxDays=''
if [ $# -gt 0 ]; then
  maxDays=$1
fi

logRoot=${TPP_HOME}/ampexc/logs/export

echo $0 start at `date -u +"%F %T %Z"`
echo "  Cleaning ${logRoot}, max days to keep = ${maxDays} ..."
if [ -n "$maxDays" ]; then
  \find ${logRoot} -name "exportamps*.log*" -mtime +${maxDays} -exec \rm {} \;
else
  echo "  No logs deleted, no max days set"
fi
echo $0 done  at `date -u +"%F %T %Z"`
