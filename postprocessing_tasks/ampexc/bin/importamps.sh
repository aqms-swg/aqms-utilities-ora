#!/bin/bash
#
# Run the amplitude import java code input $1 is a properties file
#
#RE: javarun_jar.sh
# Use -H shell option is for headless graphics 
# The -T shell option is value used for a -D java property tag
# The -B shell option is value used for forking java app to background
#
# Must still test to see if Xvfb and -H logic below is  actually needed or not -aww

# check for Java 1.6 +
vers=`java -version 2>&1  | grep "java version" | awk -F\. '{print $2}'`
if [ -n "$vers" -a "$vers" -ge 6 ]; then
  # must use headless graphics option -H
  ${TPP_HOME}/jiggle/bin/javarun_jar.sh -H -B -Timportamps org.trinet.apps.Gmp2Db ${1}
else
  # "X Virtual Frame Buffer" (Xvfb) for graphics context ?
  xvfb=`pgrep -f '[X]vfb'`
  if [ -z "$xvfb" ]; then
    echo "Program requires Xvfb process to be running on host... exiting"
    exit
  fi
  export DISPLAY=127.0.0.1:1.0
  ${TPP_HOME}/jiggle/bin/javarun_jar.sh -B -Timportamps org.trinet.apps.Gmp2Db ${1}
fi
