#!/bin/bash 

# First check runtime environment
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

if [ -z "${XDATA_HOME}" ]; then 
    echo "XDATA_HOME env variable not defined check /usr/local/etc/aqms.env"
else
    ls ${XDATA_HOME}/io/*/indir
fi
