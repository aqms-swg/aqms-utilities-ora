#!/bin/bash 
#
# delete log files in ampexc logs/import, files in ampexc import/err,
# and files in ampexc import/done that are older then 'maxDays' 
#

#First check runtime environment
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

maxDaysLog=''
if [ $# -gt 0 ]; then
  maxDaysLog=$1
fi

maxDaysData=''
if [ $# -gt 1 ]; then
  maxDaysData=$2
fi

logRoot=${TPP_HOME}/ampexc/logs/import
##outputRoot=${TPP_HOME}/ampexc/import
outputRoot=${PP_HOME}/xdata/ampexc/import

echo $0 start at `date -u +"%F %T %Z"`

if [ -n "$maxDaysLog" ]; then
  echo "  Cleaning ${logRoot}, max days to keep = ${maxDaysLog} ..."
  \find ${logRoot} -name "*.log*" -mtime +${maxDaysLog} -exec \rm -f {} \;
  echo "   log  files left: `find ${logRoot} -type f | wc -l`"
else
  echo "  No logs deleted, no max days set"
fi

if [ -n "$maxDaysData" ]; then
  echo "  Cleaning ${outputRoot}/err, max days to keep = ${maxDaysData} ..."
  \find ${outputRoot}/err -mtime +${maxDaysData} -type f -exec \rm -f {} \;
  echo "   err  files left: `find ${outputRoot}/err -type f ! -name 'checker.tmp' | wc -l`"

  echo "  Cleaning ${outputRoot}/done, max days to keep = ${maxDaysData} ..."
  \find ${outputRoot}/done -mtime +${maxDaysData} -type f -exec \rm -f {} \;
  echo "   done files left: `find ${outputRoot}/done -type f | wc -l`"

else
  echo "  No data deleted from ${outputRoot} err/done, no max days set"
fi

echo $0 done  at `date -u +"%F %T %Z"`
