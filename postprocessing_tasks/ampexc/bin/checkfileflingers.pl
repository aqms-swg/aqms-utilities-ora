#!/usr/bin/perl
#
# Check File flinger jobs
#
# OUTPUT:
#
#  Writes text file for inclusion in BB report
#  'grep' this file for the word 'red' to look for trouble
#

use strict;
use warnings;

defined ( $ENV{XDATA_HOME} ) or die "Undefined environment variable XDATA_HOME, check aqms.env";

my $xdataroot = "$ENV{XDATA_HOME}/io";

my $flagfilename = "/app/cluster/I_AM_PRIMARY";

# two seperate chunks, good & bad, so we can put the bad at the top of
# the message and it will be sent in a page.
my $goodText = "";
my $badText = "";

# Data from which to build process names to look for
my $state = "green";

my %config = readConfig();

#my @dest = ("NC", "BK", "CE");
my @dest = split( "[:, ]+", $config{"xdata.io.dirs"} );
my $d;

# If primary system, first check for processes
if (-e $flagfilename) {

  #my @proc = ("getfileII", "sendfileII", "makehbfile");
  my @proc = split( "[:, ]+", $config{"xdata.io.jobs"} );
  my $procStr;
  my $result;
  # check jobs
  my $p; 
  for ($d = 0; $d <= $#dest; $d++) {
    for ($p = 0; $p <= $#proc; $p++) {

      $procStr = "$proc[$p].*$dest[$d]";

      #print "DEBUG XXXX: $procStr \n";
      $result = grepJob("$procStr");
  
      # concatenate new text to good/bad strings
      if ($result eq "") {
        $badText=$badText."&red Process missing: $procStr\n";
        $state = "red";      # group is bad if any bad   
      } else {
        $goodText=$goodText."&green $result\n";
      }
    }
  }
} else {
  $state = "clear";
  $goodText = "System on standby";
}

# If primary system, check heartbeat filenames
if ( -e $flagfilename ) {

  # local epoch time minus 5 min
  my $staletime = time - 5*60;

  my $file;
  my $mtime;
  my $ftime;

  #my @hbfile = ("${xdataroot}/BK/indir/9999999.hrtUCB", "${xdataroot}/NC/indir/9999999.hrtMP", "${xdataroot}/CE/indir/hb.cgs");
  my $h;
  for ($d = 0; $d <= $#dest; $d++) {
    #for ($h = 0; $h <= $#hbfile; $h++) {

      #$file = $hbfile[$h];
      $file = "${xdataroot}/$dest[$d]/indir/$config{qq(xdata.io.hb.${dest[$d]})}";

      # get time last modified
      $mtime = (stat $file) [10] or $mtime = 0;

      # concatenate new text to good/bad strings
      if ($mtime eq 0) {  # no file
        $badText=$badText."&red No heartbeat file: $file\n";   
        $state = "red";      # group is bad if any bad  
    
      } else { # have file
        $ftime = localtime($mtime);
        if ($mtime lt $staletime) {
          $badText = $badText."&red Stale heartbeat file: $file  $ftime\n";   
          $state = "red";      # group is bad if any bad        
        } else {
          $goodText=$goodText."&green Recent heartbeat file: $file  $ftime\n";
        }
      }
    #}
  }
}

# -----------------------------------------------------------
# Send the BB message
my $host = `uname -n`;
chomp $host;
$host =~ s/\..*$//; # grab first element

my $date = `date -u +'%F %T %Z'`;
chomp $date;

my $line = "status fileflinger.$host $state $date 

$badText

$goodText";

# DEBUG: Write to log after stripping out redundant linefeeds 
$_ = $line;
s/\n\n+/\n/g;
print "$_\n";

my $BBserver = "$ENV{BBSERVER}";
if ( ! ${BBserver} ) { die "Error: $0, BBSERVER environment variable undefined"; }
if ( ! -x "$ENV{BBHOME}/bin/bb" ) { die "Error: $0, Unable to send message, missing executable \"$ENV{BBHOME}/bin/bb\"\n$!"; }

# command text
my $bbcmd="$ENV{BBHOME}/bin/bb $BBserver '$line'";
#print "$bbcmd\n";

#send message
system( $bbcmd );

# ===========================================================

sub grepJob 
# grepJob(jobstring)
#
# Uses customized 'ps' output  "ps -eo user,stime,time,args". 
  {
    (my $jobStr) = @_;
    my $str = `ps -eo user,stime,time,args  | grep -i "$jobStr" | grep -v "grep"`;
    chomp $str;
    return $str;
  }

sub readConfig {
    # Get site specific configuration for addon web server, path, text
    my $cfgFile = "$ENV{XDATA_HOME}/cfg/xdata.cfg";
    open (CONFIG, "<$cfgFile")
        or die "Unable to open file with data exchange config: ${cfgFile}";

    my %aHash;  # for storing configuration

    while ( <CONFIG> )
    {
       chomp;
       next if /^(\s)*#/; # skip comments
       next if /^(\s)*$/; # skip blank lines
       my ($key, $val) = split /[:=,]+/;
       $key =~ s/^\s+//;  # remove leading spaces key
       $key =~ s/\s+$//;  # remove trailing spaces key
       $val =~ s/^\s+//;  # remove leading spaces value
       $val =~ s/#.*$//;  # remove any trailing comment text from value
       $val =~ s/\s+$//;  # remove trailing spaces from value
       $aHash{$key} = $val;
    }
    close CONFIG;

    return %aHash;
}
################################################################
