#!/bin/bash
# 
# Start/restart the Strong Motion amplitude Exchange processes.
# This is run at intervals by a cron job to insure that the jobs start at boot
# time and are always present.
# 
# crontab -e 
#0,10,20,30,40,50 * * * * ${TPP_HOME}/ampexc/bin/startAmpExchange >/dev/null 2>&1
#
# For crontab job we need to source the environment variables, does oracle too

# First check runtime environment
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

usage() {

  cat <<EOF

Usage:

  Start/restart the Strong Motion amplitude import process for CSN (CJ) amps.
  This is run at intervals by a cron job to insure that the jobs start at boot
  time and are always present.
  
  Syntax: $0 [-h] [ -c maxDaysLog (import)] [ -k maxDaysData (import)] [-x]

  -h          : this help usage
  -c days     : max days back to keep logs (default = forever)
  -k days     : max days back to keep imported data packets (default = forever)
  -x          : no sendfile/getfile task check, status email, or BB status update (mode used for tests)

EOF
    exit $1
}

#NOTE: Amp exchange home directory should be in aqms, not rtem 
appHome=${TPP_HOME}/ampexc

# One startAmpExc_YYYYMMDD.log per UTC day
logTimeStamp=$(date -u +"%Y%m%d")
startLog="${appHome}/logs/start/startAmpExcCJ_${logTimeStamp}.log"

# Maximum days to keep logs
maxDaysLog=''

# Maximum days to keep gmp packets
maxDaysData=''

# Flag to enable task checking (1 => do task checking)
xcheck=1

while getopts ":c:hk:x" opt; do
  case $opt in
    c)
      maxDaysLog="$OPTARG"
      ;;
    h)
      usage 0 # non-error exit
      ;;
    k)
      maxDaysData="$OPTARG"
      ;;
    x)
      xcheck=0 # disable task check
      ;;
    \?)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Invalid command switch option: -$OPTARG" | tee -a ${startLog}
      usage 1 # error exit
      ;;
    :)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Command option -$OPTARG requires an argument." | tee -a ${startLog}
      usage 1 # error exit
      ;;
  esac
done

#Here use shift to offset input $@ to any remaining args that follow this shell's  switch options
shift $(($OPTIND - 1))

mailaddr=${TPP_ADMIN_MAIL}
host=`uname -n | cut -d'.' -f1`

#FLAGFILENAME=/app/cluster/I_AM_PRIMARY # only start if host system is PRIMARY for send/get files
FLAGFILENAME=/etc/profile # forces startup of jobs, they should just result PCS postings when not PRIMARY

# ###### IMPORT #######
#
# Import definitions
#
importJob="${appHome}/bin/importamps.sh"              # startup command
importProps="${appHome}/cfg/importampsCJ.props"  # props file  
importGrepStr="[D]importampsCJ" # string to grep for in the process list
#NOTE: importamps spawns a Gmp2Db java job that makes its own importamps.log file

# grep the pid list to see if the import job is running
#pid=`ps -ef | grep -v grep | grep "${importGrepStr}" | cut -c9-14`
pid=`pgrep -f "${importGrepStr}"`
mydate=`date -u +"%F %T %Z"`

# Block wrapper to redirect echo output to startLog
{
  echo "=========================================================="
  if [ -f "$FLAGFILENAME" ]; then
    # if job is no running, START IT
    if [ -z "$pid" ]; then
      # NOTE: import job creates the importamps.log
      echo "${mydate} Starting ${importJob}"
      ${importJob} ${importProps} >> ${startLog} 2>&1 &      
  
      if [ -n "$mailaddr" ]; then
        sleep 3
        pid=`pgrep -f "${importGrepStr}"`
        if [ -n "${pid}" ]; then
            tail -2 ${startLog} | mailx -s "Restart importampsCJ on $host (pid=$pid)"  "$mailaddr"
        else 
            echo "ERROR: Unable to restart $importJob on $host"
            tail -6 ${startLog} | mailx -s "FAILED restart importampsCJ on $host"  "$mailaddr"
        fi
      fi
  
    else 
      echo "${mydate} importAmpsCJ job already running pid= $pid"
  
    fi
  else
    echo "${mydate} On standby, no importAmpsCJ job started"
  fi
  
  #
  # Cleanup tasks
  #
  mydate=`date -u +"%F %T %Z"`
  echo "${mydate} Doing cleanup..."
  ${appHome}/bin/cleanupImport.sh "$maxDaysLog" "$maxDaysData"
  echo "----------------------------------------------------------"
  ${appHome}/bin/cleanupStartLogs.sh "$maxDaysLog"
  echo "----------------------------------------------------------"
  
  # Get new addhoc list
  #echo "${mydate} Pulling adhoc list ..."
  #${appHome}/bin/pullAdhocList.pl -v
  
  echo "=========================================================="

} >> ${startLog} 2>&1  # redirected stdout/stderr
