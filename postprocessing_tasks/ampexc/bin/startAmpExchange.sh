#!/bin/bash
# 
# Start/restart the Strong Motion amplitude Exchange processes.
# This is run at intervals by a cron job to insure that the jobs start at boot
# time and are always present.
# 
# Property files for import/export should be in cfg subdirectory
#
# crontab -e 
#0,10,20,30,40,50 * * * * ${TPP_HOME}/ampexc/bin/startAmpExchange >/dev/null 2>&1
#
# For crontab job we need to source the environment variables, does oracle too

# First check runtime environment
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

usage() {

  cat <<EOF

Usage:

  Start/restart the Strong Motion amplitude Exchange processes.
  This is run at intervals by a cron job to insure that the jobs start at boot
  time and are always present.
  
  Syntax: $0 [-h] [ -c maxDaysLog (import/export)] [ -k maxDaysData (import)] [-e or -i] [-l tag] [-p] [-x]

  -h          : this help usage
  -e          : export only, default is both, cannot specify both -e or -i 
  -i          : import only, default is both
  -c days     : max days back to keep logs (default = forever)
  -k days     : max days back to keep imported data packets (default = forever)
  -l tag      : if specified, tag is appended to the import/export log and prop filenames e.g. importamps<tag>.log
  -p          : do not pull a new adhoc channel list file from web url
  -x          : no sendfile/getfile task check, status email, or BB status update (mode used for tests)

EOF
    exit $1
}

#NOTE: Amp exchange home directory should be in aqms, not rtem 
appHome=${TPP_HOME}/ampexc

# One startAmpExc_YYYYMMDD.log per UTC day
logTimeStamp=$(date -u +"%Y%m%d")
startLog="${appHome}/logs/start/startAmpExc_${logTimeStamp}.log"

# Maximum days to keep logs
maxDaysLog=''

# Maximum days to keep gmp packets
maxDaysData=''

# Flag to enable task checking (1 => do task checking)
xcheck=1
startImport=1
startExport=1
pullAdhoc=1
tag=''
while getopts ":c:ehik:l:px" opt; do
  case $opt in
    c)
      maxDaysLog="$OPTARG"
      ;;
    e)
      startImport=0
      ;;
    h)
      usage 0 # non-error exit
      ;;
    i)
      startExport=0
      ;;
    k)
      maxDaysData="$OPTARG"
      ;;
    l)
      tag="$OPTARG"
      ;;
    p)
      pullAdhoc=0
      ;;
    x)
      xcheck=0 # disable task check
      ;;
    \?)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Invalid command switch option: -$OPTARG" | tee -a ${startLog}
      usage 1 # error exit
      ;;
    :)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Command option -$OPTARG requires an argument." | tee -a ${startLog}
      usage 1 # error exit
      ;;
  esac
done

if [ $startImport -eq 0 -a $startExport -eq 0 ]; then
  echo "====================================================" >> ${startLog}
  echo "$0 Error: Cannot have both command option -e and -i." | tee -a ${startLog}
  usage 1
fi 

#Here use shift to offset input $@ to any remaining args that follow this shell's  switch options
shift $(($OPTIND - 1))

mailaddr=${TPP_ADMIN_MAIL}
host=`uname -n | cut -d'.' -f1`

#FLAGFILENAME=/app/cluster/I_AM_PRIMARY # only start if host system is PRIMARY for send/get files
FLAGFILENAME=/etc/profile # forces startup of jobs, they should just result PCS postings when not PRIMARY

# ###### IMPORT #######
#
# Import definitions
#
#importJob="${appHome}/bin/importamps$tag.sh"              # startup command
importProps="${appHome}/cfg/importamps$tag.props"  # props file  
importGrepStr="[D]importamps$tag" # string to grep for in the process list
importJob="${TPP_HOME}/jiggle/bin/javarun_jar.sh -H -B -Timportamps$tag org.trinet.apps.Gmp2Db $importProps"
importGrepStr="[D]importamps$tag" # string to grep for in the process list

#NOTE: importamps spawns a Gmp2Db java job that makes its own importamps.log file

# grep the pid list to see if the import job is running
#pid=`ps -ef | grep -v grep | grep "${importGrepStr}" | cut -c9-14`
pid=`pgrep -f "${importGrepStr}"`
mydate=`date -u +"%F %T %Z"`

# Block wrapper to redirect echo output to startLog
{
  echo "=========================================================="
  if [ -f "$FLAGFILENAME" ]; then
    # if job is no running, START IT
    if [ -z "$pid" ]; then
      # NOTE: import job creates the importamps.log
      if (( $startImport )); then
        echo "${mydate} Starting importamps$tag"
        ${importJob} >> ${startLog} 2>&1 &      
        msg="FAILED"
      else
        msg="SKIP"
      fi
  
      if [ -n "$mailaddr" ]; then
        sleep 3
        pid=`pgrep -f "${importGrepStr}"`
        if [ -n "${pid}" ]; then
            tail -2 ${startLog} | mailx -s "Restart importamps$tag on $host (pid=$pid)"  "$mailaddr"
        else 
            echo "$msg: restart importamps$tag on $host"
            if [ "$msg" == "FAILED" ]; then
              tail -6 ${startLog} | mailx -s "$msg restart importamps$tag on $host"  "$mailaddr"
            fi
        fi
      fi
  
    else 
      echo "${mydate} importamps$tag job already running pid= $pid"
  
    fi
  else
    echo "${mydate} On standby, no importamps$tag job started"
  fi
  
  # ###### EXPORT #######
  #
  # Export definitions
  #
  exportProps="${appHome}/cfg/exportamps$tag.props"   # props file
  exportJob="${appHome}/bin/exportamps.pl $exportProps" # startup command
  exportGrepStr="exportamp[s]$tag" # string to grep for in the process list
  #NOTE: Changed to have exportamps.pl create log -aww
  #exportLog="${appHome}/logs/export/exportamps.log"
  
  # look for the pid of the export job, if any
  #pid=`ps -ef | grep -v grep | grep "${exportGrepStr}" | cut -c9-14`
  pid=`pgrep -f "${exportGrepStr}"`
  
  mydate=`date -u +"%F %T %Z"`
  if [ -f "$FLAGFILENAME" ]; then
  # if job is not running, START IT
    if [ -z "$pid" ]; then
      #NOTE: Changed to have exportamps.pl create log 2012/06/29 -aww
      #logTimeStamp=$(date -u +"%Y%m%d%H%M")
      # rename the previous exportamps log file
      #if [ -f "${exportLog}" ]; then
      #  mv ${exportLog} ${exportLog}_${logTimeStamp}    
      #fi
      echo "----------------------------------------------------------"
      #${exportJob} >> ${exportLog} 2>&1 &  
      if (( $startExport )); then
        echo "${mydate} Starting exportamps$tag"
        ${exportJob} >> ${startLog} 2>&1 &
        msg="FAILED"
      else
        msg="SKIP"
      fi
      
      if [ -n "$mailaddr" ]; then
        sleep 3
        pid=`pgrep -f "${exportGrepStr}"`
        if [ -n "${pid}" ]; then
            tail -2 ${startLog} | mailx -s "Restart exportamps$tag on $host (pid=$pid)"  "$mailaddr"
        else 
            echo "$msg: restart exportamps$tag on $host"
            if [ "$msg" == "FAILED" ]; then
              tail -6 ${startLog} | mailx -s "$msg restart exportamps$tag on $host"  "$mailaddr"
            fi
        fi
      fi
  
    else
      echo "${mydate} exportamps$tag job already running pid= $pid"
  
    fi
  else
    echo "${mydate} On standby, no exportamps$tag job started"
  fi
  
  # ========================================================
  if [ "$1" == "nocleanup" ]; then
    echo "File cleanup and task status checking bypassed, exiting..." 
    exit;
  fi
  echo "----------------------------------------------------------"
  #
  # Cleanup tasks
  #
  mydate=`date -u +"%F %T %Z"`
  echo "${mydate} Doing cleanup..."
  ${appHome}/bin/cleanupImport.sh "$maxDaysLog" "$maxDaysData"
  echo "----------------------------------------------------------"
  ${appHome}/bin/cleanupExport.sh "$maxDaysLog"
  echo "----------------------------------------------------------"
  ${appHome}/bin/cleanupStartLogs.sh "$maxDaysLog"
  echo "----------------------------------------------------------"
  
  # Get new addhoc list
  if (( $pullAdhoc )); then  
    echo "${mydate} Pulling new adhoc list ..."
    ${appHome}/bin/pullAdhocList.pl -v
  else
    echo "${mydate} Adhoc list update skipped..."
  fi  

  # Checking tasks
  if (( xcheck )); then
    echo "${mydate} Doing status checking tasks..."
    # Update status on Big Brother & send email if there is trouble,shoube be done by nagios 2014/08/19 -aww
    #${appHome}/bin/checkfileflingers.pl
    if [ -n "$tag" ]; then
      tag="-l $tag"
    fi
    ${appHome}/bin/checkampexchange.pl $tag
  else
    echo "${mydate} Task checking disabled by option x"
  fi
  
  echo "=========================================================="

} >> ${startLog} 2>&1  # redirected stdout/stderr
