#!/bin/bash
# 
# NOTE: Instead use startRCGcont.sh/startRCGjobs.pl to start both the RCG_RT and RCG_TRIG jobs
# 
# Start/restart the continuous Request Card Generator (RCG) with input properties file
# This is run at intervals by a cron job to insure that the jobs start at 
# boot time and are always present.
#
# For crontab job we need to source the environment variables, does oracle too
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

if [ $# -eq 0 ]; then
    echo "Syntax $0 <cfg-file-name>"
    echo "       where name like: RCG_RT.props RCG_TRIG.props"
    exit;
fi

root=${TPP_HOME}/rcg

# One start<job>_YYYYMMDD.log per UTC day
logTimeStamp=$(date -u +"%Y%m%d")
logroot=${root}/logs
cfgFile="$1"
startLog=${logroot}/start/start${cfgFile%%.*}_${logTimeStamp}.log

mailaddr=${TPP_ADMIN_MAIL}
host=`uname -n | cut -d'.' -f1`

cd ${root}

# Command string to look for in the process list.  Must be UNIQUE on the machine!
# WARNING: a less specific string could fine processes editing the script, etc.
startJob="${root}/bin/runRCG.sh $cfgFile"
#                               ^
#                               CFG FILE NAME

# look for the pid of the active job, if any
pid=`pgrep -f -- "[ ]$cfgFile"`

# if no job running, START ONE
if [ -z "$pid" ]; then

    echo "Starting: ${startJob} at `date -u +'%F %T %Z'` on $host" >> ${startLog} 2>&1
    echo "============================================================"  >> ${startLog} 2>&1

    # fork the job
    ${startJob} &   

    # send email of success or failure
    if [ -n "$mailaddr" ]; then
      sleep 3
      pid=`pgrep -f "${grepStr}"`
      if [ -n "${pid}" ]; then
          tail -2 ${startLog} | mailx -s "Restart runRCG $cfgFile on $host (pid=$pid)"  "$mailaddr"
      else 
          echo "ERROR: Unable to restart RCG $cfgFile on $host" >>${startLog} 2>&1
          tail -6 ${startLog} | mailx -s "FAILED restart runRCG $cfgFile on $host"  "$mailaddr"
      fi
    fi

else # noop 

    echo "Running : ${startJob} pid=$pid at `date -u +'%F %T %Z'` on $host" >> ${startLog} 2>&1
    echo "============================================================"  >> ${startLog} 2>&1

fi
