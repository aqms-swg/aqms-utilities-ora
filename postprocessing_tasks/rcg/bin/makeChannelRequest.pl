#!/usr/bin/perl
# From cfg
use Time::Local;
use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use DbConn;
use MasterDbs;

use Getopt::Std;

our ( $opt_d, $opt_h, $opt_r, $opt_s );
getopts('d:hrs:');

# AQMS site specific values:
die "\nUndefined AQMS_NET_AUTH environment variable\n $!" if ! defined $ENV{AQMS_NET_AUTH}; 
my $auth=$ENV{AQMS_NET_AUTH};
my $subsrc='makeCR';
my $staauth=$auth;
# end of cfg values

sub usage {

  my $status = @_;

  print <<EOF;

  usage: $0 [-h] [-d dbase] [-r] <evid> <n.s.c.l> <ondate> <duration>

  Create a request_card table row for the input event id and net.sta.seedchan.location having
  specified window starting datetime and duration. Times specified must be integer seconds.
  Ondate format is <yyyy-mm-dd,HH:MM:SS> or <yyyy/mm/dd,HH:MM:SS>. A comma or a colon
  can separate the date and time string components. Location code defaults to '  ' when not
  specified, else use '--' string to specify blank code.


    -h        : print usage info

    -d dbase  : alias to use for db connection (defaults to "$masterdb")

    -r        : delete any pre-existing request_card table row matching input evid and n.s.c.l
                before doing insert, otherwise if row exists, ignore input timespan and row
                reset retry count to 0.

    -s src    : request_card subsource, default = $subsrc 

   example:
   $0 -r 123456 CI.RVR.HHZ.-- 2014-08-07,23:15:15 120

EOF
  exit $status;
}

if ( $opt_h ) { 
  usage 0;
}

if ( @ARGV < 4 ) {
  print STDERR "$0 Too few input args\n";
  usage(1);
}

#my ($evid, $net, $sta, $schan, $loc, $start, $dur ) = @ARGV;
my ($evid, $nscl, $start, $dur ) = @ARGV;
my ($net, $sta, $schan, $loc) = split /\./, $nscl;

$loc = '  ' if ! defined $loc;

if ( ! $net || ! $sta || ! $schan ) {
  print STDERR "$0 Malformed net.sta.chan.loc arg: $nscl\n";
  usage(1);
}
my ($year,$mon,$day,$hr,$mn,$sec,$sfrac) = split /\W+/, $start;
if ( ! $year || ! $mon || ! $day || ! defined $hr || ! defined $mn) {
  print STDERR "$0 Malformed start time arg: $start\n";
  usage(1);
}
$sec = 0 if ! defined $sec;
if ( $sfrac ) {
  print STDERR "$0 Malformed start time arg, must be integer seconds, no fraction: $start\n";
  usage(1);
}

if ( ! ($dur =~ m/^\d+\.?$/) ) {
  print STDERR "$0 Malformed duration secs arg should be integer: $dur\n";
  usage(1);
}

my $ssecs = timegm($sec,$mn,$hr,$day,$mon-1,$year-1900);
my $esecs = $ssecs + $dur;
my $rv = 1;
my $dbase = ( $opt_d ) ? $opt_d : $masterdb;
my $dbconn = DbConn->new($dbase)->getConn();
# replace - with blank
$loc =~ s/-/ /g;
$subsrc = $opt_s if $opt_s;
my $sql;
my $stm;
eval {
      if ($opt_r) {
        $sql = "delete from request_card where evid=? and net=? and sta=? and seedchan=? and location=?";
        $stm = $dbconn->prepare($sql);
        $stm->execute($evid,$net,$sta,$schan,$loc);
        $stm->finish();
      }
      $sql = "BEGIN ? := REQUESTCARD.make_t_request(?,?,?,?,?,?,?,?,?,truetime.putEpoch(?,'UNIX'),truetime.putEpoch(?,'UNIX'),?); END;";
      $stm = $dbconn->prepare($sql);
      # This is not portable - ODBC driver does it but few others # bind value must be passed by reference :. "\$rv"
      #print "make_t_request($evid, $auth, $subsrc, $staauth, $net, $sta, $schan, $loc, $schan, $ssecs, $esecs, 1)\n";
      $stm->bind_param_inout(1, \$rv, 80);
      $stm->bind_param( 2, $evid);
      $stm->bind_param( 3, $auth);
      $stm->bind_param( 4, $subsrc);
      $stm->bind_param( 5, $staauth);
      $stm->bind_param( 6, $net);
      $stm->bind_param( 7, $sta);
      $stm->bind_param( 8, $schan);
      $stm->bind_param( 9, $loc);
      $stm->bind_param(10, $schan);
      $stm->bind_param(11, $ssecs);
      $stm->bind_param(12, $esecs);
      $stm->bind_param(13, 1);
      $stm->execute();
      # autocommit is on      $dbconn->commit;
      #or die "Execute failed: $DBI::errstr\n";
      $stm->finish();
};
# Exception handler
if( $@ ) {
     warn "$DBI::errstr\n";
     $dbconn->rollback;
     $rv = 0;
};

$dbconn->disconnect or warn "Db disconnect failed: $DBI::errstr\n";
if ( $rv <= 0 ) {
  print "Failed to make request, return status= $rv\n";
  exit $rv
}

exit 0;
