#!/usr/bin/perl

use strict;
use warnings;

use DBI;            # Load the DBI module

# get masterdb and username/password
use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use MasterDbs;  # get default dbase info

# Delete all the request cards for this event
use Getopt::Std;
our ($opt_A, $opt_a, $opt_d, $opt_h, $opt_s, $opt_x);
getopts('Aad:hs:x');

# test for command args 
if ( $opt_h || (@ARGV == 0 && !($opt_a || $opt_A)) || (@ARGV > 0 && ($opt_a || $opt_A)) ) {
  print "Syntax: $0 [-d dbase ] <evid>\n";
  print "        $0 [-a|A] [-d dbase ]\n";
  print "\nDelete request rows from request_card for an input evid.\n";
  print "  -d name : alias of database with request_card table.\n";
  print "  -A      : delete ALL C and T requests, no event evid arg.\n";
  print "  -a      : delete ALL T type requests, no event evid arg.\n";
  print "  -s src  : request row subsource must match input option value.\n";
  print "  -h      : this usage.\n";
  print "  -x      : print sql statement and exit, no execution.\n";
  exit;
}
die "Cannot specify -a -A together" if ($opt_A && $opt_a);

# Parse evid
my $evid = $ARGV[0];

my $dbase = ($opt_d) ? $opt_d : $masterdb;

$dbase = "dbi:Oracle:$dbase";

my $dbconn = DBI->connect( $dbase, $masterdbuser, $masterdbpwd,{ PrintError => 1, RaiseError => 0, AutoCommit => 1 })
         or die "Can't connect to database: $DBI::errstr\n";

my $sql = "DELETE FROM request_card";
my $result = 0;
if ( $opt_A ) { # delete all C and A types
  $sql .= " WHERE subsource='$opt_s'" if $opt_s;
}
elsif ( $opt_a ) { # delete all T type
  $sql .= " WHERE request_type='T'"; 
  $sql .= " AND subsource='$opt_s'" if $opt_s;
}
else { # only those for evid
  $sql .= " WHERE evid=$evid"; 
  $sql .= " AND subsource='$opt_s'" if $opt_s;
}
if ($opt_x) { 
    print "$sql\n";
    exit;
}

$result = $dbconn->do($sql) or die "SQL 'do' failed: $DBI::errstr\n";
$result = 0 if $result == '0E0';

if ( $evid ) {
    print "$result requests deleted for $evid.\n ";
}
else {
    print "$result requests deleted.\n ";
}

$dbconn->disconnect or warn "Disconnection failed: $DBI::errstr\n";
