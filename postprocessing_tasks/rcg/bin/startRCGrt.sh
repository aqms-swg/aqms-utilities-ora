#!/bin/bash
# 
# NOTE: Instead use startRCGcont.sh/startRCGjobs.pl to start both the RCG_RT and RCG_TRIG jobs
# 
# Start/restart the continuous Request Card Generator (RCG)
# This is run at intervals by a cron job to insure that the jobs start at 
# boot time and are always present.
#
# crontab -e 
#0,10,20,30,40,50 * * * * ${TPP_HOME}/rcg/bin/startRCGrt > /dev/null 2>&1
#
# For crontab job we need to source the environment variables, does oracle too
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi
${TPP_HOME}/rcg/bin/startRCG.sh RCG_RT.props"
