#!/usr/bin/perl
#
# Generate "Request Cards" for a single event.
# Will do nothing if the event already has any waveforms archived
# or already has requests queued.
#
# TODO: 
#  - match up user defined db alias (or masterdb) with whats in cfg file
#  - allow user to spec cfg file?

use strict;
use warnings;

use DBI;            # Load the DBI module

# get masterdb and username/password
use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use DbConn;
use MasterDbs;

# parse optional switches
use Getopt::Std;
our ( $opt_d, $opt_h, $opt_q, $opt_o, $opt_r, $opt_s, $opt_w );
getopts('d:hqorsw');

my $dbase = $masterdb;
if ($opt_d) { $dbase = $opt_d };

if ($opt_h) { usage(0); };

if ( @ARGV < 1 ) { 
  print "ERROR rcgone: missing required input arguments\n";
  usage(1);
}

my $evid = $ARGV[0];
chomp $evid;
if ( ! ( $evid =~ m/\d+$/) ) {
  print "ERROR rcgone: required input event id argument must be an integer\n";
  usage(1);
}

my $cfgfile;
if ( @ARGV > 1 ) { 
  $cfgfile = $ARGV[1];
  chomp $cfgfile;
  if ( ! -e $cfgfile ) {
    print "ERROR rcgone: cfg file not found: $cfgfile\n";
    usage(1);
  }
}

# ####################################################################
# Sanity checks: don't run if waveforms or requests already exist
 
my @data;
my $evidCheck;
my $wfcount;
my $rcgcount;

my $dbh = DbConn->new($dbase)->getConn();

# Check #0
my $sql = $dbh->prepare( "Select evid from Event where evid = ?" );
$sql->execute($evid);

if ($sql) {
  $sql->execute($evid) or die "Query for evid failed";
  @data = $sql->fetchrow_array();
  $evidCheck = $data[0] || '0';
} else {
  $evidCheck = 0;
}

#if ($evidCheck != $evid) {
#      print "ERROR rcgone: event $evid is not in $dbase\n";
#      exit 1; 
#}

my $doChecks = ( $opt_s ) ? 0 : 1;
my $doWaveCheck = ( $opt_w ) ? 0 : 1;
my $doReqCheck = ( $opt_r ) ? 0 : 1;

if ( $doChecks && $doWaveCheck ) {

  # Check #1
  $sql = $dbh->prepare( "Select count(wfid) from AssocWaE where evid = ?" );
  $sql->execute($evid);

  if ($sql) {
      $sql->execute($evid) or die "Query for AssocWaE count failed";
      @data = $sql->fetchrow_array();
      $wfcount = $data[0] || '0';
  } else {
      $wfcount = 0;
  }

  if ($wfcount > 0) {
      print "INFO rcgone: no-op, event $evid already has $wfcount waveforms\n";
      exit 0; 
  }

} # done wf check

if ( $doChecks && $doReqCheck ) {
  # Check #2
  $sql = $dbh->prepare( "Select count(*) from Request_Card where evid = ?" );
  $sql->execute($evid);

  if ($sql) {
      $sql->execute($evid) or die "Querry for request_card count failed";
      @data = $sql->fetchrow_array();
      $rcgcount = $data[0] || '0';
  } else {
      $rcgcount = 0;
  }

  if ($rcgcount > 0) {
      print "INFO rcgone: no-op, event $evid already has $rcgcount request cards\n";
      exit 0;
  }

} # done req check

$sql->finish;
$dbh->disconnect or warn "Disconnection failed: $DBI::errstr\n";

# End of sanity check - Green light
# ####################################################################

# Run the code
#
if ( ! defined $cfgfile ) {
    $cfgfile = "$ENV{TPP_HOME}/rcg/cfg/rcgone-${dbase}.props";
    print "INFO rcgone: no props file argument specified, defaulting to: $cfgfile\n";
}

if ( ! -e $cfgfile ) {
    print "ERROR rcgone: cfg props file does not exist: $cfgfile\n";
    exit 1;
}

my $jrun = "$ENV{TPP_HOME}/jiggle/bin/javarun_jar.sh";
if (! -e $jrun) {
    print "ERROR rcgone: local java run file does not exist: $jrun\n";
    exit 1;
}

# Create logfile name:
my $timestamp = `date -u '+%Y%m%d-%H%M'`;
chomp $timestamp;

# Where to send std output, defaults to log, unless opt_o 
my $logStr='';
if ( ! $opt_o ) {
 $logStr = ">$ENV{TPP_HOME}/rcg/logs/rcgone/rcgone-${evid}-${timestamp}.log";
 print "INFO rcgone: output redirected to: $logStr\n";
}

# declare rcg java job to run
my $jobstr = "$jrun org.trinet.storedprocs.waveformrequest.RequestGenerator $evid $cfgfile $logStr 2>&1";

if ( $opt_q ) { # wait for job to complete
  print "INFO rcgone: waiting for the java RequestGenerator to finish ... \n";
  system("$jobstr");
}
else { # don't wait for java job just fork it to backgound, so java app exit status unknown
  print "INFO rcgone: job forked to background java RequestGenerator\n";
  system("$jobstr &");
}

# check last system call exit status 
my $estatus = 0;
if ($? == -1) {
     print "ERROR rcgone: failed to execute java job: $!\n";
     $estatus = 1;
}
elsif ($? & 127) {
     printf "ERROR rcgone: java child died with signal %d, %s coredump\n",
          ($? & 127),  ($? & 128) ? 'with' : 'without';
     $estatus = 1;
}
else {
    $estatus = ( $? >> 8); 
}

exit $estatus;
 
# -----------------------------------------------------------
# Help message about this script and how to use it
#
sub usage {

    my ($status) = @_;

    print <<"EOF";

    Generate request cards (archive waveform requests) for one event.
    The waveforms are later archived by a wavearchiver program.

    Does a no-op if the input event already has waveforms archived
    or already has requests queued. To disable no-op action for pre-
    existing waveforms or requests use options described below.

    usage: $0 [-d dbase] <evid> [java props file]
     
     -h        : print usage info
     -d dbase  : use this particular db alias (defaults to "$dbase")

     -q        : wait for java job to complete and exit with its exit status
                 otherwise fork java job to background and exit 

     -o        : send error and print output to standard out, default is a redirect to:
                 '$ENV{TPP_HOME}/rcg/logs/rcgone/rcgone-<evid>-<yyyymmdd-HHMM>.log'

     -r        : skip existing request cards check
     -w        : skip existing waveforms check

     -s        : skip both existing waveforms and request cards checks

    when not specified as command line argument the properties file default to:
        $ENV{TPP_HOME}/rcg/cfg/rcgone-<dbase>.props

    which is this case is:
        $ENV{TPP_HOME}/rcg/cfg/rcgone-${dbase}.props
         
    example: $0 -d mydb 8735346  cfg/request.props
EOF
    exit $status;

}
