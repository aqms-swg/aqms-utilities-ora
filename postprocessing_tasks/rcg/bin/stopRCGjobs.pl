#!/usr/bin/perl
#
# Stop RCG jobs.

use strict;
use warnings;

defined( $ENV{TPP_HOME} ) or die "Error: undefined env var TPP_HOME\n";

my $root="$ENV{TPP_HOME}/rcg";

my @joblist;
# Get job propfile names from cfg file, else default
if ( -e "$root/cfg/jobsRCG.cfg" ) { # override defaults
  open (my $jobfile, "<", "$root/cfg/jobsRCG.cfg");
  foreach my $line (<$jobfile>) {
    $line =~ s/#.*//;
    next if $line eq '';
    chomp($line);
    push(@joblist, split(' ',$line));
  }
  close $jobfile;
}
else { # default
  @joblist = ("RCG_RT.props","RCG_TRIG.props"); 
}

# One stop_YYYYMMDD.log per UTC day
my $logTimeStamp=`date -u +"%Y%m%d"`;
chomp $logTimeStamp;
my $logroot = "${root}/logs";
my $stopLog = "${logroot}/start/stopRCG_${logTimeStamp}.log";

open(STDOUT, ">>", "$stopLog") or die "Cant' open log $stopLog\n";
#redirect and flush
open(STDERR, ">&", *STDOUT) or die "Cant' redirect STDERR to $stopLog\n $!";
select(STDERR);
$| = 1;
select(STDOUT);
$| = 1;

my $npids = 0;
my $gmt = gmtime();
chomp($gmt);

foreach my $job ( @joblist ) {
  open( my $fh, "-|", "ps -ef" ) or die "Can't open pipe from ps: $!";
  while( <$fh> ) {
    if (/-D${job}/) {
      $npids++;
      print $_;
      my @fields = split /\s+/, $_;
      my $pid = $fields[1];
      print "Killing $job with pid=$pid $gmt\n";
      system `kill -TERM $pid`;
    }
  }
  close( $fh );
}

if ($npids == 0) {
  print "The RCG is not running $gmt\n";
}
print "\n";
exit;
