#!/usr/bin/perl
# ####################################################################
# Wrapper for running the org.trinet.apps.NewTrig java class application
# The default properties file is:  ../cfg/newtrig.props
# Use the -h command line switch to print more info about the java app's options.

use strict;
use warnings;

# Process the command line arguments
# The perl interpreter "consumes" the line and strips off the quotes
# so you must do this to re-quote the time strings that contain a space
my $cmdString;
my $numArgs = $#ARGV + 1;
my $defPropsFile = "../cfg/newtrig.props";

# rebuild the command line
foreach my $argnum (0 .. $#ARGV) {
 if ( $ARGV[$argnum] =~ m/ /) {
   $cmdString .= '"'. $ARGV[$argnum] . '" ';  # put quotes back on
 } else {
   $cmdString .=  $ARGV[$argnum] . ' ';
 }
} 
# no args: show syntax, java app exits with -h option
if ($numArgs == 0) {$cmdString = "-h"}
if ( $cmdString =~ m/-h/ ) {
  print <<EOF;
  Create a new subnet trigger, or with option -i use an existing evid, and use
  the properties file configured channel time window model to create a set of 
  new channel waveform requests to be written to db.

  Default properties file: $defPropsFile.
  Use the 'model' property to declare which channel time window model to use:
  model=org.trinet.jasi.PowerLawTimeWindowModel
  model=org.trinet.jasi.NewTriggerChannelTimeWindowModel

  Runs a java application: org.trinet.apps.NewTrig, found in jiggle.jar.

EOF
  # Rest of help from java app doc
  print `$ENV{TPP_HOME}/jiggle/bin/javarun_jar.sh org.trinet.apps.NewTrig -h`;
  exit;
}

# Else actually run the command, check for properties option, if none use default
chdir "$ENV{TPP_HOME}/rcg/bin";

if ( $cmdString !~ m/-P/ ) {
  die "Missing default properties file $defPropsFile\n" if ! -f $defPropsFile;
  $cmdString .= " -P $defPropsFile";
}

# Create logfile name
my $timestamp = `date -u '+%Y%m%d%H%M'`;
chomp $timestamp;
my $logFile = "../logs/newtrig/newtrig-${timestamp}.log";

print "Output redirected to log file: $logFile\n";

# Wait for java runtime to return
system "$ENV{TPP_HOME}/jiggle/bin/javarun_jar.sh org.trinet.apps.NewTrig $cmdString >$logFile 2>&1";

exit ($? >> 8);
