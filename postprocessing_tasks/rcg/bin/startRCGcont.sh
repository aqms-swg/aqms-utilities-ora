#!/bin/bash
# 
# Wrapper to define aqms environment for continuous perl script job
# start/restart the continuous Request Card Generator (RCG) for events and triggers
# crontab -e 
#
#0,10,20,30,40,50 * * * * $ENV{TPP_HOME}/rcg/bin/startRCGcont.sh >/dev/null 2>&1
#

# For crontab job we need to source the environment variables, does oracle too
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

usage() {

  cat <<EOF

Usage:

  Start/restart two processes that create requests for database archiving of waveforms.
  This is run at intervals by a cron job to insure that the jobs start at
  boot time and are always present.

  One process handles the PCS state postings for events (eg. rcg_rt) and the other, 
  triggers (eg. rcg_trig).

  After checking job status script optionally purges logs files found in logs directory
  that are older than maximum days age value that can be specified with command line
  option -c.
  
  Syntax: $0 [ -c maxDaysBack ] [ -h ]

  -c days     : max days back to keep logs (default = forever)
  -h          : this help usage

EOF
    exit $1
}

appHome=${TPP_HOME}/rcg

# One startRCGcont_YYYYMMDD.log per UTC day
logdir=${appHome}/logs
logTimeStamp=$(date -u +"%Y%m%d")
startLog="${logdir}/start/startRCGcont_${logTimeStamp}.log"

cd ${appHome}

maxDaysBack=''

while getopts ":c:h" opt; do
  case $opt in
    h)
      usage 0 # non-error exit
      ;;
    c)
      maxDaysBack="$OPTARG"
      ;;
    \?)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Invalid command switch option: -$OPTARG" | tee -a ${startLog}
      usage 1 # error exit
      ;;
    :)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Command option -$OPTARG requires an argument." | tee -a ${startLog}
      usage 1 # error exit
      ;;
  esac
done

#Don't use shift to offset input $@ to any remaining args that follow this shell's  switch options
#shift $(($OPTIND - 1))

# Redirect print output from script to start log
${appHome}/bin/startRCGjobs.pl $@ >>${startLog} 2>&1 &
