#!/bin/bash 
#  
# Start up one continuous process that makes Event Request Cards.
# This can be run from the terminal or started by a cron job.
#
# #####################################################################
#
# % runRCG  RCG_RT.props

if [ $# -eq 0 ]; then
  echo "Usage: ${0} <name of prop_file found in ../cfg directory>"
  exit 1
fi

# Setup environment, if undefined,does oracle too
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

#
# Process identifier string - need some unique string visible to 'ps'
# So we use the props file name.
#
propFile=${1}

# NOTE: below variable value is used by the java code to find the dir 
#       where props files are found. The variable is: <prog-name>_HOME_DIR
#       which should match the appname property in the cfg/*.props file
#       and the databse APP_HOST_ROLE table, if there's one
propsHomeDir="RCG_CONT_USER_HOMEDIR=${TPP_HOME}/rcg/cfg"

# RequestGeneratorCont Class Switches:
# -h
# -s <state>      Override pcsStateName property
# -u <username>   Override dbaseUser property
# -p <password>   Override dbasePasswd property
# -P <Props-file>
# -e <evid> one-shot processing (terminates when done)
# -f <filename> containing evid list (terminates when done)
# -X do not write request cards to dbase (test mode)
#
# Script below does background fork with -B option, and -T option is for setting java property for a pgrep tag id
# The -- separates the shell options from the java command line options before the classname
echo "${TPP_HOME}/jiggle/bin/javarun_jar.sh -v -B -T${propFile} -- -D$propsHomeDir org.trinet.apps.RequestGeneratorCont -P $propFile"
${TPP_HOME}/jiggle/bin/javarun_jar.sh -v -B -T${propFile} -- -D$propsHomeDir org.trinet.apps.RequestGeneratorCont -P $propFile
