#!/bin/bash

if [ -z "$TPP_HOME" ]; then source /usr/local/etc/aqms.env; fi

path=`dirname $0`
$path/newtrig.pl "$@"

# Below logic is implemented in newtrig.pl script, defaults to cfg properties 
#cmdargs="$@"
#if [[ ( "$cmdargs" =~ -P ) ]]; then
#  $path/newtrig.pl $@
#else
#  $path/newtrig.pl -P $path/../cfg/newtrig.props $@
#fi
