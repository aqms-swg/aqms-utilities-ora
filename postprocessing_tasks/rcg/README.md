# Request Card Generator  
  
Sub-directory *bin* has the executable scripts while *cfg* has the configuration files.  

<span style="color: green;"> Note : The configuration or property files can be split up or combined as desired.</span>  
  

### *bin*  
  
*  ``rcgone.pl`` : Generate "Request Cards" for a single event. Will do nothing if the event already has any waveforms archived or already has requests queued.  
*  ``newtrig.pl`` : Wrapper for running the org.trinet.apps.NewTrig java class application.  
*  ``deleteRequests.pl`` : Delete all the request cards for this event.  
*  ``makeChannelRequest.pl`` : Create a request_card table row for the input event id and net.sta.seedchan.location having
  specified window starting datetime and duration.  
*  ``rcg1.sh`` : Source environment variables and call ``rcgone.pl``.    
*  ``newtrig.sh`` : Wrapper around ``newtrig.pl``.  
* ``runRCG.sh`` : Start up one continuous process that makes Event Request Cards.  
*  ``startRCGjobs.pl`` : Start up all RCG jobs.  
*  ``stopRCGjobs.pl`` : Stop RCG jobs.  
*  ``startRCG.sh`` : Start/restart the continuous Request Card Generator (RCG) with input properties file.  
*  ``startRCGrt.sh``, ``startRCGtrig.sh`` : Start/restart the continuous Request Card Generator (RCG) with ``RCG_RT.props`` and ``RCG_TRIG.props``. This is run at intervals by a cron job to insure that the jobs start at boot time and are always present. Use ``startRCGjob.pl`` to start both the jobs.  
*  ``startRCGcont.sh`` : Wrapper to define aqms environment for continuous perl script job start/restart the continuous Request Card Generator (RCG) for events and triggers, run as a cron job.  
  
### *cfg*   
  
*  ``rcgone.props`` : Primary config file for ``rcgone.pl``. Typically, several versions of this file are created, each corresponding to the database rcg connects to.  
*  ``PowerLaw_RCG_module.props`` : Secondary config file that defines the model, used for request card generation, and it's params.  
*  ``Trigger_RCG_module.props`` : An example of ``PowerLaw_RCG_module.props`` for a candidate channel list named RCG-TRINET. Shows how to create different versions of the ``PowerLaw_RCG_module.props`` based on candidate channel lists.  
*  ``RCG_TRIG.props`` : An example config file that contains params related to candidate channel list (RCG-TRINET).      
*  ``RCG_RT.props`` : An example config file that contains params related to candidate channel list (RCG_RT).  
*  ``newtrig.props`` : Config file for ``newtrig.pl``.  
*  ``pcs-cfg.prpps`` : The name of the pcs thread to use for request card generation. This is a file used at Caltech for convenience for how they have their processing workflow organized into silos.  
*  ``jobsRCG.cfg`` : Config file which lists the .props files for rcg processes to be started simultaneously, used by ``startRCGjobs.pl`` and ``stopRCGjobs.pl``.   
  

