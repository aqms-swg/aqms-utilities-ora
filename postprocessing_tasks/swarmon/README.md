# Swarmon 
  
**Author** : Pete Lombard  
**Modified by** : Allan Walter (2015/08)    


swarmon is a PCS client program for monitor for swarms of earthquakes in configured geographic regions. It is NOT designed for detecting swarms in arbitrary areas of a seismic network. swarmon processes events detected by the real-time system to determine if they constitute a swarm as specified in the configuration files. swarmon DOES NOT consider events that have been relocated
or deleted by post-processing programs such as jiggle.  
  
Detailed information regarding swarmon is available here https://aqms-swg.gitlab.io/aqms-docs/swarmon.html  
  
The executable scripts are stored in the sub-directory *bin* and the corresponding configurations in *cfg*.  
  
### *bin*  
  
*  ``swarmon.pl`` :  Also referred to simply as swarmon, is a perl script. It uses the perl event loop module Event.pm, available from cpan.org. Do not confuse this with the AQMS-provided Event.pm in TPP_BIN_HOME/perlmodules; they are entirely different code.  
  
There is one configurable parameter in the swarmon perl script. "$pager" specifies the path to a command-line program for delivering text messages to pagers. It expects a command line of:  
  
  ``pager recipient[,recipient,...] message``    
  
*  ``startSwarmon.sh`` : Start/restart the swarm monitor process. This is run at intervals by a cron job to insure that the jobs start at boot time and are always present.  
*  ``listSwarmEvents.pl`` : Print a summary of events found in swarm_events table
*  ``swarmonReport.sh`` : Runs ``listSwarmEvents.pl`` and emails output to a list of email addresses.  
*  ``regionDB.pl`` : Load region information into database.  
  

### *cfg*  
  
*  ``swarmon.cfg`` : ``swarmon`` uses as its main configuratiion file *cfg/swarmon.cfg*. This file specifies the general configuration parameters used by swarmon, as well as one or more swarm region files. The swarm region file(s) specifiy specific parameters for each geographic region within which swarms are expected. The actual geographic coordinates of the region is specified in the gazetteer_region database table.  
  
  swarmon uses the following database tables to store its working information:  
    
    swarm_events  
    swarm_state    
  
*  ``region.coords`` : Lists the expected three regions for swarm monitoring. region.coords was loaded into database gazetteer_region table using perl script ``regionDB.pl``, use with "-h" option to see the syntax.    
*  ``Brawley-Seismic.cfg``, ``Golden-Trout.cfg``, ``Salton-Buttes.cfg``, ``Coso.cfg``, ``Lavic-Lake.cfg``, ``Ubehebe.cfg`` : Configuration for each region defined in region.coords. These files are to be used as templates by users that can be renamed and  contents changed as needed. 
  


