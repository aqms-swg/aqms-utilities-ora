#!/usr/bin/perl
#
# VERSION 0.0.4: Changed deleteOldRegions sql to only delete rows matching names parsed from the input regions file -aww 2015/07/21
#                added -x option to print only those regions names parsed from the in input regions file -aww

use strict;
use warnings;
use DBI;
use File::Basename;
use Getopt::Std;

# get masterdb and username/password 
 use lib "$ENV{TPP_BIN_HOME}/perlmodules";
 use MasterDbs;

# Default configuration file: adjust as necessary
my $config = "../cfg/swarmon.cfg";

# Global variables
our($dbName, $dbUser, $dbPass, $dbh, $VERSION, %Regions );
our($opt_c, $opt_d, $opt_v, $opt_h, $opt_p, $opt_D, $opt_X, $opt_x );

$VERSION = "0.0.4";

##############################################################################
#    print_syntax    - print syntax and exit.
##############################################################################
sub print_syntax {
    my ($cmdname) = @_;
    print << "EOF";    
    $cmdname version $VERSION
    $cmdname : load region information into database
Syntax:
    $cmdname [-c config] [-d] [-p] [-D] [-v] region-file
    $cmdname [-c config] [-d] -X
    $cmdname -h
where:
    -d           : db name alias (e.g. archdb) overrides DB_NAME in config-file

    -D           : Delete all existing entries; used when reloading
                   the entire region list.
                   Normally $cmdname will only insert new rows.

    -X           : Format to stdout the region information found in db.
    -x           : Format to stdout region information found in db
                   matching region names parsed from region-file.

    -c config    : specify an alternate configuration file

    The default config file is $config
    -p           : populate mode; default does not populate any tables
    -h           : prints this help message.
    -v           : prints verbose messages related to sql actions.

NOTES:
    This program cannot reliably compare existing region locations in DB
    against new region locations. Instead, the user must extract the 
    existing list from the database using -X flag, and manually edit 
    the list to make changes to region coordinates.
    Then the old entries can be deleted while entering the new list,
    by running $cmdname again with the -D flag.
    Regions that are known to be new to DB may be entered without
    using -D and -X options.
EOF
   exit(0);
}


exit main();

# The main function
sub main {

    my ($me) = basename($0);

# Process command-line arguments
    getopts ('DXxc:d:hpv');
    
    print_syntax($me) if ($opt_h);
    print_syntax($me) if ($opt_D && $opt_X);
    unless (($opt_X && scalar(@ARGV) == 0) || scalar(@ARGV) == 1) {
      print_syntax($me);
    }
    
    if ($opt_D) {
      warn "You have selected to delete ALL entries from gazetteer_region table matching names in $ARGV[0]\n";
      print STDERR "Are you you sure? (yes or no)";
      my $yorn = <STDIN>;
      chomp $yorn;
      exit unless ($yorn eq 'yes');
    }

    $config = $opt_c if ($opt_c);
    parse_config($config);
    # allow user command override
    $dbName = $opt_d if ($opt_d);
    #die "Database name missing, specify -d <name> option or DB_NAME <name> in cfg\n" unless (defined $dbName);
    # default to local masterdb
    $dbName = $masterdb unless $dbName;
    
    $dbh = DBI->connect("dbi:Oracle:" . $dbName, $dbUser, $dbPass, 
            {RaiseError => 1, PrintError => 0, AutoCommit => 0});

    extractList() if ($opt_X); # exits when done
    
    parseRegionFile($ARGV[0]);

    extractList() if ($opt_x); # exits when done

    # Do something to the database
    # Put all this in an eval block so we commit everything or nothing
    eval {
      deleteOldEntries() if ($opt_D);
      if ($opt_p) {
        addNewEntries();
        $dbh->commit();
        warn "All entries committed\n" if ($opt_v);
      }
    };
    if ($@) {
      warn "Error while manipulating tables in $@;\tall changes rolled back.\n";
      $dbh->rollback();
    }
    $dbh->disconnect();
    return 0;
}


sub parse_config {
    my $config = shift;

    open CF, "< $config" or die "Error opening $config: $!\n";
    while (<CF>) {
        if (/^\s*DB_USER\s*=\s*(\S+)/) {
            $dbUser = $1;
        }
        elsif (/^\s*DB_PASSWORD\s*=\s*(\S*)/) {
            $dbPass = $1;
        }
        elsif (/^\s*DB_NAME\s*=\s*(\S*)/) {
            $dbName = $1;
        }
    }
    #die "DB_NAME missing\n" unless (defined $dbName);
    # from MasterDbs.pm
    $dbUser = $masterdbuser if ! defined $dbUser;
    die "DB_USER missing\n" unless ($dbUser);
    $dbPass = $masterdbpwd if ! defined $dbPass;
    die "DB_PASS missing\n" unless ($dbPass);

    return;
}

# Extract the list of region entries from the DB and format into a region file.
# This will allow humans to merge old and new entries.
# It must be done this way because there are no reliable "keys" to match
# old and new region entries: latitude and longitude are floating-point values
# which don't match well. region names are variable.
sub extractList {

    my $vals = (defined %Regions) ? join(",", map { $dbh->quote($_) } keys %Regions) : ''; 

    eval {
      my $sth;
      if ($vals) {
        $sth = $dbh->prepare("SELECT name FROM gazetteer_region WHERE name in ($vals) ORDER BY name");
      }
      else {
        $sth = $dbh->prepare(q{ SELECT name FROM gazetteer_region ORDER BY name });
      }
      my $sth2 = $dbh->prepare(q{
        SELECT lat, lon FROM TABLE 
        (SELECT border FROM gazetteer_region
         WHERE name = :name) 
        });

      $sth->execute();
      my $aryref = $sth->fetchall_arrayref({});
      if (scalar @$aryref == 0) {
        warn "no regions found in DB\n";
      }
      my ($lat,$lon,$name,$regref);

      foreach my $row (@$aryref) {
        $name = $row->{'NAME'};

        $sth2->bind_param(":name", $name);
        $sth2->execute();

        $regref = $sth2->fetchall_arrayref({});
        my $count = 0;
        foreach my $coord (@$regref) {
          $lat = $coord->{LAT};
          $lon = $coord->{LON};
          if ($count == 0) {
            printf "%6.4f %7.4f %s\n", $lat, $lon, $name;
          }
          else {
            printf "%6.4f %7.4f\n", $lat, $lon;
          }
          $count++;
        }
        print "\n";
      }
    };
    if ($@) {
      die;
    }
    $dbh->disconnect();
    exit;
}

     
sub parseRegionFile {
    my $file = shift;

    my ($name, $newname, $lat, $lon);

    open FH, "< $file" or die "Can't open $file: $!\n";

    while (<FH>) {
      chomp;
      if (/^\#/ or /^\s*$/) {
        undef $name;
        next;
      }
      $_ =~ s/^\s+//;
      $_ =~ s/\s+$//;
      ($lat, $lon, $newname) = split(/\s+/, $_, 3);
      if ($newname) {
        die "name <$newname> too long; max length 24\n" 
        if (length($newname) > 24);
        $name = $newname;
      }
    
      $Regions{$name} = [] unless exists $Regions{$name};
      push @{ $Regions{$name} }, $lat, $lon;
    }
    close FH;
    return;
}

sub deleteOldEntries {
    my ($action, $aryref);

    # We're being called inside an eval, but we handle our onw errors anyway
    eval {
      # below sql only deletes those regions parsed from input regions file, no others.
      my $vals = join(",", map { $dbh->quote($_) } keys %Regions);
      my $rows_deleted = $dbh->do("DELETE FROM gazetteer_region where name in ($vals)");
      print "$rows_deleted rows while $action\n" if ($opt_v);
    };
    if ($@) {
      warn "Error while deleting from gazetteer_region: $@\n";
      die "deleteOldEntries\n";  # Throw again so we quit and roll back
    }
    return;
}


sub addNewEntries {
    
    eval {
# gazetteer_region has a nested table. Proper way to manipulate nested tables
# is to access them directly (as in $sth2) and not indirectly through
# the parent table. So we first insert an empty table in the new row
# of the parent
      my $sth = $dbh->prepare(q{ INSERT INTO gazetteer_region (name, border) VALUES (:name, coordinates()) });

# Here we add rows to the nested table directly.
      my $sth2 = $dbh->prepare(q{
        INSERT INTO TABLE ( SELECT border FROM gazetteer_region WHERE name = :name) VALUES ( latlon( :lat, :lon ) ) });

      my ($lat, $lon);
      foreach my $name (keys %Regions) {
        warn "Inserting data for $name:\n" if ($opt_v);
        
        $sth->bind_param(":name", $name);
        $sth->execute();

        while (@{ $Regions{$name} } ) { 
          $lat = shift @{ $Regions{$name} };
          $lon = shift @{ $Regions{$name} };
          warn "\t$lat $lon \n" if ($opt_v);

          $sth2->bind_param(":name", $name);
          $sth2->bind_param(":lat", $lat);
          $sth2->bind_param(":lon", $lon);
          $sth2->execute();
        }
        warn "\n" if ($opt_v);
      }
    };
    if ($@) {
      warn "Error while inserting region data: $@\n";
      die "addNewEntries\n";
    }
    return;
}
