#!/usr/bin/perl
#
# Dump swarm_events info 
#
use strict;
use warnings;

use DBI;

# get masterdb and username/password
use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use DbConn;
use MasterDbs;

use Getopt::Std;
our ( $opt_C, $opt_c, $opt_d, $opt_h, $opt_i, $opt_n, $opt_s, $opt_t, $opt_x );
getopts('Ccd:hi:n:s:t:x') or die;

if ($opt_h) { usage(); }

if ( defined $opt_i && $opt_i !~ /^(\d+)$/ ) {
    print "\nError: -i $opt_i evid must be a number > 0\n";
    usage(1);
}
if ( $opt_t && $opt_s ) {
  print "\nError: specify either -t or -s option, not both\n";
  usage(1);
}

my $dbase = $masterdb;
if ($opt_d) { $dbase = $opt_d; }  

my $dbconn = DbConn->new($dbase)->getConn();
die "Unable to created db connection" if ! $dbconn;

my $nstr;
if ( $opt_n ) {
  my @names = split('[,:]', $opt_n);
  my $knt = scalar @names;
  my $idx = 0;
  $nstr = "(";
  foreach my $name (@names) {
    $nstr .= "region like \'$name\'";
    $idx++;
    $nstr .= " or " if ( $knt > 1 && $idx != $knt);
  }
  $nstr .= ")";
}

my $cnt=0;
my $sth;
my $sql;
if ( $opt_c || $opt_C ) {
  $sql = "select region,count(*) from swarm_events s, event e";
  $sql .= " where s.evid=e.evid and e.selectflag=1";
  $sql .= " and $nstr" if $opt_n;
  $sql .= " and datetime >= truetime.putString(\'$opt_t\')" if $opt_t;
  if ( $opt_s ) {
    my $now = time();
    my $start = $now - ($opt_s * (3600));
    $sql .= " and (datetime between truetime.putEpoch($start,'POSIX') and truetime.putEpoch($now,'POSIX'))\n";
  }
  $sql .= " and evid=$opt_i" if $opt_i;
  $sql .= " group by region order by region";
  print "$sql\n" if $opt_x;
  $sth = $dbconn->prepare($sql) or die "failure preparing SQL statement, use -x to print sql";
  $sth->execute();
  while (my @data = $sth->fetchrow_array()) {
    my $ii = 0;
    $cnt++;
    print "#REGION                   COUNT\n" if $cnt == 1;
    my $region = $data[$ii++] || ' ';
    my $knt = $data[$ii++] || ' ';
    printf("%-24s %6d\n", $region,$knt);
  }
}
if ( ! $opt_c ) {
  print "================================================\n" if $opt_C;
  $sql = "select s.evid,s.region,truetime.getString(s.datetime) dt, n.magnitude,n.magtype,o.lat,o.lon,o.depth,o.mdepth,o.rflag";
  $sql .= " from swarm_events s, event e, netmag n, origin o";
  $sql .= " where s.evid=e.evid and n.magid=e.prefmag and o.orid=e.prefor and e.selectflag=1";
  $sql .= " and $nstr" if $opt_n;
  $sql .= " and s.datetime >= truetime.putString(\'$opt_t\')" if $opt_t;
  if ( $opt_s ) {
    my $now = time();
    my $start = $now - ($opt_s * (3600));
    $sql .= " and (s.datetime between truetime.putEpoch($start,'POSIX') and truetime.putEpoch($now,'POSIX'))\n";
  }
  $sql .= " and s.evid=$opt_i" if $opt_i;
  $sql .= " order by s.region,o.datetime";
  print "$sql\n" if $opt_x;
  $sth = $dbconn->prepare($sql) or die "failure preparing SQL statement, use -x to print sql";
  $sth->execute();
  $cnt=0;
  while (my @data = $sth->fetchrow_array()) {
    my $ii = 0;
    $cnt++;
    print "#EVID        MAG    DATETIME               LAT      LON    Z   MZ F REGION                  \n" if $cnt == 1;
    my $evid = $data[$ii++] || ' ';
    my $region = $data[$ii++] || ' ';
    my $dt = $data[$ii++] || ' ';
    $dt =~ s/\//-/g;
    $dt =~ s/\ /,/;
    my $mag = $data[$ii++] || 0;
    my $magtype = $data[$ii++] || 'n';
    my $lat = $data[$ii++] || 0;
    my $lon = $data[$ii++] || 0;
    my $depth = $data[$ii++] || 0;
    my $mdepth = $data[$ii++] || 0;
    my $rflag = $data[$ii++] || '-';
    printf("%-10d %5.2f M%s %-19s %6.3f %8.3f %4.1f %4.1f %1s %-24s\n", $evid,$mag,$magtype,$dt,$lat,$lon,$depth,$mdepth,$rflag,$region);
  } # end of while loop
  print "No events found\n" if $sth->rows == 0;
}

# Disconnect from the database
$sth->finish;
$dbconn->disconnect 
  or warn "Disconnection failed: $DBI::errstr\n";
  
exit;

# -----------------------------------------------------------------
sub usage {

    my $status = shift;

    print <<"EOF";

    usage: $0 [-h] [-d dbase] [-cC] [-n region-names] [-i evid] [-t datetime] [-s hrsback]

    Print a summary of events found in swarm_events table
     
    -h           : print usage info
    -d dbase     : alias to use for db connection (defaults to "$masterdb")
    -c           : print only the total count of events found in regions
    -C           : print both total counts and list of events in region 
    -n regions   : list of region names to include, delimited by ',' or ':'
                   default is all regions
    -i evid      : list only specified evid
    -t datetime  : list only since specified datetime string

    -s hrsback   : list only events since hours back from now (not compatible with -t)
    
    example: $0 -n Coso 

EOF
    exit $status;
}
