#!/bin/bash
# 
# Start/restart the swarm monitor process.
# This is run at intervals by a cron job to insure that the jobs start at boot
# time and are always present.
# 
# crontab -e 
#0,10,20,30,40,50 * * * * /home/tpp/swarmon/bin/startSwarmon > /dev/null 2>&1
#
# Modified from Pete Lombard's original -aww 2015/07/25

# Setup AQMS environment
if [ -z "$PP_HOME" -o -z "$LD_LIBRARY_PATH" ]; then source /usr/local/etc/aqms.env; fi

mailaddr=$TPP_ADMIN_MAIL
maxDaysBack=30

# set flag to assume failure 
failed=1

# host used for messages
host=`uname -n`
host=${host%%\.*}

sendmail() {
    if [ -n "$mailaddr" ]; then
      subject="Restart of swarmon on $host"
      if [ $failed -gt 0 ]; then
        subject="$subject, failed"
      fi 
      mailx -s "$subject" "$mailaddr" <<< "$1"
    fi
}

usage() {

  cat <<EOF

  usage: $0 [-c maxDaysBack ]

  Starts swarmon job if not already running. Cleans log directory.

  -h             : this help usage
  -c days-back   : number of days back to keep logs (default=$maxDaysBack)

EOF
    exit $1
}

while getopts ":hc:" opt; do
  case $opt in
    c)
      maxDaysBack="$OPTARG" # days to keep logs
      ;;
    h)
      usage 0 # non-error exit
      ;;
    \?)
      echo
      echo "$0 Error: Invalid command switch option: -$OPTARG"
      usage 1 # error exit
      ;;
    :)
      echo
      echo "$0 Error: Command option -$OPTARG requires an argument."
      usage 1 # error exit
      ;;
  esac
done

#Here use shift to offset input $@ to any remaining args that follow this shell's switch options
shift $(($OPTIND - 1))

if [ -z "$PP_HOME" ]; then
  msg="$0 Error: AQMS environment not defined, exiting"
  echo $msg
  sendmail "$msg"
  exit 1
fi

root=$TPP_HOME/swarmon
if [ ! -d "$root" ]; then
  msg="$0 Error: directory $root does not exist, exiting"
  echo $msg
  sendmail "$msg"
  exit 1
fi

logdir=$root/logs
if [ ! -d "$logdir" ]; then
  msg="$0 Error: logs directory: $logdir does not exist, exiting"
  echo $msg
  sendmail "$msg"
  exit 1
fi

cd $root
# job config
config="cfg/swarmon.cfg"
if [ ! -f "$config" ]; then
  msg="$0 Error: Missing config file: $config, exiting" 
  echo $msg
  sendmail "$msg"
  exit 1
fi

if ! [[ $maxDaysBack =~ ^[0-9]+$ ]]; then 
  msg="$0 Error: invalid -c max days back to keep logs: $maxDaysBack"
  echo $msg
  sendmail "$msg"
  exit 1
fi

# create a date label for logs
logtag=`date -u +%Y%m%d`

timeStamp=`date -u +"%Y-%m-%d,%H:%M:%S"`

# block of i/o redirection to log file
{

echo "========================================================"


echo "$timeStamp Using primary db? `$TPP_BIN_HOME/amPrimaryDb.pl`"

grepStr="/swarmon.pl"
echo "$timeStamp Checking for $grepStr job on $host"
# check the for job in process table
pid=`pgrep -f "$grepStr"`

# job command
# use -t option to prefix alert message subject with TEST 
#startJob="bin/swarmon.pl -v -t $config"
# production, no -t option
startJob="bin/swarmon.pl -v $config"

# if job is not running, start it
failed=0
started=0
if [ "x$pid" == "x" ]; then
  echo "$timeStamp Starting: $startJob &"
  $startJob  &  
  # give a few seconds for process to start
  sleep 3
  # check and log job status
  timeStamp=`date -u +"%Y-%m-%d,%H:%M:%S"`
  pid=`pgrep -f "$grepStr"`
  if [ "x$pid" == "x" ]; then
    jobStr="$timeStamp FAILURE: swarmon job not found"
    failed=1
  else
    jobStr="$timeStamp SUCCESS: swarmon job started with pid= $pid"
    started=1
  fi
else
  jobStr="$timeStamp NO-OP: swarmon job already running with pid= $pid"
fi

echo "$jobStr"

if [ $started -gt 0 -o $failed -gt 0 ]; then
    sendmail "$jobStr"
fi

#========================================================
# Cleanup logs
if [ $maxDaysBack -gt 0 ]; then 
  echo "$timeStamp Doing cleanup, deleting files in $logdir older than $maxDaysBack days"
  find "$logdir" -type f -name '*.log' -mtime "+$maxDaysBack" -print -exec \rm '{}' \;
fi
#
echo "========================================================"
} >> $logdir/startSwarmon_$logtag.log 2>&1
#========================================================
