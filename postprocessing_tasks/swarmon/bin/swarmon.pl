#!/usr/bin/perl

# Continuously running PCS program to monitor for swarms of earthquakes.
# This program is intended to see events from the real-time PCS stream
# and to provide warnings to seismo staff about swarms of events.
# It is NOT intended to do a final analysis of event swarms after they
# have been reviewed by anaylists.
# If an event is detected by the RT system but is relocated into or out of
# a swarm region, that is not of concern to swarmon. This is intended only 
# as a first pass at swarm counting as the swarm is happening.
#
# Written by Pete Lombard, UC Berkeley 2009/08/20
#
# 0.0.4 version code modifications for CI processing - AWW 2015/07/21
# 0.0.5 version code modifications for CI processing - AWW 2015/09/25
# 0.0.6 adding tweet notification - JA 2016/10/05
#

use strict;
use warnings;

use English;
use Data::Dumper;
use DBI;
use Event;  # The PERL Event loop module from CPAN
use File::Copy;
use File::Basename;
use Time::Local;
use Getopt::Long;
Getopt::Long::Configure("no_ignore_case");
#
# get masterdb and username/password 
 use lib "$ENV{TPP_BIN_HOME}/perlmodules";
 use MasterDbs;

our ($VERSION, %params, %sregions, $pager);
our ($test, %testData, $testfile, $unittest);
our $amEnabledForPCS = "false";

$VERSION = "0.0.6";

our ($opt_d, $opt_h, $opt_f, $opt_t, $opt_v, $opt_u);

sub usage {

    my $retval = shift;

    print  <<"EOT";
usage: $PROGRAM_NAME [-h] [-f testfile] [ -t] <config-file>

    -h          : this help

    -d          : db name alias (e.g. archdb) overrides DB_NAME in config-file


    -f testfile : file of test events having PCS posts for ids in file
                  with lines of format:
                  <evid> <region-name> <origin-datetime-secs>

    -t          : prefix test mode messages with TEST

    -u          : unit test for distribution

    -v          : verbose, prints parsed config to STDOUT console

EOT
    exit $retval;
}

if ( ! GetOptions( "h"=>\$opt_h, "f=s"=>\$opt_f, "t"=>\$opt_t, "v"=>\$opt_v, "u"=>\$opt_u ) ) { 
  print STDERR "Error: Unknown command line option $!\n";
  usage(-1);
}

usage(0) if $opt_h;

$test = ($opt_t) ?  1 : 0;
$testfile = ($opt_f) ?  $opt_f : '';
$unittest = ($opt_u) ?  1 : 0;

# Configurable paramters; rarely need changing:
# $pager specifies a program for sending pager messages.
# It expects a command line of:
#   pager recipient[,recipient,...] message
# If the message is not provided on the command line, it will be read
# from stdin.
# If your paging command does not meet this expectation, make local
# changes to this code or provide a wrapper that emulates pager.
#
#$pager = "/usr/local/bin/pager";
$pager="$ENV{PP_HOME}/utils/paging/page.py";
# End of configurable paramters

exit main();

sub main {
    if (scalar @ARGV == 0) {
      print STDERR "Error: $PROGRAM_NAME missing config file input argument\n";
      usage(-1);
    }

    parse_config($ARGV[0]);

    if ($unittest) { 
        unittest();
        return 0;
    }

    $params{DB_NAME} = $opt_d if ($opt_d);
    #die "Database name missing, specify -d <name> option or DB_NAME <name> in cfg\n" unless (defined $params{DB_NAME});
    # default to local masterdb
    $params{DB_NAME} = $masterdb unless $params{DB_NAME};
    $params{PCS_TABLE} = $params{DB_NAME} unless defined $params{PCS_TABLE};

    rotateLog();

    # write the config files at startup into the log
    #open(my $FH, "<", $ARGV[0]) || die "Cannot open config file:$ARGV[0]\n";
    #logMessage("============== Contents of cfg file: $ARGV[0]:\n");
    #while ( my $line = <$FH> ) {
    #    logMessage( $line );
    #    if ( $line =~ /SWARM_CONF\s*=\s*(\S+)/ ) {
    #      my $sfile = "$params{CONF_DIR}/" . $1;
    #      open( my $SFH, "<", "$sfile") or die "cannot open $sfile\n";
    #      logMessage("============== Contents of region cfg file: $sfile:\n");
    #      while ( my $line2 = <$SFH> ) {
    #        logMessage( $line2 );
    #      } 
    #      close($SFH);
    #    }
    #}
    #close($FH);

    # Terminate the loop on uncaught exceptions:
    $Event::DIED = sub {
    Event::verbose_exception_handler(@_);
      Event::unloop_all();
      };
    
    # Restart any swarms in progress
    do_restarts();

    # Install signal handlers:
    # INT handler for clean exit
    my $int_handler = Event->signal(desc    => "SIGINT handler",
                    cb      => \&int_handler,
                    signal  => 'INT');
    my $term_handler = Event->signal(desc    => "SIGTERM handler",
                     cb      => \&int_handler,
                     signal  => 'TERM');

    # Install dir watcher
    my $dir_watcher = Event->timer(desc     => "PCS watcher",
                   cb       => \&check_pcs,
                   interval => $params{WATCH_INTERVAL});

    # Install housekeeping timer
    Event->timer(desc     => "Housekeeper",
         cb       => \&do_housekeeping,
         at       => timeNextDay());

    # Run the event loop forever
    Event::loop();
    return 0;
}

sub parse_config {
    my $file = shift;

    $params{DEBUG} = 0;
    open FH, "<$file" or die "Can't open $file: $!\n";
    while(<FH>) {
      print $_ if $opt_v;
      chomp;
      next if (/^\s*$/ or /^\#/); # skip empty lines and comments
      if (/DB_USER\s*=\s*(\S+)/) {
            $params{DB_USER} = $1;
      }
      elsif (/DB_PASSWORD\s*=\s*(\S+)/) {
            $params{DB_PASSWORD} = $1;
      }
      elsif (/DB_NAME\s*=\s*(\S+)/) {
            $params{DB_NAME} = $1;
      }
      elsif (/PCS_GROUP\s*=\s*(\S+)/) {
            $params{PCS_GROUP} = $1;
      }
      elsif (/PCS_TABLE\s*=\s*(\S+)/) {
            $params{PCS_TABLE} = $1;
      }
      elsif (/PCS_STATE\s*=\s*(\S+)/) {
            $params{PCS_STATE} = $1;
      }
      elsif (/WATCH_INTERVAL\s*=\s*(\d+)/) {
        $params{WATCH_INTERVAL} = $1;
      }
      elsif (/CONF_DIR\s*=\s*(\S+)/) {
        $params{CONF_DIR} = $1;
      }
      elsif (/LOG_DIR\s*=\s*(\S+)/) {
        $params{LOG_DIR} = $1;
      }
      elsif (/MAXDAYSTOKEEP\s*=\s*(\d+)/) {
        $params{MAXDAYSTOKEEP} = $1;
      }
      elsif (/DEBUG\s*=\s*(\d+)/) {
        $params{DEBUG} = $1;
      }
      elsif (/SWARM_CONF\s*=\s*(\S+)/) {
        parse_swarmConf($1);
      }
    }
    close FH;

    my $ok = 1;
    foreach my $param (qw{PCS_GROUP PCS_STATE WATCH_INTERVAL CONF_DIR LOG_DIR MAXDAYSTOKEEP}) {
      unless (exists $params{$param}){
        print STDERR "$param missing from $file\n";
        $ok = 0;
      }
    }
    die "Invalid configuration\n" unless ($ok);
    die "No Swarm regions defined\n" unless (scalar(keys %sregions) > 0);

    # from MasterDbs.pm
    $params{DB_USER} = $masterdbuser if ! defined $params{DB_USER};
    die "DB_USER missing\n" unless ($masterdbuser);
    $params{DB_PASSWORD} = $masterdbpwd if ! defined $params{DB_PASSWORD};
    die "DB_PASSWORD missing\n" unless ($masterdbpwd);

}


sub parse_swarmConf {
    my $file = "$params{CONF_DIR}/" . shift;
    my $ok = 1;

    unless (open SFH, "<$file") {
      print STDERR "parse_swarmConf: cannot open $file: $!\n";
      return;
    }
    my $sregion = {
    NOTIFY_MAIL => [],
    NOTIFY_PAGE => [],
    NOTIFY_TWEET => [],
    IN_PROGRESS => 0,
    };
    while (<SFH>) {
      print $_ if $opt_v;
      chomp;

      next if (/^\s*$/ or /^\#/); # skip empty lines and comments
      if (/NAME\s*=\s*(\S.+)/) {  # spaces allowed in swarm name
        $sregion->{NAME} = $1;
      }
      elsif (/REGION\s*=\s*(\S+)/) {
        $sregion->{REGION} = $1;
      }
      elsif (/MINMAG\s*=\s*(\S+)/) {
        $sregion->{MINMAG} = $1;
      }
      elsif (/MAXDEPTH\s*=\s*(\S+)/) {
        $sregion->{MAXDEPTH} = $1;
      }
      elsif (/DETECT_INTERVAL\s*=\s*(\S+)/) {
        $sregion->{DETECT_INTERVAL} = $1;
      }
      elsif (/BASE_RATE_THRESHOLD\s*=\s*(\S+)/) {
        $sregion->{BASE_RATE_THRESHOLD} = $1;
      }
      elsif (/TURNOFF_RATE_THRESHOLD\s*=\s*(\S+)/) {
        $sregion->{TURNOFF_RATE_THRESHOLD} = $1;
      }
      elsif (/INCREMENT\s*=\s*(\S+)/) {
        $sregion->{INCREMENT} = $1;
      }
      elsif (/RERATE_INTERVAL\s*=\s*(\S+)/) {
        $sregion->{RERATE_INTERVAL} = $1;
      }
      elsif (/NOTIFY_INTERVAL\s*=\s*(\S+)/) {
        $sregion->{NOTIFY_INTERVAL} = $1;
      }
      elsif (/NOTIFY_MAIL\s*=\s*(\S+)/) {
        push @{$sregion->{NOTIFY_MAIL}}, $1;
      }
      elsif (/NOTIFY_PAGE\s*=\s*(\S+)/) {
        push @{$sregion->{NOTIFY_PAGE}}, $1;
      }
      elsif (/NOTIFY_TWEET\s*=\s*(\S+)/) {
        push @{$sregion->{NOTIFY_TWEET}}, $1;
      }
    }
    close SFH;
    foreach my $param (qw{NAME REGION MINMAG DETECT_INTERVAL 
                  BASE_RATE_THRESHOLD INCREMENT RERATE_INTERVAL 
                  NOTIFY_INTERVAL TURNOFF_RATE_THRESHOLD}) {
      unless (exists $sregion->{$param}){
        print STDERR "$param missing from $file\n";
        $ok = 0;
      }
    }
    return unless ($ok);
    unless (exists $sregion->{MAXDEPTH}) {
      $sregion->{MAXDEPTH} = 800.0;
    }
    if ($sregion->{BASE_RATE_THRESHOLD} < $sregion->{TURNOFF_RATE_THRESHOLD}) {
      printf STDERR "%s turnoff rate (%s) is greater than base rate (%s)",
      $sregion->{REGION}, $sregion->{TURNOFF_RATE_THRESHOLD}, 
      $sregion->{BASE_RATE_THRESHOLD};
      print STDERR "skipping $sregion->{REGION}\n";
      return;
    }
    if ($sregion->{INCREMENT} < 1) {
      print STDERR "$sregion->{REGION} INCREMENT ($sregion->{INCREMENT}) illegal value; should at least 1.0\n";
      print STDERR "INCREMENT is a multiplicative factor applied to BASE_RATE to raise the threshold rate after the first swarm alarm\n";
      return;
    }
    
    $sregion->{CURRENT_RATE_THRESHOLD} = $sregion->{BASE_RATE_THRESHOLD};
    $sregions{$sregion->{REGION}} = $sregion;
    return;
}

# Since Event blocks signals while callbacks are in progress
# we can assume that swarm_state rows will be written if an signal
# arrives while processing an event. Therefore we can simply exit here.
sub int_handler {
    my $w = $_[0]->w;  # currently not used
    
    logMessage(timeString() . " caught signal; exiting\n");
    Event::unloop();
    exit(0);
}


sub openLog {
    open $params{LOGFH}, ">$params{logfile}" 
      or die "Cannot open $params{logfile}: $!\n";
    logMessage(timeString() . " Log file opened.\n");

    select $params{LOGFH};
    $| = 1;                       # force auto flush of output buffer

    logMessage(timeString() . " $PROGRAM_NAME version $VERSION " .
           (($test) ? " in TEST mode" : "") . ".\n");

    #for DEBUG of the params hash
    #logMessage("Params parsed from : $ARGV[0]\n");
    #for my $prop (sort keys %params) {
    #    logMessage( $prop . ' = ' . $params{"$prop"} . "\n" );
    #}
    #logMessage("=======  End of params =======\n\n");

    # Get runtime params in string
    {
      local $Data::Dumper::Varname = 'PARAMS';
      my $message = Dumper \%params;
      # remove password from text
      $message =~ s/(.* 'DB_PASSWORD' => ')(\w+)(.*)/$1xxx$3/ms;
      # print params string
      logMessage( $message );
      local $Data::Dumper::Varname = 'REGIONS';
      # print regions string
      logMessage( Dumper \%sregions );
    }
}

# rotateLog ();
# You can call this at startup to rename an old existing log
# and open a new one.
sub rotateLog {

    unless (exists $params{logfile}) {
      $params{logfile} = "$params{LOG_DIR}/swarmon.log";
    }
    if (-e $params{logfile}) {
        if (defined($params{LOGFH}) ) {
            close($params{LOGFH});
        }
        #UTC
        my $timestamp = `date -u +'%Y%m%d%H%M'`;
        chomp $timestamp;
        my ( $fname, $path, $suffix ) = fileparse("$params{logfile}", qr/\..[^.]*$/ );
        move($params{logfile}, "$path${fname}_${timestamp}${suffix}");
        #move $params{logfile}, $params{logfile}.$timestamp;
    }

    openLog();
}

# generate a date/time string for the time given, or current time if none given
sub timeString {
    my $time = shift;
    $time = time() unless (defined $time);
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) =
    gmtime($time);
    my $string = sprintf("%04d/%02d/%02d %02d:%02d:%02d",
             $year + 1900, $mon + 1, $mday, $hour, $min, $sec);
    return $string;
}

sub logMessage {
    my $message = shift;

    print { $params{LOGFH} } $message;
}

# return the Unix time at the start of tomorrow.
sub timeNextDay {
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = 
    gmtime(time() + 24 * 3600);
    
    my $time = timegm(0,0,0,$mday,$mon,$year);
    return $time;
}

# Event callback function to do daily housekeeping tasks:
# rotate log file; clean up old swarm DB table entries
sub do_housekeeping {
    my $w = $_[0]->w;
    
    rotateLog();

    if ( "$amEnabledForPCS" eq 'true' ) { # only run clean up if enabled to avoid replication errors
       cleanTables();
    }

    Event->timer(desc     => "Housekeeper",
         cb       => \&do_housekeeping,
         at       => timeNextDay());
    
    $w->cancel;  # Clean up my watcher
}

# Delete old entries from swarm_event table
sub cleanTables {
    
    return if ($params{MAXDAYSTOKEEP} <= 0);

    my $oldTime = time() - $params{MAXDAYSTOKEEP} * 24 * 3600;
    my $dbh;
    eval {
    $dbh = DBI->connect("dbi:Oracle:" . $params{DB_NAME}, $params{DB_USER},
                $params{DB_PASSWORD},
                {PrintError => 0,
                 RaiseError => 1,
                 AutoCommit => 0});

    my $sth = $dbh->prepare(q{
        DELETE FROM swarm_events 
        WHERE datetime < truetime.putEpoch(?, 'UNIX')
        });
    my $e_rows = $sth->execute($oldTime);
    logMessage(timeString() . " deleted $e_rows from swarm_events\n");
    $dbh->commit();
    };
    if ($@) {
      $dbh->rollback();
      $dbh->disconnect();
      logMessage(timeString() . " cleanTables error: $@\n");
      exit(1);
    }
    $dbh->disconnect();
    return;
}


# Check for new events in the PCS system; check if they are in a monitored
# region, check if they represent a swarm.
# Post the result back to PCS.
sub check_pcs {
    my $w = $_[0]->w;  # currently not used
    
    my ($dbh, $sth_next, $sth_event, $sth_post);
    my $again = 1;

    if ($testfile) {
      unless (%testData) {
        open IN, "< $testfile" or die "Can't open $testfile: $!\n";
        while (<IN>) {
          chomp;
          my ($inid, $tepoch, $tregion) = split;
          $testData{$inid} = {
            "REGION" => $tregion,
            "TEPOCH" => $tepoch,
          };
        }
        close IN;
      }
    }

    eval {
    # connect and prepare our statements; they might be used mutliple times
    $dbh = DBI->connect("dbi:Oracle:" . $params{DB_NAME}, $params{DB_USER},
                $params{DB_PASSWORD},
                {PrintError => 0,
                 RaiseError => 1,
                 AutoCommit => 0});
    $sth_next = $dbh->prepare(q{
        BEGIN
        :ret := PCS.next_id(:group, :table, :state, :delay);
        END;
    });
    $sth_event = $dbh->prepare(q{
        SELECT o.datetime, 
        geo_region.inside_border(:region, o.lat, o.lon) inside
        FROM event e, origin o, netmag n
        WHERE e.prefor = o.orid
        AND e.prefmag = n.magid
        AND n.magnitude >= :minmag
        AND o.depth <= :maxdepth
        AND e.evid = :evid
        });
    $sth_post = $dbh->prepare(q{
        BEGIN
        :ret := PCS.putResult(:group, :table, :evid, :state, :result);
        END;
    });
    };
    if ($@) {
      logMessage(timeString() ." check_pcs: database error: \n");
      return;
    }
    my $delay = 0;
    my $event_id = -1;
    my $isOutside = 1;
    # Keep getting events from PCS until there are none for us.
    # In rare cases this may take some time and violate the assumption
    # that event actions should be short, but this is where we want to be
    # in that case.
    do {
      eval {
        my $ret;
        $sth_next->bind_param(":group", $params{PCS_GROUP});
        $sth_next->bind_param(":table", $params{PCS_TABLE});
        $sth_next->bind_param(":state", $params{PCS_STATE});
        $sth_next->bind_param(":delay", $delay);
        $sth_next->bind_param_inout(":ret", \$event_id, 10);
        $sth_next->execute();
        
        if ($event_id == 0) { # No events to process at this time
          $dbh->disconnect() if ($dbh);
          $again = 0;
          return;
        }

        # Check for processing of test input file whose evids matching posted ids
        if ($testfile) {
          foreach my $region (sort keys %sregions) {
              if ($testData{$event_id}->{REGION} eq $region) {
                processSwarmEvent($dbh, $region, $event_id, $testData{$event_id}->{TEPOCH});
              }
          }
        }
        else { # production
          # check db for local host app processing status
          $amEnabledForPCS = `$ENV{TPP_BIN_HOME}/enabledForPCS.pl swarmon $params{PCS_STATE}`;  
          chomp $amEnabledForPCS; # may have newline at end
          if ( "$amEnabledForPCS" ne 'true' ) {
            logMessage(timeString() . " Not primary app host for swarmon $params{PCS_STATE} processing, a no-op for event:  $event_id\n");
          } 
          else { # check if event is inside configured regions 
            foreach my $region (sort keys %sregions) {
              my $minmag = $sregions{$region}->{MINMAG};
              my $maxdepth = $sregions{$region}->{MAXDEPTH};
              $sth_event->bind_param(":evid", $event_id);
              $sth_event->bind_param(":region", $region);
              $sth_event->bind_param(":minmag", $minmag);
              $sth_event->bind_param(":maxdepth", $maxdepth);
              $sth_event->execute();
              my $aryref = $sth_event->fetchall_arrayref({});
              next unless (scalar @$aryref);
              if ($aryref->[0]->{INSIDE}) { # needs monitoring, process it
                $isOutside = 0;
                my $otime = $aryref->[0]->{DATETIME};
                processSwarmEvent($dbh, $region, $event_id, $otime);
              }
            }
          }
        }

        logMessage(timeString() . " Event $event_id is NOT inside a configured swarm region, resulting post\n") if $isOutside;

        # result current id post
        my $result = 1;
        $sth_post->bind_param(":group", $params{PCS_GROUP});
        $sth_post->bind_param(":table", $params{PCS_TABLE});
        $sth_post->bind_param(":evid", $event_id);
        $sth_post->bind_param(":state", $params{PCS_STATE});
        $sth_post->bind_param(":result", $result);
        $sth_post->bind_param_inout(":ret", \$ret, 10);
        $sth_post->execute();
        $dbh->commit();
    };
    if ($@) {
        logMessage(timeString() ."check_pcs ($event_id) error: $@\n");
        if ($dbh->{Active}) {
          $dbh->rollback();
          $dbh->disconnect();
        }
        Event->unloop();
        exit(1);
    }
    } while($again); # another id posting in state table?
    $dbh->disconnect();

    return;
}

# process a new event for this swarm-name
sub processSwarmEvent {
    my ($dbh, $region, $event_id, $otime) = @_;

    my $sregion = $sregions{$region};
    # startTime is time from which we count events up to current time:
    my $now = time();
    my $startTime = $now - $sregion->{DETECT_INTERVAL} * 3600;
    if ($sregion->{IN_PROGRESS}) {
      $startTime = $sregion->{LAST_NOTIFY_TIME} if 
        ($startTime < $sregion->{LAST_NOTIFY_TIME});
    }
    my $interval = ($now - $startTime) / 3600;  # time in hours since startTime
    # Number of events required to declare swarm alarm:
    my $minCount = $sregion->{DETECT_INTERVAL} * $sregion->{CURRENT_RATE_THRESHOLD};
    $minCount = int($minCount + 0.5);
    logMessage(timeString() . " Event $event_id INSIDE region: $region, swarm ". (($sregion->{IN_PROGRESS}) ? "in progress" :
                    "not in progress") .  "; threshold count: $minCount, updating db ...\n"); # if ($params{DEBUG});
    eval {
      my $q_sth = $dbh->prepare(q{ SELECT evid FROM swarm_events WHERE evid=:evid and region=:region });
      $q_sth->bind_param(":evid", $event_id);
      $q_sth->bind_param(":region", $region);
      $q_sth->execute();
      my $aryref = $q_sth->fetchall_arrayref({});
      if (scalar(@$aryref) == 0) {
        my $sth = $dbh->prepare(q{
            INSERT INTO swarm_events (evid, region, datetime) 
            VALUES (:evid, :region, :datetime)
            });
        $sth->bind_param(":evid", $event_id);
        $sth->bind_param(":region", $region);
        $sth->bind_param(":datetime", $otime);
        $sth->execute();

      }
      else {
        logMessage( timeString() . " Event $event_id already in swarm_events table for region $region\n");
      }
      $q_sth->finish();  # just in case there's more to read...

      my $count = events_since($region, $startTime, $dbh);
      logMessage(timeString() . " $count events for $region since " .
           timeString($startTime) . "\n"); # if ($params{DEBUG});
      if ($count >= $minCount) {
        # Swarm alarm!
        my $rate = $count / $interval;
        notify($sregion, $rate, $startTime);

        delete_timers($sregion);
        $sregion->{IN_PROGRESS} = 1;
        $sregion->{LAST_NOTIFY_TIME} = $now;
        $sregion->{LAST_RERATE_TIME} = $now;
        $sregion->{LAST_RATE} = $rate;
        # Raise the rate threshold for next time:
        $sregion->{CURRENT_RATE_THRESHOLD} *= $sregion->{INCREMENT};

        set_renotify_timer($sregion);
        set_rerate_timer($sregion);
        save_state($sregion, $dbh);
      }
    };
    # processSwarmEvent is called from an eval block;
    # we don't have to close the connection because our caller will.
    if ($@) {
      $dbh->rollback();
      $dbh->disconnect() if ($dbh);
      die "processSwarmEvent($event_id) error: $@\n";
    }

    return;
}


# Delete any active timers for this swarm region
sub delete_timers {
    my $sregion = shift;

    my $count = 0;
    if (exists $sregion->{RENOTIFY_TIMER} and ! $sregion->{RENOTIFY_TIMER}->is_cancelled) {
      $sregion->{RENOTIFY_TIMER}->cancel;
      delete $sregion->{RENOTIFY_TIMER};
      $count++;
    }
    if (exists $sregion->{RERATE_TIMER} and ! $sregion->{RERATE_TIMER}->is_cancelled) {
      $sregion->{RERATE_TIMER}->cancel;
      delete $sregion->{RERATE_TIMER};
      $count++;
    }
    if ($count) {
      logMessage(timeString() ." removed $count timer(s) for $sregion->{REGION}\n");
    }
    else {
      logMessage(timeString() ." no timers to remove for $sregion->{REGION}\n");
    }
    return;    
}

# Set the renotify timer. Timer will be set to NOTIFY_INTERVAL after
# last_time if provided or after current time otherwise; but never
# earlier than current time.
sub set_renotify_timer {
    my $sregion = shift;
    my $last_time = shift;

    my $region = $sregion->{REGION};
    
    my $now = time();
    my $next_time = ((defined $last_time) ? $last_time : $now) + 
    $sregion->{NOTIFY_INTERVAL} * 3600;
    # If next_time is in the past (such as when swarmon has been shut down
    # for a long time, change next_time to ten minues from now, so that
    # any events queued in PCS can be processed.
    $next_time = $now + 600 if ($next_time < $now);

    # Set the timer 10 seconds later. This is to try to get the renotification
    # to happen AFTER a rerate (or termination). It looks dumb to announce
    # that a swarm is continuing and then immediately announce that it has
    # been terminated.
    $next_time += 10;

    my $timer = Event->timer(desc    => "renotify timer for $region",
                 cb      => \&renotify_cb,
                 at      => $next_time,
                 data    => $sregion);
    $sregion->{RENOTIFY_TIMER} = $timer;
    logMessage(timeString($now) ." sregion $region renotify timer set for ".
           timeString($next_time)."\n");

    return;
}

# The Event callback function to handle notification that a swarm may 
# be continuing.
sub renotify_cb {
    my $w = $_[0]->w;
    
    my $sregion = $w->data;
    my $dbh;
    eval {
    $dbh = DBI->connect("dbi:Oracle:" . $params{DB_NAME}, $params{DB_USER},
                $params{DB_PASSWORD},
                {PrintError => 0,
                 RaiseError => 1,
                 AutoCommit => 0});

    renotify($sregion, $dbh);
    save_state($sregion, $dbh);
    set_renotify_timer($sregion);
    };
    if ($@) {
      logMessage(timeString() . " renotify_cb error: $@\n");
      if ($dbh->{Active}) {
        $dbh->rollback();
        $dbh->disconnect();
      }
      exit(1);
    }
    $dbh->disconnect() if ($dbh);
    $w->cancel; # clean up this watcher since it won't be used again.
}

# Set the rerate timer. Timer will be set to RERATE_INTERVAL after
# last_time if provided or after current time otherwise; but never
# earlier than current time.
sub set_rerate_timer {
    my $sregion = shift;
    my $last_time = shift;
    
    my $region = $sregion->{REGION};
    my $now = time();
    my $next_time = ((defined $last_time) ? $last_time : $now) + 
    $sregion->{RERATE_INTERVAL} * 3600;
    # If next_time is in the past (such as when swarmon has been shut down
    # for a long time, change next_time to ten minues from now, so that
    # any events queued in PCS can be processed.
    $next_time = $now + 600 if ($next_time < $now);

    my $timer = Event->timer(desc    => "rerate timer for $region",
                 cb      => \&rerate_cb,
                 at      => $next_time,
                 data    => $sregion);
    $sregion->{RERATE_TIMER} = $timer;
    logMessage(timeString($now) ." sregion $region rerate timer set for ".
           timeString($next_time). "\n");
    
    return;
}

# the Event callback function for handling rerate actions.
sub rerate_cb {
    my $w = $_[0]->w;
    
    my $sregion = $w->data;
    my $dbh;
    eval {
    $dbh = DBI->connect("dbi:Oracle:" . $params{DB_NAME}, $params{DB_USER},
                $params{DB_PASSWORD},
                {PrintError => 0,
                 RaiseError => 1,
                 AutoCommit => 0});

    rerate($sregion, $dbh);
    # DEBUG
    logMessage("\trerate_cb: saving state for $sregion->{REGION}\n");
    save_state($sregion, $dbh);
    set_rerate_timer($sregion) if ($sregion->{IN_PROGRESS});
    };
    if ($@) {
      logMessage(timeString() . " rerate_cb error: $@\n");
      if ($dbh->{Active}) {
        $dbh->rollback();
        $dbh->disconnect();
      }
      exit(1);
    }
    $dbh->disconnect() if ($dbh);
    $w->cancel; # clean up this watcher since it won't be used again.
}

# perform restarts of eny swarms that were in process at last shutdown
sub do_restarts {

    my ($dbh);
    eval {
    $dbh = DBI->connect("dbi:Oracle:" . $params{DB_NAME}, $params{DB_USER},
                $params{DB_PASSWORD},
                {PrintError => 0,
                 RaiseError => 1,
                 AutoCommit => 0});

    foreach my $sr( keys %sregions ) {
        my $sregion = query_restart($dbh, $sr);
        next unless (defined $sregion);
        
        # Copy params from the configured sregion into the restart
        my $old = $sregions{$sregion->{REGION}};
        foreach my $param (qw{NAME DETECT_INTERVAL BASE_RATE_THRESHOLD 
                      INCREMENT RERATE_INTERVAL NOTIFY_INTERVAL
                      TURNOFF_RATE_THRESHOLD MINMAG MAXDEPTH NAME
                      NOTIFY_MAIL NOTIFY_PAGE NOTIFY_TWEET}) {
        $sregion->{$param} = $old->{$param};
        }

        if ($sregion->{IN_PROGRESS}) {
          set_renotify_timer($sregion, $sregion->{LAST_NOTIFY_TIME});
          set_rerate_timer($sregion, $sregion->{LAST_RERATE_TIME});
        }
        $sregions{$sregion->{REGION}} = $sregion;
    }
    };
    if ($@) {
      die "do_restarts error: $@\n";
      $dbh->disconnect() if ($dbh);
    }
    $dbh->disconnect();
    return;
}

# Query DB for restart info. Assumed to be called within
# an eval block, so we don't do much error handling here.
sub query_restart {
    my $dbh = shift;
    my $sr = shift;

    my $sth = $dbh->prepare(q{ 
    SELECT in_progress, last_notify, last_rerate, rate_threshold
        FROM swarm_state
        WHERE region = :region});
    $sth->bind_param(":region", $sr);
    $sth->execute();
    my $aryref = $sth->fetchall_arrayref({});
    if (scalar(@$aryref) == 0) {
      logMessage(timeString() . " no restart data found for $sr\n");
      return undef;
    }

    my $sregion = {};
    $sregion->{REGION} = $sr;
    $sregion->{IN_PROGRESS} = ($aryref->[0]->{IN_PROGRESS} =~ /t/i) ? 1 : 0;
    $sregion->{LAST_NOTIFY_TIME} = $aryref->[0]->{LAST_NOTIFY};
    $sregion->{LAST_RERATE_TIME} = $aryref->[0]->{LAST_RERATE};
    $sregion->{CURRENT_RATE_THRESHOLD} = $aryref->[0]->{RATE_THRESHOLD};

    $sth->finish(); # might be more rows although that would violate primary key constraint
    return $sregion;
}

# Save the state of the current region to the database to allow for restarts
# This should be called within an eval block to catch errors thrown by DBI
sub save_state {
    my $sregion = shift;
    my $dbh = shift;
    my $region = $sregion->{REGION};

    # First see if a row for this region exists
    my $q_sth = $dbh->prepare(q{ SELECT last_notify FROM swarm_state WHERE region = :region });
    $q_sth->bind_param(":region", $region);
    $q_sth->execute();
    my $aryref = $q_sth->fetchall_arrayref({});
    my $u_sth;
    if (scalar(@$aryref) == 0) {
      $u_sth = $dbh->prepare(q{
        INSERT INTO swarm_state (region, in_progress, last_notify,
                     last_rerate, rate_threshold)
        VALUES (:region, :inprog, :last_note, :last_rerate, :rate)
        });
    }
    else {
      $u_sth = $dbh->prepare(q{
        UPDATE swarm_state SET in_progress = :inprog, 
        last_notify = :last_note, last_rerate = :last_rerate, 
        rate_threshold = :rate
        WHERE region = :region
        });
    }
    $u_sth->bind_param(":region", $region);
    my $in_prog = ($sregion->{IN_PROGRESS}) ? "T" : "F";
    $u_sth->bind_param(":inprog", $in_prog);
    $u_sth->bind_param(":last_note", $sregion->{LAST_NOTIFY_TIME});
    $u_sth->bind_param(":last_rerate", $sregion->{LAST_RERATE_TIME});
    $u_sth->bind_param(":rate", $sregion->{CURRENT_RATE_THRESHOLD});
    $u_sth->execute();

    $q_sth->finish();  # just in case there's more to read...
    $dbh->commit();

    return;
}

# Send notifications that the swarm rate has exceeded a threshold
sub notify {
    my ($sregion, $rate, $startTime) = @_;

    my $message = (($test) ? "TEST " : "") .  "Swarm ";
    $message .= ($sregion->{IN_PROGRESS}) ? "ongoing ": "starts ";
    $message .= "for $sregion->{NAME}, ";
    $message .= ($sregion->{IN_PROGRESS}) ? "rate increase, " : "" ;
    $message .= sprintf("%.1f/hr M>=%.1f, since %s UTC.", $rate, $sregion->{MINMAG}, timeString($startTime));

    my $regnospace = $sregion->{NAME};
    $regnospace =~ s/\s+//g;
    my $tweet = (($test) ? "TEST " : "") .  "AutoEq #EarthquakeSwarm ";
    $tweet .= ($sregion->{IN_PROGRESS}) ? "rate increase ": "starts ";
    $tweet .= "for #$regnospace ";
    $tweet .= sprintf("%s UTC, %.1f/hr M>=%.1f. ", timeString($startTime), $rate, $sregion->{MINMAG});
    $tweet .= "http://www.scsn.org/index.php/earthquakes/earthquake-swarms/";

    if ($unittest) { 
        print "\n";
        print $tweet;
        print "\n";
        print "\n";
        print $message;
        print "\n";
        return;
    }

    my ($recipients, $cmd);
  EMAIL: {
      if (scalar @{ $sregion->{NOTIFY_MAIL} } > 0) {
        $recipients = join ' ', @{$sregion->{NOTIFY_MAIL}};
        logMessage(timeString() . " notify send mail to: $recipients\n");
        my $subject = (($test) ? "TEST " : "") . 
          "Swarm Alarm for $sregion->{NAME}";
        $cmd = "/bin/mailx -s \"$subject\" $recipients";
        unless (open MAIL, "| $cmd") {
          logMessage(timeString(time()) ." Error opening $cmd: $!\n");
          last EMAIL;
        }
        print MAIL $message or
          logMessage(timeString(time()) ." Error running $cmd: $!\n");
        close MAIL or
          logMessage(timeString(time()) ." Error finishing $cmd: $!\n");
      }
  }
  PAGE: {
      if (scalar @{ $sregion->{NOTIFY_PAGE} } > 0) {
        $recipients = join ',', @{$sregion->{NOTIFY_PAGE}};
        logMessage(timeString() . " notify send page to: $recipients\n");
        $cmd = "$pager -u $recipients -m \"$message\"";
        system($cmd) && logMessage(timeString(time()) ." Error running $cmd: $!\n");
        #unless (open PAGE, "| $cmd") {
        #  logMessage(timeString(time()) ." Error opening $cmd: $!\n");
        #  last PAGE;
        #}
        #print PAGE $message or
        #  logMessage(timeString(time()) ." Error running $cmd: $!\n");
        #close PAGE or
        #  logMessage(timeString(time()) ." Error finishing $cmd: $!\n");
      }
  }
  TWEET: {
      if (scalar @{ $sregion->{NOTIFY_TWEET} } > 0) {
        # Consider whether we want public and/or private tweets...
        if ($test) {
            $cmd = "$ENV{RT_HOME}/utils/bin/TweetQuakePrivate \"$tweet\"";
            system($cmd);
        } else {
            logMessage(timeString() . " notify send tweet\n");
            $cmd = "$ENV{RT_HOME}/utils/bin/TweetQuake \"$tweet\"";
            system($cmd) && logMessage(timeString(time()) ." Error running $cmd: $!\n");
        }
      }
  }

    $sregion->{LAST_NOTIFY_TIME} = time();

    return;   
}

# Send notifications that a swarm is continuing but not exceeding the 
# current threshold for NOTIFY_INTERVAL.
sub renotify {
    my $sregion = shift;
    my $dbh = shift;

    return unless $sregion->{IN_PROGRESS};

    my $region = $sregion->{REGION};
    my $startTime = $sregion->{LAST_NOTIFY_TIME};
    my $count = events_since($region, $startTime, $dbh);
    logMessage(timeString() ." renotify $count events for $region since " .
           timeString($startTime) . "\n"); # if ($params{DEBUG});

    my $rate = $count / $sregion->{NOTIFY_INTERVAL};

    my $message = (($test) ? "TEST " : "") .  "Swarm ongoing for $sregion->{NAME}, ";
    $message .= sprintf("%.1f/hr M>=%.1f, since %s UTC (re-alert at >= %.2f/hr).",
                  $rate, $sregion->{MINMAG}, timeString($startTime), $sregion->{CURRENT_RATE_THRESHOLD});

    my $regnospace = $sregion->{NAME};
    $regnospace =~ s/\s+//g;
    my $tweet = (($test) ? "TEST " : "") .  "AutoEq #EarthquakeSwarm ongoing for #$regnospace ";
    $tweet .= sprintf("%s UTC, %.1f/hr M>=%.1f. ", timeString($startTime), $rate, $sregion->{MINMAG});
    $tweet .= "http://www.scsn.org/index.php/earthquakes/earthquake-swarms/";
   
    if ($unittest) { 
        print "\n";
        print $tweet;
        print "\n";
        print "\n";
        print $message;
        print "\n";
        return;
    }

    my ($recipients, $cmd);
  EMAIL: {
      if (exists $sregion->{NOTIFY_MAIL}) {
        $recipients = join ' ', @{$sregion->{NOTIFY_MAIL}};
        logMessage(timeString() . " renotify send mail to: $recipients\n");
        my $subject = (($test) ? "TEST " : "") . 
          "Swarm continues for $sregion->{NAME}";
        $cmd = "/bin/mailx -s \"$subject\" $recipients";
        unless (open MAIL, "| $cmd") {
          logMessage(timeString(time()) ." Error opening $cmd: $!\n");
          last EMAIL;
        }
        print MAIL $message or
          logMessage(timeString(time()) ." Error running $cmd: $!\n");
        close MAIL or
          logMessage(timeString(time()) ." Error finishing $cmd: $!\n");
      }
  }
  PAGE: {
      if (scalar @{ $sregion->{NOTIFY_PAGE} } > 0) {
        $recipients = join ',', @{$sregion->{NOTIFY_PAGE}};
        logMessage(timeString() . " renotify send page to: $recipients\n");
        $cmd = "$pager -u $recipients -m \"$message\"";
        system($cmd) && logMessage(timeString(time()) ." Error running $cmd: $!\n");
      }
  }
  TWEET: {
      if (scalar @{ $sregion->{NOTIFY_TWEET} } > 0) {
        # Consider whether we want public and/or private tweets...
        if ($test) {
            $cmd = "$ENV{RT_HOME}/utils/bin/TweetQuakePrivate \"$tweet\"";
            system($cmd);
        } else {
            logMessage(timeString() . " notify send tweet\n");
            $cmd = "$ENV{RT_HOME}/utils/bin/TweetQuake \"$tweet\"";
            system($cmd) && logMessage(timeString(time()) ." Error running $cmd: $!\n");
        }
      }
  }
    $sregion->{LAST_NOTIFY_TIME} = time();
    return;
}

# Send notification that a swarn has terminated
sub notify_terminate {
    my $sregion = shift;
    my $rate = shift;

    my $message = (($test) ? "TEST " : "") . "Swarm ends for $sregion->{NAME}, $rate/hr M>=$sregion->{MINMAG}, since " .
            timeString($sregion->{TERMINATE_START_TIME}) . " UTC.";
   
    my $regnospace = $sregion->{NAME};
    $regnospace =~ s/\s+//g;
    my $tweet = (($test) ? "TEST " : "") .  "AutoEq #EarthquakeSwarm ends for #$regnospace from ";
    $tweet .= timeString($sregion->{TERMINATE_START_TIME}) . " UTC. ";
    $tweet .= "http://www.scsn.org/index.php/earthquakes/earthquake-swarms/";

    if ($unittest) { 
        print "\n";
        print $tweet;
        print "\n";
        print "\n";
        print $message;
        print "\n";
        return;
    }

     my ($recipients, $cmd);

  EMAIL: {
      if (exists $sregion->{NOTIFY_MAIL}) {
        $recipients = join ' ', @{$sregion->{NOTIFY_MAIL}};
        logMessage(timeString() . " notify_terminate send mail to: $recipients\n");
        my $subject = (($test) ? "TEST " : "") . 
          "Swarm Terminated for $sregion->{NAME}";
        $cmd = "/bin/mailx -s \"$subject\" $recipients";
        unless (open MAIL, "| $cmd") {
          logMessage(timeString(time()) ." Error opening $cmd: $!\n");
          last EMAIL;
        }
        print MAIL $message or
          logMessage(timeString(time()) ." Error running $cmd: $!\n");
        close MAIL or
          logMessage(timeString(time()) ." Error finishing $cmd: $!\n");
      }
  }
  PAGE: {
      if (scalar @{ $sregion->{NOTIFY_PAGE} } > 0) {
        $recipients = join ',', @{$sregion->{NOTIFY_PAGE}};
        logMessage(timeString() . " notify_terminate send page to: $recipients\n");
        $cmd = "$pager -u $recipients -m \"$message\"";
        system($cmd) && logMessage(timeString(time()) ." Error running $cmd: $!\n");
      }
  }
  TWEET: {
      if (scalar @{ $sregion->{NOTIFY_TWEET} } > 0) {
        # Consider whether we want public and/or private tweets...
        if ($test) {
            $cmd = "$ENV{RT_HOME}/utils/bin/TweetQuakePrivate \"$tweet\"";
            system($cmd);
        } else {
            logMessage(timeString() . " notify send tweet\n");
            $cmd = "$ENV{RT_HOME}/utils/bin/TweetQuake \"$tweet\"";
            system($cmd) && logMessage(timeString(time()) ." Error running $cmd: $!\n");
        }
      }
  }
    $sregion->{LAST_NOTIFY_TIME} = time();
    return;
}
# Evaluate the swarm rate and possibly alter the current threshold
# Called only when no alarms have been declared during this RERATE_INTERVAL.
sub rerate {
    my $sregion = shift;
    my $dbh = shift;
    
    my $region = $sregion->{REGION};
    my $startTime = $sregion->{LAST_RERATE_TIME};
    my $count = events_since($region, $startTime, $dbh);

    # The rate parameters we can consider are:
    my $current_rate = $count / $sregion->{NOTIFY_INTERVAL};
    my $last_alarm_rate = $sregion->{LAST_RATE};
    my $current_threshold = $sregion->{CURRENT_RATE_THRESHOLD};
    my $base_threshold = $sregion->{BASE_RATE_THRESHOLD};
    my $turnoff_threshold = $sregion->{TURNOFF_RATE_THRESHOLD};
    my $next_threshold = $current_threshold / $sregion->{INCREMENT};
    logMessage("\n" . timeString() ." rerate $count events for $region since " .
           timeString($startTime) . "\n");
    logMessage("\tcurrent_rate: $current_rate\n");
    logMessage("\tcurrent threshold: $current_threshold\n");
    logMessage("\tnext threshold: $next_threshold\n");
    logMessage("\tbase threshold: $base_threshold\n");
    logMessage("\tturnoff threshold: $turnoff_threshold\n");

    # Decide what to do about this swarm!
    if ($current_rate < $turnoff_threshold and $next_threshold < $base_threshold) {
      logMessage("\tswarm terminated; threshold set to base\n");
      $sregion->{IN_PROGRESS} = 0;
      $sregion->{CURRENT_RATE_THRESHOLD} = $sregion->{BASE_RATE_THRESHOLD};
      notify_terminate($sregion, $current_rate);
      delete_timers($sregion);
    }
    elsif ($current_rate < $base_threshold) {
      logMessage("\tchanging threshold to $next_threshold\n");
      $sregion->{CURRENT_RATE_THRESHOLD} = $next_threshold;
    }
    else {
      logMessage("\tno changes needed\n");
    }

    $sregion->{TERMINATE_START_TIME} = $startTime;
    $sregion->{LAST_RERATE_TIME} = time();
    return;
}

# Return number of events in a regions since $start
# Should be called within eval block to catch exceptions
sub events_since {
    my ($region, $startTime, $dbh) = @_;
    
    my $sth = $dbh->prepare(q{
    SELECT COUNT(*) count FROM swarm_events
        WHERE region = :region
        AND datetime > truetime.putEpoch(:time, 'UNIX')
    });
    $sth->bind_param(":region", $region);
    $sth->bind_param(":time", $startTime);
    $sth->execute();
    my $aryref = $sth->fetchall_arrayref({});
    if (scalar(@$aryref) == 0) {
      my $msg = "No events found for $region since " .
        timeString($startTime) . "; logic error?\n";
      Event::unloop();
      die $msg;
    }
    my $count = $aryref->[0]->{COUNT};
    return $count;
}

# Start on unit test options, beginning with just distribution of messages 
sub unittest {
    notify($sregions{"Coso-CVO"}, 0, "2016/10/05 18:20:01 UTC");
    #renotify($sregions{"Coso-CVO"}, 0, "2016/10/05 18:20:01 UTC");
    notify_terminate($sregions{"Coso-CVO"}, 0, "2016/10/05 18:20:01 UTC");
}
