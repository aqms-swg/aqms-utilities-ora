#!/bin/bash
# 
# Setup AQMS environment
if [ -z "$PP_HOME" -o -z "$LD_LIBRARY_PATH" ]; then source /usr/local/etc/aqms.env; fi
#mailaddr=$TPP_ADMIN_MAIL
# be better just to have mail group alias used:
mailaddr=email id1, email id2,...

datestr=`date -u +'%Y%m%d_%H%M'`
filename="/tmp/swarmon_${datestr}.$LOGNAME"
$TPP_HOME/swarmon/bin/listSwarmEvents.pl -C -s 24 >$filename 2>&1
primary=`$TPP_BIN_HOME/amPrimaryHost.pl`
if [ "$primary" == "true" ]; then
  mailx -s "Swarmon region's events in 24 hours before $datestr" "$mailaddr" < $filename
fi
