#!/bin/bash
#
# Send alarm messages via CMS for a new or modified event.
# If the file ${alarmDir}/cfg/sendCMS_${dbase}.cfg does not exist
# it is created.
#
# First check runtime environment
if [ -z "${TPP_HOME}" -o -z "${LD_LIBRARY_PATH}" ]; then . /usr/local/etc/aqms.env; fi

alarmDir=${TPP_HOME}/alarm
alarmUtilHome="${RT_HOME}/utils/alarm/"
if [ -n "$ALARM_UTIL_HOME" ]; then
    alarmUtilHome="${ALARM_UTIL_HOME}"
fi

# Is this path env redefinition needed?
#LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/openwin/lib:/usr/dt/lib:/usr/local/lib
#LD_LIBRARY_PATH=/app/aqms/pp/lib:$LD_LIBRARY_PATH

# Used to be:
#logfile="${alarmDir}/logs/alarmevent.log"
#
# Now separate send/cancel logs
logfile="${alarmDir}/logs/alarmsendCMS.log"

usage() {
  echo "Syntax: ${0} [-s] <evid> <dbasename>"
  echo "        Send alarm messages via CMS for a new or modified event."
  #echo "        -s : write CUBE E (update) to EIDS/polldir on this host"
  echo "        -s : send qml to product distribution PDL on this host"
  echo "        If file ${alarmDir}/cfg/sendCMS_${dbase}.cfg does not exist it is created."
  echo "        Output is logged to $logfile"
  exit $1 
}

# By default, =0, do not send CUBE msg via EIDS backdoor
doCUBE2EIDS=0

while getopts ":hs" opt; do
  case $opt in
    h)
      usage 0
      ;;
    s)
      doCUBE2EIDS=1
      ;;
    \?)
      echo "Error: Invalid command switch option: -$OPTARG" >&2
      usage 1
      ;;
    :)
      echo "Error: Command option -$OPTARG requires an argument." >&2
      usage 1
      ;;
  esac
done

#Here we use shift to offset input $@ to the rest of args following switch options
shift $(($OPTIND - 1))

if [ $# -lt 2 ]; then
   echo "Missing input arguments"
   usage 1;
fi

# for clarity
evid=${1}
dbase=${2}

cfgFile=${alarmDir}/cfg/sendCMS_${dbase}.cfg
if [ ! -e ${cfgFile} ]; then ${alarmDir}/bin/makeAlarmCfgCMS.sh ${dbase} update > ${cfgFile}; fi

# Block wrapper for redirect of print output to logfile
{
  echo " "
  echo "===================== Start send ${evid}"

  cnt=1
  # Make log file entry
  echo "From  $cnt: ${0} db=${dbase} evid=${evid} @ `date -u +'%F %T %Z'`"
  # reset all CANCELLED alarm action_statess to !CANCELLED
  rflag=`$TPP_BIN_HOME/getRflag.pl ${evid}`
  res=$?
  # don't check for cancelled actions if automatic processing state
  # because uncancel function converts  both 'A' or 'C' to rflag 'H'
  doit=1
  if [[ $res == 0 && -n "$rflag" ]]; then
    str=`$TPP_BIN_HOME/getstates.pl -d $dbase -i $evid TPP TPP ALARMSTIFLE`
    if [ $rflag == 'A' ]; then
      if [ -n "$str" ]; then
        doit=0
      fi
    else
      if [ -n "$str" ]; then
        echo "delstates -d $dbase -i $evid TPP TPP ALARMSTIFLE"
        $TPP_BIN_HOME/delstates.pl -d $dbase -i $evid TPP TPP ALARMSTIFLE
      fi
      cmd="$TPP_BIN_HOME/uncancel.pl -d ${dbase} ${evid}"
      echo "Event rflag=$rflag: $cmd"
      ${cmd}
      res=$?
      if [ $res -ne 0 ]; then
          echo "   uncancel failed return status: $res"
      fi
    fi
  fi

  if ! (( $doit )); then
    echo "   Skipping sending alarms, $evid is posted for ALARMSTIFLE"
  else
    #cmd="${alarmDir}/bin/alarmevent -c ${cfgFile} ${evid}"
    #cmd="${RT_HOME}/utils/alarm/alarmevent -c ${cfgFile} ${evid}"
    cmd="${alarmUtilHome}/alarmevent -c ${cfgFile} ${evid}"

    echo "Begin $cnt: $cmd"

    # Do the event alarm and output results
    ${cmd}

    if [ $doCUBE2EIDS -eq 1 ]; then
      #echo "   ${alarmDir}/bin/sendCUBE2EIDS.sh ${evid}"
      #${alarmDir}/bin/sendCUBE2EIDS.sh ${evid}
      etype=`${TPP_BIN_HOME}/getEtype.pl $evid`
      if [ "$etype" == 'st' ]; then
        echo "Skipping sending qml xml to PDL $evid is $etype"
      else
        #ver=`${TPP_BIN_HOME}/eventVersion.pl $evid`
        ver=''
        echo "   ${RT_HOME}/alarm/alarms/EVTPRM2PDL/EVTPRM2PDL ${evid} $ver"
        ${RT_HOME}/alarm/alarms/EVTPRM2PDL/EVTPRM2PDL ${evid} $ver
      fi
    else
      echo "   info: local host PDL send qml disabled"
    fi
  fi

  echo "===================== End send ${evid}"

} >> ${logfile} 2>&1

