#!/bin/bash
#
# Send "cancel" alarm messages for alarm systems
#
# When the db name argument is not null, sendcancel uses that database's config 
# and if ${alarmDir}/cfg/cancelCMS_${dbase}.cfg does not exist, it is created.
#
# Also this script sources the file ${alarmDir}/cfg/alarm-cancel.cfg to obtain 
# an optional array list of other databases to which to send thecancel message,
# these would typically be the RT db, e.g.:
#
# Is this path env redefinition needed?
#LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/openwin/lib:/usr/dt/lib:/usr/local/lib
#LD_LIBRARY_PATH=/app/aqms/pp/lib:$LD_LIBRARY_PATH

# First check runtime environment
if [ -z "${TPP_HOME}" -o -z "${LD_LIBRARY_PATH}" ]; then . /usr/local/etc/aqms.env; fi

#alarm root and log path
alarmDir="${TPP_HOME}/alarm"
alarmUtilHome="${RT_HOME}/utils/alarm/"
if [ -n "${ALARM_UTIL_HOME}" ]; then
    alarmUtilHome="${ALARM_UTIL_HOME}"
fi

# Used to be
# logfile="${alarmDir}/logs/alarmevent.log"
#
# Now, separate send/cancel logs
logfile="${alarmDir}/logs/alarmcancelCMS.log"

usage() {
  echo "Syntax: ${0} [-o] [-s] <evid> [dbase]"
  echo "        If no dbase name, sendcancel to db names listed in 'dblist' array variable"
  echo "        whose definition is found by sourcing file ${alarmDir}/cfg/alarm-cancel.cfg"
  echo "        -o : sendcancel only to command line dbase, skips the 'dblist' processing"
  #echo "        -s : write CUBE D (cancel) to EIDS/polldir on this host"
  echo "        -s : send qml (cancel) to product distribution PDL on this host"
  echo "        Output is logged to $logfile"
  exit $1
}

# By default, =0, forward to other alarm hosts 
doOnlyCmdDb=0

# By default, =0, do not send CUBE msg via EIDS backdoor
doCUBE2EIDS=0

while getopts ":hos" opt; do
  case $opt in
    o)
      doOnlyCmdDb=1
      ;;
    h)
      usage 0
      ;;
    s)
      doCUBE2EIDS=1
      ;;
    \?)
      echo "Error: Invalid command switch option: -$OPTARG" >&2
      usage 1
      ;;
    :)
      echo "Error: Command option -$OPTARG requires an argument." >&2
      usage 1
      ;;
  esac
done

#Here we use shift to offset input $@ to the rest of args following switch options
shift $(($OPTIND - 1))
if [ $# -lt 1 ]; then
   echo "Missing input arguments"
   usage 1;
fi
if [[ $doOnlyCmdDb -eq 1 &&  -z "$2" ]]; then
  echo "Option to do only the command line db is missing the db name arg"
  usage 1;
fi

# for clarity map input args to local vars
evid=${1}
dbase=${2}

# NOTE: Cancel on arch db because it contains the latest info (see alarmsend).
# Also Cancel on originating RT system because that alarming system too
# knows what was already sent and what must be cancelled.
# Alarms may have originated only in PostProc db.

# Block wrapper for redirect of print text to logfile
{
  echo " "
  echo " ===================== Start cancel ${evid}"
  cnt=1
  # First send cancel msg for PostProc if a dbase is specified on command line
  if [ -n "$dbase" ]; then

    echo "From  $cnt: ${0} db=${dbase} evid=${evid} @ `date -u +'%F %T %Z'`"

    cfgFile=${alarmDir}/cfg/cancelCMS_${dbase}.cfg
    if [ ! -e ${cfgFile} ]; then ${alarmDir}/bin/makeAlarmCfgCMS.sh ${dbase} cancel > ${cfgFile}; fi

    #cmd="${alarmDir}/bin/sendcancel -c ${cfgFile} ${evid}"
    #cmd="${RT_HOME}/utils/alarm/sendcancel -c ${cfgFile} ${evid}"
    cmd="${alarmUtilHome}/sendcancel -c ${cfgFile} ${evid}"
    echo "Begin $cnt: ${cmd}"

    # Do the cmd and output results
    ${cmd}

    if [ $doCUBE2EIDS -eq 1 ]; then
      #echo "   ${alarmDir}/bin/cancelCUBE2EIDS.sh ${evid}"
      #${alarmDir}/bin/cancelCUBE2EIDS.sh ${evid}
      etype=`${TPP_BIN_HOME}/getEtype.pl $evid`
      if [ "$etype" == 'st' ]; then
        echo "Skipping sending qml xml CANCEL to PDL $evid is $etype"
      else
        #ver=`${TPP_BIN_HOME}/eventVersion.pl $evid`
        ver=''
        echo "   ${RT_HOME}/alarm/alarms/EVTPRM2PDL/CANCEL_EVTPRM2PDL ${evid} $ver"
        ${RT_HOME}/alarm/alarms/EVTPRM2PDL/CANCEL_EVTPRM2PDL ${evid} $ver
      fi
    else
      #echo "   info: local host CUBE2EIDS send disabled"
      echo "   info: local host PDL send cancel qml disabled"
    fi

  fi

  #==============================================================================
  # Send cancel to other databases only if flag unset
  if [ $doOnlyCmdDb -eq 0 ]; then
    source ${alarmDir}/cfg/alarm-cancel.cfg
    # Send cancel msg to all databases configured in the sourced array list of databases, if any
    for dbase in ${dblist[@]}; do

        cnt=$(($cnt+1))  # bump counter for labels

        echo " "
        echo "From  $cnt: ${0} db=${dbase} evid=${evid} @ `date -u +'%F %T %Z'`"

        cfgFile=${alarmDir}/cfg/cancelCMS_${dbase}.cfg
        if [ ! -e ${cfgFile} ]; then ${alarmDir}/bin/makeAlarmCfgCMS.sh ${dbase} cancel > ${cfgFile}; fi

        #cmd="${alarmDir}/bin/sendcancel -c ${cfgFile} ${evid}"
        #cmd="${RT_HOME}/utils/alarm/sendcancel -c ${cfgFile} ${evid}"
        cmd="${alarmUtilHome}/sendcancel -c ${cfgFile} ${evid}"
        echo "Begin $cnt: ${cmd}"

        ${cmd}

    done;
  fi

  echo " ===================== End cancel ${evid}"

} >> ${logfile} 2>&1
