#!/bin/bash

if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

timestamp=`date -u '+%Y%m%d%H%M'`

sLog=${TPP_HOME}/alarm/logs/alarmsendCMS
cLog=${TPP_HOME}/alarm/logs/alarmcancelCMS

mv ${sLog}.log ${sLog}_${timestamp}.log
mv ${cLog}.log ${cLog}_${timestamp}.log
