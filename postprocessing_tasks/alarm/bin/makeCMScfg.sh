#!/bin/bash
# 
# Script has hard-coded host network dependencies 

if [ $# -lt 1 ]; then
    echo "Syntax: ${0}  <cms-server-hostname>"
    exit
fi

# First check runtime environment
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

#alarm root and log path
alarmDir="${TPP_HOME}/alarm"

cmsServer=${1}
queDir="${alarmDir}/msgque"

#
#echo "# Auto generated cms.cfg file `date -u +'%F %T %Z'`"
echo "Project             DC"
echo "Host                ${cmsServer}.${TPP_IP_DOMAIN}:38800"
echo "CorbaVersion        1.2"
echo "MaxQFileSize        1000"
echo "MaxConnRetry        4"
echo "QueueFileHomeDir    ${queDir}"
echo "SubscriberHost      ${cmsServer}.${TPP_IP_DOMAIN}"
echo "SubscriberPort      39978"
echo "SleepBetweenRetries 20"
