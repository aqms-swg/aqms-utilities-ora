#!/bin/bash
#
# cancel alarm messages for evid ($1) on one db host ($2)
#
# First check runtime environment
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

${TPP_HOME}/alarm/bin/alarmcancelCMS.sh -s -o ${1} ${2}
