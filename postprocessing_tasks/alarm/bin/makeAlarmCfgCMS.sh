#!/bin/bash
#
# args:  dbhost  msgtype
#
# <msgtype> must be "update" or "cancel"
#
# This script sources the file ${alarmDir}/cfg/alarm-user.cfg to obtain extra config

if [ $# -lt 2 ]; then
  echo "Syntax: ${0}  <dbname> <msgtype: [update|cancel]>" 
  exit
fi

# First check runtime environment
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi
alarmDir="${TPP_HOME}/alarm"

dbname=${1}
msgtype=${2}

# Db account info is from include file 
#dbu="UNKNOWN"; dbp="UNKNOWN"
if [ ! -e "${alarmDir}/cfg/alarm-user.cfg" ]; then
  echo "Error missing cfg file : ${alarmDir}/cfg/alarm-user.cfg"
  exit
fi
source ${alarmDir}/cfg/alarm-user.cfg

# There are two flavors
# Note connects to CMS QWFeeder java stdout goes to alarmsendCMS or alarmcancelCMS logs
if [ "${msgtype}" == "update" ]; then
    progname="ALARM_EVENT"
    #alarmevent_<date>.log has junk lines related to binary startup,initialization, and termination info.
    logfile="${alarmDir}/logs/alarmevent"
    signalType="UpdateSubject  /signals/alarmevent/update"
elif [ "${msgtype}" == "cancel" ]; then
    progname="SEND_CANCEL"
    #NOTE:
    #sendcancel_<date>.log has junk lines related to binary startup,initialization, and termination info.
    logfile="${alarmDir}/logs/sendcancel"
    signalType="CancelSubject  /signals/events/cancel"
else
    echo "Invalid message type: must be update or cancel -> "${msgtype}
    exit
fi

# cms config for cms host
cmsConfigFile="${alarmDir}/cfg/cms_${dbname}.cfg"

# NOTE: CMS code requires DB account info to be plain-text here
#echo "# Auto generated cms.cfg file `date -u +'%F %T %Z'`"
echo "Logfile         ${logfile}"
echo "LoggingLevel    ${loggingLevel}"
echo "ProgramName     ${progname}"
echo "CMSConfig       ${cmsConfigFile}"
echo "${signalType}"
echo "DBService       ${dbname}"
echo "Include         ${dbauth}"

