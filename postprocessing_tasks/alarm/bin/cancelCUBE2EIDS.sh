# Create and copy a CUBE D event (CANCEL) msg to EIDS polling directory 
#!/bin/bash 
if [ -z "$TPP_HOME" ]; then source /usr/local/etc/aqms.env; fi
if [ $# -eq 0 ]; then
  echo "
  Missing input event evid argument
  Syntax: $0 <evid>"
  exit 1
fi
evid=$1
destination=$RT_HOME/EIDS/polldir
etype=`${TPP_BIN_HOME}/getEtype.pl $evid`
if [ "$etype" == 'st' ]; then
  echo "Skipping sending EIDS CUBE message to $destination, $evid is $etype"
  exit 0
fi
echo evid=$evid
cubeMsg=`${TPP_BIN_HOME}/cubeMessage.pl -D $evid` 
if [ $? != 0 ]; then
  echo  "Error creating EIDS CUBE msg:$cubeMsg"
  exit 1
else
  if [ ! -d  "$destination" ]; then
    echo "Error sending EIDS CUBE msg, missing target dir: $destination"
    exit 1
  fi
  echo $cubeMsg
  destination=$destination/$evid.map
  echo "Writing above EIDS CUBE message to $destination"
  echo $cubeMsg > $destination
fi
