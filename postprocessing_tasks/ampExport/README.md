# Exporting amps as "nsmp" XML files from AQMS into PDL  

## Dependencies  

*  Perl DBI, XML Writer  
*  [Qtime] (http://www.ncedc.org/ftp/pub/programs/Qtime/)  


## Files in this directory  
  

| Filename | Description|
  -------- | ----------- 
**db2nsmp_xml**	| perl script for generating and distributing XML files  
**db2nsmp_xml.conf** | configuration for db2nsmp_xml  
**eqrun_db2nsmp.cfg** | configuration for using eqrun to execute db2nsmp_xml  
**run_db2nsmp**  | wrapper script to pass extra arguments to db2nsmp_xml  
**amp_exch.ini** | sample ProductClient configuration to use with db2nsmp_xml  
  
## Description

This code provides a way to export ground motion amps (PGA, PGV, etc) from an AQMS database as XML files published into PDL. The XML format is one used by NSMP for sending amps to the global ShakeMap system.    
  
*db2nsmp_xml* is intended to be run as soon as ground motion amps have been written to the AQMS data, e.g. by ampgen. To accomplish this, we use the AQMS RT program "eqrun". eqrun will run an arbitrary program with a single argument: an event ID. Any other arguments must be supplied by a wrapper script, in this case "*run_db2nsmp*". Note that eqrun will only run its configured program when its database role is "primary", so only one of the two AQMS RT systems will run *db2nsmp_xml* to publish amps. You should configure eqrun to subscribe to the CMS subject published by ampgen (its AmpSubject). If your AQMS system runs more than one instance of *ampgen* (e.g. an early instance and a late instance) you should have eqrun subscsribe to the last ampgen's subject. Otherwise, you may export mutiple copies of some amps.  
  
*db2nsmp_xml* wroks as follows. After parsing its configuration file, it queries the database for the ground motion amps. If no amps are found, *db2nsmp_xml* exits. For each station with amps, *db2nsmp_xml* generates an XML file in the configured XMLdir. After all the amps for this event have been written to files, *db2nsmp_xml* runs the ProductClient program to publish the amps into PDL. The sample "*amp_exch.ini*" file provides a sample ProductClient configuration file for this purpose.  
  
Note that *db2nsmp_xml* publishes the XML files as "unassociated-amplitude" product; the event ID from your AQMS system is not included. This is because your event ID may not mean anything to the recipient of these amps. So it is up to the recipient to associate the amps with her own event. 

Pete Lombard    
2015/11/17  

