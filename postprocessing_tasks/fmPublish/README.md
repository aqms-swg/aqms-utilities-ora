# Publishing focal mechanisms  
  
fmPublish are a set of perl and shell scripts for obtaining focal mechanism files files from the database and copying them to web servers and/or creating quakeml for PDL client, or links for EIDS.  
  
Quakeml is an XML representation of seismological data, the specification can be found at https://quake.ethz.ch/quakeml  
  
The executable scripts are stored in the sub-directory *bin* and the corresponding configurations in *cfg*.    
  

### *bin*  
  
*  ``fmPublish.pl`` : Driver script for obtaining focal mechanism files files from the database and copying them to web servers and/or creating quakeml for PDL client, or links for EIDS.    
*  ``fmPublish.sh`` : A wrapper shell script that sets up the environment and runs ``fmPublish.pl``  
*  ``fmPublishCont.pl`` : Continuously running script to send QuakeML from web service to PDL hub. Acts on events posted to the configured PCS state description (e.g fmPublish)  
*  ``startFmPublishCont.sh`` :  Start/restart ``fmPublishCont.pl``. This is run at intervals by a cron job to insure that the jobs start at boot time and are always present.  
*  ``deleteFm.sh`` : Wrapper around ``fmPublish.pl`` with options pre-set to delete FA focmec from db and PDL products for input evid  
*  ``mecBlob2File.pl`` : Reads from  MecObject table data for an mecid and writes that row's blob data to the meta column filename.  
  

### *cfg*  
  
*  ``fmPublish.cfg`` : The default configuration file used by ``fmPublish.pl``  
*  ``fmPublishCont.props`` : The default configuration file used by ``fmPublishCont.pl``  
*  ``pdl-submit.props`` : Configuration for a PDL EIDSInputWedge submitter.  
*  ``webPrivateDestinations.cfg`` : List directories to which we  will send html and gif files  


