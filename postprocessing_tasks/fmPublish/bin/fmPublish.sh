#!/bin/bash
if [ -z "${TPP_HOME}" -o -z "${LD_LIBRARY_PATH}" ]; then . /usr/local/etc/aqms.env; fi
cd $TPP_HOME/fmPublish/bin
./fmPublish.pl "$@"
