#!/usr/bin/perl
# ###########################################################
# fmPublishCont- Continuously running script to send QuakeML from web service to PDL hub.
# Acts on events posted to the configured PCS state description (e.g fmPublish)
# Usage $0 [prop-file]
# If a prop-file is unspecified, it defaults to $ENV{TPP_HOME}/fmPublish/cfg/fmPublishCont.props
# This script expects a getquakeml.py script to be in same bin directory.
####################################################################################

use strict;
use warnings;

use File::Basename;

use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use MasterDbs;
use Utils;

# parse optional switches
use Getopt::Std;
our ( $opt_d, $opt_c, $opt_h, $opt_v );
getopts('c:d:hv');

# Make STDOUT "hot" e.g. autoflush
# By default perl running in background flushes its buffers only once/min
# so monitoring the log file makes it appear the script isn't working!
select STDERR; $| = 1;
select STDOUT; $| = 1;

# Environmental variables supplied by a bash wrapper sourcing /usr/local/etc/aqms.env
# First check runtime environment
defined $ENV{TPP_HOME} or die "$0 Error: Required aqms.env environment vars are not defined\n$!";

usage() if $opt_h;

chdir "$ENV{TPP_HOME}/fmPublish/bin";

# Host machine running this script
my $host = `hostname`;
chomp $host;
$host =~ s/\..*$//; # grab first element

# Direct all output to log file, rename existing log if present
my $logFile  = "$ENV{TPP_HOME}/fmPublish/logs/" . basename($0,'.pl') . '.log';
print "All output will be directed to $logFile\n";
rotateLogs($logFile);

# Environmental variables supplied by a bash wrapper sourcing /usr/local/etc/aqms.env
my $date = `date -u +'%F %T %Z'`;
chomp $date;
print "Starting $0 on $host at $date\n";

# ==========================================================================
# Behavior variables 
# ==========================================================================
# Hash array to hold configuration properties, like object attributes
my ( %myprops );
# Property set vars:
my ( $pcsCommand, $pcsCommand2 );
my ( $pcsAppName, $pcsAppProps, $pcsDbase, $pcsGroup, $pcsTable, $pcsState, $pcsResult, $pcsStallTime );
my ( $sleepTime, $archivalTime, $minMag, $maxDaysBack );
my ( $propFile, $debug, $verbose, $reconfig, $noop );

# Define default runtime properties 
setDefaultProperties(\%myprops);

# Check for properties file to override the default config
$propFile = "$ARGV[0]" if (@ARGV > 0);
die "$0 Error: missing property file: $propFile\n$!" if (! -e $propFile);
print "Loading properties from file: $propFile\n";

loadProperties(\%myprops, $propFile);

# Report starting configuration
printConfig();

# Are all props valid ?
checkProperties();

# set verbose mode if option set, and not in props
$verbose = 1 if $opt_v;

# Declare dynamic variables used inside infinite while loop
# that may need to be accessed outside block scope

my $amEnabledForPCS = 'true'; # reset in loop by checking with db
my ($evid, $age, $mag, $wait, $cutoff, $status);

my $dayNumber = `date -u '+%d'`; #  note LF is not stripped

# ---- MAIN CONTINUOUS LOOP ----
while (1) {

      if ($dayNumber != `date -u '+%d'`) {
        print("Executing change-of-day chores @ ". `date -u +'%F %T %Z'`);
        rotateLogs($logFile);
        printConfig();
        $dayNumber = `date -u '+%d'`;
      }
      else {
        my $now = time();
        utime $now, $now, $logFile;  # keep file timestamp current (proof of redo looping)
      }

    # check db host app processing status
    $amEnabledForPCS = `$ENV{TPP_BIN_HOME}/enabledForPCS.pl $pcsAppName $pcsState`;  
    chomp $amEnabledForPCS; # may have newline at end

    # See if an event id is posted to PCS_STATE table in db
    # Increase the stall time to allow for db magnitude update delays.
    $evid = `$ENV{TPP_BIN_HOME}/next.pl -d $pcsDbase -s $pcsStallTime $pcsGroup $pcsTable $pcsState`;
    chomp $evid;

    if ($evid eq '' || $evid eq '0') { # No event: sleep - zzzz
        sleep $sleepTime;
        if ($reconfig) { # reload props file
            if ( ! loadProperties(\%myprops, $propFile) ) {
              checkProperties();
              printConfig() if ( $reconfig > 1 );
            }
        }
        redo;
    }

    # Have event id
    $date = `date -u +'%F %T %Z'`;
    chomp $date;
    print "\nNEXT: $evid \@ $date using primary db:" . `$ENV{TPP_BIN_HOME}/amPrimaryDb.pl`;

    if ( "$amEnabledForPCS" ne 'true' ) {
        print "SKIP: Not primary app host on $host for $pcsAppName $pcsState, a no-op for $evid\n";
        next; # result it
    } 

    # check age
    $age = getEventAge($evid); # age of event in seconds
    if ( $age <= 0 ) {  # Bogus FUTURE event (yes, this happened)
        print "SKIP: No valid event found in db, age = $age secs for $evid\n";
        next; # result it
    }

    # check cutoff age only if input property for maxDaysBack was specified
    $cutoff = (86400 * $maxDaysBack);
    if ( $cutoff > 0 ) {
        if ( $age > $cutoff ) {
            printf "SKIP: Too old, %8.2f > $maxDaysBack days for %d\n", ($age/86400), $evid;
            next; # result it
        }
    }

    # allow at least archivalTime secs post-origin for data to be generated (e.g. amps, mags, waveforms ...)
    if ( $age < $archivalTime ) {
        $wait = ($archivalTime - $age);  # wait for data to be archived to db
        print "WAIT: $wait secs to age $evid, for its data to be archived ...\n";
        sleep $wait;
    }

    my $rflag=`$ENV{TPP_BIN_HOME}/getRflag.pl -d $pcsDbase $evid`;
    chomp $rflag;
    if ( "$rflag" eq "C" ) {
        print "SKIP: Event cancelled rflag='C' for $evid\n";
        next; # result it
    }

    my $desc = `$ENV{TPP_BIN_HOME}/getstates.pl -d $pcsDbase -i $evid TPP TPP ALARMSTIFLE`;
    chomp $desc;
    if ( $desc =~ /$evid/) {
        print "SKIP: ALARMSTIFLE post for $evid\n";
        next; # result it
    }

    # check for qualifying magnitude
    $mag = getEventMag($evid);
    if ( $mag < $minMag ) {
        print "SKIP: Too small, $mag < $minMag (minMag) for $evid\n";
        next; # result it
    }

    print "PROC: $evid Mag = $mag \@ " . `date -u +'%F %T %Z'`;
    $status = processEvent($evid);
    print "STAT:" . ($status >> 8) . " for $evid  \@ " . `date -u +'%F %T %Z'` if $status && $verbose;

} continue { # done with event

    # The transition defined in PCS for result removes posting from PCS 
    # Done - result it
    my $result = $pcsResult;
    # $result = ($status >> 8) if $status; #  reset to returned result, like error code ?
    print `$ENV{TPP_BIN_HOME}/result.pl -d $pcsDbase $evid $pcsGroup $pcsTable $pcsState $result`;
    print "DONE: $evid \@ " .  `date -u +'%F %T %Z'`;

} # end of infinite loop

exit;

####################################################################################

sub processEvent {
   my ($evid) = @_;

   my $cmd;
   # must force substitution of variables from $pcsCommand and need quotes around expanded string
   my $command;
   print `$ENV{TPP_BIN_HOME}/catone.pl -d $pcsDbase -t $evid`;
   my $gtype = getEventGType($evid);
   if ( $gtype  =~ /^[lr]$/ ) {
       $command = '$cmd = ' . "\"$pcsCommand\"";
   }
   elsif ( $pcsCommand2 ) {
       print "INFO: $evid gtype:$gtype is not 'l' or 'r'\n" if $verbose;
       $command = '$cmd = ' . "\"$pcsCommand2\"";
   }

   eval $command;

   if ( $noop ) {
     print "EXEC: $cmd\n" if ( $debug ne '0' );
     print "NOOP: $evid\n" if $verbose;
     return 0; # assumes 0 is the same as system success
   }
   print "EXEC: $cmd\n" if $verbose;

   # Note /bin/sh shell forked by system does not have the ENV vars
   return system($cmd);
}

# Returns '-' if no gtype.
sub getEventGType {
   my ($evid) = @_;
   my $type = `$ENV{TPP_BIN_HOME}/getGtype.pl -d $pcsDbase $evid`;
   chomp $type;
   return $type;
}

# Return the age of the given event in integer seconds
# Returns '0' if no such event.
sub getEventAge {
   my ($evid) = @_;
   my $secsOld = `$ENV{TPP_BIN_HOME}/eventAge.pl -d $pcsDbase $evid`;  #age of event in secs
   chomp $secsOld;
   return $secsOld;
}

# Return event magnitude, or -9 if undefined
sub getEventMag {
    my ($evid) = @_;
    my $emag = `$ENV{TPP_BIN_HOME}/eventMag.pl -d $pcsDbase $evid`;
    chomp $emag;
    if ( $emag !~ m/\d/ ) {
        print "WARNING: No magnitude found for $evid, retrying after sleeping 10 secs...\n";
        sleep 10;
        $emag = `$ENV{TPP_BIN_HOME}/eventMag.pl -d $pcsDbase $evid`;
        chomp $emag;
        if ( $emag !~ m/\d/ ) {
            print "ERROR: No magnitude found for $evid\n";
            return -9; 
        }
    }
    $emag = -9 if $emag < -9; 
    return $emag;
}

# Read properties into hash from properties file, merge with defaults
sub loadProperties {
    my ($defPropsRef, $propFile) = @_;
    my $fileHashRef = {};

    die "Invalid input, properties file D.N.E.\n $!" if ! -e "$propFile";

    $date = `date -u +'%F %T %Z'`;
    chomp $date;
    print "Loading properties from file: $propFile \@ $date\n"  if ( $reconfig > 1 );

    eval {
      # Optional regex for split delimiter for key value is a string like "[=: ]+" 
      $fileHashRef = Utils::->readPropsFromFile($fileHashRef, $propFile, " *[=] *");
    };
    die "Error loading properties from $propFile\n $!" if ( $@ );

    # Check if properties file changed
    my $retval = equalsHash($fileHashRef, $defPropsRef);
    #print "retval=$retval\n";

    if ( scalar( keys %$fileHashRef ) != 0 ) {
        setProperties( $fileHashRef );
        # Use slice assignment to merge prop file hash into the default props by refernce:
        @{$defPropsRef}{ keys %$fileHashRef } = values %$fileHashRef;
    }

    if ( exists $defPropsRef->{'debug'} ) {
      $debug = $defPropsRef->{'debug'};
      if ( $debug  eq 'true' ) {
         $debug = '1';
      } elsif ( $debug eq 'false' ) {
         $debug = '0';
      }
    }
    else {
      $debug = '0';
    }

    if ( $debug ne '0' ) {
        $verbose = 1;
        print "DEBUG $0 Properties:\n";
        Utils::->printProps($defPropsRef, " = ");
    }

    return $retval;  # 0 or 1
}

# Lookup property in input hash, return default if no key
sub getProperty {
    my ($propsRef, $propName, $defaultVal) = @_;
    my $val = $propsRef->{$propName};
    $val = $defaultVal if ! defined $val;
    return $val;
}

####################################################################################

# Populate input hash with current config
sub getProperties {

    my ($propsRef) = @_;

    $propsRef->{pcsAppName} = $pcsAppName;
    $propsRef->{pcsAppProps} = $pcsAppProps;
    $propsRef->{pcsDbase} = $pcsDbase;
    $propsRef->{pcsGroup} = $pcsGroup;
    $propsRef->{pcsTable} = $pcsTable;
    $propsRef->{pcsState} = $pcsState;
    $propsRef->{pcsResult} = $pcsResult;
    $propsRef->{pcsStallTime} = $pcsStallTime;
    $propsRef->{pcsCommand} = $pcsCommand;
    $propsRef->{pcsCommand2} = $pcsCommand2;
    $propsRef->{noop} = $noop;
    $propsRef->{reconfig} = $reconfig;
    $propsRef->{sleepTime} = $sleepTime;
    $propsRef->{archivalTime} = $archivalTime;
    $propsRef->{maxDaysBack} = $maxDaysBack;
    $propsRef->{minMag} = $minMag;
    $propsRef->{debug} = $debug;

    return $propsRef;
}

# set internal config to property hash values
sub setProperties {

    my ($propsRef) = @_;

    my $str = '';

    # PCS config props
    $str = getProperty($propsRef, 'pcsAppName');
    if ( $str ) { $pcsAppName = $str; }

    $str = getProperty($propsRef, 'pcsAppProps');
    if ( $str ) { $pcsAppProps = $str; }

    $str = getProperty($propsRef, 'pcsDbase'); 
    if ( $str ) {
       $pcsDbase = $str;
       $pcsTable = $str;
    }

    $str = getProperty($propsRef, 'pcsGroup'); 
    if ( $str ) { $pcsGroup = $str; }

    $str = getProperty($propsRef, 'pcsTable');
    if ( $str ) { $pcsTable = $str; }

    $str = getProperty($propsRef, 'pcsState'); 
    if ( $str ) { $pcsState = $str; }

    $str = getProperty($propsRef, 'pcsResult'); 
    if ( $str ) { $pcsResult = $str; }

    $str = getProperty($propsRef, 'pcsStallTime'); 
    if ( $str ) { $pcsStallTime = $str; }

    # Note app args are specific to implementation like where to insert props and $evid in cmd string
    $str = getProperty($propsRef, 'pcsCommand'); 
    if ( $str ) { $pcsCommand = $str; }
    $str = getProperty($propsRef, 'pcsCommand2'); 
    if ( $str ) { $pcsCommand2 = $str; }

    # Other props
    $str = getProperty($propsRef, 'verbose');
    if ( $str ) { $verbose = $str; }
    else { $verbose = 0; }

    $str = getProperty($propsRef, 'noop');
    if ( $str ) { $noop = $str; }

    $str = getProperty($propsRef, 'reconfig');
    if ( $str ) { $reconfig = $str; }

    $str = getProperty($propsRef, 'sleepTime');
    if ( $str ) { $sleepTime = $str; }

    $str = getProperty($propsRef, 'archivalTime');
    if ( $str ) { $archivalTime = $str; }

    $str = getProperty($propsRef, 'maxDaysBack');
    if ( $str ) { $maxDaysBack = $str; }

    $str = getProperty($propsRef, 'minMag');
    if ( $str ) { $minMag = $str; }
        
}

# print current internal config
sub printConfig {
    print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
    print "Property file=$propFile\n";
    print "App $pcsAppName on $host connects to $pcsDbase and processes PCS state posts of:\n";
    print "  $pcsGroup $pcsTable $pcsState for result $pcsResult where\n";
    print "  pcsStallTime=$pcsStallTime seconds\n";
    print "  sleepTime=$sleepTime secs hibernation when no evid is posted\n";
    print "  reconfig=$reconfig (=1 reloads properties after sleep)\n";
    print "  verbose=$verbose (=1 prints extra processing output and app properties)\n";
    print "  archivalTime=$archivalTime min secs of event aging before processing\n";
    print "  maxDaysBack=$maxDaysBack max days age eligible for processing (any age if <= 0 )\n";
    print "  minMag=$minMag is min magnitude eligible for processing\n";
    print "  debug=$debug\n"; 
    print "\nProcessing $pcsCommand\n";
    print "for event NOT local/regional: $pcsCommand2\n";

    if ($pcsAppProps) {
       print "Using pcsAppProps file $pcsAppProps\n";
       if ( $verbose ) {
         print "\nProperties:\n";
         open(my $fh, $pcsAppProps);
         while (my $line = <$fh>) { print $line; }
         close($fh);
       }
    }

    print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
}

sub setDefaultProperties {

    my ($propsRef) = @_;

    $propFile = "$ENV{TPP_HOME}/fmPublish/cfg/fmPublishCont.props";

    $pcsAppName = "fmPublishCont";
    $pcsAppProps = '../cfg/fmPublish.cfg';

    # NOTE: single quotes, var substitution is done at time of execution by "eval" in processEvent function 
    $pcsCommand = '$ENV{TPP_HOME}/fmPublish/bin/fmPublish.pl -c $pcsAppProps -d $pcsDbase -E -w -P -v $evid';
    $pcsCommand2 = '$ENV{TPP_HOME}/fmPublish/bin/fmPublish.pl -c $pcsAppProps -d $pcsDbase -R -E -w -P -v $evid';

    #Default to master else parse command line arg option
    $pcsDbase = $masterdb;
    if ($opt_d) { $pcsDbase = $opt_d; }


    # NOTE: pcs_transition table sourceTable could be some string other than the $pcsDbase
    # default PCS state description to operate on:
    $pcsGroup = "EventStream";
    $pcsTable = "$pcsDbase";
    $pcsState = "fmPublish";
    $pcsResult = 1;         # after processing
    $pcsStallTime = 0;

    $noop = 0;              # =0 always process, =1 do not process posts, for testing
    $reconfig = 0;          # do not reload properties after sleep between next post poll
    $sleepTime  = 60;       # secs to sleep between checks for new events
    $archivalTime = 90;    # secs post-origin to wait for data to be archived
    $minMag = -9;           # minimum magnitude to process 

    #NOTE: maxDaysBack special value 0 implies do not check age for cutoff 
    $maxDaysBack = 0;    # Don't process events older than this
    if ($opt_c) { $maxDaysBack = $opt_c; }
    
    getProperties($propsRef);
}

sub checkProperties {
    # Aux app properties file not required for exportwf 
    die "$0 Error: missing property file: $pcsAppProps\n$!" if ($pcsAppProps && ! -e $pcsAppProps); 
}

# This belongs in the utils pm ?
sub equalsHash {
    my ($a, $b) = @_;
    my @ka = keys %$a;
    my @kb = keys %$b;
    return 0 if ( @ka != @kb );
    my %cmp = map { $_ => 1 } @ka;
    for my $key (@kb) {
      last unless exists $cmp{$key};
      last unless $a->{$key} eq $b->{$key};
      delete $cmp{$key};
    }
    return ( scalar( keys %cmp ) ? 0 : 1 );
}

sub usage {

  print <<'EOF';

Usage $0  [-d -c -v] [prop-file]
   -d dbase, default for PCS when not in props
   -c cutoff maxDays when not in props
   -v verbose output (e.g command strings and current properties)
   -h usage

DEFAULT propFile when not specified on command line is: $ENV{TPP_HOME}/fmPublish/cfg/fmPublishCont.props
Default values below are used when NOT defined in property file:
 pcsDbase     $masterdb value or -d switch override
 pcsGroup     EventStream
 pcsTable     the pcsDbase value
 pcsState     fmPublish
 pcsResult    1
 pcsStallTime 0   seconds
 sleepTime    60  seconds
 archivalTime 90 seconds
 maxDaysBack  0   no cutoff age
 minMag       -9
 noop         0   always process, =1 do not process posts, only for testing
 reconfig     0   do not reload properties after sleep between next post polls

 pcsAppName = fmPublishCont
 # NOTE: vars values are substituted at time of execution, default:
 pcsCommand = $ENV{TPP_HOME}/fmPublish/bin/fmPublish.pl -c $pcsAppProps -d $pcsDbase -E -w -P -v $evid

EOF
  exit;
}
# ###################################################
#
# open log file.
#
sub openLogs {
    my ($logFile) = @_;   # get arg

    open STDOUT, "> $logFile" or die "Can't redirect STDOUT to $logFile: $!\n";
    open(STDERR, ">&STDOUT") || die "Can't dup err to stdout";

    select(STDERR); $| = 1; # make unbuffered
    select(STDOUT); $| = 1; # make unbuffered

    print("Log file opened: " . `date -u +'%F %T %Z'`);

  }

# ###################################################
# Rotate log files
#
# rotateLogs ();
#
# You can call this at startup to rename an old existing log
# and open a new one.
sub rotateLogs {
    my ($logFile) = @_;   # get arg

    if (-e $logFile) {
        my $timestamp = `date -u '+%Y%m%d%H%M'`;
        chomp $timestamp;
        #my $cmd = "mv $logFile ${logFile}.$timestamp";
        my ($fname,$path,$suffix) = fileparse("$logFile",qr"\..[^.]*$");
        my $cmd = "mv $logFile ${path}${fname}_${timestamp}${suffix}";
        `$cmd`;
    }

    # open new
    openLogs($logFile);
}

