#!/bin/bash
#
# Start/restart the process that distributes event focal mechanism to
# webservers, EIDS polldir and/or QuakeML via PDL client hub to Golden NEIC.
# This is run at intervals by a cron job to insure that the jobs start at
# boot time and are always present.
#
# #####################################################################
#
# crontab -e
#0,10,20,30,40,50 * * * * ${TPP_HOME}/fmPublish/bin/startFmPublishCont.sh


# For crontab job we need to source the environment variables, does oracle too
if [ -z "${TPP_HOME}" ]; then . /usr/local/etc/aqms.env; fi

usage() {

  cat <<EOF

Usage:

  Start/restart the process that distributes event focal mechanisms of the
  configured mechtype to webservers, and/or sends a  link message via EIDS/polldir,
  and/or sends QuakeML and image file via a PDL product client to Golden NEIC hub.

  This is run at intervals by a cron job to insure that the jobs start at boot time
  and are always present.

  Syntax: $0 [ -c maxDaysBack]

  -c days     : max days back to keep logs
  -h          : this help usage

EOF
    exit $1
}

appHome=${TPP_HOME}/fmPublish
logdir=${appHome}/logs

# One startFmPublishCont_YYYYMMDD.log per UTC day
logTimeStamp=$(date -u +"%Y%m%d")
startLog="${logdir}/startFmPublishCont_${logTimeStamp}.log"

maxDaysBack=''

while getopts ":c:h" opt; do
  case $opt in
    h)
      usage 0 # non-error exit
      ;;
    c)
      maxDaysBack="$OPTARG"
      ;;
    \?)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Invalid command switch option: -$OPTARG" | tee -a ${startLog}
      usage 1 # error exit
      ;;
    :)
      echo "====================================================" >> ${startLog}
      echo "$0 Error: Command option -$OPTARG requires an argument." | tee -a ${startLog}
      usage 1 # error exit
      ;;
  esac
done

#Here use shift to offset input $@ to any remaining args that follow this shell's  switch options
shift $(($OPTIND - 1))

startJob="${appHome}/bin/fmPublishCont.pl"

mailaddr=${TPP_ADMIN_MAIL}
host=`uname -n | cut -d'.' -f1`

cd $appHome/bin

# look for the pid of the active job, if any
grepStr="[f]mPublishCont"
pid=`pgrep $grepStr`

# force to run ( allows to do no-op resulting of pcs posts)
FLAGFILENAME="/etc/profile" # force it to run

# Block wrapper to redirect echo output to startLog
{
  echo "======================================================================"
  if [ -f "$FLAGFILENAME" ]; then
      # if no job running, START ONE
      if [ -z "$pid" ]
      then
      
        echo "Starting: ${startJob} at `date -u +'%F %T %Z'` on $host"
      
        # fork the job, put in background
        # NOTE: Use command line args to specify perl script options
        # -d for the db name (defaults to value of $masterdb) also default for pcs table
        ${startJob} $@ >>${startLog} 2>&1 &
      
        # send email of success or failure
        if [ -n "$mailaddr" ]; then
  
          sleep 5   # give it time to start run then check for pid
          pid=`pgrep $grepStr`
  
          if [ -n "$pid" ]; then
              echo "$startJob now running as pid = $pid"
              tail -3 $startLog | mailx -s "Restart fmPublishCont on $host (pid=$pid)" "$mailaddr"
          else
              echo "$startJob restart failed, no matching process found"
              tail -3 $startLog | mailx -s "FAILED restart fmPublishCont on $host"  "$mailaddr"
          fi
        fi
      
      # noop but gives error if no 'else' block!
      else
        echo "$startJob running as pid=$pid on $host `date -u +'%F %T %Z'`"
      fi
  else
    echo "$host on standby, NOT starting $startJob: `date -u +'%F %T %Z'`"
  fi
  
  # Run log cleanup
  if [ -n "$maxDaysBack" ]; then 
    echo "----------------------------------------------------"
    if [[ "$maxDaysBack" =~ ^[0-9]+$ && "$maxDaysBack" > "0"  ]]; then
      echo "Deleting files in $logdir older than $maxDaysBack days"
      find $logdir -mtime "+$maxDaysBack" -print -exec \rm '{}' \;
    else
      echo "Invalid max days back to keep logs: $maxDaysBack"
    fi
  fi 
 } >>${startLog} 2>&1
