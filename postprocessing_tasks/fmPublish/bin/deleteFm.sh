#!/bin/bash
# Wrapper around fmPublish.pl with options pre-set to delete FA focmec from db and PDL products for input evid
if [ -z "${TPP_HOME}" -o -z "${LD_LIBRARY_PATH}" ]; then . /usr/local/etc/aqms.env; fi

if [ $# -eq 0 -o "$1" == "-h" ]; then
 dbase=`masterdb`
 echo "
    usage: $0 [-d dbase] <evid>

    Wrapper around fmPublish.pl with options '-R -P -v' pre-set to delete
    the FA focmec database data associated with the input evid argument
    and to send the delete focmec qml message via PDL product jar. 

    -h          : print this usage info

    -d dbase    : alias to use for db connection (defaults to masterdb: $dbase)
    -V version  : Specifies PDL version number; default is event.version from db."

  exit
fi

cd $TPP_HOME/fmPublish/bin

./fmPublish.sh  -R -P -v "$@"
