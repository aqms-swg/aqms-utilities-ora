#!/usr/bin/perl

# Driver script for obtaining focal mechanism files files from the database and 
# copying them to web servers and/or creating quakeml for PDL client, or links for EIDS.
#
# NOTE: below code was extracted and edited from source scripts written by P. Lombard
# for FPFIT/FPPLOT focal mechanism production and publishing.

use strict;
use warnings;

use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use MasterDbs;
use DBI;
use File::Basename;
use File::Copy;
use Getopt::Std;
use XML::Writer;
use IO::File;
use POSIX qw(strftime);

# Global variables
our ($opt_c, $opt_d, $opt_h, $opt_k, $opt_P, $opt_E, $opt_R, $opt_v, $opt_V, $opt_w);

our ( $urlBase, $webPrivateCfg, $webPublicCfg, $EidsTmp, $EidsDir, $eidsSubmitter );
our ( $VERSION, $mechType, $minMechQual, $outputDir, $workDir, $imageType);
our ( $LItype, $FMversion, $LIdescription, $webCopyCmd, $pubRegion );
our ( $PDLkey, $PDLjar, $PDLconf, $PDLDelConf );
our ( $PDLjava, $PDLclient, $QML, $qmlPath, $qmlConf );
our ( $dbase, $dbh );
our ( @webfiles );

# The following two parameters should not need to be changed:
$VERSION = "1.1"; # The version number of this script
$QML = "quakeml.xml";
$pubRegion = "";

# Below parameter settings can be overridden by values parsed from cfg file 
$mechType = "FA";
$minMechQual=0;
$outputDir = "../output";
$imageType = "jpg";
#
# Below settable with input -V option
$FMversion = 1;

# Determine if the preferred origin has any MECHTYPE mechanisms.
# Actually it returns the number of MECHTYPE mechanisms.
# Cache the result, so we don't have to ask again.
my %preforHasFM;
my %epmHasFM;

# Find the number of MECHTYPE solutions for all previous origins (origins that are
# not the current preferred origin).
# Returns a reference to a hash containing:
#  ORID => origin_id
#  NUM  => number_of_solutions
# Cache the result in case we get asked again.
my %numPrevFM;
my $cmdname = basename($0);

##############################################################################
#       usage    - print syntax and exit with valus passed as input arg.
##############################################################################
sub usage {

    my $status = shift;
    print << "EOF";
$cmdname version $VERSION
  Publish MECH_TYPE fault plane solutions: copy html and IMAGE_TYPE (e.g. jpg or gif)
  files to declared web destinations, and/or send an addon link via EIDS, and/or create
  QML xml for PDL hub.

  The MECH_TYPE defaults to "$mechType" and the IMAGE_TYPE defaults to "$imageType";
  these values can be set by declaring like named properties in the configuration file.
  Script arguments are the event evid followed by an optional configuration filename,
  which defaults to ../cfg/fmPublish.cfg.

Syntax:
    $cmdname [-d DB] [-EPRvw] [-V version] eventID [config-file (default ../cfg/fmPublish.cfg)] 
    $cmdname -h
where:
  -d DB       - Specify masterDB instead of that returned by \$TPP_BIN_HOME/masterdb.
  -E          - Send add-on (CUBE LI format) to EIDS.
  -w          - Copy html/IMAGETYPE files to server/paths listed WEB_PRIVATE_CFG and also 
                in WEB_PUBLIC_CFG when mec.quality >= MIN_MECH_QUALITY property (default = 0).
  -P          - Send (or delete) QuakeML, and IMAGETYPE files files to PDL hub.
  -R          - Delete event preferred MECHTYPE mechanism rows from all database tables.
  -h          - Prints this help message.
  -k          - Do not unlink (delete) files created in output work directory for event
                Upon successful completion (default action is to removed these files).
  -v          - Verbose mode, prints more messages to console (e.g. for debug).
  -V version  - Specifies EIDS/PDL version number; default is event.version from DB.

EOF
   exit $status;
}


exit main();

# The main function
sub main {

    # Process command-line arguments
    getopts ('c:d:EhkPRvV:w');

    if ($opt_h) {
      usage(0);
    }

    if ( scalar @ARGV < 1) {
       print STDERR "Error: Missing the required input arg: the event evid\n";
       usage(1);
    }

    my $eventID = $ARGV[0];    
    unless ( $eventID =~ m/^[0-9]+$/ ) { 
       print STDERR "\nInput event evid must be a number, not: $eventID\n";
       usage 1;
    }

    my $config = ( @ARGV > 1 ) ? $ARGV[1] : "../cfg/fmPublish.cfg";

    if ( ! -f $config ) {
       print STDERR "Error: Input cfg file $config not found!\n";
       usage(1);
    }

    #if ($opt_E && !$opt_w) {
    #    print STDERR "Can't send addons (-E) without publishing $imageType (-w)\n";
    #    usage(1);
    #}

    $FMversion = $opt_V if ($opt_V);

    parseConfig($config);

    $dbase = ($opt_d) ? $opt_d : $masterdb;
    my $dbusername = $masterdbuser; # from MasterDbs.pm
    my $dbpassword = $masterdbpwd;  # from MasterDbs.pm

    $dbh = DBI->connect("dbi:Oracle:" . $dbase, $dbusername, $dbpassword,
                        {RaiseError => 1, PrintError => 0, AutoCommit => 0});

    # This "eval" block provides exception handling: any "die" statement
    # causes us to exit the eval block with $@ set to the "die" argument
    # This construction saves us from convoluted "if" constructs.    
    my $status = 1;
    eval {

      $workDir = "$outputDir/$eventID";
      if ( ! -d $workDir ) {
        mkdir $workDir or die "Unable to create directory $workDir\n";
      }

      if ($opt_R) {
        #if (preforHasMecType($eventID)) {
        if (hasPrefMecOfType($eventID)) {
          PDLdeleteCurrent($eventID) if ($opt_P);
          deleteFM($eventID);
        }
        else {
          print "Event $eventID has no $mechType mechanisms to delete\n";
        }
      }

      my $distribStatus = 0;
      my $nSols = 1;

      my $quality = getMechQuality($eventID);
      if ( ! defined $quality ) {
        # Do the below code lines work? -aww 2013/10
        #$nSols = preforHasMecType($eventID);
        $nSols = hasPrefMecOfType($eventID);
        print "Event $eventID has no qualifying eventprefmec of type $mechType, deleting prior FM products\n";
        deleteOldAddons($eventID, $nSols);
        PDLdeletePrior($eventID, $nSols);
      }
      else {

        my $cmd = "./mecBlob2File.pl -d $dbase -w $workDir -e $mechType -A $eventID";

        (system($cmd) == 0) or die "Error running $cmd: $!\n";

        my $imgFile = "$workDir/*${eventID}*.$imageType";
        my $htmlFile = "$workDir/*${eventID}*.html";
        if ( -f glob $imgFile ) {
          push(@webfiles, $imgFile);
        }
        else {
          print STDERR "Missing file $imgFile\n";
          $distribStatus += 1;
        }

        if ( -f glob $htmlFile ) {
          push(@webfiles, $htmlFile);
        }
        else {
          print STDERR "Missing file $htmlFile\n";
          $distribStatus += 1;
        }

        $distribStatus += distributeFM($webPrivateCfg) if ($opt_w);

        if ( $quality >= $minMechQual ) {
          if ( $pubRegion eq "" or inRegion($pubRegion, $eventID) ) {
            $distribStatus += distributeFM($webPublicCfg) if ($opt_w);
            distributeAddon($eventID, $nSols) if ($opt_E);
            $distribStatus += PDLsubmit($eventID, $nSols) if ($opt_P);
          }
          else {
              print "INFO: no public distribution, event outside auth reporting region $pubRegion\n";
              #$status = 0;
          }
        }
        else {
              print "INFO: no public distribution, mec quality $quality < $minMechQual MIN_MECH_QUAL\n";
        }
      }
    
      # We finished!
      $status = ($distribStatus > 0); # return error if distribution problems
    };
    if ($@) {
      print STDERR $@;
      if ($? == -1) {
        print STDERR "command fork failed to execute: $!\n";
      }
      elsif ($? & 127) {
        printf STDERR "child died with signal %d, %s coredump\n",
            ($? & 127),  ($? & 128) ? 'with' : 'without';
      }
      else {
        printf STDERR "child exited with value %d\n", $? >> 8;
      }

      print STDERR strftime("%F %T",gmtime()) . " $cmdname failed for evid $eventID\n" unless ($status == 0);
      #$status = 1;
    }

    unlink glob "$workDir/*" unless $opt_k;
    rmdir $workDir unless $opt_k;

    return($status);
}


# Parse our configuration file
sub parseConfig {

    my $config = shift;

    open CF, "< $config" or die "Error opening $config: $!\n";
    while (<CF>) {
      # EIDS/CUBE add-on stuff
      if (/^\s*WEB_PRIVATE_CFG\s*=\s*(\S+)/i) {
            $webPrivateCfg = $1;
      }
      elsif (/^\s*WEB_PUBLIC_CFG\s*=\s*(\S+)/i) {
            $webPublicCfg = $1;
      }
      elsif (/^\s*WEB_COPY_CMD\s*=\s*(\S+)/i) {
            $webCopyCmd = $1;
      }
      elsif (/^\s*EIDS_TMP\s*=\s*(\S+)/i) {
            $EidsTmp = $1;
      }
      elsif (/^\s*EIDS_DIR\s*=\s*(\S+)/i) {
            $EidsDir = $1;
      }
      elsif (/^\s*URL_BASE\s*=\s*(\S+)/i) {
            $urlBase = $1;
      }
      elsif (/^\s*EIDS_SUBMITTER\s*=\s*(\S+)/i) {
            $eidsSubmitter = $1;
      }
      elsif (/^\s*LINK_TYPE\s*=\s*(\S+)/i) {
            $LItype = $1;
      }
      elsif (/^\s*LINK_DESCRIPTION\s*=\s*(.*)\s*$/i) {
            $LIdescription = $1;
      }
      elsif (/^\s*PUBREGION\s*=\s*(\S+)/i) { # optional
            $pubRegion = $1;
      }
      elsif (/^\s*PDL_DEL_CONF\s*=\s*(\S+)/i) {
            $PDLDelConf = $1;
      }
      # PDL stuff
      elsif (/^\s*PDL_CONF\s*=\s*(\S+)/i) {
            $PDLconf = $1;
      }
      elsif (/^\s*PDL_KEY\s*=\s*(\S+)/i) {
            $PDLkey = $1;
      }
      elsif (/^\s*PDL_CLIENT\s*=\s*(\S+)/i) {
            $PDLclient = $1;
      }
      elsif (/^\s*PDL_JAVA\s*=\s*(\S+)/i) {
            $PDLjava = $1;
      }
      elsif (/^\s*QML_PATH\s*=\s*(\S+)/i) {
            $qmlPath = $1;
      }
      elsif (/^\s*QML_CONF\s*=\s*(\S+)/i) {
            $qmlConf = $1;
      }
      elsif (/^\s*MECH_TYPE\s*=\s*(\S+)/i) {
            $mechType = $1;
      }
      elsif (/^\s*MIN_MECH_QUAL\s*=\s*(\S+)/i) {
            $minMechQual = $1;
      }
      elsif (/^\s*OUTPUT_DIR\s*=\s*(\S+)/i) {
            $outputDir = $1;
      }
      elsif (/^\s*IMAGE_TYPE\s*=\s*(\S+)/i) {
            $imageType = $1;
            $imageType =~ s/^\.//;
      }
    }

    if (defined $opt_w) {
      die "WEB_COPY_CMD missing from $config\n" unless (defined $webCopyCmd);
      die "WEB_PRIVATE_CFG missing from $config\n" unless (defined $webPrivateCfg);
      die "WEB_PUBLIC_CFG missing from $config\n" unless (defined $webPublicCfg);
    }
    if (defined $opt_E) {
      die "EIDS_TMP missing from $config\n" unless (defined $EidsTmp);
      die "EIDS_DIR missing from $config\n" unless (defined $EidsDir);
      die "URL_BASE missing from $config\n" unless (defined $urlBase);
      die "EIDS_SUBMITTER missing from $config\n" unless (defined $eidsSubmitter);
      die "LINK_TYPE missing from $config\n" unless (defined $LItype);
      die "LINK_DESCRIPTION missing from $config\n" unless (defined $LIdescription);
    }

    if (defined $opt_P) {
      # Some EIDS stuff used with PDL
      die "LINK_DESCRIPTION missing from $config\n" unless (defined $LIdescription); # needed in html 
      die "PDL_CONF missing from $config\n" unless (defined $PDLconf);
      die "PDL_KEY missing from $config\n" unless (defined $PDLkey);
      die "PDL_CLIENT missing from $config\n" unless (defined $PDLclient);
      die "PDL_JAVA missing from $config\n" unless (defined $PDLjava);
      die "QML_PATH missing from $config\n" unless (defined $qmlPath);
      die "QML_CONF missing from $config\n" unless (defined $qmlConf);
      $PDLDelConf = $PDLconf unless (defined $PDLDelConf);
    }

    return;
}

# ------------------------------
# Read the file that defines the web targets to which html and IMAGETYPE
# could be sent via rsync.or scp
# Returns an array of targets
sub readWebTargets {

    my $targetfile = shift;

    # read in list of targets to which we will distribute products
    open FILE, $targetfile or die "Can't open $targetfile: $!\n";
    my @targetlist;
    
    while (<FILE>) {
        chomp;
        next if (m/^\#/);    # skip "#" comments
        s/\s+$//;            # trim trailing whitespace
        s/^\s+//;            # trim leading whitespace
    
        push @targetlist, $_;
    }
    close FILE;
    return @targetlist;
}

sub preforHasMecType {

    my $eventID = shift;

    return $preforHasFM{$eventID} if (exists $preforHasFM{$eventID});

    my $hasFM = 0;
    eval {
        my $sth = $dbh->prepare(q{
        SELECT count(m.mecid) count
        FROM event e, origin o, mec m
        WHERE e.prefor = o.orid AND o.orid = m.oridin
        AND m.mechtype = :mechtype
        AND e.evid = :evid
        });
        $sth->bind_param(":mechtype", $mechType);
        $sth->bind_param(":evid", $eventID);
        $sth->execute();
        my $aryref = $sth->fetchall_arrayref({});
        if (scalar @$aryref == 0) {
          return $hasFM;
        }
        $hasFM = $aryref->[0]->{COUNT};
        $sth->finish();
    };
    if ($@) {
        die "preforHasMecType: $@";
    }
    return $preforHasFM{$eventID} = $hasFM;
}

sub hasPrefMecOfType {

    my $eventID = shift;

    return $epmHasFM{$eventID} if (exists $epmHasFM{$eventID});

    my $hasFM = 0;
    eval {
        my $sth = $dbh->prepare(q{
        SELECT count(m.mecid) count
        FROM eventprefmec e, mec m
        WHERE 
        e.evid = :evid AND
        e.mechtype = :mechtype AND
        m.mecid = e.mecid
        });
        $sth->bind_param(":mechtype", $mechType);
        $sth->bind_param(":evid", $eventID);
        $sth->execute();
        my $aryref = $sth->fetchall_arrayref({});
        if (scalar @$aryref == 0) {
          return $hasFM;
        }
        $hasFM = $aryref->[0]->{COUNT};
        $sth->finish();
    };
    if ($@) {
        die "hasPrefMecOfType: $@";
    }
    return $epmHasFM{$eventID} = $hasFM;
}

# clear this eventID from the preforHasFM cache
sub clearPreforHasFM {
    my $eventID = shift;
    delete $preforHasFM{$eventID};
    return;
}

# Distribute the FM IMAGETYPE and html files to web servers, etc.
sub distributeFM {

    my $cfgFile = shift; 

    my $webfiles = join ' ', @webfiles;

    $FMversion = 99 if ($FMversion > 99);
    # rsync options: -a archive mode, -v verbose, -u update mode; skip files that are newer on receiver
    my $basecmd = "$webCopyCmd $webfiles";

    my $error = 0;

    my @distribList = readWebTargets($cfgFile);

    foreach my $dest (@distribList) {
      my $cmd = "$basecmd $dest";
      print STDERR "Running $cmd\n" if ($opt_v);
      unless (system($cmd) == 0) {
        print STDERR strftime("%F %T",gmtime()) . " distributeFM: $cmd failed: $!\n";
        $error++;
      }
    }
    # We're called within an eval block. This "die" will throw us
    # out and skip any steps following the call to distributeFM.
    # If we distributed to ANY web servers, we want to send the CUBE LI
    # message to EIDS.
    if (scalar(@distribList) == $error) {
      die "Errors pushing to ALL web servers\n";
    }
    return $error;
}    

# Create the EIDS LI files and distribute them to EIDS
sub distributeAddon {
    my ($eventID, $nSols) = @_;
    my $status = 0;

    deleteOldAddons($eventID, $nSols);

    for (my $count = 1; $count <= $nSols; $count++) {
      my $type = "$LItype$count";
      #my $name = "$eidsSubmitter$eventID.$type";
      my $name = "$eidsSubmitter$eventID.${eidsSubmitter}fm$count";
      my $addonurl = "$urlBase/$name.html";
      my $description = "$LIdescription $count";
    
      eval {
        my $eidslinkfile = sprintf("$name.%02d.add", $FMversion);
        my $tmpfile = "$EidsTmp/$eidslinkfile";
        my $destfile = "$EidsDir/$eidslinkfile";

        open Q, ">$workDir/$eidslinkfile"
        or die "distributeAddon: Error creating $eidslinkfile: $!\n";
        printf Q "%2s%8s%2s%02d %s %s %s\n", "LI", "$eventID",
        "$eidsSubmitter", $FMversion, "$type", "$addonurl", "$description";
        close Q;
        copy "$workDir/$eidslinkfile", $tmpfile
        or die "distributeAddon: Error copying $eidslinkfile to $tmpfile: $!\n";
        move $tmpfile, $destfile
        or die "distributeAddon: Error moving $tmpfile to $destfile: $!\n";
        print strftime("%F %T",gmtime()) . " submitted $eidslinkfile to EIDS\n";
      };
      if ($@) {
        print STDERR $@;
        printf STDERR strftime("%F %T",gmtime()) . " distributeAddon: failed to send EIDS LI message for $eventID\n";
        $status = 1;
      }
    }
    die "distributeAddon failed\n" if ($status == 1);
    return;
}

# If there were more FM solutions from a previous origin than there are for 
# the current origin, issue EIDS LI deletions for the numbers of those 
# excess solutions. 
sub deleteOldAddons {

    my ($eventID, $nSols) = @_;
    my $status = 0;

    my $ref = getNumPrevFM($eventID);
    my $nPrevSols = (exists $ref->{NUM}) ? $ref->{NUM} : 0;
    print "deleteOldAddons: $nPrevSols previous solutions; $nSols current solutions for $eventID\n" if ($opt_v);
    return if ( $nPrevSols == 0 );
    
    for (my $count = 1; $count <= $nPrevSols; $count++) {
      my $type = "$LItype$count";
      #my $name = "$eidsSubmitter$eventID.$type";
      my $name = "$eidsSubmitter$eventID.${eidsSubmitter}fm$count";
      my $addonurl = "$urlBase/$name.html";
      my $description = "delete";
    
      eval {
        my $eidslinkfile = sprintf("$name.%02d.del", $count);
        my $tmpfile = "$EidsTmp/$eidslinkfile";
        my $destfile = "$EidsDir/$eidslinkfile";

        open Q, ">$workDir/$eidslinkfile"
        or die "deleteOldAddons: Error creating $eidslinkfile: $!\n";
        printf Q "%2s%8s%2s%02d %s %s %s\n", "LI", "$eventID",
        "$eidsSubmitter", $count, "$type", "$addonurl", "$description";
        close Q;
        copy "$workDir/$eidslinkfile", $tmpfile
        or die "deleteOldAddons: Error copying $eidslinkfile to $tmpfile: $!\n";
        move $tmpfile, $destfile
        or die "deleteOldAddons: Error moving $tmpfile to $destfile: $!\n";
        print "Submitted $eidslinkfile to EIDS\n";
      };
      if ($@) {
        print STDERR $@;
        printf STDERR strftime("%F %T",gmtime()) . " deleteOldAddons: failed to send EIDS LI message for $eventID\n";
        $status = 1;
      }
    }
    die "deleteOldAddons failed\n" if ($status == 1);
    return;
    
}

sub getMechQuality {

    my $eventID = shift;
    
    my $orid = 0;
    my $quality = undef;
    eval {
        # Assume no deleted events are to be processed
        my $sth = $dbh->prepare(q{
        SELECT NVL(m.quality, 0.) FROM event e, eventprefmec p, mec m WHERE e.evid = :evid AND
        e.selectflag=1 AND p.evid=e.evid AND p.mechtype = :mtype AND m.mecid=p.mecid });
        $sth->bind_param(":evid", $eventID);
        $sth->bind_param(":mtype", $mechType);
        $sth->execute();
        # should only be one row in eventprefmec of type
        ($quality) = $sth->fetchrow_array();
        $sth->finish();
    };
    if ($@) {
        die "getMechQuality: $@\n";
    }
    return $quality;
}

sub getNumPrevFM {

    my $eventID = shift;
    
    return $numPrevFM{$eventID} if (exists $numPrevFM{$eventID});
    
    my $numPrevFM = 0;
    my $orid = 0;
    eval {
        my $sth = $dbh->prepare(q{
        SELECT o.orid, o.lddate, COUNT(m.mecid) num 
        FROM event e, origin o, mec m
        WHERE m.oridin = o.orid 
        AND o.evid = :evid 
        AND e.evid = o.evid
        AND o.orid != e.prefor
        AND m.mechtype = :mtype
        GROUP BY o.orid, o.lddate 
        ORDER BY o.lddate desc
        });
        $sth->bind_param(":evid", $eventID);
        $sth->bind_param(":mtype", $mechType);
        $sth->execute();
        # Look for the largest number of previous solutions
        my $aryref = $sth->fetchall_arrayref({});
        foreach my $ref (@$aryref) {
          if ($numPrevFM < $ref->{NUM}) {
            $numPrevFM = $ref->{NUM};
            $orid = $ref->{ORID};
          }
        }
        $sth->finish();
    };
    if ($@) {
        die "getNumPrevFM: $@\n";
    }
    return $numPrevFM{$eventID} = {ORID => $orid, NUM => $numPrevFM, };
}

sub deleteFM {
    # WARNING !
    # This routine DELETES from database the EVENTPREFMEC MEC row, and all MECxxxx  associated table rows 
    # that are associated with EVENT.PREFOR after NULLING the  prefmec in the Event and Origin tables if
    # the prefmec has same MECHTYPE as configured !!

    my $eventID = shift;

    my $action = '';
    eval {
      $action = "nulling event.prefmec of $mechType for $eventID";
      my $rows = $dbh->do(q{ UPDATE event e SET e.prefmec=NULL WHERE e.evid=? and
         e.prefmec=(SELECT m.mecid FROM mec m WHERE  m.mechtype=? AND m.mecid=e.prefmec) }, undef, $eventID, $mechType);
      print "$rows while $action\n";

      $action = "nulling preferred origin.prefmec if $mechType for $eventID";
      $rows = $dbh->do(q{ UPDATE origin o SET o.prefmec=NULL WHERE
         o.orid=(SELECT e.prefor FROM event e WHERE  e.evid = ?) AND 
         e.prefmec=(SELECT m.mecid FROM mec m WHERE  m.mechtype=? AND m.mecid=o.prefmec) }, undef, $eventID, $mechType);
      print "$rows while $action\n";

      $action = "deleting $mechType row from eventprefmec for $eventID";
      $rows = $dbh->do(q{
        DELETE FROM eventprefmec m WHERE m.mechtype=? AND m.evid=?)}, undef, $mechType, $eventID);
      print "$rows while $action\n";
    
      $action = "deleting from mecchannel";
      $rows = $dbh->do(q{
        DELETE FROM mecchannel WHERE mecdataid in
        (SELECT md.mecdataid 
         FROM mecdata md, mec m, event e
         WHERE md.mecid = m.mecid AND m.oridin = e.prefor
         AND m.mechtype = ? AND e.evid = ?)}, undef, $mechType, $eventID);
      print "$rows while $action\n";

      $action = "deleting from mecdata";
      $rows = $dbh->do(q{
        DELETE FROM mecdata WHERE mecid in
        (SELECT m.mecid
         FROM mec m, event e
         WHERE m.oridin = e.prefor
         AND m.mechtype = ? AND e.evid = ?)}, undef, $mechType, $eventID);
      print "$rows while $action\n";

      $action = "deleting from mecobject";
      $rows = $dbh->do(q{
        DELETE FROM mecobject WHERE mecid in
        (SELECT m.mecid
         FROM mec m, event e
         WHERE m.oridin = e.prefor
         AND m.mechtype = ? AND e.evid = ?)}, undef, $mechType, $eventID);
      print "$rows while $action\n";

      $action = "deleting from mec";
      $rows = $dbh->do(q{
        DELETE FROM mec WHERE mecid in
        (SELECT m.mecid
         FROM mec m, event e
         WHERE m.oridin = e.prefor
         AND m.mechtype = ? AND e.evid = ?)}, undef, $mechType, $eventID);
      print "$rows while $action\n";

      $action = "committing transaction";
      $dbh->commit();
    };
    if ($@) {
      $dbh->rollback();
      die "deleteFM: Error while $action: $@\ntransaction rolled back\n";
    }
    clearPreforHasFM($eventID);
    return;
}

sub PDLsubmit {

    my ($eventID, $nSols) = @_;
    my $status = 0;

    PDLdeleteOld($eventID, $nSols);

    my $QMLbase = "$qmlPath -c $qmlConf";
    my $PDLbase = "$PDLjava -jar $PDLclient --configFile=$PDLconf --privateKey=$PDLkey";
    #my $qml = "$eidsSubmitter${eventID}_$QML";
    my $qml = "$QML";

    eval {
      for (my $nsol = 1; $nsol <= $nSols; $nsol++) {
        my $cmd = "$QMLbase -o $workDir/$qml -f $nsol $eventID";
        print "running $cmd\n" if ($opt_v);
        (system($cmd) == 0) or die "Error running $cmd: $!\n";
        
        #my $img = "$eidsSubmitter$eventID.$LItype$nsol.$imageType";
        my $img = "$eidsSubmitter$eventID.${eidsSubmitter}fm$nsol.$imageType";
        ## added html file attachment to code below - aww 2014-04-23
        my $html = "$eidsSubmitter$eventID.${eidsSubmitter}fm$nsol.html";
        my $xml = generateXML($eventID, $nsol, $img, $qml, $html);

        #$cmd = "$PDLbase --file=$workDir/$qml --attach=$workDir/$img";
        $cmd = "$PDLbase --file=$workDir/$qml --attach=$workDir/$img --attach=$workDir/$html";
        if ( defined $xml && -f "$workDir/$xml" ) {
          $cmd .= " --attach=$workDir/$xml";
        }
        runPDL($cmd);
      }
    };
    if ($@) {
      $status = 1;
      print STDERR strftime("%F %T",gmtime()) . " PDLsubmit failed for $eventID: $@\n";
    }
    else {
      print strftime("%F %T",gmtime()) . " submitted $eidsSubmitter$eventID qml,xml,$imageType to PDL\n";
    }
    return $status;
}

sub PDLdeletePrior {

    my ($eventID, $nSols) = @_;
    my $status = 0;

    my $ref = getNumPrevFM($eventID);
    my $nPrevSols = 0;
    my $prevOrid = 0;
    if (defined $ref) {
      $nPrevSols = $ref->{NUM};
      $prevOrid = $ref->{ORID};
    }
    print "PDLdeletePrior: $nPrevSols previous solutions; $nSols current solutions for $eventID\n" if ($opt_v);
    return $status if ( $nPrevSols == 0 );

    my $PDLbase = "$PDLjava -jar $PDLclient --configFile=$PDLconf --privateKey=$PDLkey";
    eval {
      for (my $nsol = 1; $nsol <= $nPrevSols; $nsol++) {
        my $cmd = "$PDLbase --source=$eidsSubmitter --type=focal-mechanism --code=$eidsSubmitter${eventID}_fm$nsol --delete --send";
        print "running $cmd\n" if ($opt_v);
        (system($cmd) == 0) or die "Error running $cmd: $!\n";
      }
    };
    if ($@) {
      print STDERR "PDLdeletePrior failure for $eventID: $@\n";
      $status = 1;
    }
    return $status;
}

sub PDLdeleteOld {

    my ($eventID, $nSols) = @_;
    my $status = 0;

    my $ref = getNumPrevFM($eventID);
    my $nPrevSols = 0;
    my $prevOrid = 0;
    if (defined $ref) {
      $nPrevSols = $ref->{NUM};
      $prevOrid = $ref->{ORID};
    }
    print "PDLdeleteOld: $nPrevSols previous solutions; $nSols current solutions for $eventID\n" if ($opt_v);
    return $status if ( $nPrevSols == 0 );

    my $QMLbase = "$qmlPath -c $qmlConf -D -a $prevOrid";
    my $PDLbase = "$PDLjava -jar $PDLclient --configFile=$PDLDelConf --privateKey=$PDLkey";
    #my $qml = "$eidsSubmitter${eventID}_$QML";
    my $qml = "$QML";

    eval {
      for (my $nsol = 1; $nsol <= $nPrevSols; $nsol++) {
        #my $cmd = "$PDLbase --delete --send --source=$eidsSubmitter --type=focal-mechanism --code=${eidsSubmitter}${eventID}_fm$nsol";
        ###my $cmd = "$QMLbase -o $workDir/$qml -f $nsol $eventID";
        ###print "running $cmd\n" if ($opt_v);
        ###(system($cmd) == 0) or die "Error running $cmd: $!\n";
        ###$cmd = "$PDLbase --file=$workDir/$qml";
        #print "running $cmd\n" if ($opt_v);
        #(system($cmd) == 0) or die "Error running $cmd: $!\n";
        my $cmd = "$QMLbase -o $qml -f $nsol $eventID";
        print "running $cmd\n" if ($opt_v);
        (system($cmd) == 0) or die "Error running $cmd: $!\n";
 
        # PDL EIDSinputwedge doesn't currently (version 1.8.8) understand QuakeML 
        # focal-mechanism deletion.
        # Instead, use bare PDL with command-line params to delete the focal-mechanism
        $cmd = "$PDLbase --send --delete --source=$eidsSubmitter --type=focal-mechanism --code=$eidsSubmitter${eventID}_fm$nsol" .
               " --eventsource=$eidsSubmitter --eventsourcecode=${eventID} --file=$qml";
        runPDL($cmd);
      }
    };
    if ($@) {
      print STDERR "PDLdeleteOld failure for $eventID: $@\n";
      $status = 1;
    }
    return $status;
}

# Generate QuakeML FM deletion message, submit to PDL
sub PDLdeleteCurrent {

    my $eventID = shift;
    my $status = 0;

    #my $nSols = preforHasMecType($eventID);
    my $nSols = hasPrefMecOfType($eventID);
    my $QMLbase = "$qmlPath -c $qmlConf -D";
    my $PDLbase = "$PDLjava -jar $PDLclient --configFile=$PDLDelConf --privateKey=$PDLkey";
    #my $qml = "$eidsSubmitter${eventID}_$QML";
    my $qml = "$QML";

    eval {
      for (my $nsol = 1; $nsol <= $nSols; $nsol++) {
        #my $cmd = "$PDLbase --delete --send --source=$eidsSubmitter --type=focal-mechanism --code=${eidsSubmitter}${eventID}_fm$nsol";
        ##my $cmd = "$QMLbase -o $workDir/$qml -f $nsol $eventID";
        ##print "running $cmd\n" if ($opt_v);
        ##(system($cmd) == 0) or die "Error running $cmd: $!\n";
        
        ##$cmd = "$PDLbase --file=$workDir/$qml";
        #print "running $cmd\n" if ($opt_v);
        #(system($cmd) == 0) or die "Error running $cmd: $!\n";
        my $cmd = "$QMLbase -o $qml -f $nsol $eventID";
        print "running $cmd\n" if ($opt_v);
        (system($cmd) == 0) or die "Error running $cmd: $!\n";
            
        # PDL EIDSinputwedge doesn't currently (version 1.8.8) understand QuakeML 
        # focal-mechanism deletion.
        # Instead, use bare PDL with command-line params to delete the focal-mechanism
        $cmd = "$PDLbase --send --delete --source=$eidsSubmitter --type=focal-mechanism --code=$eidsSubmitter${eventID}_fm$nsol" .
               " --eventsource=$eidsSubmitter --eventsourcecode=${eventID} --file=$qml";
        runPDL($cmd);
      }
    };
    if ($@) {
      print STDERR "PDLdeleteCurrent failure for $eventID: $@\n";
      $status = 1;
    }
    return $status;
}

sub generateXML {

    my ($eventID, $nsol, $img, $qmlRef, $htmlRef) = @_;

    #my $filename = "$eidsSubmitter${eventID}_contents.xml";
    my $filename = "contents.xml";
    my $fh = new IO::File "> $workDir/$filename" or die "Couldn't create $workDir/$filename";

    my $writer = new XML::Writer(OUTPUT => \*$fh, DATA_MODE => 1, DATA_INDENT => "  ");
    $writer->xmlDecl("UTF-8");
    $writer->startTag("contents");

    # IMAGETYPE, qml files
    $writer->startTag("file", "title" => "$LIdescription $nsol", "id" => "fm$nsol");

    $writer->startTag("caption");
    $writer->cdata("$LIdescription $nsol");
    $writer->endTag();

    $writer->emptyTag("format", "href" => "$img", "type" => "image/$imageType");
    $writer->emptyTag("format", "href" => "$qmlRef", "type" => "text/xml");
    $writer->emptyTag("format", "href" => "$htmlRef", "type" => "text/html");

    $writer->endTag();
    $writer->endTag();
    $writer->end();
    $fh->close;

    return $filename;
}

# Run a ProductCLient command, evaluate and optioanlly report its exit status.
# dies if command failed, so this should be called within an evel block.
sub runPDL {
    my $cmd = shift;

    print "running $cmd\n" if ($opt_v);
    if (system($cmd) == 0) {
        print "cmd succeeded\n" if ($opt_v);
        return;
    }

    my $exitStatus = $? >> 8;

    # Evaluate ProductCLient exit status. See PDL documentation, e.g.
    # http://ehppdl1.cr.usgs.gov/userguide/commandLine.html#exit
    if ($exitStatus == 0) {
        print "cmd succeeded\n" if ($opt_v);
    }
    elsif ($exitStatus == 1) {
        die "ProductClient: invalid arguments\n";
    }
    elsif ($exitStatus == 2) {
        die "ProductClient: unable to build product\n";
    }
    elsif ($exitStatus == 3) {
        die "ProductClient: failed to send product\n";
    }
    elsif ($exitStatus == 4) {
        print STDERR "ProductClient: sent to some but not all destinations\n";
    }
    else {
        die "ProductClient failed with unknown exit status $exitStatus\n";
    }

    return;
}

sub inRegion {
    my $region = shift;
    my $evid = shift;

    my $sth = $dbh->prepare(q{SELECT geo_region.inside_border(:region, o.lat, o.lon) AS inside 
               FROM event e, origin o WHERE e.prefor = o.orid AND e.evid = :evid});
    $sth->bind_param(":evid", $evid);
    $sth->bind_param(":region", $region);
    $sth->execute();

    my $aryref = $sth->fetchall_arrayref({});
    if (scalar @$aryref == 0) {
        die "Error: event region: \"$region\" not found in db table\n";
    }
    my $inside = $aryref->[0]{INSIDE};
    $sth->finish();
    return $inside;
}
