#!/usr/bin/perl

use strict;
use warnings;
use DBI;            # Load the DBI module
use DBD::Oracle qw(:ora_types);

# get masterdb and username/password
use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use DbConn; 
use MasterDbs;

use Getopt::Std;
our ( $opt_A, $opt_d, $opt_e, $opt_h, $opt_m, $opt_v, $opt_w );

# parse optional switches
getopts('Ad:e:hmvw:');

sub usage {
  my ($status) = @_;
  print <<EOF;
  Reads from  MecObject table data for an mecid and
  writes that row's blob data to the meta column filename.
  Output file path is the working directory unless -w is specified.
  
  The first argument is the mecid, or the evid with the -e option.
  For the -e option the mecid must be found in the eventprefmec table
  for the mechtype matching the value of the -e option.

  A second input argument is not required if writing all objects
  using the -A option.

  Otherwise a dataid or mimetype whose value matches the second argument.
  which is by default the dataid, use -m for the mimetype.
  
  Syntax: $0 [-d dbase] [-w path] [-A] [-m] [-e mtype] <id> <attrib>
  -d dbase   : alias name of the database connection.
  -w path    : path for output files.
  -A         : all objects in mecobject table.
  -e mtype   : the mechtype, FA, FP, or MT etc.
  -m         : second argument value is the mimetype
               by default it is the dataid number.
  -v         : verbose log of actions
EOF
  exit $status;
}

if ( $opt_h ) {
  usage 0;
}
if ( @ARGV < 1 || (! $opt_A && @ARGV < 2)  ) {
 print STDERR "Missing required input arguments\n";
  usage 1;
}
#dataid mimetype
# 2     text/html
# 3     image/jpg
my $id = $ARGV[0];
my $cvalue = ( $opt_A ) ? '' : $ARGV[1];

my $path = ( $opt_w ) ? "$opt_w" : '.'; 
$path = $path . '/' if ( $path !~ '/$' );
$path =~ s/\/\//\//g;
if ( ! -d $path ) {
  print STDERR "$0 error: input path $path D.N.E\n";
  exit 1;
} 

my $dbase = $masterdb;
if ( $opt_d ) {$dbase = $opt_d; }
my $connect = "dbi:Oracle:$dbase";

print "==================================================================================\n" if $opt_v;
print "$0 connecting to: $dbase, as $masterdbuser\n" if $opt_v;
my $status=0;
eval {
  my $dbconn = DBI->connect( $connect, $masterdbuser, $masterdbpwd,
  {RaiseError => 1, PrintError => 1, ShowErrorStatement => 1, AutoCommit => 0})
    or die "Can't connect to Oracle database\n";

  $dbconn->{LongTruncOk} = 1;
  $dbconn->{LongReadLen} = 2*1024*1024;

  my $sth;
  my $mecid = $id;

  if ( $opt_e ) {
    $sth = $dbconn->prepare( "SELECT mecid From EVENTPREFMEC where mechtype = ? AND evid = ?" );
    $sth->execute($opt_e, $id);
    my ($mid) = $sth->fetchrow_array();
    if ( defined $mid && $mid > 0 ) {
      $mecid = $mid;
    }
    else {
      $status=2;
      die "No mecid in EventPrefmec for input evid=$id and mechtype=$opt_e\n";
    }
  }

  if ( $opt_A ) {
    $sth = $dbconn->prepare( "SELECT data,meta From MECOBJECT where mecid = ?", { ora_auto_lob => 0 } );
    $sth->execute($mecid);
  }
  else {
    my $coltype = ( $opt_m ) ? 'mimetype' : 'dataid';
    $sth = $dbconn->prepare( "SELECT data,meta From MECOBJECT where mecid = ? and  $coltype = ?", { ora_auto_lob => 0 } );
    $sth->execute($mecid, $cvalue);
  }

  while ( my ($blobloc, $fn) = $sth->fetchrow_array() ) {
    my $len =$dbconn->ora_lob_length($blobloc);
    #print "Read db blob len=$len\n";

    my $chunk_size = 4096;   # Arbitrary chunk size, for example

    my $offset = 1;   # Offsets start at 1, not 0
    my ($junk, $fn2) = split '=', $fn; 
    $fn2 = $path . $fn2;
    print "Writing for mecid: $mecid $cvalue to $fn2\n" if $opt_v;

    open(BLOB,">${fn2}") or die "Unable is open $fn2 $!";
    binmode BLOB;
    while( my $data = $dbconn->ora_lob_read( $blobloc, $offset, $chunk_size ) ) {
        print BLOB $data;
        $offset += $chunk_size;
    }
    close BLOB;
  }

  if ( $sth->rows() == 0 ) {
    print STDERR "No rows found in mecobject table for : $mecid $cvalue\n";
    $status=3;
  }

  $sth->finish();
  $dbconn->disconnect();

};
if ( $@ ) {
  print STDERR "DBI $dbase: $DBI::errstr\n";
  $status=1;
}

exit $status;
