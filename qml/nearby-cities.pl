#!/usr/bin/perl

#This script generates the nearby cities json for PDL

use strict; 
use warnings;
use File::Basename;
use Getopt::Std;
use DBI;
use XML::Writer;
use IO::File;
use IO::Handle;
use Math::MatrixReal;  # CPAN module
use Math::Trig;
use Math::Trig ':pi';
use Time::Local;
use Time::HiRes qw( time );
use List::Util qw(min max);
use Data::Dumper;  # for debugging


use vars qw ( $VERSION $defConfig $dbname $dbuser $dbpass $cmdname );
use vars qw ( $opt_c $opt_d $opt_h $opt_i $opt_o );
use vars qw ( $dbh $timeBase );
use vars qw ( %evSql %evStmt %utilSql %utilStmt %event %priKey );

my @nearestLocales;

$VERSION = '$Id: nearby-cities 0000 2017-04-20 23:31:08Z aparnab $';
$defConfig = "/app/aqms/utils/qml/cfg/qml.conf";

%evSql = (
       	 EVENT => qq{SELECT * FROM event WHERE evid = :evid},
	 NETMAG => qq{SELECT * FROM netmag
		       WHERE magid IN (
		       SELECT prefmag FROM event WHERE evid = :evid)},
         ORIGIN => qq{SELECT * FROM origin
			 WHERE orid = :orid},
         #EPREFOR => qq{SELECT * FROM eventprefor
         #             WHERE evid = :evid AND type = :type},
	   );

%utilSql = (
         TIME => q{SELECT truetime.timeBaseIsTrue() FROM dual}, 
       	 WHERES => q{SELECT wheres.locale_by_type(:lat, :lon, :elev, :cnt, :type) "LOCALE" FROM DUAL},
	 CITIES => q{SELECT distinct p.lat,p.lon from gazetteerpt p left outer join gazetteerbigtown t on p.gazid = t.gazid where p.type in (10,12,70) and p.name = :name },
         );

%priKey = (
	   EVENT         => 'EVID',
           NETMAG        => 'MAGID',
           ORIGIN        => 'ORID',
          );


###############################################################################
#	print_syntax	- print syntax and exit.
###############################################################################
sub print_syntax {
    my ($cmdname) = @_;
    print << "EOF";
    $cmdname version $VERSION
	$cmdname - generate quakeml XML for one or more events
    Syntax:
	$cmdname [-c config] [-d debug] event-id...
	
	$cmdname [-c config] [-d debug] -i eventlist_file

	$cmdname -h
    where:
	-c config	- specify an alternate configuration file.
		The default config file is $defConfig.
	-i file		- read event IDs from <file> instead of from
		commandline. To read from STDIN, use "-i -".
	-o out_file	- write json to out_file; default is to stdout.
	-d level	- specify the debug level.	
	-h		- prints this help message.

EOF
  exit(0);
}

exit main();

sub main {
    
    # Parse command line
    getopts('c:hi:d:o:');
    $cmdname = basename($0);
    
    print_syntax($cmdname) if ($opt_h);

    my $config = ($opt_c) ? $opt_c : $defConfig;
    parseConfig($config);

    # Prepare output file:
    my $output;
     if ($opt_o) {
	$output = new IO::File(">$opt_o");
	die "Error creating $opt_o: $!\n" unless (defined $output);
    } else {
	$output = new IO::Handle;
	$output->fdopen(fileno(STDOUT),"w");
    }

    # collect all the eventIDs from whereever we get them
    my @evids;
    my $input;
    if ($opt_i) {
	if ($opt_i eq '-') {
	    $input = new IO::Handle;
	    $input->fdopen(fileno(STDIN), "r")
	      or die "Can't fdopen STDIN: $!\n";
	} else {
	    $input = new IO::File;
	    $input->open("< $opt_i")
	      or die "Can't fopen $opt_i: $!\n";
	}
	while (<$input>) {
	    chomp;
	    push @evids, $_;
	}
	$input->close() if (defined $input && $opt_i ne '-');
    } else {
	@evids = @ARGV;
    }
    unless (@evids) {
	print STDERR "No event IDs found\n";
	print_syntax($cmdname);
    }

    # Set Oracle data format so we can parse it in parseTime():
    $ENV{NLS_DATE_FORMAT} = 'yyyy-mm-dd hh24:mi:ss';
    eval {
	$dbh = DBI->connect("dbi:Oracle:" . $dbname, $dbuser, $dbpass,
			    {PrintError => 0,
			     RaiseError => 1,
			     FetchHashKeyName => "NAME_uc",
			    }) or 
	die "Can't continue after errors\n"; 

	my @keys;
	@keys = ("EVENT","NETMAG","ORIGIN");#,"EPREFOR");
	foreach my $key (@keys) {
	    eval {
		$evStmt{$key} = $dbh->prepare($evSql{$key});
	    };
	    if ($@) {
		print STDERR "prepare error at $key: $@\n";
		return;
	    }
	}
	
	foreach my $key (keys %utilSql) {
	    eval {
		$utilStmt{$key} = $dbh->prepare($utilSql{$key});
	    };
	    if ($@) {
		print STDERR "error at $key: $@\n";		
	    }
	}

	getTimeBase();

	foreach my $evid (@evids) {
	    getData($evid);
	}
    };
    return 0;
}

sub getData {
    my $evid = shift;

    # queries by eventID
    my @keys = ("EVENT", "NETMAG");
    foreach my $key (@keys) {
	eval {	    
	if (exists $evStmt{$key}) {
		my $sth = $evStmt{$key};
		$sth->bind_param(":evid", $evid);
		$sth->execute();
		my $saveKey = $key;	       
		$event{$evid}{$saveKey} = $sth->fetchall_hashref($priKey{$key});
	    }
	};

	if ($@) {
	    die "fetch error at $key: $@\n";
	}
    }
    my $prefor = $event{$evid}{EVENT}{$evid}{PREFOR};
    my %origin;

    my $key = "ORIGIN";
    eval {
	if (exists $evStmt{$key}) {
	    my $sth = $evStmt{$key};		
	    $sth->bind_param(":orid", $prefor);
	    $sth->execute();
	    $event{$evid}{$key} = $sth->fetchall_hashref($priKey{$key});
	}
    };

    wheresJSON($evid, $event{$evid});
    
}

# Determine the time base used by this database:
#   True Epoch Time (includes leapseconds)
# or Nominal Epoch Time (does not include leapseconds)
# $timeBase is then used for converting database times
# to calendar date-times.
sub getTimeBase {    
    my $sth = $utilStmt{TIME};  
    $sth->execute();
    my @row = $sth->fetchrow_array();
    die "NULL return from timeBaseIsTrue query\n"
      unless (@row);
    my $isTrue = $row[0];
    $timeBase = ($isTrue) ? "T" : "N";
    $sth->finish();
}

sub parseConfig {
    my $config = shift;
    
    open CF, "< $config" or die "Error opening $config: $!\n";
    while (<CF>) {
	if (/DB_USER\s*=\s*(\S+)/i) {
	    $dbuser = $1;
	} elsif (/DB_PASSWORD\s*=\s*(\S*)/i) {
	    $dbpass = $1;
	} elsif (/DB_NAME\s*=\s*(\S*)/i) {
	    $dbname = $1;
	}
    }

    die "DB_USER missing\n" unless (defined $dbuser);
    die "DB_PASSWORD missing\n" unless (defined $dbpass);
    die "DB_NAME missing\n" unless (defined $dbname);

    return;
}


sub wheresJSON {
    my ($evid, $eref) = @_;

#    return unless (defined $eref->{EVENT}{$evid}{PREFOR} &&
#                   exists $eref->{ORIGIN}{$eref->{EVENT}{$evid}{PREFOR}});
    my $orid = $eref->{EVENT}{$evid}{PREFOR};
    my $oref = $eref->{ORIGIN}{$orid};
    return unless defined $oref;

    my $mag = $eref->{NETMAG}{$eref->{EVENT}{$evid}{PREFMAG}}{MAGNITUDE};
    my $townCnt = 5;
    my $bigTownCnt = 0;
    my $poiCnt = 0;
    if ( $mag ) {
        if ( $mag > 6.45 ) {
            $townCnt = 2;
	    $bigTownCnt = 1;
            $poiCnt = 4;
        }
        elsif ( $mag > 5.45 ) {
            $townCnt = 2;
            $bigTownCnt = 3;
            $poiCnt = 3;
        }
        elsif ($mag > 4.45 ) {
            $townCnt = 3;
            $bigTownCnt = 2;
            $poiCnt = 2;
        }
        elsif ($mag > 3.45 ) {
            $townCnt = 3;
            $bigTownCnt = 2;
            $poiCnt = 1;
        }
        elsif ($mag > 2.45 ) {
            $townCnt = 4;
            $bigTownCnt = 1;
        }
    }

    my $name = getQuakeName($oref, $townCnt);
    return unless defined $name;

#        #$bigTownCnt =4;
#        #$poiCnt =4;

    my @x = loadClosestBigTowns($oref, $bigTownCnt, \@nearestLocales);
    push (@nearestLocales, @x) if $bigTownCnt > 0 && scalar @x > 0;
    @x = loadClosestEqPOI($oref, $poiCnt, \@nearestLocales);
    push (@nearestLocales, @x) if $poiCnt > 0 && scalar @x > 0;
    writeNearbyCitiesJson($evid);

}

# Table of state and other regions and their abbreviations.
my %stateMap = (
		"AB" => "Alberta",
		"AK" => "Alaska",
		"AL" => "Alabama",
		"AR" => "Arkansas",
		"AZ" => "Arizona",
		"BC" => "Britush Columbia",
		"BN" => "Baja California",
		"CA" => "California",
		"CO" => "Colorado",
		"CT" => "Connecticut",
		"DC" => "District of Columbia",
		"DE" => "Delaware",
		"FL" => "Florida",
		"GA" => "Georgia",
		"HI" => "Hawaii",
		"IA" => "Iowa",
		"ID" => "Idaho",
		"IL" => "Illinois",
		"IN" => "Indiana",
		"KS" => "Kansas",
		"KY" => "Kentucky",
		"LA" => "Louisiana",
		"MA" => "Massachusetts",
		"MB" => "Manitoba",
		"MD" => "Maryland",
		"ME" => "Maine",
		"MI" => "Michigan",
		"MN" => "Minnesota",
		"MO" => "Missouri",
		"MS" => "Mississippi",
		"MT" => "Montana",
		"NC" => "North Carolina",
		"ND" => "North Dakota",
		"NE" => "Nebraska",
		"NH" => "New Hampshire",
		"NJ" => "New Jersey",
		"NM" => "New Mexico",
		"NS" => "Nova Scotia",
		"NV" => "Nevada",
		"NY" => "New York",
		"OH" => "Ohio",
		"OK" => "Oklahoma",
		"ON" => "Ontario",
		"OR" => "Oregon",
		"PA" => "Pennsylvania",
		"PR" => "Puerto Rico",
		"QC" => "Quebec",
		"RI" => "Rhode Island",
		"SC" => "South Carolina",
		"SD" => "South Dakota",
		"SK" => "Saskatchewan",
		"TN" => "Tennessee",
		"TX" => "Texas",
		"UT" => "Utah",
		"VA" => "Virginia",
		"VI" => "Virgin Islands",
		"VT" => "Vermont",
		"WA" => "Washington",
		"WI" => "Wisconsin",
		"WV" => "West Virginia",
		"WY" => "Wyoming",
	       );



sub getQuakeName {
    my $oref = shift;
#merge my $cnt=1;
    my $cnt = shift;
    my $sth = $utilStmt{WHERES};
    $sth->bind_param(":lat", $oref->{LAT}); 
    $sth->bind_param(":lon", $oref->{LON}); 
    $sth->bind_param(":elev", 0 ); 
    $sth->bind_param(":cnt", $cnt); 
    $sth->bind_param(":type", 'TOWN'); 
    $sth->execute();

    my $aryref = $sth->fetchall_arrayref({});
    return '' unless (1 == scalar @$aryref);

    my $results = $aryref->[0]{LOCALE};
    #print "$results\n";
    my @locales = split(/[\r\n]/,$results);
    #        #print "@locales\n";
    
    my $name = '';
    my $idx = 0;
    
    foreach my $locale (@locales) {
        my $str = reformatWheresString($locale);
        push ( @nearestLocales, $str);
        if ( $idx == 0 ) {
            if ( $locale =~ m/\s+(\d+(\.\d+)?) km.*mi\) (\w+) .*from (\w.*,) (\w+)/ ) {
            my $state = expandStateCode($5);
            $name = sprintf("%.0fkm $3 of $4 $state", $1);
           }
    }
    $idx++;
    }

    return $name;
}

sub reformatWheresString {
    my $locale = shift;
    my $str = '';

    if ( $locale =~ m/\s+(\d+(\.\d+)?) km.*mi\) (\w+) .*from (\w.*,) (\w+)/ ) {
            my $state = expandStateCode($5);
            #print "$1 km $3 of $4 $5\n";
            my $sth2 = $utilStmt{CITIES};
            $sth2->bind_param(":name", substr($4,0,-1));
            $sth2->execute();
            my $aryref = $sth2->fetchall_arrayref({});
            my $lat =0;
            my $lon =0;
            my $pop="null"; # remove population in nearby-cities.json 
            #if (1 == scalar @$aryref) {
                $lat = $aryref->[0]{LAT};
                $lon = $aryref->[0]{LON};
#                $pop = $aryref->[0]{POP} || 1; # remove population in nearby-cities.json
            #print "$1 km $3 of $4 $5 $lat $lon\n";
            #}
            $str = sprintf("{\"distance\":%.0f,\"direction\":\"$3\",\"name\":\"$4 $state\",\"longitude\":$lon,\"latitude\":$lat,\"population\":$pop}", $1);
    }
    
    return $str;
}

sub expandStateCode() {
    my $locale = shift;

    return ((exists $stateMap{$locale}) ? $stateMap{$locale} : $locale)
}


sub loadClosestBigTowns {
    my $oref = shift;
    my $cnt = shift;
    return if $cnt == 0;

    my $towns = shift;

    my $maxcnt = 5;
    my $sth = $utilStmt{WHERES};
    $sth->bind_param(":lat", $oref->{LAT});
    $sth->bind_param(":lon", $oref->{LON});
    $sth->bind_param(":elev", 0 );
    $sth->bind_param(":cnt", $maxcnt);
    $sth->bind_param(":type", 'BIGTOWN');
    $sth->execute();

    my $aryref = $sth->fetchall_arrayref({});
    return unless (1 == scalar @$aryref);

    my $results = $aryref->[0]{LOCALE};
    print "$results\n";
    my @locales = split(/[\r\n]/,$results);
    my @bigtowns;
    foreach my $locale (@locales) {
        my $str = reformatWheresString($locale);
        push ( @bigtowns, $str);
    }
    my @z = removeBfromA(\@bigtowns, \@$towns);
    $cnt = min($cnt-1,$#z);
    #return @z[ 0 .. $cnt ];
    return ( $cnt >= 0 ) ? @z[ 0 .. $cnt ] : ();
}

sub loadClosestEqPOI {
    my $oref = shift;
    my $cnt = shift;
    return if $cnt == 0;
    my $towns = shift;

    my $maxcnt = 5;

    my $sth = $utilStmt{WHERES};
    $sth->bind_param(":lat", $oref->{LAT});
    $sth->bind_param(":lon", $oref->{LON});
    $sth->bind_param(":elev", 0 );
    $sth->bind_param(":cnt", $maxcnt);
    $sth->bind_param(":type", 'EQ_POI');
    $sth->execute();

    my $aryref = $sth->fetchall_arrayref({});
    return unless (1 == scalar @$aryref);

    my $results = $aryref->[0]{LOCALE};
    #print "$results\n";
    my @locales = split(/[\r\n]/,$results);
    my @poi;
    foreach my $locale (@locales) {
        my $str = reformatWheresString($locale);
        push ( @poi, $str);
    }
    my @z = removeBfromA(\@poi,\@$towns);
    $cnt = min($cnt-1,$#z);
    #return @z[ 0 .. $cnt ];
    return ( $cnt >= 0 ) ? @z[ 0 .. $cnt ] : ();
}

sub writeNearbyCitiesJson {
    my $evid = shift;
    eval {
        my $json = "nearby-cities.json";
        if ($opt_o ) {
           my ($fname, $dirpath, $suffix) = fileparse($opt_o);
           $json = "$dirpath/$json";
        }
        open FH, ">$json" or die;
        print FH "[\n";
        my $cnt = 1;
        #foreach my $locale ( @nearestLocales ) {
        #foreach my $locale (uniq( @nearestLocales )) {
        #foreach my $locale ( uniq(sort { ($a =~ /(\d+)/)[0] <=> ($b =~ /(\d+)/)[0] } @nearestLocales) ) {
        foreach my $locale ( uniq(sort cmp_1st_number @nearestLocales) ) {
                print FH ",\n" if ( $cnt > 1);
                print FH '    ' . $locale;
                $cnt++;
        }
        print FH "\n]\n";
        close FH;
   };
   if ( $@ ) {
        print STDERR "Error writeNearbyCitiesJson : $@\n";
   }
}

# use hash to remove duplicates from input array
sub uniq {
  my %nodup;
  return grep { !$nodup{$_}++ } @_;
}

# comparator used to sort by 1st number element in list
# @y = sort cmp_1st_number @x
sub cmp_1st_number {
    my ( $anum ) = $a =~ /(\d+)/;
    my ( $bnum ) = $b =~ /(\d+)/;
    ( $anum || 0 ) <=> ( $bnum || 0 );
}


# used to remove elements in B array from A array
sub removeBfromA {
  my $aa = shift;
  my $bb = shift;
  my %bhash;
  foreach (@$bb) {
     $bhash{$_} = 1;
  }
  my @zz = grep {!$bhash{$_}} @$aa;
  return @zz;
}
