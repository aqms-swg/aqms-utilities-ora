#!/usr/bin/perl
#
# Accept a trigger in the dbase.
# Set event.selectflag=1, Origin.bogusFlag=0, Origin.rflag='H'.
#
# DDG 2/25/08 - Generalized for CISN rollout

use strict;
use warnings;

# get masterdb and username/password
use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use EventActions;

# parse optional switches
use Getopt::Std;
use vars qw/ $opt_d $opt_h/;   #prevent "only used once" error
getopts('d:h');               # "d:" means a value must follow

if ( $opt_h || @ARGV == 0 ) { usage(); }

my $db = $masterdb;
if ($opt_d) { $db = $opt_d };

my $evid  = $ARGV[0]; 

# $status ALWAYS 0E0 because this uses a procedure not a function
my $status = EventActions->acceptTrigger($evid, $db);

my $host = `uname -n`;
chomp $host;
$host =~ s/\..*$//; # grab first element

my $user = $ENV{USER} || $ENV{LOGNAME};

my $date = `date -u +'%F %T %Z'`;
chomp $date;
  
if ($status) {
    print("Trigger ${evid} ACCEPTED db= ${db}: user= ${user} host= $host \@ ${date}\n");
} else {
    print("ACCEPT TRIGGER FAILED: Evid= ${evid} db= ${db} user= ${user} host= $host \@ ${date}\n");
}
  
exit 1;

# -----------------------------------------------------------
# Help message about this program and how to use it
#
sub usage {
print <<"EOF";

    Accept a Trigger in the dbase (mark as human reviewed) 
        
    usage: $0 [-d dbasename]  <evid>
     
     -h        : print usage info
     -d dbase  : use this particular dbase (defaults to "$masterdb")

    example: $0 -d k2db 8735346
EOF
exit;
}
