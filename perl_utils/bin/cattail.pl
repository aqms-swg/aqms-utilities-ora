#!/usr/bin/perl
#
# Write a summary of events in the past 'N' hours or within
# the time range specified by optinal start and end time date strings

#
# DEBUG option   -x : prints primary query text and exits
#
#------------------------------------------------------------------
use strict;
use warnings;

use DBI;

# get masterdb and username/password
use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use DbConn;
use MasterDbs;
use Time::Local;
use POSIX qw(strftime);

our ( $opt_2, $opt_a, $opt_B, $opt_c, $opt_D, $opt_d, $opt_e, $opt_es, $opt_f, $opt_F, $opt_fz, $opt_FZ, $opt_g, $opt_G, $opt_h, $opt_I, $opt_i,
      $opt_L, $opt_M, $opt_m, $opt_MAD, $opt_mad, $opt_ma, $opt_mt, $opt_mobs, $opt_MOBS, $opt_ms, $opt_mz, $opt_nph, $opt_NPH,
      $opt_o, $opt_oa, $opt_os, $opt_q, $opt_r, $opt_rf, $opt_RMS, $opt_rms, $opt_u, $opt_w, $opt_wfs, $opt_WFS, $opt_x,
      $opt_z, $opt_Z
);

use Getopt::Long;
Getopt::Long::Configure("no_ignore_case");

# parse optional switches
GetOptions(
 "2" =>\$opt_2,
 "a" =>\$opt_a,
 "B=i" =>\$opt_B,
 "c" =>\$opt_c,
 "D" =>\$opt_D,
 "d=s" =>\$opt_d,
 "e=s" =>\$opt_e,
 "es=s" =>\$opt_es,
 "f=s" =>\$opt_f,
 "F=s" =>\$opt_F,
 "fz" =>\$opt_fz,
 "FZ" =>\$opt_FZ,
 "g=s" =>\$opt_g,
 "G=s" =>\$opt_G,
 "h" =>\$opt_h,
 "I=i" =>\$opt_I,
 "i=i" =>\$opt_i,
 "L" =>\$opt_L,
 "ma:s" =>\$opt_ma,
 "mt=s" =>\$opt_mt,
 "mad=f" =>\$opt_mad,
 "MAD=f" =>\$opt_MAD,
 "m=f" =>\$opt_m,
 "M=f" =>\$opt_M,
 "ms=s" =>\$opt_ms,
 "mz" =>\$opt_mz,
 "o" =>\$opt_o,
 "oa:s" =>\$opt_oa,
 "os=s" =>\$opt_os,
 "q" =>\$opt_q,
 "r" =>\$opt_r,
 "rf=s" =>\$opt_rf,
 "rms" =>\$opt_rms,
 "RMS" =>\$opt_RMS,
 "u=s" =>\$opt_u,
 "w" =>\$opt_w,
 "x" =>\$opt_x,
 "z=f" =>\$opt_z,
 "Z=f" =>\$opt_Z,
 "nph=i" =>\$opt_nph,
 "NPH=i" =>\$opt_NPH,
 "mobs=i" =>\$opt_mobs,
 "MOBS=i" =>\$opt_MOBS,
 "wfs=i" =>\$opt_wfs,
 "WFS=i" =>\$opt_WFS
) or die "Error: Unknown command line option $!\n";

if ( $opt_h ) { usage(); }

# database alias to use for connection
my $db = $masterdb;
if ($opt_d) { $db = $opt_d; }

# event etype code list for query filter (default no type filter)
my $types = '';
if ( $opt_e ) {
    my @types = split('[,: ]', $opt_e);
    $types = join(',', map { "'$_'" } @types);
}

# origin gtype code list for query filter (default no type filter)
my $gtypes = '';
if ( $opt_g || $opt_G ) {
    my @gtypes = split('[,: ]', $opt_g) if $opt_g;
    @gtypes = split('[,: ]', $opt_G) if $opt_G;
    $gtypes = join(',', map { "'$_'" } @gtypes);
}
 
# query flag filter, default: primary events with valid locations (i.e no triggers/deleted)
my $sFlagFilter = "and e.selectflag = 1\n";

# include subnet triggers too
my $bFlagFilter = "";

if ( $opt_a && defined $opt_D ) {
  die "Error: conflicting options, instead of -a, use -D and/or -B 0|1 options\n";
}

if ( $opt_a ) { #don't restrict selection by flags
 $sFlagFilter = "";
 $bFlagFilter = "";
}
elsif ( defined $opt_D ) { # only deleted or shadow system duplicats
 $sFlagFilter = "and e.selectflag=0\n";  
 $bFlagFilter = "";
}

if ( defined $opt_B && $opt_a ) {
  die "Error: conflicting options, instead of -a, use -D and/or -B 0|1 options\n";
}
if ( defined $opt_B ) {
  if ( $opt_B eq '0'  || $opt_B eq '1' ) {
    $bFlagFilter = "and o.bogusflag = $opt_B\n";
  }
  else {
    die "Error: invalid -B $opt_B, bogusflag option value must be 0 or 1\n";
  }
}

# query selection to include distance to nearest town (default = no)
my $where = "";
if ( $opt_w ) { $where = ",WHERES.LOCALE_BY_TYPE(o.lat,o.lon,o.depth,1,'town')\n"};

# sort results by time ascending order (most recent last) (default)
my $sortString = "order by o.datetime";

# sort results by time descending order (most recent first)
if ( $opt_r ){ $sortString = " order by o.datetime DESC"; }

# Max age in hours back of event origin
my $MAX_DAY_LIMIT= 30;
my $hrsBack = 24;
if ( @ARGV > 0 ) {
  $hrsBack = $ARGV[0];
  my $factor = 1;

  if ( defined $opt_u ) {
    if ($opt_u =~ /[dD]/ ) {
      $factor = 24;
    }
    elsif ( $opt_u =~ /[wW]/ ) {
      $factor = 7*24;
    }
    elsif ( $opt_u =~ /[mM]/ ) {
      $factor = 30*24;
    }
    else {
      die "Unknown interval time units: $opt_u, must be d (for day),w (for week), or m (for month)";
    }
  }

  if ( $hrsBack =~ /^\d+$/) {
    $hrsBack *= $factor;
    if ( $hrsBack/(24.*$factor) > $MAX_DAY_LIMIT ) {
      print STDERR "Hours back $hrsBack > $MAX_DAY_LIMIT days, continue (y/n)?";
      my $ans = <STDIN>;
      chomp $ans;
      exit 0 if ( $ans ne 'y' ); 
    }
  }
} 

my $startDate = '';
my $endDate = '';
if ( $hrsBack !~ m/^\d+\.?\d*$/ ) {
  $startDate = $ARGV[0];
  if ( $startDate !~ m/^\d\d\d\d[\/-]\d\d[\/-]\d\d([:\-_, ]\d\d:\d\d)?(:\d\d)?$/ ) {
    die "Error: optional first input arg: $ARGV[0] must be a number of hours,\nor a date string like: yyyy/mm/dd[ HH:MM:SS]\n"; 
  }
  if ( @ARGV > 1 ) {
    $endDate = $ARGV[1];
    if ( $endDate !~ m/^\d\d\d\d[\/-]\d\d[\/-]\d\d([:\-_, ]\d\d:\d\d)?(:\d\d)?$/ ) {
      die "Error: optional second input arg: $ARGV[1] must be a number of hours,\nor a date string like: yyyy/mm/dd[ HH:MM:SS]\n";
    }
  }

  my ($yyyy,$mo,$dd,$junk1,$hh,$mi,$junk2,$ss) = $startDate =~ /(\d+).(\d+).(\d+)([:\-_, ](\d\d):(\d\d))?(:(\d\d))?/;
  my $startUnix = 0.;
  my $endUnix = $startUnix;
  eval {
    $startUnix = timegm($ss,$mi,$hh,$dd,$mo-1,$yyyy);
    $endUnix = $startUnix;
    if ( $endDate ne '' ) {
      ($yyyy,$mo,$dd,$junk1,$hh,$mi,$junk2,$ss) = $endDate =~ /(\d+).(\d+).(\d+)([:\-_, ](\d\d):(\d\d))?(:(\d\d))?/;
      $endUnix = timegm($ss,$mi,$hh,$dd,$mo-1,$yyyy);
    }
    else {
      my @time = gmtime();
      $endUnix = timegm(@time);
      $endDate = strftime("%Y/%m/%d %H:%M:%S", @time);
    }

    if ( ($endUnix - $startUnix) > ($MAX_DAY_LIMIT*84600.) ) {
      print STDERR "Timespan " . sprintf("%-.2f", ($endUnix-$startUnix)/86400.) . " days is > $MAX_DAY_LIMIT days, continue (y/n)?";
      my $ans = <STDIN>;
      chomp $ans;
      exit 0 if ( $ans ne 'y' ); 
    }
  };
  if ( $@) {
    die "Error: bad date range check: $startDate $endDate : $@\n";
  }
  if ( $endUnix le $startUnix ) {
      die "Error: endDate: $ARGV[1] must be greater than startDate: $ARGV[0]\n"; 
  }
}
elsif ( @ARGV > 1 ) {
  die "Error: unknown input argument @ARGV\n";
}

my $id = '';
if ( defined $opt_i ) {
  $id = $opt_i;
}
if ( defined $opt_I ) {
  $id = $opt_I;
}
if ( defined $opt_i  &&  defined $opt_I ) {
  die "Error: you cannot specify both -i and -I options\n";
}
if ( defined $opt_i  || defined $opt_I ) {
  if ( $id !~ m/^\d+$/ || $id <= 0) {
    die "Error: optional evid: $id, must be an integer number > 0\n"; 
  }
}

if ( $opt_f && $opt_F ) {
  die "Error: you cannot specify both -f and -F options\n";
}
if ( $opt_f || $opt_F ) {
  die "Error: you cannot specify -f or -F  with -i or -I options\n" if ( $opt_i || $opt_I );
  my $filename = ( $opt_f ) ?  $opt_f : $opt_F;
  die "Error: -f $filename does not exist\n" if ! -f $filename;
  open FILE, $filename or die $!;
  my @lines; 
  @lines = <FILE>;
  my @idlist;
  for my $line (@lines) {
    next if $line =~ m/^\#/;   # skip "#" comments
    next if $line =~ m/^ *$/;  # skip blanks
    chomp $line;
    my @toks = split " ", $line;  # parse the line, only want 1st token
    my $evid = $toks[0];
    defined $toks[0] && ($toks[0] =~ /^\d+$/) or next;  # skip if not a positive number
    push( @idlist, $toks[0] ); 
  }
  $id = join ',', @idlist;
}

my $minZ = '';
if ( defined $opt_z ) {
  $minZ = $opt_z;
  if ( $minZ !~ m/^\d*\.?\d*$/ ) {
    die "Error: optional minZ: $minZ, must be a number\n"; 
  }
}
my $maxZ = '';
if ( defined $opt_Z ) {
  $maxZ = $opt_Z;
  if ( $maxZ !~ m/^\d*\.?\d*$/ ) {
    die "Error: optional maxZ: $maxZ, must be a number\n"; 
  }
}

my $minRMS = '';
if ( defined $opt_rms ) {
  $minRMS = $opt_rms;
  if ( $minRMS !~ m/^\d*\.?\d*$/ ) {
    die "Error: optional minRMS: $minRMS, must be a number\n"; 
  }
}
my $maxRMS = '';
if ( defined $opt_RMS ) {
  $maxRMS = $opt_RMS;
  if ( $maxRMS !~ m/^\d*\.?\d*$/ ) {
    die "Error: optional maxRMS: $maxRMS, must be a number\n"; 
  }
}

my $minNPH = '';
if ( defined  $opt_nph ) {
  $minNPH = $opt_nph;
  if ( $minNPH !~ m/^\d*\.?\d*$/ ) {
    die "Error: optional minNPH: $minNPH, must be a number > 0\n"; 
  }
}
my $maxNPH = '';
if ( defined $opt_NPH ) {
  $maxNPH = $opt_NPH;
  if ( $maxNPH !~ m/^\d*\.?\d*$/ ) {
    die "Error: optional maxNPH: $maxNPH, must be a number > 0\n"; 
  }
}

my $minMag = '';
if ( defined $opt_m ) {
  $minMag = $opt_m;
  if ( $minMag !~ m/^\d*\.?\d*$/ ) {
    die "Error: optional minMag: $minMag, must be a number\n"; 
  }
}
my $maxMag = '';
if ( defined $opt_M ) {
  $maxMag = $opt_M;
  if ( $maxMag !~ m/^\d*\.?\d*$/ ) {
    die "Error: optional maxMag: $maxMag, must be a number\n"; 
  }
}

my $minMAD = '';
if ( defined $opt_mad ) {
  $minMAD = $opt_mad;
  if ( $minMAD !~ m/^\d*\.?\d*$/ ) {
    die "Error: optional minMAD: $minMAD, must be a number\n"; 
}
}
my $maxMAD = '';
if ( defined $opt_MAD ) {
  $maxMAD = $opt_MAD;
  if ( $maxMAD !~ m/^\d*\.?\d*$/ ) {
    die "Error: optional maxMAD: $maxMAD, must be a number\n"; 
  }
}

my $minMOBS = -1;
if ( defined $opt_mobs ) {
  $minMOBS = $opt_mobs;
  if ( $minMOBS !~ m/^\d*\.?\d*$/ ) {
    die "Error: optional minMOBS: $minMOBS, must be a number > 0\n"; 
  }
}
my $maxMOBS = -1;
if ( defined $opt_MOBS ) {
  $maxMOBS = $opt_MOBS;
  if ( $maxMOBS !~ m/^\d*\.?\d*$/ ) {
    die "Error: optional maxMOBS: $maxMOBS, must be a number > 0\n"; 
  }
}

my $mtypes = '';
if ( defined $opt_mt ) {
    my @mtypes = split('[,: ]', $opt_mt);
    $mtypes = join(',', map { "'$_'" } @mtypes);
}

my $esrc = '';
if ( defined $opt_es ) {
  $esrc = $opt_es;
}

my $osrc = '';
if ( defined $opt_os ) {
  $osrc = $opt_os;
}

my $msrc = '';
if ( defined $opt_ms ) {
  $msrc = $opt_ms;
}

my $pflags='';
if ( defined $opt_rf ) {
  my @types = split('[,: ]', $opt_rf);
  $pflags = join(',', map { "'$_'" } @types);
}

my $minWFS = -1;
if ( defined $opt_wfs ) {
  $minWFS = $opt_wfs;
  if ( $minWFS !~ m/^\d*\.?\d*$/ ) {
    die "Error: optional minWFS: $minWFS, must be a number > 0\n"; 
  }
}
my $maxWFS = -1;
if ( defined $opt_WFS ) {
  $maxWFS = $opt_WFS;
  if ( $maxWFS !~ m/^\d*\.?\d*$/ ) {
    die "Error: optional maxWFS: $maxWFS, must be a number > 0\n"; 
  }
}

# Build query string
my $sql = "select e.evid,o.orid,n.magid,n.magnitude,n.magtype,n.nobs,n.nsta,n.uncertainty,n.magalgo,n.subsource,";
$sql .= ( $opt_2 ) ? "TrueTime.getStringf(o.datetime)," : "TrueTime.getString(o.datetime),";
$sql .= "\n  o.lat,o.lon,";
$sql .= ( $opt_mz ) ? "o.mdepth," : "o.depth,";
$sql .="o.nbs,o.ndef,o.wrms, o.gap,o.nbfm,e.etype,o.gtype,o.rflag,o.subsource,o.algorithm,";
$sql .= "\n e.selectflag,o.bogusflag,o.fdepth,e.version,e.subsource,nvl(e.prefmec,0)\n"; 

$sql .= $where;

$sql .= "from Event e,Origin o,Netmag n WHERE (e.prefor=o.orid(+)) and (e.prefmag=n.magid(+))\n";

# id filter 
if ( $opt_i || $opt_I ) {
  $sql .= " and (e.evid=$id)\n";
}
if ( $opt_f || $opt_F ) {
  $sql .= " and e.evid in ( $id )\n";
}

# Single event like "catone" or 
if ( $opt_F ) {
  $sql .= $sortString;
}
elsif ( ! $opt_I  ) { #don't apply filters
  # selectflag filter
  if ( $sFlagFilter ne "" ) {
    $sql .= $sFlagFilter;
  }

  # bogusflag filter
  if ( $bFlagFilter ne "" ) {
    $sql .= $bFlagFilter;
  }

  # event etype filter 
  if ( $types ne '' ) {
    $sql .= " and (e.etype in ($types))\n";
  }

  # gtypes filter? 
  if ( $gtypes ne '' ) {
    $sql .= " and (o.gtype in ($gtypes))\n" if $opt_g;
    $sql .= " and (o.gtype in ($gtypes) or o.gtype is NULL)\n" if $opt_G;
  }
  # event subsource filter 
  if ( $esrc ne '' ) {
    $sql .= " and (e.subsource like \'${esrc}\')\n";
  }

  # netmag subsource filter 
  if ( $msrc ne '' ) {
    $sql .= " and (n.subsource like \'${msrc}\')\n";
  }

  # origin subsource filter 
  if ( $osrc ne '' ) {
    $sql .= " and (o.subsource like \'${osrc}\')\n";
  }

  # origin algorithm filter
  if ( defined $opt_oa && $opt_oa ne '' ) {
    $sql .= " and (o.algorithm like \'$opt_oa\')\n";
  }

  if ( $opt_FZ ) {
    $sql .= " and (o.fdepth='y')\n";
  }

  if ( defined $opt_ma && $opt_ma ne '' ) {
    $sql .= " and (n.magalgo like \'$opt_ma\')\n";
  }

  # magnitude filter 
  if ( $minMag ne '' ) {
    $sql .= " and (n.magnitude>=$minMag)\n";
  }
  if ( $maxMag ne '' ) {
    $sql .= " and (n.magnitude<=$maxMag)\n";
  }

  # magtype filter 
  if ( $mtypes ne '' ) {
    $sql .= " and (n.magtype in ($mtypes))\n";
  }

  # netmag MAD uncertainty filter
  if ( $minMAD ne '' ) {
    $sql .= " and (n.uncertainty>=${minMAD})\n";
  }
  if ( $maxMAD ne '' ) {
    $sql .= " and (n.uncertainty<=${maxMAD})\n";
  }

  # origin processing rflag filter
  if ( $pflags ne '') {
    $sql .= " and (o.rflag in ($pflags))\n";
  }

  # origin depth filter
  if ( $minZ ne '' ) {
    $sql .= " and (o.depth >= ${minZ})\n";
  }
  if ( $maxZ ne '' ) {
    $sql .= " and (o.depth <= ${maxZ})\n";
  }

  # origin wrms residual filter
  if ( $minRMS ne '' ) {
    $sql .= " and (o.wrms>=${minRMS})\n";
  }
  if ( $maxRMS ne '' ) {
    $sql .= " and (o.wrms<=${maxRMS})\n";
  }

  # origin ndef phase count filter 
  if ( $minNPH ne '' ) {
    $sql .= " and (o.ndef>=$minNPH)\n";
  }
  if ( $maxNPH ne '' ) {
    $sql .= " and (o.ndef<=$maxNPH)\n";
  }

  # origin time filter
  if ( $startDate ) {
    $startDate = "'" . $startDate  . "'";
    $endDate = "'" . $endDate  . "'";
    $sql .= " and (o.datetime between truetime.putString($startDate) and truetime.putString($endDate))\n";
  }
  else {
    my $now = time();
    my $start = $now - ($hrsBack * (60*60));
    $sql .= " and (o.datetime between truetime.putEpoch($start,'POSIX') and truetime.putEpoch($now,'POSIX'))\n";
  }

  # append sort order for multiple rows
  $sql .= $sortString;

}

if ( $opt_x ) {
  print "$sql\n";
  exit 0;
}

# Create db connection 
my $dbconn = DbConn->new($db)->getConn();
my $sth = $dbconn->prepare( $sql ) or die "Error: sql prepare failed\n:$sql\n";

$sth->execute() or die "Error: sql query failed\n:$sql" ; 

# Just return total cnt in query results
if ( $opt_c ) {
  my $aref = $sth->fetchall_arrayref() or die "Error: DBI fetching array";
  my $cnt = scalar @{$aref}; 
  print "${cnt}\n";
  $sth->finish;
  $dbconn->disconnect;
  exit 0;
}

if ( !$opt_q) { 
  my $filler = ( $opt_2 ) ? '   ' : ''; 
  if ( defined $opt_ma ) {
    print "#     EVID MALGO       MAG MT  MOBS  MAD DATE       TIME(UTC)$filler     LAT       LON     Z   NBS  PHS   RMS GAP NFM ET GT RF MSRC OSRC ESRC  V   WFS SmAMP FP SF BF\n";
    if ( $opt_o ) {
#
      print "#col#    1       2       3  4     5    6    7              8        9        10     11   12   13    14  15  16 17 18 19   20   21   22 23    24    25 26 27 28\n";
    }
    else {
      print "#        #               #        #    # year-mo-dy hr:mn:sc$filler      deg       deg    km      #   #     #   #   #                          #     #     #  #  #  #\n";
    }
  }
  else {
    print "#     EVID  MAG MT  MOBS  MAD DATE       TIME(UTC)$filler     LAT       LON     Z   NBS  PHS   RMS GAP NFM ET GT RF MSRC OSRC ESRC  V   WFS SmAMP FP SF BF\n";
    if ( $opt_o ) {
      print "#col#    1    2  3     4    5          6$filler        7        8         9    10    11   12    13  14  15 16 17 18   19   20   21 22    23    24 25 26 27\n";
    }
    else {
      print "#        #    #        #    # year-mo-dy hr:mn:sc$filler      deg       deg    km     #    #     #   #   #                          #     #     #  #  #  #\n";
    }
  }
}

# Separate subquery to lookup Waveform count in loop
my $sth2 = $dbconn->prepare( "select count(wfid) from AssocWaE where evid=?" );

# Separate subquery lookup Strong motion Amp count in loop
my $sth3 = $dbconn->prepare( "select count(*) from AssocEvAmpset e, Ampset s where s.ampsetid=e.ampsetid and e.isvalid=1 and e.evid = ?" );

# Separate subquery lookup Strong motion Amp count in loop
my $sth4 = $dbconn->prepare( "select count(*) from AssocCoM m, Coda c where c.coid=m.coid and m.weight>0. and m.magid = ?" );

# Separate subquery lookup Strong motion Coda count in loop
my $sth5 = $dbconn->prepare( "select count(*) from AssocAmM m, Amp a where a.ampid=m.ampid and m.weight>0. and m.magid = ?" );

# Read the column results into separate variables          
my $rowCnt = 0;
while (my @data = $sth->fetchrow_array()) {
    my $ii = 0;
    my $evid = $data[$ii++] || 0;
    my $orid = $data[$ii++] || 0;
    my $magid = $data[$ii++] || 0;
    my $mag = $data[$ii++] || 0;
    my $magtype = $data[$ii++] || 'n';
    my $nobs = $data[$ii++] || 0;
    my $nsta = $data[$ii++] || 0;
    my $mad = $data[$ii++] || 0;
    my $malgo = $data[$ii++] || '-';
    my $msrc = $data[$ii++] || '-';
    my $timstr = $data[$ii++] || '-';
    my $lat = $data[$ii++] || 0;
    my $lon = $data[$ii++] || 0;
    my $z = $data[$ii++] || 0;
    my $nbs = $data[$ii++] || 0;
    my $nph = $data[$ii++] || 0;
    my $rms = $data[$ii++] || 0;
    my $gap = $data[$ii++] || 0;
    my $nfm = $data[$ii++] || 0;
    my $etype = $data[$ii++] || '-';
    my $gtype = $data[$ii++] || '-';
    my $rflag = $data[$ii++] || '-';
    my $osrc = $data[$ii++] || '-';
    my $oalgo = $data[$ii++] || '-';
    my $selectflag = $data[$ii++] || 0;
    my $bogusflag = $data[$ii++] || 0;
    my $fz = $data[$ii++] || ' ';
    $fz = ( $opt_fz && $fz eq 'y' ) ? '*' : ' ';
    my $vers = $data[$ii++] || 0;
    my $esrc = $data[$ii++] || '-';
    my $mecid = ( $data[$ii++] ) ? 1 : 0;
    my $town = "";
    if ($where) {
        $town = $data[$ii++] || 'null';
        #print "$town\n";
        # parse/compose town string
        #1234567890123456789012345678901234567890123456789012345678901234567890
        #   5.8 km (   3.6 mi) SE  ( 129. azimuth) from Diamond Bar, CA
        my $km = substr($town, 0,6);
        my $az = substr($town, 22, 3);
        my $ref= substr($town, 47);  
        $town = sprintf ("%5.1f km %3s of %s", $km, $az, $ref);
    }
    
    my $wfCnt = 0;
    if ($sth2) { # if table exists
      $sth2->execute($evid) or die "Error: waveform count query failed.";
      my @data2 = $sth2->fetchrow_array();
      $wfCnt = $data2[0] || 0;
    }
    next if ( $maxWFS >= 0 && $wfCnt > $maxWFS);
    next if ( $minWFS >= 0 && $wfCnt < $minWFS);

    my $smAmpCnt = 0;
    if ($sth3) { # if table exists
      $sth3->execute($evid) or die "Error: strong motion amps count query failed" ; 
      my @data3 = $sth3->fetchrow_array();
      $smAmpCnt = $data3[0] || 0;
    }

    my $magObsCnt = 0;
    if ( $magtype eq 'd' ) {
      if ($sth4) { # if table exists
        $sth4->execute($magid) or die "Error: magnitude codas count query failed" ; 
        my @data4 = $sth4->fetchrow_array();
        $magObsCnt = $data4[0] || 0;
        if ( $magObsCnt == 0 ) { # if none associated check netmag row values
          ##$magObsCnt = ( $nobs ) ? -$nobs : -$nsta;
          $magObsCnt = -$nobs;
        }
      }
    }
    else {
      if ($sth5) { # if table exists
        $sth5->execute($magid) or die "Error: magnitude amps count query failed" ; 
        my @data5 = $sth5->fetchrow_array();
        $magObsCnt = $data5[0] || 0;
        if ( $magObsCnt == 0 ) { # if none associated check netmag row values
          ##$magObsCnt = ( $nobs ) ? -$nobs : -$nsta;
          $magObsCnt = -$nobs;
        }
      }
    }
    next if ( $maxMOBS >= 0 && abs($magObsCnt) > $maxMOBS);
    next if ( $minMOBS >= 0 && abs($magObsCnt) < $minMOBS);


    $magtype = 'x' if $magtype eq 'un';

    # handle precision formatting
    # NOTE: sprintf BUG: f1.1 of 3.05 comes out at "3.0" not "3.1" unless I do this! DDG 1/15/03
    $mag += .00001; 
    my $smag = ( $opt_2 ) ? sprintf("%03.2f", $mag) : sprintf("%01.1f", $mag);
    my $sz;
    if ( $opt_o ) {
      $sz = ( $opt_2 ) ? sprintf("%05.2f", $z) : sprintf("%05.1f", $z);
    }
    else {
      $sz = ( $opt_2 ) ? sprintf("%5.2f", $z) : sprintf("%5.1f", $z);
    }
    my $slat = sprintf "%8.4f", $lat;
    my $slon = sprintf "%9.4f", $lon;

    my $srms = sprintf "%01.2f", $rms;
    my $smad = sprintf "%01.2f", $mad;
    my $sgap = ( $opt_o ) ?  sprintf("%0.3d", $gap) : sprintf("%0.3d", $gap);

    $timstr =~ s/\//-/g;
    $timstr = substr($timstr,0,22) if $opt_2; 
    if ( defined $opt_ma ) {
      if ( $opt_o ) {
        printf "%0.10d %-10s %4.4s M%-2s %0.4d %s %s %s %s %s%s %0.4d %0.4d %05.2f %s %0.3d %2s %2.2s %2s %4.4s %4.4s %4.4s %0.2d %0.5d %0.5d  %0.1d  %0.1d  %0.1d %s\n",
          $evid,$malgo,$smag,$magtype,$magObsCnt,$smad,$timstr,$slat,$slon,$sz,$fz,$nbs,$nph,$srms,$sgap,$nfm,$etype,$gtype,$rflag,
          $msrc,$osrc,$esrc,$vers,$wfCnt,$smAmpCnt,$mecid,$selectflag,$bogusflag,$town
      }
      else {
        printf "%10d %-10s %4.4s M%-2s %4d %s %s %s %s %s%s %4d %4d %5.2f %s %3d %2.2s %2.2s %2s %4.4s %4.4s %4.4s %2d %5d %5d  %1d  %1d  %1d %s\n",
          $evid,$malgo,$smag,$magtype,$magObsCnt,$smad,$timstr,$slat,$slon,$sz,$fz,$nbs,$nph,$srms,$sgap,$nfm,$etype,$gtype,$rflag,
          $msrc,$osrc,$esrc,$vers,$wfCnt,$smAmpCnt,$mecid,$selectflag,$bogusflag,$town
      }
    }
    else {
      if ( $opt_o ) {
        printf "%0.10d %4.4s M%-2s %0.4d %s %s %s %s %s%s %0.4d %0.4d %05.2f %s %0.3d %2s %2.2s %2s %4.4s %4.4s %4.4s %0.2d %0.5d %0.5d  %0.1d  %0.1d  %0.1d %s\n",
          $evid,$smag,$magtype,$magObsCnt,$smad,$timstr,$slat,$slon,$sz,$fz,$nbs,$nph,$srms,$sgap,$nfm,$etype,$gtype,$rflag,
          $msrc, $osrc,$esrc,$vers,$wfCnt,$smAmpCnt,$mecid,$selectflag,$bogusflag,$town
      }
      else {
        printf "%10d %4.4s M%-2s %4d %s %s %s %s %s%s %4d %4d %5.2f %s %3d %2.2s %2.2s %2s %4.4s %4.4s %4.4s %2d %5d %5d  %1d  %1d  %1d %s\n",
          $evid,$smag,$magtype,$magObsCnt,$smad,$timstr,$slat,$slon,$sz,$fz,$nbs,$nph,$srms,$sgap,$nfm,$etype,$gtype,$rflag,
          $msrc,$osrc,$esrc,$vers,$wfCnt,$smAmpCnt,$mecid,$selectflag,$bogusflag,$town
      }
    }

    $rowCnt++;
    
} # end of "while" loop
if ( ($rowCnt >= 22) && !$opt_q && !$opt_o ) { 
  my $filler = ( $opt_2 ) ? '   ' : ''; 
  if ( defined $opt_ma ) {
    print "#        #               #        #    # year-mo-dy hr:mn:sc$filler      deg       deg    km     #    #     #   #   #                          #     #     #  #  #  #\n";
    print "#     EVID MALGO       MAG MT  MOBS  MAD DATE       TIME(UTC)$filler     LAT       LON     Z   NBS  PHS   RMS GAP NFM ET GT RF MSRC OSRC ESRC  V   WFS SmAMP FP SF BF\n";
  }
  else {
    print "#        #    #        #    # year-mo-dy hr:mn:sc$filler      deg       deg    km     #    #     #   #   #                          #     #     #  #  #  #\n";
    print "#     EVID  MAG MT  MOBS  MAD DATE       TIME(UTC)$filler     LAT       LON     Z   NBS  PHS   RMS GAP NFM ET GT RF MSRC OSRC ESRC  V   WFS SmAMP FP SF BF\n";
  }
}
if ( $rowCnt == 0 ) {
    print "No event matched.\n\n";
}
elsif ( ! $opt_q ) { 
    if ( ! ( $opt_i || $opt_I ) ) {
        print  "#\n# " . $sth->rows . " events found.\n\n";
    }
}

if ( $opt_L && !$opt_q ) {
  print "# MALGO = magnitude algorithm\n" if defined $opt_ma; 
  print <<EOD;
# MT    = magnitude type
# MOBS  = # of channels used for event preferred magnitude ( -# => no assocamm/assoccom rows)
# MAD   = median absolute deviation of channel mags 
# Z     = origin depth km
# NBS   = # of S phases used for location
# PHS   = # of phases used for location
# RMS   = of location traveltime residuals 
# GAP   = max azimuthal gap of origin arrivals
# NFM   = # of first motions
# ET    = event type
# GT    = origin gtype
# RF    = origin review flag (A, H, I, F, C)
# MSRC  = magnitude subsource
# OSRC  = origin subsource
# ESRC  = event subsource
# V     = # of event version
# WFS   = # of archived waveforms
# SmAMPS= # of strong-motion amps
# FP    = 1, has a focal mechanism in database
# SF    = event selected (=1 primary, =0 secondary or deleted)
# BF    = bogusflag (=1 bogus origin time or lat,lon,z) 
EOD
}

$sth->finish if $sth;
$sth2->finish if $sth2;
$sth3->finish if $sth3;
$sth4->finish if $sth4;
$sth5->finish if $sth5;

### Now, disconnect from the database
$dbconn->disconnect
  or warn "Disconnection failed: $DBI::errstr\n";

exit 0;

#
# Help message about this program and how to use it
#
sub usage {
    print <<"EOF";

    usage: $0 [option-list] [[#interval] or [startDate [endDate]]]
    options: [-h] [-c] [-d dbase] [-a or [ [-D] [-B0|1]]] [-i|I]
             [-f file] [-e types] [-g|G gtypes] [-p rflags] [-m mag -M mag]
             [ -mt magtypes] [-ma magalgo]
             [-mad rval -MAD rval] [-rms rval -RMS rval]
             [-mz] [-z depth -Z depth] [ -fz]
             [-es esrc -os osrc -ms msrc ] -[2rwL]
             [-u d|w|m ]

    Writes a summary list for  events in the past 'N' hours (default = 24), or specified date range
    Column values are from the db tables Event e, Origin o, Netmag n outer joined on prefor and prefmag: 
    EVID,MAGNITUDE,MAGTYPE,MAGALGO,UNCERTAINTY,DATETIME,LAT,LON,DEPTH,WRMS,GAP,ETYPE,GTYPE,RFLAG,SUBSOURCE 
    followed by counts of of waveforms, strong-motion amps, and non-zero weight magnitude associated amps
    or codas; however, if no observations are associated with event preferred magnitude a negative
    netmag.nobs value is shown.

    Time interval units defaults to hours unless -u option is used for days, weeks, or months units. 

    Defaults to last 24 hrs if no input arguments or options. Input arguments must be hours back,
    or a starting date, and optionally, an ending date, if no ending date is specified it defaults
    to the current GMT time.

    Date are of the form yyyy-mm-dd ; you can use either '-' or '/' as the year, month and day separator.

    Date arguments can optionally include a time of the form: HH:MM or HH:MM:SS
    Quote string if using a white space separator between them: '2013-12-01 13:00',
    otherwise, use character set [,-_] to separate a date from its optional time field:
    2013-03-13,13:00 2013-03-13-13:00 or 2013-03-13:13:00 or  2013/03/13_13:00:00 
     
     -h        : print this usage info

     -d dbase  : alias to use for db connection (defaults to "$masterdb")

     -c        : print only the total count of events

     -q        : do not print title or legend, only event summmary

     -L        : include a legend for the title column acronyms

     -r        : reverse time order sort (most recent at top)

     -w        : add info on nearest town and its distance from event,
                 requires populated gazetteer_towns table in database

     -2        : print 2-decimal magnitude, time seconds, and depth km values

     -fz       : append '*' character after depth for fixed depth origin

     Query list filter options:

     -mz         : depth (z) is the origin's model depth

     -f file     : file with one or more lines whose first column is an event evid
     -F file     : file with one or more lines whose first column is an event evid
                   but ignores all other filter criteria including time bounds
                   (e.g. cattail -F idFile.txt)

     -i evid     : event specified by id if its data matches filter criteria
     -I evid     : event specified by id ignoring all other filter criteria
                   including time bounds (e.g. cattail -I 12345)

     -a          : all events (includes deleted and shadow system duplicates)
                   note: not compatible together with -D and/or -B option

     -D          : only deleted or shadow system duplicate events (selectflag=0)

     -B flag     : =1 show bogus origins only, =0 show non-bogus origins

     -e types    : only events matching specified types, where types
                   is a concatenation of Event.etype codes delimited by
                   ',' or ':', for example: 'eq:ex:qb' or 'eq,ex,qb'

     -FZ         : only events with fixed depth origin, fdepth='y'

     -gG types   : only events matching specified gtypes, where types
                   is a concatenation of Origin.gtype codes delimited by
                   ',' or ':', for example: 'l:r:t' or 'l,r,t', use -G
                   option to include events with null gtype value

     -es src     : only events with like matching event subsource
                   string can contain db sql wildcard characters '%' or '_'

     -os src     : only events with like matching origin subsource
                   string can contain db sql wildcard characters '%' or '_'

     -oa algo    : only events with like matching origin algorithm, e.g. BINDER,
                   string can contain db sql wildcard characters '%' or '_'

     -rf flags   : only events whose review flag matches specified flags, 
                   a concatenation of Origin.rflag codes delimited by
                   ',' or ':', for example: 'I:F' or 'A,H'

     -ms src     : only events with like matching netmag subsource
                   string can contain db sqlwildcard characters '%' or '_'

     -ma algo    : show magnitude algorithmn column in after evid in listing,
                   for magnitudes whose algo matches string, e.g. RichterMl2,
                   string can contain db sqlwildcard characters '%' or '_',
                   an empty string includes nulls for triggers

     -mt mtypes  : only events whose magnitude type matches specified types,
                   where mtype is a concatenation of Netmag.magtype codes 
                   delimited by ',' or ':', for example: 'l:d:w' or 'l,d,w'

     -m mag      : only events whose magnitude >= mag
     -M mag      : only events whose magnitude <= mag

     -z rval     : only events whose depth >= rval
     -Z rval     : only events whose depth <= rval

     -rms rval   : only events with origin residual rms >= rval
     -RMS rval   : only events with origin residual rms <= rval

     -mad rval   : only events with netmag uncertainty (MAD) >= rval
     -MAD rval   : only events with netmag uncertainty (MAD) <= rval

     -mobs rval  : only events with netmag nobs >= rval
     -MOBS rval  : only events with netmag nobs <= rval

     -nph rval   : only events with origin ndef phases >= rval
     -NPH rval   : only events with origin ndef phases <= rval

     -wfs rval   : only events with waveform count >= rval
     -WFS rval   : only events with waveform count <= rval

     -o          : prints column numbers below title line (e.g useful for awk)

     -u d|w|m    : time interval units (days, weeks, or months) for input times

     -x          : for debugging only, prints database sql query string

    example: $0 -d mydb -2 -u d -e qb 7

EOF
   exit;
}
