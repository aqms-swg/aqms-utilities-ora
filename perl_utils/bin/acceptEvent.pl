#!/usr/bin/perl
#
# Accept an event in the dbase.
# Set the 'rflag' field = 'H' to indicate the event
# has been reviewed and approved by a human.
# Post event so that alarms are run again.
#

use strict;
use warnings;

# get masterdb and username/password
use lib "$ENV{TPP_BIN_HOME}/perlmodules";
use MasterDbs;
use EventActions;

# parse optional switches
use Getopt::Std;
use vars qw/ $opt_d $opt_h/;   #prevent "only used once" error
getopts('fd:h');               # "d:" means a value must follow

if ( $opt_h || @ARGV == 0 ) { usage(); }

my $db = $masterdb;
if ($opt_d) { $db = $opt_d };

my $evid  = $ARGV[0]; 

my $status = EventActions->acceptEvent($evid, $db);

my $user = $ENV{USER} || $ENV{LOGNAME};
my $host = `uname -n`;
chomp $host;
$host =~ s/\..*$//; # grab first element

my $date = `date -u +'%F %T %Z'`;
chomp $date;
  
if ($status) {
    print("Event ${evid} ACCEPTED db= ${db}: user= ${user}  host= $host \@ ${date}\n");
} else {
    print("ACCEPT FAILED: Evid= ${evid} db= ${db} user= ${user} host= $host \@ ${date}\n");
}

#
# The above call does NOT resend alarms, :. send them now.
# (This may change some day but 2 alarms are better than 0.)
#
if ($status) {

    my $status2 = EventActions->sendAlarms($evid, $db);
   
    $date = `date -u +'%F %T %Z'`;
    chomp $date;  

    if ($status2) {
      print("Event ${evid} SendAlarms db= ${db}: user= ${user}  host= $host \@ ${date}\n");
    } else {
      print("SendAlarms FAILED: Evid ${evid} db= ${db} user= ${user} host= $host \@ ${date}\n");
    }
}

exit 1;


# -----------------------------------------------------------
# Help message about this program and how to use it
#
sub usage {
print <<"EOF";

    Accept an event in the dbase (mark as human reviewed) 
        
    usage: $0 [-d dbasename]  <evid>
     
     -h        : print usage info
     -d dbase  : use this particular dbase (defaults to "$masterdb")

    example: $0 -d k2db 8735346
EOF

exit;
}
